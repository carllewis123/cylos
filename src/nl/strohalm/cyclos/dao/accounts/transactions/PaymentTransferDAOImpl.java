package nl.strohalm.cyclos.dao.accounts.transactions;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import org.apache.commons.lang.StringUtils;
import nl.strohalm.cyclos.dao.BaseDAOImpl;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.exceptions.DaoException;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;

public class PaymentTransferDAOImpl extends BaseDAOImpl<PaymentTransfer>
		implements PaymentTransferDAO {

	public PaymentTransferDAOImpl() {
		super(PaymentTransfer.class);
	}

	@Override
	public boolean existTicket(String paymentTicket) throws DaoException {
		try {
			loadTicket(paymentTicket);
			return true;
		} catch (final Exception e) {
			return false;
		}
	}

	@Override
	public <T extends PaymentTransfer> T loadTicket(final String value,
			final Relationship... fetch) throws EntityNotFoundException,
			DaoException {

		final Map<String, ?> params = Collections.singletonMap("paymentTicket",
				value);
		T ticket = this
				.<T> uniqueResult(
						"from PaymentTransfer t where t.paymentTicket = :paymentTicket",
						params);
		if (ticket == null) {
			throw new EntityNotFoundException(PaymentTransfer.class);
		}
		if (ArrayUtils.isNotEmpty(fetch)) {
			ticket = getFetchDao().fetch(ticket, fetch);
		}
		return ticket;

	}

	@Override
	public boolean existPtNumber(String ptNumberUnique) throws DaoException {
		try {
			loadPtNumber(ptNumberUnique);
			return true;
		} catch (final Exception e) {
			return false;
		}
	}

	@Override
	public <T extends PaymentTransfer> T loadPtNumber(String value,
			Relationship... fetch) throws EntityNotFoundException, DaoException {
		final Map<String, ?> params = Collections.singletonMap("ptNumberUnique",value);
		T ptNumberUnique = this.<T> uniqueResult("from PaymentTransfer t where t.ptNumberUnique = :ptNumberUnique",params);
		if (ptNumberUnique == null) {
			throw new EntityNotFoundException(PaymentTransfer.class);
		}
		if (ArrayUtils.isNotEmpty(fetch)) {
			ptNumberUnique = getFetchDao().fetch(ptNumberUnique, fetch);
		}
		return ptNumberUnique;
	}

	@Override
	public int allPtNumberExist(final String ptKey, final BigDecimal amount)
			throws DaoException {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("ptKey", ptKey);
        params.put("amount", amount);
        return this.<Integer> uniqueResult("select count(*) from PaymentTransfer t where t.ptKey = :ptKey and t.amount = :amount", params);
	}

	@Override
	public PaymentTransfer load(String ptNumberUnique, String paymentTicket)
			throws EntityNotFoundException, DaoException {
		final Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("ptNumberUnique", ptNumberUnique);
        parameters.put("paymentTicket", paymentTicket);
        final StringBuilder hql = new StringBuilder();
        
        hql.append("from PaymentTransfer r");
 	    hql.append(" where");
 	    
       if(!StringUtils.isEmpty(ptNumberUnique) && !StringUtils.isEmpty(paymentTicket)){
    	   hql.append(" r.ptNumberUnique = :ptNumberUnique");
    	   hql.append(" and");
    	   hql.append(" r.paymentTicket = :paymentTicket");
    	   
       }else if(!StringUtils.isEmpty(ptNumberUnique)){
    	   hql.append(" r.ptNumberUnique = :ptNumberUnique");
    	   
       }else{
    	   hql.append(" r.paymentTicket = :paymentTicket");
       }
       
       final PaymentTransfer pt = uniqueResult(hql.toString(), parameters);
        
        return pt;
	}
	
}
