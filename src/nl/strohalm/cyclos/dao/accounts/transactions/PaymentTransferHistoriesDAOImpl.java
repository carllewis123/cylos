package nl.strohalm.cyclos.dao.accounts.transactions;

import nl.strohalm.cyclos.dao.BaseDAOImpl;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransferHistories;

public class PaymentTransferHistoriesDAOImpl extends
		BaseDAOImpl<PaymentTransferHistories> implements
		PaymentTransferHistoriesDAO {

	public PaymentTransferHistoriesDAOImpl() {
		super(PaymentTransferHistories.class);
	}
}
