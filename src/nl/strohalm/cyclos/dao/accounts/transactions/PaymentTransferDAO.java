package nl.strohalm.cyclos.dao.accounts.transactions;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import nl.strohalm.cyclos.dao.BaseDAO;
import nl.strohalm.cyclos.dao.DeletableDAO;
import nl.strohalm.cyclos.dao.InsertableDAO;
import nl.strohalm.cyclos.dao.UpdatableDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.accounts.transactions.Ticket;
import nl.strohalm.cyclos.entities.exceptions.DaoException;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;

public interface PaymentTransferDAO extends BaseDAO<PaymentTransfer>,
		UpdatableDAO<PaymentTransfer>, InsertableDAO<PaymentTransfer>, DeletableDAO<PaymentTransfer>{
	
	boolean existTicket(String paymentTicket) throws DaoException;
	
	public <T extends PaymentTransfer> T  loadTicket(String paymentTicket, Relationship... fetch) throws EntityNotFoundException, DaoException;
	
	boolean existPtNumber(String ptNumberUnique) throws DaoException;
	
	public <T extends PaymentTransfer> T  loadPtNumber(String ptNumberUnique, Relationship... fetch) throws EntityNotFoundException, DaoException;
 
    public int allPtNumberExist(String ptKey, BigDecimal amount)  throws DaoException; 
    
    public PaymentTransfer load(String ptNumberUnique, String paymentTicket)throws EntityNotFoundException, DaoException;
}
