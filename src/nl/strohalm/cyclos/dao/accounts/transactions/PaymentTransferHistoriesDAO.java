package nl.strohalm.cyclos.dao.accounts.transactions;

import nl.strohalm.cyclos.dao.BaseDAO;
import nl.strohalm.cyclos.dao.DeletableDAO;
import nl.strohalm.cyclos.dao.InsertableDAO;
import nl.strohalm.cyclos.dao.UpdatableDAO;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransferHistories;

public interface PaymentTransferHistoriesDAO extends
		BaseDAO<PaymentTransferHistories>,
		InsertableDAO<PaymentTransferHistories>,
		UpdatableDAO<PaymentTransferHistories>,
		DeletableDAO<PaymentTransferHistories> {

}
