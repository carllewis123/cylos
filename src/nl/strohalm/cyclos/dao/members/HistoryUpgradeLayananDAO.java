package nl.strohalm.cyclos.dao.members;

import java.util.List;

import nl.strohalm.cyclos.dao.BaseDAO;
import nl.strohalm.cyclos.dao.InsertableDAO;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;

public interface HistoryUpgradeLayananDAO extends BaseDAO<HistoryUpgradeLayanan>, InsertableDAO<HistoryUpgradeLayanan> {
	List<HistoryUpgradeLayanan> search(HistoryUpgradeLayananQuery query);
	
	List<GroupRemark> searchByAgent(HistoryUpgradeLayananQuery query);
}
