package nl.strohalm.cyclos.dao.members;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ptdam.emoney.EmoneyConfiguration;
import net.sf.ehcache.hibernate.HibernateUtil;
import nl.strohalm.cyclos.dao.BaseDAOImpl;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.utils.hibernate.HibernateHelper;

public class HistoryUpgradeLayananDAOImpl extends BaseDAOImpl<HistoryUpgradeLayanan> implements HistoryUpgradeLayananDAO {

	
	 public HistoryUpgradeLayananDAOImpl() {
	        super(HistoryUpgradeLayanan.class);
	    }

	    public List<HistoryUpgradeLayanan> search(final HistoryUpgradeLayananQuery query) {
	    	
	        final Map<String, Object> namedParameters = new HashMap<String, Object>();
	        final Set<Relationship> fetch = query.getFetch();
	        final StringBuilder hql = HibernateHelper.getInitialQuery(GroupRemark.class, "r", fetch);
	        HibernateHelper.addParameterToQuery(hql, namedParameters, "r.writer", query.getWriter());
	        
	        Long idRegisteredAgent = 0L;
	        Long idUnregistered = 0L;
	        Long idUnregPendingAgent = 0L;
	        Long idUnregPendingAgentRekhape = 0L;
	        Long idRegisteredAgentRekhape = 0L;
	        Long idUnregisteredRekhape = 0L;
			try {
				idUnregPendingAgent = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgent.group.id"));
				idRegisteredAgent = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Registered.Agent.group.id"));
				idUnregistered = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Unregistered.group.id"));
				
				idUnregPendingAgentRekhape = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgentRekhape.group.id"));
				idRegisteredAgentRekhape = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Registered.Agent.Rekhape.group.id"));
				idUnregisteredRekhape = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredRekhape.group.id"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	        MemberGroup oldGroup = new MemberGroup();
	        oldGroup.setId(idUnregPendingAgent);
	        
	        MemberGroup oldGroupRekhape = new MemberGroup();
	        oldGroupRekhape.setId(idUnregPendingAgentRekhape);
	        
//	        List listParameterOldGroup = new ArrayList<MemberGroup>();
//	        if(listParameterOldGroup.size() == 0 ){
//	        	listParameterOldGroup.add(oldGroup);
//	        	listParameterOldGroup.add(oldGroupRekhape);
//	        }
	        
	        final StringBuilder tmp1 = new StringBuilder("1=1");
            final StringBuilder tmp2 = new StringBuilder("1=1");
	        
	        HibernateHelper.addParameterToQuery(tmp1, namedParameters, "r.oldGroup", oldGroup);
	        HibernateHelper.addParameterToQuery(tmp2, namedParameters, "r.oldGroup", oldGroupRekhape);
	        hql.append(" and (").append(tmp1).append(" or ").append(tmp2).append(")");
	        
//	        HibernateHelper.addInParameterToQuery(hql, namedParameters, "r.group", listParameterOldGroup);
	        
	        MemberGroup approvedGroup = new MemberGroup();
	        approvedGroup.setId(idRegisteredAgent);
	        
	        MemberGroup approvedGroupRekhape = new MemberGroup();
	        approvedGroupRekhape.setId(idRegisteredAgentRekhape);
	        
	        if(query.getAction() == HistoryUpgradeLayanan.Action.APPROVED){
	        	List listParameterApprovedGroup = new ArrayList<MemberGroup>();
	 	        if(listParameterApprovedGroup.size() == 0 ){
	 	        	listParameterApprovedGroup.add(approvedGroup);
	 	        	listParameterApprovedGroup.add(approvedGroupRekhape);
	 	        }
	        	HibernateHelper.addInParameterToQuery(hql, namedParameters, "r.newGroup", listParameterApprovedGroup);
	        }
	        
	        MemberGroup rejectedGroup = new MemberGroup();
	        rejectedGroup.setId(idUnregistered);
	        
	        MemberGroup rejectedGroupRekhape = new MemberGroup();
	        rejectedGroupRekhape.setId(idUnregisteredRekhape);
	        
	        if(query.getAction() == HistoryUpgradeLayanan.Action.REJECTED){
	        	List listParameterRejectedGroup = new ArrayList<MemberGroup>();
	 	        if(listParameterRejectedGroup.size() == 0 ){
	 	        	listParameterRejectedGroup.add(rejectedGroup);
	 	        	listParameterRejectedGroup.add(rejectedGroupRekhape);
	 	        }
	        	HibernateHelper.addInParameterToQuery(hql, namedParameters, "r.newGroup", listParameterRejectedGroup);
	        }
	        
	        HibernateHelper.addPeriodParameterToQuery(hql, namedParameters, "r.date", query.getPeriod());
	        
	        if (query.getMember() != null) {
	            hql.append(" and exists (select ma.id from MemberAccount ma where ma.member = :member and ma.member.id = r.subject.id)");
	            namedParameters.put("member", query.getMember());
	        }
	        HibernateHelper.appendOrder(hql, "r.date desc");
	        
	        return list(query, hql.toString(), namedParameters);
	        
	    }

		@Override
		public List<GroupRemark> searchByAgent(HistoryUpgradeLayananQuery query) {
			// TODO Auto-generated method stub
			final Map<String, Object> namedParameters = new HashMap<String, Object>();
	        final Set<Relationship> fetch = query.getFetch();
	        final StringBuilder hql = HibernateHelper.getInitialQuery(GroupRemark.class, "r", fetch);
	        System.out.println("HQL awal: "+hql);
	        HibernateHelper.addParameterToQuery(hql, namedParameters, "r.writer", query.getWriter());
	        
	        Long idRegisteredAgent = 0L;
	        Long idUnregistered = 0L;
	        Long idUnregPendingAgent = 0L;
			try {
				idRegisteredAgent = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Registered.Agent.group.id"));
				idUnregistered = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Unregistered.group.id"));
				idUnregPendingAgent = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgent.group.id"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
//	        MemberGroup oldGroup = new MemberGroup();
//	        oldGroup.setId(idUnregistered);
//	        HibernateHelper.addParameterToQuery(hql, namedParameters, "r.oldGroup", oldGroup);
	        
			MemberGroup newGroup = new MemberGroup();
	        newGroup.setId(idUnregPendingAgent);
	        HibernateHelper.addParameterToQuery(hql, namedParameters, "r.newGroup", newGroup);
	        
	        /*Hide Member's Group UNREGISTERED_PENDING_AGENT*/
	        HibernateHelper.addParameterToQueryOperator(hql, namedParameters, "r.subject.group.id","!=", idUnregPendingAgent);
	        	        
	        if(query.getAction() == HistoryUpgradeLayanan.Action.APPROVED){
	        	HibernateHelper.addParameterToQuery(hql, namedParameters, "r.subject.group.id", idRegisteredAgent);
	        }
	        
	        	        
	        if(query.getAction() == HistoryUpgradeLayanan.Action.REJECTED){
	        	HibernateHelper.addParameterToQuery(hql, namedParameters, "r.subject.group.id", idUnregistered);
	        }
	        
	        
	        HibernateHelper.addPeriodParameterToQuery(hql, namedParameters, "r.date", query.getPeriod());
	        
	        if (query.getMember() != null) {
	            hql.append(" and exists (select ma.id from MemberAccount ma where ma.member = :member and ma.member.id = r.subject.id)");
	            namedParameters.put("member", query.getMember());
	        }
	        HibernateHelper.appendOrder(hql, "r.date desc");
	        return list(query, hql.toString(), namedParameters);
	        
		}

}
