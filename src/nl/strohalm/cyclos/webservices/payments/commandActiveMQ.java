package nl.strohalm.cyclos.webservices.payments;
import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;


public class commandActiveMQ {
	 private JmsTemplate jmsTemplate;
	 
	 public void SendNoReply(String destination, Object msg) throws JmsException {
		 
		 jmsTemplate.convertAndSend(destination, msg, new MessagePostProcessor(){

			@Override
			public Message postProcessMessage(Message message) throws JMSException {
				// TODO Auto-generated method stub
				return message;
			}

	    });
		 
	 }

	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
}
