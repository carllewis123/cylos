/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.webservices.payments;

import java.io.Serializable;

import com.ptdam.emoney.webservices.edcpayment.EDCPaymentReqMsg;
import com.ptdam.emoney.webservices.hosttohost.HosttoHostPaymentReqMsg;
import com.ptdam.emoney.webservices.mobile.PaymentRequestMsg;

import ptdam.emoney.webservice.transactions.CashOutMsgRequest;

/**
 * Parameters used to confirm a payment
 * @author luis
 */
public class ConfirmPaymentParameters_Extended implements Serializable {

    private static final long serialVersionUID = -327047637621256514L;
    private String            ticket;
    private String            credentials;
  
	private CashOutMsgRequest cashOutRequest;
    private EDCPaymentReqMsg  edcRequest;
    //modification for agen tarik tunai - 23 Juni 2014
    private PaymentRequestMsg  requestAgen;
    
    public PaymentRequestMsg getRequestAgen() {
		return requestAgen;
	}

	public void setRequestAgen(PaymentRequestMsg requestAgen) {
		this.requestAgen = requestAgen;
	}
    //modification for agen tarik tunai - 16 Sept 2014
    private HosttoHostPaymentReqMsg  requestH2H;

	public String getCredentials() {
        return credentials;
    }

    public String getTicket() {
        return ticket;
    }

    public void setCredentials(final String credentials) {
        this.credentials = credentials;
    }

    public void setTicket(final String ticket) {
        this.ticket = ticket;
    }
    
    public CashOutMsgRequest getCashOutRequest() {
        return cashOutRequest;
    }

    public void setCashOutRequest(CashOutMsgRequest cashOutRequest) {
        this.cashOutRequest = cashOutRequest;
    }

    /**
     * Getter for property edcRequest
     */
    public EDCPaymentReqMsg getEdcRequest() {
        return edcRequest;
    }

    /**
     * Setter for property edcRequest
     */
    public void setEdcRequest(EDCPaymentReqMsg edcRequest) {
        this.edcRequest = edcRequest;
    }

    @Override
    public String toString() {
        return "ConfirmPaymentParameters [credentials=****" + ", ticket=" + ticket + "]";
    }

	public HosttoHostPaymentReqMsg getRequestH2H() {
		return requestH2H;
	}

	public void setRequestH2H(HosttoHostPaymentReqMsg requestH2H) {
		this.requestH2H = requestH2H;
	}

}
