/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.webservices.payments;

/**
 * Possible statuses for a payment
 * @author luis
 */
public enum PaymentStatus {

    /**
     * The payment was successfully processed
     */
    PROCESSED,

    /**
     * The payment was created, but is awaiting authorization
     */
    PENDING_AUTHORIZATION,

    /**
     * OTP salah
     */
    INVALID_OTP,
    
    /**
     * Invalid credentials was entered
     */
    INVALID_CREDENTIALS,

    /**
     * Credentials were blocked by exceeding allowed tries
     */
    BLOCKED_CREDENTIALS,

    /**
     * The payment was being performed from a channel the member doesn't have access
     */
    INVALID_CHANNEL,

    /**
     * One or more parameters were invalid
     */
    INVALID_PARAMETERS,

    /**
     * The given from member was not found
     */
    FROM_NOT_FOUND,

    /**
     * The given to member was not found
     */
    TO_NOT_FOUND,

    /**
     * The payment couldn't be performed, because there was not enough amount on the source account
     */
    NOT_ENOUGH_CREDITS,

    /**
     * The payment couldn't be performed, because the maximum amount today has been exceeded
     */
    MAX_DAILY_AMOUNT_EXCEEDED,

    /**
     * The payment couldn't be performed, because the destination account would surpass it's upper credit limit
     */
    RECEIVER_UPPER_CREDIT_LIMIT_REACHED,

    /**
     * In a bulk action, when a payment result in error, all next payments will have this status
     */
    NOT_PERFORMED,
    
    /**
     * In a bulk action, when a payment result in error, all next payments will have this status
     */
    INVALID_DENOM,

    /**
     * The ATM cashout exceeds the maximum denom allowed in ATM
     */
    MAX_DENOM_EXCEEDED,
    
    /**
     * The given from member transaction exceeded the monthly cashout
     */
    MAX_MONTH_AMOUNT_EXCEEDED,
    
    /**
     * The given from member is not allowed to perform the payment
     */
    TRANSFER_TYPE_NOT_ALLOWED,
    
    /**
     * The given EDC amount exceeds the request amount
     */
    EDC_AMOUNT_EXCEEDS_REQUEST_AMOUNT,
    
    /**
     * The given EDC amount exceeds the request amount
     */
    OTP_NOT_FOUND,
    
    /**
     * The given to member group is dormant, Dormant group can't receive payment
     */
    TO_MEMBER_IS_DORMANT,
    
    
    /**
     * The given topupcc transaction exceeded montly topup amount
     */
    EXCEED_MAX_LIMIT_TOPUP_PER_MONTH,
    
    /**
     * The given topupcc transaction exceeded day topup amount
     */
    EXCEED_MAX_LIMIT_TOPUP_PER_DAY,

    /**
     * Any other unexpected error will fall in this category
     */
    UNKNOWN_ERROR, H2H_AMOUNT_EXCEEDS_REQUEST_AMOUNT,
    /**
     * The given from member is not allowed to perform the payment for Registered
     */
    INVALID_PARAMETERS_FOR_REGISTERED,
    /**
     * The given from member is not allowed to perform the payment for Registered
     */
    TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED,
    
    MAX_MONTH_AMOUNT_RECEIVER_EXCEEDED,
    
    MAX_DAILY_AMOUNT_RECEIVER_EXCEEDED,
    
    /**
     * Handling to avoid payment double posting
     */
    TRX_ALREADY_POSTED,
    
    /**
     * Handling to catch minimum amount transaction
     */
    MIN_AMOUNT_TRANSACTION,
    /**
     * Payment record for a specific trace number was not found
     */
    TRX_NOT_FOUND,
    
    /**
     * No Ecash not match any regex
     */
    PATTERN_NOT_MATCH;

    /**
     * Returns whether this status is success
     */
    public boolean isSuccessful() {
        return this == PROCESSED || this == PENDING_AUTHORIZATION;
    }
}
