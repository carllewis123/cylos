/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.webservices.payments;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.entities.accounts.LockedAccountsOnPayments;
import nl.strohalm.cyclos.entities.accounts.SystemAccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentRequestTicket;
import nl.strohalm.cyclos.entities.accounts.transactions.Ticket;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.exceptions.PermissionDeniedException;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.access.ChannelServiceLocal;
import nl.strohalm.cyclos.services.access.exceptions.BlockedCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCredentialsException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.application.ApplicationServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.services.ServiceClientServiceLocal;
import nl.strohalm.cyclos.services.transactions.BulkChargebackResult;
import nl.strohalm.cyclos.services.transactions.BulkPaymentResult;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.TicketServiceLocal;
import nl.strohalm.cyclos.services.transactions.TransferAuthorizationServiceLocal;
import nl.strohalm.cyclos.services.transactions.exceptions.InvalidChannelException;
import nl.strohalm.cyclos.services.transactions.exceptions.MinAmountTransactionException;
import nl.strohalm.cyclos.services.transactions.exceptions.NotEnoughCreditsException;
import nl.strohalm.cyclos.utils.Pair;
import nl.strohalm.cyclos.utils.notifications.AdminNotificationHandler;
import nl.strohalm.cyclos.utils.notifications.MemberNotificationHandler;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.accounts.AccountHistorySearchParameters;
import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.model.AccountStatusVO;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.model.TransferTypeVO;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.PaymentHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.transactions.CashOutMsgRequest;

import com.ptdam.emoney.webservices.edcpayment.EDCPaymentReqMsg;
import com.ptdam.emoney.webservices.hosttohost.HosttoHostPaymentReqMsg;
import com.ptdam.emoney.webservices.hosttohost.HosttoHostPaymentStatus;
import com.ptdam.emoney.webservices.mobile.PaymentRequestMsg;

/**
 * Implementation for payment web service
 * @author luis
 */
@WebService(name = "payment", serviceName = "payment")
public class PaymentWebServiceImpl implements PaymentWebService {

    private final Log               logger    = LogFactory.getLog(PaymentWebServiceImpl.class);
    
    private static class PrepareParametersResult {
        private final PaymentStatus                 status;
        private final AccountOwner                  from;
        private final AccountOwner                  to;
        private final Collection<MemberCustomField> fromRequiredFields;
        private final Collection<MemberCustomField> toRequiredFields;

        public PrepareParametersResult(final PaymentStatus status, final AccountOwner from, final AccountOwner to, final Collection<MemberCustomField> fromRequiredFields, final Collection<MemberCustomField> toRequiredFields) {
            this.status = status;
            this.from = from;
            this.to = to;
            this.fromRequiredFields = fromRequiredFields;
            this.toRequiredFields = toRequiredFields;
        }

        public AccountOwner getFrom() {
            return from;
        }

        public Collection<MemberCustomField> getFromRequiredFields() {
            return fromRequiredFields;
        }

        public PaymentStatus getStatus() {
            return status;
        }

        public AccountOwner getTo() {
            return to;
        }

        public Collection<MemberCustomField> getToRequiredFields() {
            return toRequiredFields;
        }
    }

    /**
     * Common interface used by chargeback / reverse
     * @author andres
     * @param <V>
     */
    private interface TransferLoader<V> {
        Transfer load(V v);
    }

    private AccountServiceLocal               accountServiceLocal;
    private AccessServiceLocal                accessServiceLocal;
    private ApplicationServiceLocal           applicationServiceLocal;
    private PaymentServiceLocal               paymentServiceLocal;
    private TicketServiceLocal                ticketServiceLocal;
    private ElementServiceLocal               elementServiceLocal;
    private MemberCustomFieldServiceLocal     memberCustomFieldServiceLocal;
    private ServiceClientServiceLocal         serviceClientServiceLocal;
    private TransferAuthorizationServiceLocal transferAuthorizationServiceLocal;
    private PaymentHelper                     paymentHelper;
    private MemberHelper                      memberHelper;
    private WebServiceHelper                  webServiceHelper;
    private AccountHelper                     accountHelper;
    private ChannelHelper                     channelHelper;
    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP };

    private MemberNotificationHandler         memberNotificationHandler;
    private AdminNotificationHandler          adminNotificationHandler;
    
    public void setMemberNotificationHandler(final MemberNotificationHandler memberNotificationHandler) {
        this.memberNotificationHandler = memberNotificationHandler;
    }
    
    public void setAdminNotificationHandler(final AdminNotificationHandler adminNotificationHandler) {
        this.adminNotificationHandler = adminNotificationHandler;
    }

    @Override
    public ChargebackResult chargeback(final Long transferId) {
        return reverse(transferId, new TransferLoader<Long>() {
            @Override
            public Transfer load(final Long transferId) {
                return paymentServiceLocal.load(transferId);
            }
        });
    }

    @Override
    public PaymentResult confirmPayment(final ConfirmPaymentParameters params) {
        Exception errorException = null;
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        Member fromMember = null;
        Member toMember = null;

        // It's nonsense to use this if restricted to a member
        if (WebServiceContext.getMember() != null) {
            throw new PermissionDeniedException();
        }
        final Channel channel = WebServiceContext.getChannel();
        final String channelName = channel == null ? null : channel.getInternalName();

        PaymentStatus status = null;
        AccountHistoryTransferVO transferVO = null;

        // Get the ticket
        PaymentRequestTicket ticket = null;
        try {
            // Check that the ticket is valid
            final Ticket t = ticketServiceLocal.load(params.getTicket());
            fromMember = t.getFrom();
            toMember = t.getTo();

            if (!(t instanceof PaymentRequestTicket) || t.getStatus() != Ticket.Status.PENDING) {
                throw new Exception("Invalid ticket and/or status: " + t.getClass().getName() + ", status: " + t.getStatus());
            }
            // Check that the channel is the expected one
            ticket = (PaymentRequestTicket) t;
            if (!ticket.getToChannel().getInternalName().equals(channelName)) {
                throw new Exception("The ticket's destination channel is not the expected one (expected=" + channelName + "): " + ticket.getToChannel().getInternalName());
            }
        } catch (final Exception e) {
            errorException = e;
            status = PaymentStatus.INVALID_PARAMETERS;
        }

        // Validate the Channel and credentials
        Member member = null;
        if (status == null) {
            member = ticket.getFrom();
            if (!accessServiceLocal.isChannelEnabledForMember(channelName, member)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
            if (status == null && WebServiceContext.getClient().isCredentialsRequired()) {
                try {
                    checkCredentials(member, channel, params.getCredentials());
                } catch (final InvalidCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
            }
        }
        Transfer transfer = null;
        // Confirm the payment
        if (status == null) {
            try {
                transfer = paymentServiceLocal.confirmPayment(ticket.getTicket());
                status = paymentHelper.toStatus(transfer);
                transferVO = accountHelper.toVO(member, transfer, null);

                if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS)) {
                    if (WebServiceContext.getMember() == null) {
                        fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                        toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                    } else if (WebServiceContext.getMember().equals(fromMember)) {
                        fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                    } else {
                        toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                    }
                }
            } catch (final Exception e) {
                errorException = e;
                if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                    // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                    // exception.
                    status = paymentHelper.toStatus(e);
                } else if (status == null) {
                    /*
                     * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified
                     * by this exception unless there isn't a status, in which case return this exception.
                     */
                    status = paymentHelper.toStatus(e);
                }
            }
        }

        if (!status.isSuccessful()) {
            if (errorException != null) {
                webServiceHelper.error(errorException);
            } else {
                webServiceHelper.error("Confirm payment status: " + status);
            }
        } 
        // Build the result
        return new PaymentResult(status, transferVO, accountHelper.toVO(fromMemberStatus), accountHelper.toVO(toMemberStatus));
    }

    @Override
    public List<ChargebackResult> doBulkChargeback(final List<Long> transfersIds) {
        return doBulkChargeback(transfersIds, new TransferLoader<Long>() {
            @Override
            public Transfer load(final Long transfersId) {
                return paymentServiceLocal.load(transfersId);
            }
        });
    }

    @Override
    public List<PaymentResult> doBulkPayment(final List<PaymentParameters> params) {
        final int size = params == null ? 0 : params.size();
        final List<PaymentResult> results = new ArrayList<PaymentResult>(size);
        final DoPaymentDTO[] dtos = new DoPaymentDTO[size];
        final PrepareParametersResult[] loadedPrepareParameters = new PrepareParametersResult[size];
        if (size > 0) {
            // We should lock at once all from accounts for all payments, but only if all accounts are passed ok
            boolean hasError = false;
            final List<AccountDTO> allAccounts = new ArrayList<AccountDTO>();
            for (int i = 0; i < params.size(); i++) {
                final PaymentParameters param = params.get(i);
                final PrepareParametersResult result = prepareParameters(param);

                if (result.getStatus() == null) {
                    try {
                        final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(param, result.getFrom(), result.getTo());
                        if (!validateTransferType(dto)) {
                            results.add(new PaymentResult(PaymentStatus.INVALID_PARAMETERS, null));
                            webServiceHelper.error("The specified transfer type is invalid: " + dto.getTransferType());
                            hasError = true;
                        } else {
                            allAccounts.add(new AccountDTO(result.getFrom(), dto.getTransferType().getFrom()));
                            results.add(new PaymentResult(PaymentStatus.NOT_PERFORMED, null));
                        }
                        loadedPrepareParameters[i] = result;
                        dtos[i] = dto;
                    } catch (final Exception e) {
                        webServiceHelper.error(e);
                        hasError = true;
                        results.add(new PaymentResult(paymentHelper.toStatus(e), null));
                    }
                } else {
                    hasError = true;
                    results.add(new PaymentResult(result.getStatus(), null));
                    webServiceHelper.error("Bulk payment validation status [" + i + "]: " + result.getStatus());
                }
            }
            if (!hasError) {
                // No static validation error. Perform all payments
                final List<BulkPaymentResult> bulkResults = paymentServiceLocal.doBulkPayment(Arrays.asList(dtos));
                for (int i = 0; i < params.size(); i++) {
                    final PaymentParameters param = params.get(i);
                    BulkPaymentResult bulkResult;
                    try {
                        bulkResult = bulkResults.get(i);
                    } catch (final IndexOutOfBoundsException e) {
                        bulkResult = null;
                    }
                    PaymentResult result;
                    if (hasError || bulkResult == null) {
                        result = new PaymentResult(PaymentStatus.NOT_PERFORMED, null);
                    } else {
                        result = new PaymentResult();
                        final Transfer transfer = (Transfer) bulkResult.getPayment();
                        PaymentStatus status = null;

                        try {
                            if (transfer == null) {
                                status = paymentHelper.toStatus(bulkResult.getException());
                            } else {
                                status = paymentHelper.toStatus(transfer);
                                result.setTransfer(accountHelper.toVO(WebServiceContext.getMember(), transfer, null, loadedPrepareParameters[i].getFromRequiredFields(), loadedPrepareParameters[i].getToRequiredFields()));
                            }
                            if (!status.isSuccessful()) {
                                hasError = true;
                            }

                            // Set the account status, as requested
                            final AccountStatusVO[] statuses = getAccountStatusesForPayment(param, transfer);
                            result.setFromAccountStatus(statuses[0]);
                            result.setToAccountStatus(statuses[1]);
                        } catch (Exception e) {
                            webServiceHelper.error(e);
                            if (status == null) {
                                /*
                                 * doBulkPayment always opens a new transaction to perform the payments. So this exception won't roll back the
                                 * payments. The status sholuldn't be modified by this exception unless there isn't a status, in which case return
                                 * this exception.
                                 */

                                status = paymentHelper.toStatus(e);
                            }
                        }
                        result.setStatus(status);

                    }
                    results.set(i, result);
                }
            }
        }
        return results;
    }

    @Override
    public List<ChargebackResult> doBulkReverse(final List<String> traces) {
        return doBulkChargeback(traces, new TransferLoader<String>() {
            @Override
            public Transfer load(final String traceNumber) {
                return paymentServiceLocal.loadTransferForReverse(traceNumber);
            }
        });
    }

    @Override
    public PaymentResult doPayment(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        Member fromMember = null;
        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferType(dto)) {
                	if(dto.getFrom() != null) {
	                	fromMember = (Member) dto.getFrom();
	                	String fromMemberId = fromMember.getGroup().getId().toString();
	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
	                    } else {
	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
	                    }
                	} else {
                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                	}
                    webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                    
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];

                    } else {
                    	if(dto.getFrom() != null) {
    	                	fromMember = (Member) dto.getFrom();
    	                	String fromMemberId = fromMember.getGroup().getId().toString();
    	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
    	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
    	                    } else {
    	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
    	                    }
                    	} else {
                    		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    	}
                        webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        status = isTransferExist(status, params.getTraceNumber());
        
        return new PaymentResult(status, transferVO, fromMemberStatus, toMemberStatus);
    }
    
    //to handler transaction system to system from webservice
    public PaymentResult_Extended doPaymentExtended(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";

        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTOExtended(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferTypeExtended(dto)) {
                    status = PaymentStatus.INVALID_PARAMETERS;
                    webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                    status = paymentHelper.toStatus(transfer);
                    transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                    final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                    fromMemberStatus = statuses[0];
                    toMemberStatus = statuses[1];
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            errorCode = e.getMessage();
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }
        
        status = isTransferExist(status, params.getTraceNumber());
        
        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);

        result.setField1(errorCode);

        return result;
    }

    @Override
    public boolean expireTicket(final String ticketStr) {
        try {
            final PaymentRequestTicket ticket = (PaymentRequestTicket) ticketServiceLocal.load(ticketStr, PaymentRequestTicket.Relationships.FROM_CHANNEL);
            // Check the member restriction
            final Member restricted = WebServiceContext.getMember();
            if (restricted != null && !restricted.equals(ticket.getTo())) {
                throw new Exception();
            }

            // Check the from channel
            final Channel resolvedChannel = WebServiceContext.getChannel();
            final Channel fromChannel = ticket.getFromChannel();
            final Channel toChannel = ticket.getToChannel();
            if ((fromChannel == null || !fromChannel.equals(resolvedChannel)) && (toChannel == null || !toChannel.equals(resolvedChannel))) {
                throw new Exception();
            }

            // Check by status
            if (ticket.getStatus() == Ticket.Status.PENDING) {
                ticketServiceLocal.expirePaymentRequestTicket(ticket);
                return true;
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            // Ignore exceptions
        }
        return false;
    }

    @Override
    public RequestPaymentResult requestPaymentConfirmation(final RequestPaymentParameters params) {
        Exception errorException = null;
        PaymentRequestStatus status = null;
        // Get the to member
        Member toMember = null;
        final Member restricted = WebServiceContext.getMember();
        if (restricted != null) {
            // When restricted to a member, he is always the to
            toMember = restricted;
        } else {
            try {
                toMember = paymentHelper.resolveToMember(params);
            } catch (final EntityNotFoundException e) {
                status = PaymentRequestStatus.TO_NOT_FOUND;
            }
            // When not restricted to a member, check the channel access of the payment receiver
            if (status == null && !memberHelper.isChannelEnabledForMember(toMember)) {
                status = PaymentRequestStatus.TO_INVALID_CHANNEL;
            }
        }
        // Get the from member
        Member fromMember = null;
        if (status == null) {
            try {
                fromMember = paymentHelper.resolveFromMember(params);
            } catch (final EntityNotFoundException e) {
                status = PaymentRequestStatus.FROM_NOT_FOUND;
            }
        }

        // Generate the ticket if no error so far
        PaymentRequestTicket ticket = null;
        if (status == null) {
            try {
                ticket = paymentHelper.toTicket(params, null);
                ticket.setFrom(fromMember);
                ticket.setTo(toMember);
                ticket = ticketServiceLocal.generate(ticket);
                status = PaymentRequestStatus.REQUEST_RECEIVED;
            } catch (final InvalidChannelException e) {
                status = PaymentRequestStatus.FROM_INVALID_CHANNEL;
            } catch (final Exception e) {
                errorException = e;
                final PaymentStatus paymentStatus = paymentHelper.toStatus(e);
                try {
                    // Probably it's a payment status also present on payment request status
                    status = PaymentRequestStatus.valueOf(paymentStatus.name());
                } catch (final Exception e1) {
                    e1.initCause(e);
                    errorException = e1;
                    status = PaymentRequestStatus.UNKNOWN_ERROR;
                }
            }
        }

        if (!status.isSuccessful()) {
            if (errorException != null) {
                webServiceHelper.error(errorException);
            } else {
                webServiceHelper.error("Request payment confirmation status: " + status);
            }
        }

        // Build a result
        final RequestPaymentResult result = new RequestPaymentResult();
        result.setStatus(status);
        if (ticket != null) {
            result.setTicket(ticket.getTicket());
        }
        return result;
    }

    @Override
    public ChargebackResult reverse(final String traceNumber) {
        return reverse(traceNumber, new TransferLoader<String>() {
            @Override
            public Transfer load(final String traceNumber) {
                return paymentServiceLocal.loadTransferForReverse(traceNumber);
            }
        });
    }

    public void setAccessServiceLocal(final AccessServiceLocal accessService) {
        accessServiceLocal = accessService;
    }

    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }

    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        accountServiceLocal = accountService;
    }

    public void setApplicationServiceLocal(final ApplicationServiceLocal applicationServiceLocal) {
        this.applicationServiceLocal = applicationServiceLocal;
    }

    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }

    public void setMemberHelper(final MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }

    public void setPaymentHelper(final PaymentHelper paymentHelper) {
        this.paymentHelper = paymentHelper;
    }

    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }

    public void setServiceClientServiceLocal(final ServiceClientServiceLocal serviceClientService) {
        serviceClientServiceLocal = serviceClientService;
    }

    public void setTicketServiceLocal(final TicketServiceLocal ticketService) {
        ticketServiceLocal = ticketService;
    }

    public void setTransferAuthorizationServiceLocal(final TransferAuthorizationServiceLocal transferAuthorizationService) {
        transferAuthorizationServiceLocal = transferAuthorizationService;
    }

    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }

    @Override
    public PaymentStatus simulatePayment(final PaymentParameters params) {

        PaymentStatus status = null;

        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());
                if (!validateTransferType(dto)) {
                    webServiceHelper.trace(PaymentStatus.INVALID_PARAMETERS + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    return PaymentStatus.INVALID_PARAMETERS;
                } else {
                    // Simulate the payment
                    final Transfer transfer = (Transfer) paymentServiceLocal.simulatePayment(dto);
                    return paymentHelper.toStatus(transfer);
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            return paymentHelper.toStatus(e);
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Simulate payment status: " + status);
        }
        return status;
    }

    /**
     * Checks the given member's pin
     */
    private void checkCredentials(Member member, final Channel channel, final String credentials) {
        if (member == null) {
            return;
        }
        final ServiceClient client = WebServiceContext.getClient();
        final Member restrictedMember = client.getMember();
        if (restrictedMember == null) {
            // Non-restricted clients use the flag credentials required
            if (!client.isCredentialsRequired()) {
                // No credentials should be checked
                throw new InvalidCredentialsException();
            }
        } else {
            // Restricted clients don't need check if is the same member
            if (restrictedMember.equals(member)) {
                throw new InvalidCredentialsException();
            }
        }
        if (StringUtils.isEmpty(credentials)) {
            throw new InvalidCredentialsException();
        }
        member = elementServiceLocal.load(member.getId(), Element.Relationships.USER);
        accessServiceLocal.checkCredentials(channel, member.getMemberUser(), credentials, WebServiceContext.getRequest().getRemoteAddr(), WebServiceContext.getMember());
    }

    /**
     * Performs some static checks and loads the transfers to be chargedback by the bulk reverse operation.
     * @param ids: Could be trace numbers or transfer ids.
     * @param loader: The object responsible for loading the transfer by trace number or transferId.
     */
    private <V> List<ChargebackResult> doBulkChargeback(final List<V> ids, final TransferLoader<V> loader) {
        final List<Transfer> transfers = new LinkedList<Transfer>();
        final List<Transfer> actualTransfers = new LinkedList<Transfer>();
        final Member member = WebServiceContext.getMember();
        final List<ChargebackResult> results = new LinkedList<ChargebackResult>();
        boolean hasError = false;

        // Load each transfer
        for (final V id : ids) {
            Transfer transfer = null;
            try {
                transfer = loader.load(id);
                transfers.add(transfer);
                if (member != null && !transfer.getToOwner().equals(member)) {
                    throw new EntityNotFoundException();
                }

                // Preprocess the chargeback
                final Pair<ChargebackStatus, Transfer> preprocessResult = preprocessChargeback(transfer);
                final ChargebackStatus status = preprocessResult.getFirst();
                final Transfer preprocessTransfer = preprocessResult.getSecond();

                if (status == ChargebackStatus.SUCCESS) {
                    // This transfer was considered succes by other means (for example, by canceling a pending payment). Don't pass it to
                    // bulkChargeback
                    actualTransfers.add(null);
                    final AccountHistoryTransferVO chargebackVO = accountHelper.toVO(WebServiceContext.getMember(), preprocessTransfer, null);
                    final AccountHistoryTransferVO transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null);
                    results.add(new ChargebackResult(status, chargebackVO, transferVO));
                } else {
                    // The transfer could have already failed if status != null or ok if status == null;
                    actualTransfers.add(transfer);
                    results.add(new ChargebackResult(status == null ? ChargebackStatus.NOT_PERFORMED : status, null, null));
                    if (status != null) {
                        hasError = true;
                    }
                }
            } catch (final EntityNotFoundException e) {
                hasError = true;
                results.add(new ChargebackResult(ChargebackStatus.TRANSFER_NOT_FOUND, null, null));
                webServiceHelper.error(new Exception("Bulk status [Id=" + id + "]: " + ChargebackStatus.TRANSFER_NOT_FOUND, e));
            }
        }

        if (hasError) {
            // No need to go on with bulkChargeback as we already know something has failed
            return results;
        }

        // Do the bulk chargeback
        final List<BulkChargebackResult> bulkResults = paymentServiceLocal.bulkChargeback(actualTransfers);
        for (int i = 0; i < actualTransfers.size(); i++) {
            final Transfer actualTransfer = actualTransfers.get(i);
            if (actualTransfer == null) {
                // It was not passed to bulkChargeback, as we already knew the status. Skip this one
                continue;
            }
            BulkChargebackResult bulkResult;
            try {
                bulkResult = bulkResults.get(i);
            } catch (final IndexOutOfBoundsException e) {
                bulkResult = null;
            }

            ChargebackResult result;
            if (hasError || bulkResult == null) {
                result = new ChargebackResult(ChargebackStatus.NOT_PERFORMED, null, null);
            } else {
                result = new ChargebackResult();
                final Transfer chargeback = bulkResult.getTransfer();
                ChargebackStatus status = null;
                if (chargeback == null) {
                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                    hasError = true;
                } else {
                    status = ChargebackStatus.SUCCESS;
                    try {
                        result.setChargebackTransfer(accountHelper.toVO(WebServiceContext.getMember(), chargeback, null));
                        result.setOriginalTransfer(accountHelper.toVO(WebServiceContext.getMember(), actualTransfer, null));
                    } catch (Exception e) {
                        webServiceHelper.error(e);
                        if (status == null) {
                            /*
                             * doBulkChargeback always opens a new transaction to perform the payments. So this exception won't roll back the
                             * chargebacks. The status sholuldn't be modified by this exception unless there isn't a status, in which case return this
                             * exception.
                             */

                            status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                        }
                    }
                }
                result.setStatus(status);
            }
            results.set(i, result);
        }
        return results;
    }

    private ChargebackResult doChargeback(final Transfer transfer) {

        final Pair<ChargebackStatus, Transfer> preprocessResult = preprocessChargeback(transfer);
        ChargebackStatus status = preprocessResult.getFirst();
        Transfer chargebackTransfer = preprocessResult.getSecond();

        // Do the chargeback
        if (status == null) {
            chargebackTransfer = paymentServiceLocal.chargeback(transfer);
            status = ChargebackStatus.SUCCESS;
            if(chargebackTransfer != null)
            memberNotificationHandler.chargebackPaymentNotification(transfer, chargebackTransfer.getTransactionNumber());
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Chargeback result: " + status);
        }

        Member member = WebServiceContext.getMember();
        // Build the result
        if (status == ChargebackStatus.SUCCESS || status == ChargebackStatus.TRANSFER_ALREADY_CHARGEDBACK) {
            AccountHistoryTransferVO originalVO = null;
            AccountHistoryTransferVO chargebackVO = null;
            try {
                final AccountOwner owner = member == null ? transfer.getToOwner() : member;
                originalVO = accountHelper.toVO(owner, transfer, null);
                chargebackVO = accountHelper.toVO(owner, chargebackTransfer, null);
            } catch (Exception e) {
                webServiceHelper.error(e);
                if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                    // The chargeback is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                    // exception.
                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                }
                // When the locking method is not NONE, the chargeback is committed on a new transaction so we'll preserve the status.
            }
            return new ChargebackResult(status, originalVO, chargebackVO);
        } else {
            return new ChargebackResult(status, null, null);
        }
    }

    /**
     * Returns the account status for the from account and to account, if the given {@link PaymentParameters#isReturnStatus()} is true and according
     * to the current invocation permissions
     */
    private AccountStatusVO[] getAccountStatusesForPayment(final PaymentParameters params, final Transfer transfer) {
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS) && params.getReturnStatus()) {
            if (WebServiceContext.getMember() == null) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            } else if (WebServiceContext.getMember().equals(paymentHelper.resolveFromMember(params))) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
            } else {
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            }
        }
        return new AccountStatusVO[] {
                accountHelper.toVO(fromMemberStatus),
                accountHelper.toVO(toMemberStatus) };
    }

    private Collection<MemberCustomField> getMemberCustomFields(final Member member, final List<String> fieldInternalNames) {
        Collection<MemberCustomField> fields = new HashSet<MemberCustomField>();

        for (final String internalName : fieldInternalNames) {
            MemberCustomFieldValue mcfv = (MemberCustomFieldValue) CollectionUtils.find(member.getCustomValues(), new Predicate() {
                @Override
                public boolean evaluate(final Object object) {
                    MemberCustomFieldValue mcfv = (MemberCustomFieldValue) object;
                    return mcfv.getField().getInternalName().equals(internalName);
                }
            });
            if (mcfv == null) {
                webServiceHelper.trace(String.format("Required field '%1$s' was not found for member %2$s", internalName, member));
                return null;
            } else {
                fields.add(memberCustomFieldServiceLocal.load(mcfv.getField().getId()));
            }
        }

        return fields;
    }

    /**
     * Prepares the parameters for a payment. The resulting status is null when no problem found
     */
    private PrepareParametersResult prepareParameters(final PaymentParameters params) {

        final Member restricted = WebServiceContext.getMember();
        final boolean fromSystem = params.getFromSystem();
        final boolean toSystem = params.getToSystem();
        PaymentStatus status = null;
        Member fromMember = null;
        Member toMember = null;
        // Load the from member
        if (!fromSystem) {
            try {
                fromMember = paymentHelper.resolveFromMember(params);
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.FROM_NOT_FOUND;
            }
        }
        // Load the to member
        if (status == null && !toSystem) {
            try {
                toMember = paymentHelper.resolveToMember(params);
                
                String dormantGroup="";
            	try {
					dormantGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("dormant.group.name");
				} catch (IOException e) {
					dormantGroup = "DORMANT";
					e.printStackTrace();
				}
            	
            	if(toMember.getMemberGroup()!= null && toMember.getMemberGroup().getName().equalsIgnoreCase(dormantGroup)){
            		status = PaymentStatus.TO_MEMBER_IS_DORMANT;
            	}
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.TO_NOT_FOUND;
            }
        }

        if (status == null) {
            if (restricted == null) {
                // Ensure has the do payment permission
                if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                    throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                }
                // Check the channel immediately, as needed by SMS controller
                if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                    status = PaymentStatus.INVALID_CHANNEL;
                }
            } else {
                // Enforce the restricted to member parameters
                if (fromSystem) {
                    // Restricted to member can't perform payment from system
                    status = PaymentStatus.FROM_NOT_FOUND;
                } else {
                    if (fromMember == null) {
                        fromMember = restricted;
                    } else if (toMember == null && !toSystem) {
                        toMember = restricted;
                    }
                }
                if (status == null) {
                    // Check make / receive payment permissions
                    if (fromMember.equals(restricted)) {
                        if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                        }
                    } else {
                        if (!WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                        }
                    }
                    // Ensure that either from or to member is the restricted one
                    if (!fromMember.equals(restricted) && !toMember.equals(restricted)) {
                        status = PaymentStatus.INVALID_PARAMETERS;
                        webServiceHelper.trace(status + ". Reason: Neither the origin nor the destination members are equal to the restricted: " + restricted);
                    }
                }
                if (status == null) {
                    // Enforce the permissions
                    if (restricted.equals(fromMember) && !WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                    } else if (restricted.equals(toMember) && !WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                    }
                }
            }
        }

        // Ensure both from and to member are present
        if (status == null) {
            if (fromMember == null && !fromSystem) {
                status = PaymentStatus.FROM_NOT_FOUND;
            } else if (toMember == null && !toSystem) {
                status = PaymentStatus.TO_NOT_FOUND;
            } else if (fromMember != null && toMember != null) {
                // Ensure the to member is visible by the from member
                final Collection<MemberGroup> visibleGroups = fromMember.getMemberGroup().getCanViewProfileOfGroups();
                if (CollectionUtils.isEmpty(visibleGroups) || !visibleGroups.contains(toMember.getGroup())) {
                    status = PaymentStatus.TO_NOT_FOUND;
                }
            }
        }

        // Ensure required CF are present ONLY for unrestricted client
        Collection<MemberCustomField> fromMemberfields = null, toMemberfields = null;
        if (status == null) {
            boolean hasFromRequired = CollectionUtils.isNotEmpty(params.getFromMemberFieldsToReturn());
            boolean hasToRequired = CollectionUtils.isNotEmpty(params.getToMemberFieldsToReturn());
            if (restricted != null && (hasFromRequired || hasToRequired) || hasFromRequired && fromSystem || hasToRequired && toSystem) {
                webServiceHelper.trace(restricted != null ? "Restricted web service clients are not allowed to require member custom field values" : "Can't require custom field values for a system payment");
                status = PaymentStatus.INVALID_PARAMETERS;
            }
            if (status == null && hasFromRequired) {
                fromMemberfields = getMemberCustomFields(fromMember, params.getFromMemberFieldsToReturn());
                status = fromMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }

            if (status == null && hasToRequired) {
                toMemberfields = getMemberCustomFields(toMember, params.getToMemberFieldsToReturn());
                status = toMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }
        }

        if (status == null) {
            // Check the channel
            if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
        }
        if (status == null) {
            // Check the credentials
            boolean checkCredentials;
            if (restricted != null) {
                checkCredentials = !fromMember.equals(restricted);
            } else {
                checkCredentials = !fromSystem && WebServiceContext.getClient().isCredentialsRequired();
            }
            if (checkCredentials) {
                try {
                    checkCredentials(fromMember, WebServiceContext.getChannel(), params.getCredentials());
                } catch (final InvalidCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
            }
        }

        // No error
        final AccountOwner fromOwner = fromSystem ? SystemAccountOwner.instance() : fromMember;
        final AccountOwner toOwner = toSystem ? SystemAccountOwner.instance() : toMember;
        return new PrepareParametersResult(status, fromOwner, toOwner, fromMemberfields, toMemberfields);
    }

    private Pair<ChargebackStatus, Transfer> preprocessChargeback(final Transfer transfer) {
        ChargebackStatus status = null;
        Transfer chargebackTransfer = null;

        // Check if the transfer can be charged back
        if (!paymentServiceLocal.canChargeback(transfer, false)) {
            if (transfer.getChargedBackBy() != null) {
                chargebackTransfer = transfer.getChargedBackBy();
                status = ChargebackStatus.TRANSFER_ALREADY_CHARGEDBACK;
            } else {
                if (transfer.getStatus() == Payment.Status.PENDING) {
                    final TransferAuthorizationDTO transferAuthorizationDto = new TransferAuthorizationDTO();
                    transferAuthorizationDto.setTransfer(transfer);
                    transferAuthorizationDto.setShowToMember(false);
                    chargebackTransfer = transferAuthorizationServiceLocal.cancel(transferAuthorizationDto);
                    status = ChargebackStatus.SUCCESS;
                } else {
                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                }
            }
        }

        return new Pair<ChargebackStatus, Transfer>(status, chargebackTransfer);
    }

    private <V> ChargebackResult reverse(final V transferId, final TransferLoader<V> loader) {
        Exception errorException = null;
        ChargebackStatus status = null;
        Transfer transfer = null;

        try {
            transfer = loader.load(transferId);
            // Ensure the member is the one who received the payment
            final Member member = WebServiceContext.getMember();
            if (member != null && !transfer.getToOwner().equals(member)) {
                throw new EntityNotFoundException();
            } else {
                final Collection<TransferType> possibleTypes = serviceClientServiceLocal.load(WebServiceContext.getClient().getId(), ServiceClient.Relationships.CHARGEBACK_PAYMENT_TYPES).getChargebackPaymentTypes();
                if (!possibleTypes.contains(transfer.getType())) {
                    throw new EntityNotFoundException();
                }
            }
        } catch (final EntityNotFoundException e) {
            errorException = e;
            status = ChargebackStatus.TRANSFER_NOT_FOUND;
        }

        if (status == null) {
            try {
                return doChargeback(transfer);
            } catch (final Exception e) {
                webServiceHelper.error(e);
                return new ChargebackResult(ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK, null, null);
            }
        } else {
            if (!status.isSuccessful()) {
                if (errorException != null) {
                    webServiceHelper.error(errorException);
                } else {
                    webServiceHelper.error("Chargeback status: " + status);
                }
            }
            return new ChargebackResult(status, null, null);
        }
    }

    private boolean validateTransferType(final DoPaymentDTO dto) {
        final Collection<TransferType> possibleTypes = paymentHelper.listPossibleTypes(dto);
        return possibleTypes != null && possibleTypes.contains(dto.getTransferType());
    }
    
    private boolean validateTransferTypeExtended(final DoPaymentDTO dto) {
        final Collection<TransferType> possibleTypes = paymentHelper.listPossibleTypesExtended(dto);
        return possibleTypes != null && possibleTypes.contains(dto.getTransferType());
    }
    
    @Override
    public PaymentResult confirmPayment_extended(final ConfirmPaymentParameters_Extended params) {
        Exception errorException = null;
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        Member fromMember = null;
        Member toMember = null;

        // It's nonsense to use this if restricted to a member
        if (WebServiceContext.getMember() != null) {
            throw new PermissionDeniedException();
        }
        final Channel channel = WebServiceContext.getChannel();
        final String channelName = channel == null ? null : channel.getInternalName();

        PaymentStatus status = null;
        AccountHistoryTransferVO transferVO = null;

        // Get the ticket
        PaymentRequestTicket ticket = null;
        try {
            // Check that the ticket is valid
            final Ticket t = ticketServiceLocal.load(params.getTicket());
            fromMember = t.getFrom();
            toMember = t.getTo();

            if (!(t instanceof PaymentRequestTicket) || t.getStatus() != Ticket.Status.PENDING) {
                throw new Exception("Invalid ticket and/or status: " + t.getClass().getName() + ", status: " + t.getStatus());
            }
            // Check that the channel is the expected one
            ticket = (PaymentRequestTicket) t;
            if (!ticket.getToChannel().getInternalName().equals(channelName)) {
                throw new Exception("The ticket's destination channel is not the expected one (expected=" + channelName + "): " + ticket.getToChannel().getInternalName());
            }
        } catch (final Exception e) {
            errorException = e;
            status = PaymentStatus.INVALID_PARAMETERS;
        }

        // Validate the Channel and credentials
        Member member = null;
        if (status == null) {
            member = ticket.getFrom();
            if (!accessServiceLocal.isChannelEnabledForMember(channelName, member)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
            if (status == null && WebServiceContext.getClient().isCredentialsRequired()) {
                try {
                    checkCredentials(member, channel, params.getCredentials());
                } catch (final InvalidCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
            }
        }
        
        String cashOutTempAccount = null;
        String offlineMerchant = null;
        String topupccAccount = null;
        
        try {
           offlineMerchant = EmoneyConfiguration.getEmoneyProperties().getProperty("edc.offline_merchant");
           cashOutTempAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.cashout.tempaccount");
           topupccAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.tempaccount");
        }
        catch (IOException e) {
            status = PaymentStatus.UNKNOWN_ERROR;
        }
        
        Transfer transfer = null;
        PaymentResult result = null;
        
        // Confirm the payment
        if (status == null) {

            //final Ticket t = ticketServiceLocal.load(params.getTicket());    
            
            if (toMember.getUsername().equals(cashOutTempAccount)) {
                
                BigDecimal withdrawAmount = ticket.getAmount();
                
                CashOutMsgRequest cashOutReq = params.getCashOutRequest();
                
                BigDecimal denom = new BigDecimal(cashOutReq.getDenom());
                
                denom = denom.multiply(new BigDecimal(1000));
                
                BigDecimal val = withdrawAmount.remainder(denom);
                
                BigDecimal maxMoneyOut = BigDecimal.ZERO;
                int maxMoneyOutCount = 0;
                
                try {
                    maxMoneyOutCount = Integer.parseInt(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.max_money_out"));
                } catch (NumberFormatException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                
                maxMoneyOut = denom.multiply(new BigDecimal(maxMoneyOutCount));
                
                if (val.compareTo(BigDecimal.ZERO) == 0) {
                
                    if (withdrawAmount.compareTo(maxMoneyOut) <= 0) {
                    
                        try {
                            PaymentParameters withdrawParams = new PaymentParameters();
                            withdrawParams.setToSystem(true);
                            withdrawParams.setFromMember(cashOutReq.getEmoneyAccountNo());
                            withdrawParams.setAmount(withdrawAmount);
                            
                            final StringBuffer revKey = new StringBuffer();
                            revKey.append(cashOutReq.getTerminalId());
                            revKey.append(cashOutReq.getRetrievalReferenceNo());
                            revKey.append(cashOutReq.getLocalTransactionDate().substring(2)); // Untuk menyesuaikan dengan data yang dikirim saat reversal, maka yang dipakai hanya MMDD
                            revKey.append(cashOutReq.getLocalTransactionTime());
                            
                            withdrawParams.setTraceNumber(revKey.toString());
                            
                            // Rekonsiliasi u/ tarik tunai
                            final StringBuffer rekonKey = new StringBuffer();
                            rekonKey.append(" ");
                            rekonKey.append("|");
                            rekonKey.append(cashOutReq.getTerminalId());
                            rekonKey.append("/");
                            rekonKey.append(cashOutReq.getRetrievalReferenceNo());
                            rekonKey.append("|");
                            rekonKey.append(" ");
                            
                            withdrawParams.setTraceData(rekonKey.toString());
                            
                            String customFieldName = "";
            	            String customValue = "";
            	            try {
            	                customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("cashout.terminal.resi");
            	                if(cashOutReq.getTerminalId() != null && cashOutReq.getRetrievalReferenceNo() != null){
            	                	customValue = cashOutReq.getTerminalId() + "/" + cashOutReq.getRetrievalReferenceNo();
            	                }else{
            	                	customValue = "";
            	                }
            	            } catch (final IOException e) {
            	                logger.error(e);
            	            }
            	            
            	            List<FieldValueVO> customValues = null;
            	            if (customFieldName != null && !customFieldName.isEmpty()) {
            	                FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
            	                customValues = Arrays.asList(customFieldValue);
            	                withdrawParams.setCustomValues(customValues);
            	            
            	            }
                            
            	            // Payment Channel (Isi dengan merchant id dari partner terkait)
            	            if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
            	                FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
            	                List<FieldValueVO> paymentChannelCustomValues = null; 
            	                ArrayList<FieldValueVO> addCustomValues = null;
            	                if (withdrawParams.getCustomValues() == null) {
            	                    paymentChannelCustomValues = Arrays.asList(paymentChannelCustomFieldValue);
            	                    withdrawParams.setCustomValues(paymentChannelCustomValues);
            	                } else {
            	                	addCustomValues = new ArrayList<FieldValueVO>(customValues);
            	                	addCustomValues.add(paymentChannelCustomFieldValue);
            	                	withdrawParams.setCustomValues(addCustomValues);
            	                }
            	              }                            

                            result =  doPayment(withdrawParams);
                            status = result.getStatus();
                                            
                            switch (result.getStatus()) {
                                case PROCESSED:
                                    ticketServiceLocal.markAsOk(ticket);
                                    
                                    AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
                                    searchparams.setPrincipal(cashOutReq.getEmoneyAccountNo());
    
                                    final TransferQuery query = accountHelper.toQuery(searchparams, member);
                                    final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                                    result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                                    break;

                                default:
                                    break;
                            }
                        } catch (Exception e) {
                            logger.error(e, e);
                            status = PaymentStatus.UNKNOWN_ERROR;
                        }

                    } else {
                        result = new PaymentResult();
                        status = PaymentStatus.MAX_DENOM_EXCEEDED;
                        result.setStatus(status);
                    }

                } else {
                    result = new PaymentResult();
                    status = PaymentStatus.INVALID_DENOM;
                    result.setStatus(status);
                }
            } else if (toMember.getUsername().equals(offlineMerchant)) {
                EDCPaymentReqMsg edcRequest = params.getEdcRequest();
                
                BigDecimal paymentAmount = new BigDecimal(edcRequest.getEdcTrxAmount());
                BigDecimal cashOutAmount = new BigDecimal(edcRequest.getEdcCashOutAmount());
                BigDecimal edcAmount = paymentAmount.add(cashOutAmount);
                BigDecimal ticketAmount = ticket.getAmount();
                
                if (ticketAmount.subtract(edcAmount).compareTo(BigDecimal.ZERO) >= 0) {
                    
                    try {
                    
                        PaymentParameters edcParams = new PaymentParameters();
                        edcParams.setAmount(edcAmount);
                        edcParams.setFromMember(ticket.getFrom().getUsername());
                        edcParams.setToMember(edcRequest.getMerchantId());
    
                        final StringBuffer revKey = new StringBuffer();
                        revKey.append("EDC");
                        revKey.append(edcRequest.getMerchantId());
                        revKey.append(edcRequest.getEdcId());
                        revKey.append(edcRequest.getEdcTrxRefNo());
                        revKey.append(edcRequest.getEdcTrxDateTime()); 
                        
                        edcParams.setTraceNumber(revKey.toString());
                        
                        // Rekonsiliasi u/ edc payment
                        final StringBuffer rekonKey = new StringBuffer();
                        rekonKey.append(" ");
                        rekonKey.append("|");
                        rekonKey.append(edcRequest.getMerchantId());
                        rekonKey.append(edcRequest.getEdcId());
                        rekonKey.append("/");
                        rekonKey.append(edcRequest.getEdcTrxRefNo());
                        rekonKey.append("|");
                        rekonKey.append(" ");
                        
                        edcParams.setTraceData(rekonKey.toString());
                        edcParams.setTransferTypeId(ticket.getTransferType().getId());
                        
                        FieldValueVO customFieldValue = new FieldValueVO();
                        customFieldValue.setInternalName("merchant_ref_no");
                        customFieldValue.setValue(edcRequest.getEdcTrxRefNo());
                        
                        List<FieldValueVO> customFieldValues = Arrays.asList(customFieldValue);
                        
                        edcParams.setCustomValues(customFieldValues);
                        
                        // Payment Channel (Isi dengan merchant id dari partner terkait)
                        if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
                            FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
                            List<FieldValueVO> paymentChannelCustomValues = null; 
                            ArrayList addCustomValues = null;
                            if (edcParams.getCustomValues() == null) {
                                paymentChannelCustomValues = Arrays.asList(paymentChannelCustomFieldValue);
                                edcParams.setCustomValues(paymentChannelCustomValues);
                            } else {
                            	addCustomValues = new ArrayList(customFieldValues);
                            	addCustomValues.add(paymentChannelCustomFieldValue);
                            	edcParams.setCustomValues(addCustomValues);
                            }
                            
                        }
                        
                        result =  doPayment(edcParams);
                        status = result.getStatus();
    
                        switch (result.getStatus()) {
                            case PROCESSED:
                                ticketServiceLocal.markAsOk(ticket);
                                
                                AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
                                searchparams.setPrincipal(ticket.getFrom().getUsername());
    
                                final TransferQuery query = accountHelper.toQuery(searchparams, member);
                                final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                                result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                                break;
    
                            default:
                                break;
                        }
                    
                    } 
                    catch (final Exception e) {
                        logger.error(e, e);
                        result  = new PaymentResult();
                        status = PaymentStatus.UNKNOWN_ERROR;
                        result.setStatus(status);
                    }
                    
                } else {
                    result  = new PaymentResult();
                    status = PaymentStatus.EDC_AMOUNT_EXCEEDS_REQUEST_AMOUNT;                                     
                    result.setStatus(status);
                }
                
            } else if(fromMember.getUsername().equals(topupccAccount)){
            	Long transferTypeId= 0L;
				try {
					transferTypeId = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("mobile.transferID." + params.getEdcRequest().getReserved5()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	BigDecimal topupAmount = ticket.getAmount();
            	PaymentParameters topupParams = new PaymentParameters();
            	topupParams.setFromSystem(true);
            	topupParams.setToMember(ticket.getTo().getUsername());
            	topupParams.setAmount(topupAmount);
            	topupParams.setTraceNumber(UUID.randomUUID().toString());
            	topupParams.setTraceData("traceDataTopupCC");
            	topupParams.setTransferTypeId(transferTypeId);
            	
            	if(params.getEdcRequest() != null){
	            	try {
						String customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.transaction.id");
					    String customValue = params.getEdcRequest().getReserved1() != null ? params.getEdcRequest().getReserved1() : "";
					    
						String customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.transaction.time");
					    String customValue2 = params.getEdcRequest().getReserved2() != null ? params.getEdcRequest().getReserved2() : "";
					    
					    String customFieldName3 = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.masked.card");
					    String customValue3 = params.getEdcRequest().getReserved3() != null ? params.getEdcRequest().getReserved3() : "";
					    customValue3 = maskCardNum(customValue3);
					    
					    String customFieldName4 = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.approval.code");
					    String customValue4 = params.getEdcRequest().getReserved4() != null ? params.getEdcRequest().getReserved4() : "";
					    
					    String customFieldName6 = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.terminal.id");
					    String customValue6 = params.getEdcRequest().getReserved6() != null ? params.getEdcRequest().getReserved6() : "";
					    
					    String customFieldName7 = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.merchant.id");
					    String customValue7 = params.getEdcRequest().getReserved7() != null ? params.getEdcRequest().getReserved7() : "";
					    
					    FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
				    	List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
				    	
				    	FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
				    	FieldValueVO customFieldValue3 = new FieldValueVO(customFieldName3, customValue3);
				    	FieldValueVO customFieldValue4 = new FieldValueVO(customFieldName4, customValue4);
				    	FieldValueVO customFieldValue6 = new FieldValueVO(customFieldName6, customValue6);
				    	FieldValueVO customFieldValue7 = new FieldValueVO(customFieldName7, customValue7);
				    	
				    	ArrayList addCustomValues = new ArrayList(customValues);
				    	addCustomValues.add(customFieldValue2);
				    	addCustomValues.add(customFieldValue4);
				    	addCustomValues.add(customFieldValue3);
				    	addCustomValues.add(customFieldValue6);
				    	addCustomValues.add(customFieldValue7);
				    	
				    	if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
                            FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
                            List<FieldValueVO> paymentChannelCustomValues = null; 
                            if (customValues == null) {
                                paymentChannelCustomValues = Arrays.asList(paymentChannelCustomFieldValue);
                            } else {
                                addCustomValues.add(paymentChannelCustomFieldValue);
                            }
                        }
				    	
				    	topupParams.setCustomValues(addCustomValues);
				    	
				    	String desc = "MID: "+ customValue7 + ", " + "TID: "+ customValue6 +", " + "Approval Code: "+ customValue4;
	                	topupParams.setDescription(desc);
	                	
	            	} catch (IOException e) {
						e.printStackTrace();
					}
            	}
			    
            	result = doPayment(topupParams);
            	status = result.getStatus();
            	
            	switch (result.getStatus()) {
                case PROCESSED:
                    ticketServiceLocal.markAsOk(ticket);
                    break;

                default:
                    break;
            	}
            } else{

                try {
                    transfer = paymentServiceLocal.confirmPayment(ticket.getTicket());
                    status = paymentHelper.toStatus(transfer);
                    transferVO = accountHelper.toVO(member, transfer, null);
    
                    if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS)) {
                        if (WebServiceContext.getMember() == null) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        } else if (WebServiceContext.getMember().equals(fromMember)) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                        } else {
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        }
                    }
                } catch (final NotEnoughCreditsException e) {
                    status = paymentHelper.toStatus(e);
                } catch (final Exception e) {
                    errorException = e;
                    if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                        // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                        // exception.
                        status = paymentHelper.toStatus(e);
                    } else if (status == null) {
                        /*
                         * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified
                         * by this exception unless there isn't a status, in which case return this exception.
                         */
                        status = paymentHelper.toStatus(e);
                    }
                }
            }
        }

        if (!status.isSuccessful()) {
            if (errorException != null) {
                webServiceHelper.error(errorException);
            } else {
                webServiceHelper.error("Confirm payment status: " + status);
            }
        }
        
        // Build the result
        if (result == null) {
            return new PaymentResult(status, transferVO, accountHelper.toVO(fromMemberStatus), accountHelper.toVO(toMemberStatus));
        } else {
            return result;
        }
        
    }
    
    private boolean paymentGroupCheck(final String fromMember, final TransferType type) {
        final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
        final Member member = elementServiceLocal.loadByPrincipal(principalType, fromMember, FETCH);
        boolean isPaymentAllow = false;

        if (member.getGroup().getTransferTypes().contains(type)) {
            isPaymentAllow = true;
        }
        return isPaymentAllow;
    }
    
    @Override
    public Long getTransferTypeIdFromTicket(final String params) {
        Exception errorException = null;
        Long transferTypeId = 0L;
        try {
        	transferTypeId = paymentServiceLocal.getTransferTypeId(params);
        } catch (final Exception e) {
            errorException = e;
            webServiceHelper.error(errorException);
        }
        
        return transferTypeId;
        
    }

  
    
  //Tambah service baru untuk agent tarik tunai - 30 Juni 2014
    
    
    @Override
    public PaymentResult_Extended confirmPaymentH2H_extended(final ConfirmPaymentParameters_Extended params) {
        Exception errorException = null;
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        Member fromMember = null;
        Member toMember = null;

        // It's nonsense to use this if restricted to a member
        if (WebServiceContext.getMember() != null) {
            throw new PermissionDeniedException();
        }
        
        Channel channel = null;
        String channelName = null;

        PaymentStatus status = null;
        AccountHistoryTransferVO transferVO = null;

        // Get the ticket
        PaymentRequestTicket ticket = null;
        try {
            // Check that the ticket is valid
            final Ticket t = ticketServiceLocal.load(params.getTicket());
            fromMember = t.getFrom();
            toMember = t.getTo();

            if (!(t instanceof PaymentRequestTicket) || t.getStatus() != Ticket.Status.PENDING) {
                throw new Exception("Invalid ticket and/or status: " + t.getClass().getName() + ", status: " + t.getStatus());
            }
            // Check that the channel is the expected one
            ticket = (PaymentRequestTicket) t;

            channel = WebServiceContext.getChannel();
            channelName = channel == null ? null : channel.getInternalName();
            
            if (!ticket.getToChannel().getInternalName().equals(channelName)) {
                throw new Exception("The ticket's from channel is not the expected one (expected=" + channelName + "): " + ticket.getFromChannel().getInternalName());
            }
        } catch (final Exception e) {
            errorException = e;
            status = PaymentStatus.INVALID_PARAMETERS;
        }

        // Validate the Channel and credentials
        Member member = null;
        String reserve1value = new String();
        
        reserve1value=params.getRequestH2H().getReserved1();
        if (status == null) {
        	if(reserve1value==null||reserve1value.equals("0")){
             		final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
                     member = elementServiceLocal.loadByPrincipal(principalType, params.getRequestH2H().getMerchantId(), FETCH);
                     if (!accessServiceLocal.isChannelEnabledForMember(channelName, member)) {
                         status = PaymentStatus.INVALID_CHANNEL;
                     }
             	}
//            if (status == null && WebServiceContext.getClient().isCredentialsRequired()) {
//            	try {
//                    checkCredentialsAgent_Extended(member, channel, params.getRequestAgen().getCredential());
//                } catch (final InvalidCredentialsException e) {
//                    errorException = e;
//                    status = PaymentStatus.INVALID_CREDENTIALS;
//                } catch (final BlockedCredentialsException e) {
//                    errorException = e;
//                    status = PaymentStatus.BLOCKED_CREDENTIALS;
//                }
            
        }
        
        String offlineH2h = null;
        String offlineEDC = null;
        String offlineH2hCasa = null;
        String checkPhoneNo = null;

        try {

        	offlineH2h = EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.offline_merchant");
        	offlineEDC = EmoneyConfiguration.getEmoneyProperties().getProperty("edc.offline_merchant");
        	offlineH2hCasa = EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.offline_casa");
        	checkPhoneNo = EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.checkPhoneNo");

        }
        catch (IOException e) {
            status = PaymentStatus.UNKNOWN_ERROR;
        }
        HosttoHostPaymentReqMsg h2hRequest = params.getRequestH2H();

        Transfer transfer = null;
        PaymentResult_Extended result = null;
        boolean NumberValidateOK;
        
        if(checkPhoneNo.equals("false")){
        	NumberValidateOK = true;
        }else if(checkPhoneNo.equals("true")){
        	if(ticket.getFrom().getUsername().equals(h2hRequest.getPhoneNo())){
            	NumberValidateOK = true;
        	}else{
        		NumberValidateOK = false;
        	}
        }else{
        	NumberValidateOK = false;
        }

        // Confirm the payment
        if (status == null) {
        	if (reserve1value==null||reserve1value.equals("0")) {
                BigDecimal trxAmount  = new BigDecimal(h2hRequest.getHosttohostTrxAmount());
                BigDecimal ticketAmount = ticket.getAmount();
                
                boolean isvalidnumber = ticketAmount.subtract(trxAmount).compareTo(BigDecimal.ZERO) >= 0;
                this.logger.info("trx = " + trxAmount + " ticket = " + ticketAmount + " isvalid= " + isvalidnumber);
                if (ticketAmount.subtract(trxAmount).compareTo(BigDecimal.ZERO) >= 0) {
                	if(toMember.getUsername().equals(offlineEDC)){
                	       try {
                	            
                               PaymentParameters h2hParams = new PaymentParameters();
                               h2hParams.setAmount(new BigDecimal(h2hRequest.getHosttohostTrxAmount()));
                               h2hParams.setFromMember(ticket.getFrom().getUsername());
                               h2hParams.setToMember(h2hRequest.getMerchantId());
                               h2hParams.setTransferTypeId(ticket.getTransferType().getId());
                               h2hParams.setTraceNumber(h2hRequest.getHosttohostTraceNo());
                               h2hParams.setDescription(h2hRequest.getHosttohostDescription());
                               
                            // Payment Channel (Isi dengan merchant id dari partner terkait)(Bayar Toko)
                               if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
                                   FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
                                   List<FieldValueVO> paymentChannelCustomValues = null; 
                                       paymentChannelCustomValues = Arrays.asList(paymentChannelCustomFieldValue);
                                       h2hParams.setCustomValues(paymentChannelCustomValues);
                               }
                               
                               result =  doPaymentH2H_Extended(h2hParams);
                               status = result.getStatus();

                               switch (result.getStatus()) {
                                   case PROCESSED:
                                       ticketServiceLocal.markAsOk(ticket);
                                       
                                       AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
                                       searchparams.setPrincipal(h2hParams.getToMember());

                                       final TransferQuery query = accountHelper.toQuery(searchparams, member);
                                       final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                                       result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                                       break;

                                   default:
                                       break;
                               }
                           
                           } 
                           catch (final Exception e) {
                               logger.error(e, e);
                               result  = new PaymentResult_Extended();
                               status = PaymentStatus.UNKNOWN_ERROR;
                               result.setStatus(status);
                           }
                	}else if(toMember.getUsername().equals(offlineH2h)){
                     	if(NumberValidateOK){
                            try {
                PaymentParameters h2hParams = new PaymentParameters();
                h2hParams.setAmount(new BigDecimal(h2hRequest.getHosttohostTrxAmount()));
                h2hParams.setFromMember(ticket.getFrom().getUsername());
                h2hParams.setToMember(h2hRequest.getMerchantId());
                h2hParams.setTransferTypeId(ticket.getTransferType().getId());
                h2hParams.setTraceNumber(h2hRequest.getHosttohostTraceNo());
                h2hParams.setDescription(h2hRequest.getHosttohostDescription());
                
                
                // Payment Channel (Isi dengan merchant id dari partner terkait)
                if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
                    FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
                    List<FieldValueVO> paymentChannelCustomValues = null; 
                    paymentChannelCustomValues = Arrays.asList(paymentChannelCustomFieldValue);
                    h2hParams.setCustomValues(paymentChannelCustomValues);
                }

                
                result =  doPaymentH2H_Extended(h2hParams);
                status = result.getStatus();

                switch (result.getStatus()) {
                    case PROCESSED:
                        ticketServiceLocal.markAsOk(ticket);
                        
                        AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
                        searchparams.setPrincipal(h2hParams.getToMember());

                        final TransferQuery query = accountHelper.toQuery(searchparams, member);
                        final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                        result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                        break;

                    default:
                        break;
                }
            
            } 
            catch (final Exception e) {
                logger.error(e, e);
                result  = new PaymentResult_Extended();
                status = PaymentStatus.UNKNOWN_ERROR;
                result.setStatus(status);
            }
        	}else{
                result  = new PaymentResult_Extended();
                status = PaymentStatus.FROM_NOT_FOUND;
                result.setStatus(status);
        	}
                	}
                	
           
                } else {
                    result  = new PaymentResult_Extended();
                    status = PaymentStatus.H2H_AMOUNT_EXCEEDS_REQUEST_AMOUNT;                                     
                    result.setStatus(status);
                }
                
            }else if(params.getRequestH2H().getReserved1().equals("1")){
            	   BigDecimal trxAmount = ticket.getAmount();
            	   BigDecimal ticketAmount = ticket.getAmount();
               
                 if(params.getRequestH2H().getOfflineMerchantId().equals(offlineH2hCasa)){                      
                	 if (ticketAmount.subtract(trxAmount).compareTo(BigDecimal.ZERO) >= 0) {
                	   if(NumberValidateOK){
                           try {
                 		       Long tariktunaitrxtypeid = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.trxtype_casa"));

                               PaymentParameters h2hParams = new PaymentParameters();
                               h2hParams.setAmount(new BigDecimal(h2hRequest.getHosttohostTrxAmount()));
                               h2hParams.setFromMember(ticket.getFrom().getUsername());
                               h2hParams.setToSystem(true);                          	
                               h2hParams.setTransferTypeId(tariktunaitrxtypeid);
                               h2hParams.setTraceNumber(h2hRequest.getHosttohostTraceNo());	
                               h2hParams.setDescription(h2hRequest.getHosttohostDescription());
 
                               
                               
	                            String customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
	       					    String customValue = h2hRequest.getReserved2() != null ? h2hRequest.getReserved2() : "";
	       					    
	       						String customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.journalSequence.internalName");
	       					    String customValue2 = h2hRequest.getReserved3() != null ? h2hRequest.getReserved3() : "";
	       					    
	       					    String customFieldName3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.idAgent.internalName");
	       					    String customValue3 = h2hRequest.getReserved4() != null ? h2hRequest.getReserved4() : "";
	       					    
	       					    String customFieldName4 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.nameAgent.internalName");
	       					    String customValue4 = h2hRequest.getReserved5() != null ? h2hRequest.getReserved5() : "";
	       					    
	       					    
	       					    FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
	       				    	List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
	       				    	
	       				    	FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
	       				    	
	       				    	
	       				    	FieldValueVO customFieldValue3 = new FieldValueVO(customFieldName3, customValue3);
	       				    	FieldValueVO customFieldValue4 = new FieldValueVO(customFieldName4, customValue4);
	       				    	
	       				  // Payment Channel (Isi dengan merchant id dari partner terkait)
	       		             FieldValueVO paymentChannelCustomFieldValue = null;
	       		                if (ticket.getDescription() != null && !ticket.getDescription().isEmpty()) {
	       		                    paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", ticket.getDescription());
	       		                    
	       		                }
	       				    	
	       				    	
	       				    	ArrayList addCustomValues = new ArrayList(customValues);
	       				    	addCustomValues.add(customFieldValue2);
	       				    	addCustomValues.add(customFieldValue3);
	       				    	addCustomValues.add(customFieldValue4);
	       				    	addCustomValues.add(paymentChannelCustomFieldValue);
                               
                               h2hParams.setCustomValues(addCustomValues);

                               result =  doPaymentH2H_Extended(h2hParams);
                               status = result.getStatus();
                 
                               switch (result.getStatus()) {
                                   case PROCESSED:
                                       ticketServiceLocal.markAsOk(ticket);
                                  
//                                       AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
//                                       searchparams.setPrincipal(h2hParams.getFromMember());
//                                       final TransferQuery query = accountHelper.toQuery(searchparams, member);
//                                       final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
//                                       result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                                       break;

                                   default:
                                       break;
                               }
                           } catch (Exception e) {
                               logger.error(e, e);
                               status = PaymentStatus.UNKNOWN_ERROR;
                           }
                	   }else{
                           result  = new PaymentResult_Extended();
                           status = PaymentStatus.FROM_NOT_FOUND;
                           result.setStatus(status);
                   		}
                   }else {
                       result = new PaymentResult_Extended();
                       status = PaymentStatus.H2H_AMOUNT_EXCEEDS_REQUEST_AMOUNT;
                       result.setStatus(status);
                     }
                 }else{
                     result  = new PaymentResult_Extended();
                     status = PaymentStatus.UNKNOWN_ERROR;
                     result.setStatus(status);
                 }
            } 
        	else {
            
                try {
                    transfer = paymentServiceLocal.confirmPayment(ticket.getTicket());
                    status = paymentHelper.toStatus(transfer);
                    transferVO = accountHelper.toVO(member, transfer, null);
    
                    if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS)) {
                        if (WebServiceContext.getMember() == null) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        } else if (WebServiceContext.getMember().equals(fromMember)) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                        } else {
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        }
                    }
                } catch (final NotEnoughCreditsException e) {
                    status = paymentHelper.toStatus(e);
                } catch (final Exception e) {
                    errorException = e;
                    if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                        // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                        // exception.
                        status = paymentHelper.toStatus(e);
                    } else if (status == null) {
                        /*
                         * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified
                         * by this exception unless there isn't a status, in which case return this exception.
                         */
                        status = paymentHelper.toStatus(e);
                    }
                }
            }
        }

        if (!status.isSuccessful()) {
            if (errorException != null) {
                webServiceHelper.error(errorException);
            } else {
                webServiceHelper.error("Confirm payment status: " + status);
            }
        }
        
        // Build the result
        if (result == null) {
            PaymentResult_Extended results = new PaymentResult_Extended(status, transferVO, accountHelper.toVO(fromMemberStatus), accountHelper.toVO(toMemberStatus));
            
            return results;
        } else {
            return result;
        }
   
    }
    
    @Override
    public PaymentResult_Extended doPaymentH2H_Extended(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";
        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferType(dto)) {
                    status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                    
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];

                    } else {
                        status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                        webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            
            errorCode = e.getMessage();
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        status = isTransferExist(status, params.getTraceNumber());
        
        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);
        
        result.setField1(errorCode);
        
        return result;
    }
    
    //Tambah service baru untuk agent tarik tunai - 30 Juni 2014
    @Override
    public PaymentResult_Extended confirmPaymentAgent_extended(final ConfirmPaymentParameters_Extended params) {
        Exception errorException = null;
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        Member fromMember = null;
        Member toMember = null;

        // It's nonsense to use this if restricted to a member
        if (WebServiceContext.getMember() != null) {
            throw new PermissionDeniedException();
        }
        
        Channel channel = null;
        String channelName = null;

        PaymentStatus status = null;
        AccountHistoryTransferVO transferVO = null;

        // Get the ticket
        PaymentRequestTicket ticket = null;
        try {
            // Check that the ticket is valid
            final Ticket t = ticketServiceLocal.load(params.getTicket());
            fromMember = t.getFrom();
            toMember = t.getTo();

            if (!(t instanceof PaymentRequestTicket) || t.getStatus() != Ticket.Status.PENDING) {
                throw new Exception("Invalid ticket and/or status: " + t.getClass().getName() + ", status: " + t.getStatus());
            }
            // Check that the channel is the expected one
            ticket = (PaymentRequestTicket) t;
            
            channel = WebServiceContext.getChannel();
            channelName = channel == null ? null : channel.getInternalName();
            
            if (!ticket.getToChannel().getInternalName().equals(channelName)) {
//                throw new Exception("The ticket's destination channel is not the expected one (expected=" + channelName + "): " + ticket.getToChannel().getInternalName());
                throw new Exception("The ticket's from channel is not the expected one (expected=" + channelName + "): " + ticket.getFromChannel().getInternalName());
            }
        } catch (final Exception e) {
            errorException = e;
            status = PaymentStatus.INVALID_PARAMETERS;
        }

        // Validate the Channel and credentials
        Member member = null;
        Member DariMember = null;
        if (status == null) {
        	final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            member = elementServiceLocal.loadByPrincipal(principalType, params.getRequestAgen().getTo(), FETCH);
            DariMember = elementServiceLocal.loadByPrincipal(principalType, fromMember.getUsername(), FETCH);
            if (!accessServiceLocal.isChannelEnabledForMember(channelName, member)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
//            if (status == null && WebServiceContext.getClient().isCredentialsRequired()) {
            	try {
                    checkCredentialsAgent_Extended(member, channel, params.getRequestAgen().getCredential());
                } catch (final InvalidCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    errorException = e;
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
//            }
        }
        
        String offlineAgen = null;
        
        try {

        	offlineAgen = EmoneyConfiguration.getEmoneyProperties().getProperty("agen.tarik_tunai");
        }
        catch (IOException e) {
            status = PaymentStatus.UNKNOWN_ERROR;
        }
        
        Transfer transfer = null;
        PaymentResult_Extended result = null;
        
        // Confirm the payment
        if (status == null) {
        	if (toMember.getUsername().equals(offlineAgen)) {
                PaymentRequestMsg agenRequest = params.getRequestAgen();
                
                BigDecimal ticketAmount = ticket.getAmount();
                
                if (ticketAmount.compareTo(BigDecimal.ZERO) >= 0) {
                    
                    try {
                    
                        PaymentParameters agenParams = new PaymentParameters();
                        agenParams.setAmount(ticketAmount);
                        agenParams.setFromMember(ticket.getFrom().getUsername());
                        agenParams.setToMember(agenRequest.getTo());
    
                        agenParams.setTransferTypeId(ticket.getTransferType().getId());
                        agenParams.setCredentials(agenRequest.getCredential());
                        
                        result =  doPaymentAgent_Extended(agenParams);
                        status = result.getStatus();
    
                        switch (result.getStatus()) {
                            case PROCESSED:
                                ticketServiceLocal.markAsOk(ticket);
                                
                                AccountHistorySearchParameters searchparams = new AccountHistorySearchParameters();
                                searchparams.setPrincipal(agenParams.getToMember());
    
                                final TransferQuery query = accountHelper.toQuery(searchparams, member);
                                final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                                result.setFromAccountStatus(accountHelper.toVO(accountStatus));
                                //setToAccountStatus
                                AccountHistorySearchParameters searchparams2 = new AccountHistorySearchParameters();
                                searchparams2.setPrincipal(fromMember.getUsername());
                                
                                final TransferQuery query2 = accountHelper.toQuery(searchparams2, DariMember);
                                final AccountStatus accountStatus2 = accountServiceLocal.getCurrentStatus(new AccountDTO(DariMember, query2.getType()));
                                result.setToAccountStatus(accountHelper.toVO(accountStatus2));
                                break;
    
                            default:
                                break;
                        }
                    
                    } 
                    catch (final Exception e) {
                        logger.error(e, e);
                        result  = new PaymentResult_Extended();
                        status = PaymentStatus.UNKNOWN_ERROR;
                        result.setStatus(status);
                    }
                    
                } else {
                    result  = new PaymentResult_Extended();
                    status = PaymentStatus.EDC_AMOUNT_EXCEEDS_REQUEST_AMOUNT;                                     
                    result.setStatus(status);
                }
                
            } else {
            
                try {
                    transfer = paymentServiceLocal.confirmPayment(ticket.getTicket());
                    status = paymentHelper.toStatus(transfer);
                    transferVO = accountHelper.toVO(member, transfer, null);
    
                    if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS)) {
                        if (WebServiceContext.getMember() == null) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        } else if (WebServiceContext.getMember().equals(fromMember)) {
                            fromMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(fromMember, transfer.getFrom().getType()));
                        } else {
                            toMemberStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(toMember, transfer.getTo().getType()));
                        }
                    }
                } catch (final NotEnoughCreditsException e) {
                    status = paymentHelper.toStatus(e);
                } catch (final Exception e) {
                    errorException = e;
                    if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                        // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                        // exception.
                        status = paymentHelper.toStatus(e);
                    } else if (status == null) {
                        /*
                         * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified
                         * by this exception unless there isn't a status, in which case return this exception.
                         */
                        status = paymentHelper.toStatus(e);
                    }
                }
            }
        }

        if (!status.isSuccessful()) {
            if (errorException != null) {
                webServiceHelper.error(errorException);
            } else {
                webServiceHelper.error("Confirm payment status: " + status);
            }
        }
        
        // Build the result
        if (result == null) {
            PaymentResult_Extended results = new PaymentResult_Extended(status, transferVO, accountHelper.toVO(fromMemberStatus), accountHelper.toVO(toMemberStatus));
            
            return results;
        } else {
            return result;
        }
    
    }
    
    @Override
    public PaymentResult_Extended doPaymentAgent_Extended(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";
        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferType(dto)) {
                    status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                    
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];

                    } else {
                        status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                        webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            
            errorCode = e.getMessage();
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        status = isTransferExist(status, params.getTraceNumber());
        
        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);
        
        result.setField1(errorCode);
        
        return result;
    }
    
    /**
     * Checks the given member's pin
     */
    private void checkCredentialsAgent_Extended(Member member, final Channel channel, final String credentials) {
        if (member == null) {
            return;
        }
        
        member = elementServiceLocal.load(member.getId(), Element.Relationships.USER);
        accessServiceLocal.checkCredentials(channel, member.getMemberUser(), credentials, WebServiceContext.getRequest().getRemoteAddr(), WebServiceContext.getMember());
    }
    
    @Override
    public PaymentResult_Extended doCustomPayment(final PaymentParametersExtended params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";
        String prefixErrorKey = "";
        Member fromMember = null;
        Exception exception = null;
        try {
        	if(params.getAdapterTrxType() != null && params.getAdapterTrxType().length() > 0 ){
        		String strTrxType = EmoneyConfiguration.getEmoneyProperties().getProperty("adapter.trxtype." + params.getAdapterTrxType());
        		prefixErrorKey = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.error.key." + params.getAdapterTrxType());
        		params.setTransferTypeId(Long.parseLong(strTrxType));
        		
        	}
        	
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());
                
                // Validate the transfer type
                if (!validateTransferType(dto)) {
                	if(dto.getFrom() != null) {
	                	fromMember = (Member) dto.getFrom();
	                	String fromMemberId = fromMember.getGroup().getId().toString();
	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
	                    } else {
	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
	                    }
                	} else {
                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                	}
                	webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                    
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];

                    } else {
                    	if(dto.getFrom() != null) {
    	                	fromMember = (Member) dto.getFrom();
    	                	String fromMemberId = fromMember.getGroup().getId().toString();
    	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
    	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
    	                    } else {
    	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
    	                    }
                    	} else {
                    		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    	}
                    	webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        } catch (final ExternalException e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            
            	
            	errorCode = e.getMessage();
        }catch (final Exception e) {
        	exception = e;
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);
        
        
        
        String errorMessage = "";
        
       
        try {
        	String errorKey = "";
        	if(status.equals(PaymentStatus.UNKNOWN_ERROR)){
        		 errorKey = status + prefixErrorKey + errorCode;
        	}else{
        		errorKey = status.toString();
        	}
        	
        	if(params.getMedia() != null && params.getMedia().length() > 0 ){
        		switch (params.getMedia().toLowerCase()){ 
        			case "mobile" :
        				errorMessage = EmoneyConfiguration.getMobileErrorMessage().getProperty(errorKey);
        				if ((result.getStatus().equals(PaymentStatus.MIN_AMOUNT_TRANSACTION)) && (errorMessage.contains("{amount}"))) {
        		        	DecimalFormat format = new DecimalFormat("#,###.##");
        		        	MinAmountTransactionException ex = (MinAmountTransactionException) exception;
        		         	String formattedMinAmount = format.format(ex.getAmount());
        		            errorMessage = errorMessage.replace("{amount}", formattedMinAmount);
        		          }
        				
        				break;
        			case "ussd" :
        				errorMessage = EmoneyConfiguration.getUSSDErrorMessage().getProperty(errorKey);
        				break;
        			default :
        				errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
        				break;
        		}
        	}else{
        		errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
        	}
        	
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        result.setField1(errorMessage);
        
        return result;
    }
    
    
    @Override
    public PaymentResult_Extended doPaymentExternal(final PaymentParametersExtended params) {
    	 AccountHistoryTransferVO transferVO = null;
         Transfer transfer = null;
         PaymentStatus status = null;
         AccountStatusVO fromMemberStatus = null;
         AccountStatusVO toMemberStatus = null;
         String errorCode = "";
         String prefixErrorKey = "";
         Member fromMember = null;
         
         try {
         	if(params.getAdapterTrxType() != null && params.getAdapterTrxType().length() > 0 ){
         		String strTrxType = EmoneyConfiguration.getEmoneyProperties().getProperty("adapter.trxtype." + params.getAdapterTrxType());
         		prefixErrorKey = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.error.key." + params.getAdapterTrxType());
         		params.setTransferTypeId(Long.parseLong(strTrxType));
         		
         	}
         	
             final PrepareParametersResult result = prepareParameters(params);
             status = result.getStatus();

             if (status == null) {
                 // Status null means no "pre-payment" errors (like validation, pin, channel...)
                 // Perform the payment
                 final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());
                 
                 // Validate the transfer type
                 if (!validateTransferType(dto)) {
                 	if(dto.getFrom() != null) {
 	                	fromMember = (Member) dto.getFrom();
 	                	String fromMemberId = fromMember.getGroup().getId().toString();
 	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
 	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
 	                    } else {
 	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
 	                    }
                 	} else {
                 		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                 	}
                 	webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                 } else {
                     if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                     
                         transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                         status = paymentHelper.toStatus(transfer);
                         transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                         final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                         fromMemberStatus = statuses[0];
                         toMemberStatus = statuses[1];

                     } else {
                     	if(dto.getFrom() != null) {
     	                	fromMember = (Member) dto.getFrom();
     	                	String fromMemberId = fromMember.getGroup().getId().toString();
     	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
     	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
     	                    } else {
     	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
     	                    }
                     	} else {
                     		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                     	}
                     	webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                     }
                 }
             }
         }catch (final ExternalException e) {
             webServiceHelper.error(e);
             if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                 // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                 // exception.
                 status = paymentHelper.toStatus(e);
             } else if (status == null) {
                 /*
                  * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                  * this exception unless there isn't a status, in which case return this exception.
                  */
                 status = paymentHelper.toStatus(e);
             }
             
             	
             	errorCode = e.getMessage();
         }catch (final Exception e) {
             webServiceHelper.error(e);
             if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                 // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                 // exception.
                 status = paymentHelper.toStatus(e);
             } else if (status == null) {
                 /*
                  * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                  * this exception unless there isn't a status, in which case return this exception.
                  */
                 status = paymentHelper.toStatus(e);
             }
         }
         
         status = isTransferExist(status, params.getTraceNumber());
         
         PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);

         if (!status.isSuccessful()) {
             webServiceHelper.error("Payment error status: " + status);
         }else{
		    	System.out.println("Result : " + result);
		    	try{
		    		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		    		commandActiveMQ cAMQ = (commandActiveMQ) ctx.getBean("test");
		    		
		    		StringWriter sw = new StringWriter();
		    		JAXBContext jaxbContext = JAXBContext.newInstance(PaymentResult_Extended.class);
		    		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		    		jaxbMarshaller.marshal(result, sw);
		    		String xmlString = sw.toString();
		    		
		    		if (checkTransferTypeCode(transferVO.getTransferType().getId().toString(), "queue.mpt")) {
			    		cAMQ.SendNoReply("merchant.payment.listener", xmlString);
			    		System.out.println("queue : merchant.payment.listener");
			    		System.out.println("xmlString : " + xmlString);
			    		//throw new JMSException("ExceptionJMS");
					}else{
		    		cAMQ.SendNoReply("external.payment.listener", xmlString);
		    		System.out.println("xmlString : " + xmlString);
		    		//throw new JMSException("ExceptionJMS");
					}
		    	} catch (Exception e){
		    		System.out.println(e);
		    		doChargeback(transfer);
		    	}
         }
    	
    	
    	String errorMessage = "";
        
        try {
        	String errorKey = "";
        	if(status.equals(PaymentStatus.UNKNOWN_ERROR)){
        		 errorKey = status + prefixErrorKey + errorCode;
        	}else{
        		errorKey = status.toString();
        	}
        	
        	if(params.getMedia() != null && params.getMedia().length() > 0 ){
        		switch (params.getMedia().toLowerCase()){ 
        			case "mobile" :
        				errorMessage = EmoneyConfiguration.getMobileErrorMessage().getProperty(errorKey);
        				break;
        			case "ussd" :
        				errorMessage = EmoneyConfiguration.getUSSDErrorMessage().getProperty(errorKey);
        				break;
        			default :
        				errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
        				break;
        		}
        	}else{
        		errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
        	}
        	
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        result.setField1(errorMessage);

    	return result;
    }
    
    private String maskCardNum(String value){
    	return value.substring(0,6) + "******"+ value.substring(value.length()-4);
    }
    
    public ChargebackResult doCustomChargeback(final Transfer transfer) {

        final Pair<ChargebackStatus, Transfer> preprocessResult = preprocessChargeback(transfer);
        ChargebackStatus status = preprocessResult.getFirst();
        Transfer chargebackTransfer = preprocessResult.getSecond();

        // Do the chargeback
        if (status == null) {
            chargebackTransfer = paymentServiceLocal.chargeback(transfer);
            status = ChargebackStatus.SUCCESS;
            memberNotificationHandler.chargebackPaymentNotification(transfer, chargebackTransfer.getTransactionNumber());
        }
        
        return new ChargebackResult(status, null, null);

//        if (!status.isSuccessful()) {
//            webServiceHelper.error("Chargeback result: " + status);
//        }
//
//        Member member = WebServiceContext.getMember();
//        // Build the result
//        if (status == ChargebackStatus.SUCCESS || status == ChargebackStatus.TRANSFER_ALREADY_CHARGEDBACK) {
//            AccountHistoryTransferVO originalVO = null;
//            AccountHistoryTransferVO chargebackVO = null;
//            try {
//                final AccountOwner owner = member == null ? transfer.getToOwner() : member;
//                originalVO = accountHelper.toVO(owner, transfer, null);
//                chargebackVO = accountHelper.toVO(owner, chargebackTransfer, null);
//            } catch (Exception e) {
//                webServiceHelper.error(e);
//                if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
//                    // The chargeback is committed on the same transaction so it will be rolled back by this exception, then status is given by this
//                    // exception.
//                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
//                }
//                // When the locking method is not NONE, the chargeback is committed on a new transaction so we'll preserve the status.
//            }
//            return new ChargebackResult(status, originalVO, chargebackVO);
//        } else {
//            return new ChargebackResult(status, null, null);
//        }
    }

    public boolean checkTransferTypeCode(String transferTypeCode, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(transferTypeCode)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(transferTypeCode)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    public boolean checkGroup(String groupId, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(groupId)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(groupId)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    private PaymentStatus isTransferExist(PaymentStatus status, final String traceNumber) {
        
        if (status.equals(PaymentStatus.INVALID_PARAMETERS)) {
            Transfer trx = null; 
            
            if (StringUtils.isNotEmpty(traceNumber)) {
                try {
                    trx = paymentServiceLocal.loadTransferForReverse(traceNumber);
                } catch (final Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (trx != null) {
                status = PaymentStatus.TRX_ALREADY_POSTED;
            }
        }
        return status;

    }

    @Override
    public PaymentResult checkPaymentStatusByTraceNumber(String traceNumber) {
        // TODO Auto-generated method stub
        Transfer trx = null; 
        PaymentStatus status = PaymentStatus.TRX_NOT_FOUND;
                
        if (StringUtils.isNotEmpty(traceNumber)) {
            try {
                trx = paymentServiceLocal.loadTransferForReverse(traceNumber);
                
            } catch (final EntityNotFoundException ex) {
                // If transfer record not found, prompt client with TRX_NOT_FOUND
                status = PaymentStatus.TRX_NOT_FOUND;
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        } else {
            // If traceNumber parameter is empty, prompt client with INVALID_PARAMETERS
            status = PaymentStatus.INVALID_PARAMETERS;
        }
        
        if (trx != null) {
            status = PaymentStatus.TRX_ALREADY_POSTED;
        }
        return new PaymentResult(status, null, null, null);
    }
}
