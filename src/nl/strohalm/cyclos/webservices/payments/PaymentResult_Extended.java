/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.webservices.payments;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.model.AccountStatusVO;

/**
 * Contains the payment status and the payment data
 * @author luis
 */
@XmlRootElement
public class PaymentResult_Extended implements Serializable {
    private static final long serialVersionUID = -1664521326549382934L;
    private PaymentStatus            status;
    private AccountHistoryTransferVO transfer;
    private AccountStatusVO          fromAccountStatus;
    private AccountStatusVO          toAccountStatus;
    private String field1;
    private String field2;
    private String field3;
    private String field4;
    private String field5;
    private String field6;
    private String field7;
    private String field8;
    private String field9;
    private String field10;
    

    public PaymentResult_Extended() {
    }

    public PaymentResult_Extended(final PaymentStatus status, final AccountHistoryTransferVO transfer) {
        this.status = status;
        this.transfer = transfer;
    }

    public PaymentResult_Extended(final PaymentStatus status, final AccountHistoryTransferVO transfer, final AccountStatusVO fromAccountStatus, final AccountStatusVO toAccountStatus) {
        this.status = status;
        this.transfer = transfer;
        this.fromAccountStatus = fromAccountStatus;
        this.toAccountStatus = toAccountStatus;
    }

    public AccountStatusVO getFromAccountStatus() {
        return fromAccountStatus;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public AccountStatusVO getToAccountStatus() {
        return toAccountStatus;
    }

    public AccountHistoryTransferVO getTransfer() {
        return transfer;
    }

    public void setFromAccountStatus(final AccountStatusVO fromAccountStatus) {
        this.fromAccountStatus = fromAccountStatus;
    }

    public void setStatus(final PaymentStatus status) {
        this.status = status;
    }

    public void setToAccountStatus(final AccountStatusVO toAccountStatus) {
        this.toAccountStatus = toAccountStatus;
    }

    public void setTransfer(final AccountHistoryTransferVO transfer) {
        this.transfer = transfer;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }

    public String getField6() {
        return field6;
    }

    public void setField6(String field6) {
        this.field6 = field6;
    }

    public String getField7() {
        return field7;
    }

    public void setField7(String field7) {
        this.field7 = field7;
    }

    public String getField8() {
        return field8;
    }

    public void setField8(String field8) {
        this.field8 = field8;
    }

    public String getField9() {
        return field9;
    }

    public void setField9(String field9) {
        this.field9 = field9;
    }

    public String getField10() {
        return field10;
    }

    public void setField10(String field10) {
        this.field10 = field10;
    }
    
}
