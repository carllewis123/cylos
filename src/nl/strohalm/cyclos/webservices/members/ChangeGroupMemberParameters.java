package nl.strohalm.cyclos.webservices.members;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChangeGroupMemberParameters implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long groupId;
	private String changeBy;
	private String principal;
	private String PrincipalType;
	private String description;
	private BigDecimal lowerCreditLimit;
	private BigDecimal upperCreditLimit;
	private BigDecimal dailyCashInLimit;
	private BigDecimal monthlyCashInLimit;
	private BigDecimal dailyCashoutLimit;
	private BigDecimal monthlyCashoutLimit;
	private String status;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getChangeBy() {
		return changeBy;
	}
	public void setChangeBy(String changeBy) {
		this.changeBy = changeBy;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getPrincipalType() {
		return PrincipalType;
	}
	public void setPrincipalType(String principalType) {
		PrincipalType = principalType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String descrition) {
		this.description = descrition;
	}
	public BigDecimal getLowerCreditLimit() {
		return lowerCreditLimit;
	}
	public void setLowerCreditLimit(BigDecimal lowerCreditLimit) {
		this.lowerCreditLimit = lowerCreditLimit;
	}
	public BigDecimal getUpperCreditLimit() {
		return upperCreditLimit;
	}
	public void setUpperCreditLimit(BigDecimal upperCreditLimit) {
		this.upperCreditLimit = upperCreditLimit;
	}
	public BigDecimal getDailyCashInLimit() {
		return dailyCashInLimit;
	}
	public void setDailyCashInLimit(BigDecimal dailyCashInLimit) {
		this.dailyCashInLimit = dailyCashInLimit;
	}
	public BigDecimal getMonthlyCashInLimit() {
		return monthlyCashInLimit;
	}
	public void setMonthlyCashInLimit(BigDecimal monthlyCashInLimit) {
		this.monthlyCashInLimit = monthlyCashInLimit;
	}
	public BigDecimal getDailyCashoutLimit() {
		return dailyCashoutLimit;
	}
	public void setDailyCashoutLimit(BigDecimal dailyCashoutLimit) {
		this.dailyCashoutLimit = dailyCashoutLimit;
	}
	public BigDecimal getMonthlyCashoutLimit() {
		return monthlyCashoutLimit;
	}
	public void setMonthlyCashoutLimit(BigDecimal monthlyCashoutLimit) {
		this.monthlyCashoutLimit = monthlyCashoutLimit;
	}
	
	

}
