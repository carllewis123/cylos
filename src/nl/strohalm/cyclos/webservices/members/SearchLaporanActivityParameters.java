package nl.strohalm.cyclos.webservices.members;

import java.io.Serializable;

public class SearchLaporanActivityParameters implements Serializable {
	
	private static final long serialVersionUID = 2688031869739107872L;
	
	private int memberId;

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	
	
}
