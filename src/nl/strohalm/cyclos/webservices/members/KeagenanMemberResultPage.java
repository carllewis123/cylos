package nl.strohalm.cyclos.webservices.members;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import nl.strohalm.cyclos.webservices.model.KeagenanMemberVO;
import nl.strohalm.cyclos.webservices.model.PendingMemberVO;
import nl.strohalm.cyclos.webservices.model.ResultPage;

public class KeagenanMemberResultPage extends ResultPage<KeagenanMemberVO> {

	public KeagenanMemberResultPage() {}
	
	public KeagenanMemberResultPage(final int currentPage, final int pageSize, final int totalCount, final List<KeagenanMemberVO> members) {
        super(currentPage, pageSize, totalCount, members);
    }
	
	@JsonIgnore
    @XmlElement
    public List<KeagenanMemberVO> getKeagenanMembers() {
        return getElements();
    }

    public void setKeagenanMembers(final List<KeagenanMemberVO> members) {
        setElements(members);
    }
}
