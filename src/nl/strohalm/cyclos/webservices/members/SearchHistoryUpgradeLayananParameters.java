package nl.strohalm.cyclos.webservices.members;

import java.util.Calendar;

import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.utils.StringValuedEnum;
import nl.strohalm.cyclos.webservices.model.SearchParameters;

public class SearchHistoryUpgradeLayananParameters extends SearchParameters {
		/**
	 * 
	 */
	private static final long serialVersionUID = -4184265573255446144L;
	

	
		private Period 		period;
		private String      name;
		private String      username;
		private String 		action;
		private	String 		writer;
		
		public String getWriter() {
			return writer;
		}
		public void setWriter(String writer) {
			this.writer = writer;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		
		public Period getPeriod() {
			return period;
		}
		public void setPeriod(Period period) {
			this.period = period;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		@Override
		public String toString() {
			return "SearchHistoryUpgradeLayananParameters [period=" + period + ", name=" + name + ", username="
					+ username + ", action=" + action + ", writer=" + writer + "]";
		}
		
		
}
