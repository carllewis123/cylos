/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.webservices.members;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import nl.strohalm.cyclos.entities.members.BrokeringQuery;
import nl.strohalm.cyclos.services.elements.AccountActivitiesVO;
import nl.strohalm.cyclos.services.elements.ActivitiesVO;
import nl.strohalm.cyclos.services.elements.MemberServiceLocal;
import nl.strohalm.cyclos.webservices.members.webagent.AccountActivityResult;
import nl.strohalm.cyclos.webservices.members.webagent.AccountOverviewResult;
import nl.strohalm.cyclos.webservices.members.webagent.LaporanActivityResult;
import nl.strohalm.cyclos.webservices.members.webagent.LaporanActivityResult.resultStatus;
import nl.strohalm.cyclos.entities.members.BrokeringQuery;
import nl.strohalm.cyclos.entities.members.brokerings.Brokering;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.services.elements.AccountActivitiesVO;
import nl.strohalm.cyclos.services.elements.ActivitiesVO;
import nl.strohalm.cyclos.services.elements.MemberServiceLocal;
import nl.strohalm.cyclos.webservices.members.webagent.AccountActivityResult;
import nl.strohalm.cyclos.webservices.members.webagent.AccountOverviewResult;
import nl.strohalm.cyclos.webservices.members.webagent.LaporanActivityResult;
import nl.strohalm.cyclos.webservices.members.webagent.LaporanActivityResult.resultStatus;

import javax.jws.WebParam;

import javax.jws.WebService;

import nl.strohalm.cyclos.access.AdminAdminPermission;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.PendingMember;
import nl.strohalm.cyclos.entities.members.RegisteredMember;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.exceptions.MailSendingException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.accounts.CreditLimitDTO;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.BrokeringServiceLocal;
import nl.strohalm.cyclos.services.elements.ChangeBrokerDTO;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
//import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.elements.HistoryUpgradeLayananService;
import nl.strohalm.cyclos.services.elements.HistoryUpgradeLayananServiceLocal;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupServiceLocal;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.validation.ValidationException;
import nl.strohalm.cyclos.webservices.PrincipalParameters;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.model.GroupVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.GroupHelper;
//import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hazelcast.logging.Logger;

import ptdam.emoney.EmoneyConfiguration;

/**
 * Web service implementation
 * @author luis
 */
@WebService(name = "members", serviceName = "members")
public class MemberWebServiceImpl implements MemberWebService {

	private static final Log logger = LogFactory.getLog(MemberWebServiceImpl.class);
    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    
    private GroupServiceLocal             groupServiceLocal;
    private MemberHelper                  memberHelper;
    private GroupHelper                   groupHelper;
    private ChannelHelper                 channelHelper;
    private CustomFieldHelper             customFieldHelper;
    private AccountServiceLocal			  accountServiceLocal;
    private FetchServiceLocal 			  fetchServiceLocal;
    private SmsMailingServiceLocal        smsMailingServiceLocal;
    private BrokeringServiceLocal		  brokeringServiceLocal;
	private HistoryUpgradeLayananServiceLocal historyUpgradeLayananServiceLocal;
    private MemberServiceLocal			  memberServiceLocal;
    
    private String namaAgent;

    @Override
    @SuppressWarnings("unchecked")
    public MemberResultPage fullTextSearch(final FullTextMemberSearchParameters params) {
        if (params == null) {
            return null;
        }

        final List<Member> members = (List<Member>) elementServiceLocal.fullTextSearch(memberHelper.toFullTextQuery(params));
        return memberHelper.toResultPage(members, params.getShowCustomFields(), params.getShowImages());
    }
    
    public void setHistoryUpgradeLayananServiceLocal(
			HistoryUpgradeLayananServiceLocal historyUpgradeLayananServiceLocal) {
		this.historyUpgradeLayananServiceLocal = historyUpgradeLayananServiceLocal;
	}

	@Override
	public HistoryUpgradeLayananResultPage searchHistoryUpgradeLayanan(SearchHistoryUpgradeLayananParameters params) {
		// TODO Auto-generated method stub
		HistoryUpgradeLayananQuery query = new HistoryUpgradeLayananQuery();
		Member member = null;
		if (StringUtils.isNotEmpty(params.getUsername()) || params.getUsername() != null) {
			User user = elementServiceLocal.loadUser(params.getUsername(), null);
			member.setUser(user);
		}
				
		if (StringUtils.isNotEmpty(params.getName()) || params.getName() != null) {
			member.setName(params.getName());
		}
		if (member != null) {
			query.setMember(member);
		}

		Element writer = elementServiceLocal.loadByPrincipal(null, params.getWriter(), null);
		query.setWriter(writer);
		if (params.getAction() != null){
			query.setAction(HistoryUpgradeLayanan.Action.valueOf(params.getAction()));	
		}
		if(params.getPeriod() != null) {
			query.setPeriod(params.getPeriod());
		}
		
		
		System.out.println(writer);
		System.out.println(member);
		List<GroupRemark> listOfHistoryUpgradeLayanan = historyUpgradeLayananServiceLocal
				.searchByAgent(memberHelper.toHistoryUpgradeLayananQuery(params, query));
		return memberHelper.toHistoryUpgradeLayananResultPage(listOfHistoryUpgradeLayanan);
		//return null;
	}
    @Override
    public KeagenanMemberResultPage searchKeagenanMembers(SearchKeagenanParameters params) {
    	BrokeringQuery query = new BrokeringQuery();
    	
    
    	if(StringUtils.isNotEmpty(params.getBrokerUsername()) || params.getBrokerUsername() !=null) {
    		Member broker =elementServiceLocal.loadByPrincipal(null, params.getBrokerUsername());
    		query.setBroker(broker);
    	}
    	
//		if (params.getBrokeredId() != 0) {
//			Member brokered = memberServiceLocal.loadByIdOrPrincipal(Long.valueOf(params.getBrokeredId()), "", "");
//			query.setBrokered(brokered);
//		}
    	
    	if(params.getBrokerUsername().trim().length() > 0) {
    		query.setUsername(params.getBrokeredUsername());
    	}
    	
    	if(params.getBrokeredName().trim().length() > 0) {
    		query.setName(params.getBrokeredName());
    	}
    	
    	if(params.getStartDate() != null) {
    		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
    		Calendar cal = Calendar.getInstance();

    		try {
    			cal.setTime(sdf.parse(params.getStartDate()));
    			query.setStartExpirationDate(cal);
    		} catch(ParseException pe) {
    			logger.error(pe.getMessage());
    		}
    	}
    	if(params.getGroupId().trim().length() > 0) {
    		String[] itemGroups = params.getGroupId().split(",");
    		Collection<Long> groups = new ArrayList<Long>(itemGroups.length);
    		for(int i =0; i < itemGroups.length ; i++) {
    			groups.add(Long.valueOf(itemGroups[i]));
    		}
    		Collection<MemberGroup> groupIds = groupServiceLocal.load(groups, FETCH);
    		query.setGroups(groupIds);
    	}
 
     	List<Brokering> listOfBrokerings = brokeringServiceLocal.search(memberHelper.toKeagenanMemberQuery(params, query));
    	return memberHelper.toKeagenanMemberResultPage(listOfBrokerings);
    }
    
    @Override
    public LaporanActivityResult searchLaporanActivity(SearchLaporanActivityParameters params) {
    	LaporanActivityResult result = new LaporanActivityResult();
    	try {
	    	Member member = (Member) elementServiceLocal.load(Long.valueOf(params.getMemberId()), Element.Relationships.USER, Element.Relationships.GROUP);
	    	ActivitiesVO activities = null;
	    	
			activities = memberServiceLocal.getActivitiesKeagenan(member);
			
			// object to remap the acc activity result from query
			Map<String, AccountActivityResult> accActivity = new HashMap<>();
			
			// iterate over maps reference -> https://stackoverflow.com/questions/46898/how-to-efficiently-iterate-over-each-entry-in-a-map
			Map<String, AccountActivitiesVO> actOrg = activities.getAccountActivities();
			for(Map.Entry<String, AccountActivitiesVO> entry : actOrg.entrySet()) {
				AccountActivityResult tempResult = new AccountActivityResult();
				AccountActivitiesVO voTemp = entry.getValue();
				
				// maps Account data to this class
				AccountOverviewResult overviewTemp = new AccountOverviewResult();
				overviewTemp.setBalance(voTemp.getAccountStatus().getBalance());
				overviewTemp.setCreditLimit(voTemp.getAccountStatus().getCreditLimit());
				overviewTemp.setReservedAmount(voTemp.getAccountStatus().getReservedAmount());
				overviewTemp.setUpperCreditLimit(voTemp.getAccountStatus().getUpperCreditLimit());
				
				// maps the account activity data to new mapper class
				tempResult.setAccOverview(overviewTemp);
				tempResult.setCreditsAllTime(voTemp.getCreditsAllTime());
				tempResult.setDebitsAllTime(voTemp.getDebitsAllTime());
				tempResult.setCreditsLast30Days(voTemp.getCreditsLast30Days());
				tempResult.setDebitsLast30Days(voTemp.getDebitsLast30Days());
				tempResult.setBrokerCommission(voTemp.getBrokerCommission());
				tempResult.setIncomingInvoices(voTemp.getIncomingInvoices());
				tempResult.setRemainingLoans(voTemp.getRemainingLoans());
				tempResult.setTotalFeePercentage(voTemp.getTotalFeePercentage());
				
				accActivity.put(entry.getKey(), tempResult);
			}
			result.setAccActivity(accActivity);
			result.setNumberBrokeredMembers(activities.getNumberBrokeredMembers());
			result.setStatus(resultStatus.PROCESSED);
			
			// format date to yyy-mm-dd
			SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
			result.setSinceActive(sdf.format(activities.getSinceActive().getTime()).toString());
			
    	} catch(EntityNotFoundException enfe) {
    		result.setStatus(resultStatus.NOT_FOUND);
    		enfe.printStackTrace();
    	} catch(Exception e) {
    		result.setStatus(resultStatus.UNKNOWN_ERROR);
    		e.printStackTrace();
    	}
		return result;
    }
    
    public void setMemberServiceLocal(MemberServiceLocal memberServiceLocal) {
		this.memberServiceLocal = memberServiceLocal;
	}
    
    @Override
    @SuppressWarnings("unchecked")
    public List<GroupVO> listManagedGroups() {
        final ServiceClient client = WebServiceContext.getClient();
        final List<GroupVO> groups = new ArrayList<GroupVO>();
        for (final MemberGroup group : client.getManageGroups()) {
            groups.add(groupHelper.toVO(group));
        }
        Collections.sort(groups, new BeanComparator("name"));
        return groups;
    }

    @Override
    public MemberVO load(final long id) {
        Element element;
        try {
            element = elementServiceLocal.load(id, FETCH);
            if (!(element instanceof Member)) {
                throw new Exception();
            }
        } catch (Exception e) {
            return null;
        }
        return memberHelper.toFullVO((Member) element);
    }

    @Override
    public MemberVO loadByPrincipal(final PrincipalParameters params) {
        Member member;
        try {
            final PrincipalType principalType = channelHelper.resolvePrincipalType(params.getPrincipalType());
            member = elementServiceLocal.loadByPrincipal(principalType, params.getPrincipal(), FETCH);
        } catch (Exception e) {
            return null;
        }
        return memberHelper.toFullVO(member);
    }

    @Override
    public MemberVO loadByUsername(final String username) {
        User user;
        try {
            user = elementServiceLocal.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
            if (!(user instanceof MemberUser)) {
                throw new Exception();
            }
        } catch (Exception e) {
            return null;
        }
        return memberHelper.toFullVO(((MemberUser) user).getMember());
    }

    @Override
    public MemberRegistrationResult registerMember(final RegisterMemberParameters params) {
        if (params == null) {
            throw new IllegalArgumentException();
        }
        
        try {
        	if (params.getName() != null) {
			String customFieldCifName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.nama_cif_custom_fieldname");
            
            RegistrationFieldValueVO paymentChannelCustomFieldValue = new RegistrationFieldValueVO(customFieldCifName, params.getName());
            ArrayList<RegistrationFieldValueVO> addCustomValues = null;
            	addCustomValues = new ArrayList<RegistrationFieldValueVO>(params.getFields());
            	addCustomValues.add(paymentChannelCustomFieldValue);
            	params.setFields(addCustomValues);
        	}
        } catch (Exception e) {
			System.out.println("can't insert cif name : "+e.getLocalizedMessage());
		}

        // When the generic 'credentials' is passed in, we need to either set the login password or pin
        final String credentials = params.getCredentials();
        if (StringUtils.isNotEmpty(credentials)) {
            params.setLoginPassword(null);
            params.setPin(null);

            final Channel channel = WebServiceContext.getChannel();
            if (channel != null) {
                switch (channel.getCredentials()) {
                    case DEFAULT:
                    case LOGIN_PASSWORD:
                        params.setLoginPassword(credentials);
                        break;
                    case PIN:
                        params.setPin(credentials);
                        break;
                }
            }
        }

        final Member member = memberHelper.toMember(params);
        member.getUser().setPassword(params.getLoginPassword());
        if (StringUtils.isNotEmpty(params.getPin())) {
            member.getMemberUser().setPin(params.getPin());
        }

        // Register the member
        final RegisteredMember registered = elementServiceLocal.registerMemberByWebService(WebServiceContext.getClient(), member, WebServiceContext.getRequest().getRemoteAddr());
        final MemberRegistrationResult result = new MemberRegistrationResult();
        if (registered instanceof PendingMember) {
            result.setAwaitingEmailValidation(true);
        } else {
            result.setId(registered.getId());
            result.setUsername(registered.getUsername());
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public MemberResultPage search(final MemberSearchParameters params) {
        if (params == null) {
            return null;
        }

        final List<Member> members = (List<Member>) elementServiceLocal.search(memberHelper.toQuery(params));
        return memberHelper.toResultPage(members, params.getShowCustomFields(), params.getShowImages());
    }

    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setGroupHelper(final GroupHelper groupHelper) {
        this.groupHelper = groupHelper;
    }

    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }

    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountServiceLocal = accountService;
    }

    public void setMemberHelper(final MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }
    public void setFetchServiceLocal(final FetchServiceLocal fetchService) {
        this.fetchServiceLocal = fetchService;
    }
    public void setSmsMailingServiceLocal(final SmsMailingServiceLocal smsMailingService) {
        this.smsMailingServiceLocal = smsMailingService;
    }
    public void setBrokeringServiceLocal(final BrokeringServiceLocal brokeringService) {
        this.brokeringServiceLocal = brokeringService;
    }

    @Override
    public void updateMember(final UpdateMemberParameters params) {
        final Long id = params == null || params.getId() == null || params.getId().intValue() <= 0 ? null : params.getId();
        final String principal = params == null || StringUtils.isEmpty(params.getPrincipal()) ? null : params.getPrincipal();
        Member member;
        if (id != null) {
            // Load by id, if passed
            try {
                member = elementServiceLocal.load(id, FETCH);
            } catch (final Exception e) {
                throw new EntityNotFoundException(Member.class);
            }
        } else if (principal != null) {
            // Load by principal, if passed
            final PrincipalType principalType = channelHelper.resolvePrincipalType(params.getPrincipalType());
            member = elementServiceLocal.loadByPrincipal(principalType, params.getPrincipal(), FETCH);
        } else {
            // No identification was passed
            throw new IllegalArgumentException();
        }
        member = (Member) member.clone();
        // Update regular fields
        if (StringUtils.isNotEmpty(params.getName())) {
            member.setName(params.getName());
        }
        if (StringUtils.isNotEmpty(params.getEmail())) {
            member.setEmail(params.getEmail());
        }
        // Merge the custom fields
        List<RegistrationFieldValueVO> fieldValueVOs = params.getFields();
        List<MemberCustomField> allowedFields = customFieldHelper.onlyForGroup(memberCustomFieldServiceLocal.list(), member.getMemberGroup());
        Collection<MemberCustomFieldValue> newFieldValues = customFieldHelper.mergeFieldValues(member, fieldValueVOs, allowedFields);
        member.setOldCustomValues(member.getCustomValues());
        member.setCustomValues(newFieldValues);
        
        member = elementServiceLocal.changeMemberProfileByWebService(WebServiceContext.getClient(), member);
        Long newGroup = 0L;
        try{
        	newGroup = params.getGroupId();
        }catch(NullPointerException npe){
        	newGroup = 0L;
        }
        if(newGroup != 0L){
        	MemberGroup group = groupServiceLocal.load(params.getGroupId());
        	if(!member.getMemberGroup().getName().equals(group.getName())){
	        	String description = params.getField1() != null && params.getField1().trim().length() > 0 ? params.getField1() : "changed by webservice";
	        	elementServiceLocal.changeGroupExtended(member, group, description);
        	}
    	}
        
    }

    	@Override
	public ChangeGroupMemberParameters changeGroup(
			ChangeGroupMemberParameters params) {
		// TODO Auto-generated method stub
		
	    Member member;
		
	        try {
	        	
	        	final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
	            member = elementServiceLocal.loadByPrincipal(principalType, params.getPrincipal(), FETCH);
	             
	        } catch (final Exception e) {
	        	throw new EntityNotFoundException(Member.class);
            }

	        User user = null;
	        if (params.getChangeBy()!=null) {
			
	        	try {
		            user = elementServiceLocal.loadUser(params.getChangeBy(), RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
		     
		        } catch (Exception e) {
					// TODO: handle exception
		        	throw new EntityNotFoundException(Member.class);
				}
	        	
			}
	        
	        Element elementUser = null;
	        if (user!=null) {
				elementUser = user.getElement();
			}

	        final CreditLimitDTO limits = accountServiceLocal.getCreditLimits(member);
            Map<? extends AccountType, BigDecimal> upperLimitPerType = limits.getUpperLimitPerType();
            Map<? extends AccountType, BigDecimal> cashInDailyLimitPerType = limits.getCashInPerDayLimitPerType();
            Map<? extends AccountType, BigDecimal> cashInMonthlyLimitPerType = limits.getCashInPerMonthLimitPerType();
            Map<? extends AccountType, BigDecimal> cashOutDailyLimitPerType = limits.getCashOutPerDayLimitPerType();
            Map<? extends AccountType, BigDecimal> cashOutMonthlyLimitPerType = limits.getTrxPerMonthLimitPerType();
            
            
            
            limits.setUpperLimitPerType(upperLimitPerType);
            limits.setCashInPerDayLimitPerType(cashInDailyLimitPerType);
            limits.setCashInPerMonthLimitPerType(cashInMonthlyLimitPerType);
            limits.setCashOutPerDayLimitPerType(cashOutDailyLimitPerType);
            limits.setTrxPerMonthLimitPerType(cashOutMonthlyLimitPerType);
            
            accountServiceLocal.setCreditLimitExtendedValue(member, limits, params.getLowerCreditLimit(), params.getUpperCreditLimit(), params.getDailyCashInLimit(), params.getMonthlyCashInLimit(), params.getDailyCashoutLimit(), params.getMonthlyCashoutLimit());
            
        	MemberGroup group = groupServiceLocal.load(params.getGroupId());

      		elementServiceLocal.changeGroupExtendedValue(member, group, params.getDescription(),elementUser);
		params.setStatus("SUCCESS");
		return params;
	}

    
    public void setGroupServiceLocal(final GroupServiceLocal groupService) {
        groupServiceLocal = groupService;
    }

    @Override
    public MemberPendingUnregAgentResultPage searchPendingUnregAgent(MemberPendingUnregAgentRequest params) throws EntityNotFoundException, IOException {
	List<Object[]> pendingUnregAgentTemp = (List<Object[]>) elementServiceLocal.searchUnregisterPendingAgent(params);

	return memberHelper.toPendingUnregAgentResultPage(pendingUnregAgentTemp);
	}

	@Override
	public void upgradeMember(UpgradeMemberParameters params) throws NumberFormatException, IOException {
		 final Long id = params == null || params.getId() == null || params.getId().intValue() <= 0 ? null : params.getId();
	     final String principal = params == null || StringUtils.isEmpty(params.getPrincipal()) ? null : params.getPrincipal();
	     Member member;
	     User broker = null;
	     Member agent;
	      
        if (id != null) {
            // Load by id, if passed
            try {
                member = elementServiceLocal.load(id, FETCH);
            } catch (final Exception e) {
                throw new EntityNotFoundException(Member.class);
            }
        } else if (principal != null) {
            // Load by principal, if passed
            final PrincipalType principalType = channelHelper.resolvePrincipalType(params.getPrincipalType());
            member = elementServiceLocal.loadByPrincipal(principalType, params.getPrincipal(), FETCH);
            broker  = elementServiceLocal.loadUser(params.getAgentUsername(), FETCH);
        } else {
            // No identification was passed
            throw new IllegalArgumentException();
        }
        member = (Member) member.clone();
        // Update regular fields
        if (StringUtils.isNotEmpty(params.getName())) {
            member.setName(params.getName());
        }
        if (StringUtils.isNotEmpty(params.getEmail())) {
            member.setEmail(params.getEmail());
        }
        
        // Merge the custom fields
        List<RegistrationFieldValueVO> fieldValueVOs = params.getFields();
        List<MemberCustomField> allowedFields = customFieldHelper.onlyForGroup(memberCustomFieldServiceLocal.list(), member.getMemberGroup());
        Collection<MemberCustomFieldValue> newFieldValues = customFieldHelper.mergeFieldValues(member, fieldValueVOs, allowedFields);
        member.setOldCustomValues(member.getCustomValues());
        member.setCustomValues(newFieldValues);
        
		
		agent = elementServiceLocal.load(broker.getId(), Member.Relationships.CUSTOM_VALUES);
		
    	Long kodeCabangId = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.kodecabangmikro.field.id"));
    	
    	boolean status = false;
    	final Iterator<MemberCustomFieldValue> it = agent.getCustomValues().iterator(); 
    	while (it.hasNext()) {
    		final MemberCustomFieldValue field = it.next();
    		if(field.getField().getId().equals(kodeCabangId)){
    			status = true;
    			if (field.getValue()== null || field.getValue() == ""){
    				throw new IOException();
    			}
    			break;
    		}
    	}
    	if(!status) {
    		throw new IOException();
    	}
    	
	
    	member.setBroker(agent);
    	
    	if (member.isTransient()) {
            throw new ValidationException();
        }
    	
    	 try {
    		 member = elementServiceLocal.changeMemberProfileByWebService(WebServiceContext.getClient(), member);
         } catch (final MailSendingException e) {
        	 throw new IOException();
         }
    	 
    	 MemberGroup newGroup = new MemberGroup();
    	
         List<? extends Group> possibleNewGroups = elementServiceLocal.getPossibleNewGroups(member);
         
        
      

         for (int i = 0; i < possibleNewGroups.size(); i++)
         {
             String newGroupName = possibleNewGroups.get(i).getName();
             String unregPenAgent = groupServiceLocal.load(params.getGroupId()).getName();
             if (newGroupName.equals(unregPenAgent))
             {
                 newGroup = (MemberGroup) possibleNewGroups.get(i);  
                 CreditLimitDTO limits = accountServiceLocal.getCreditLimits(member);
                 Map<? extends AccountType, BigDecimal> upperLimitPerType = limits.getUpperLimitPerType();

                 final Map<AccountType, BigDecimal> newUpperLimitPerType = new HashMap<AccountType, BigDecimal>();
                 if (upperLimitPerType != null) {
                     for (AccountType accountType : upperLimitPerType.keySet()) {
                         final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultUpperCreditLimit();
                         accountType = fetchServiceLocal.fetch(accountType);
                         newUpperLimitPerType.put(accountType, limit);
                     }
                 }

                 Map<? extends AccountType, BigDecimal> trxPerMonthLimitPerType = limits.getTrxPerMonthLimitPerType();
                 final Map<AccountType, BigDecimal> newTrxPerMonthLimitPerType = new HashMap<AccountType, BigDecimal>();
                 if (trxPerMonthLimitPerType != null) {
                     for (AccountType accountType : trxPerMonthLimitPerType.keySet()) {
                         final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultTrxPerMonthLimit();
                         accountType = fetchServiceLocal.fetch(accountType);
                         newTrxPerMonthLimitPerType.put(accountType, limit);
                     }
                 }

                 limits.setTrxPerMonthLimitPerType(newTrxPerMonthLimitPerType);
                 limits.setUpperLimitPerType(newUpperLimitPerType);
                 
                 accountServiceLocal.setCreditLimitExtended(member, limits);
                 break;
             }
         }
         final Element element = elementServiceLocal.load(member.getId());
         final String remarks = EmoneyConfiguration.getEmoneyProperties().getProperty("agent.upgrade.member.comment");
         elementServiceLocal.changeGroupExtended(element, newGroup, remarks);
     	
     	ChangeBrokerDTO dto = new ChangeBrokerDTO();
     	dto.setMember(member);
     	dto.setNewBroker(agent);
     	dto.setSuspendCommission(true);
     	dto.setComments("auto update by agent");
     	brokeringServiceLocal.changeBrokerExtended(dto,agent);
     	      

        sendSMS(member);

	}
	
	 public void sendSMS(Member member){
	        
	        try {
	           final SmsMailing sms = new SmsMailing();
	           final String smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.activation.sms");
	           final int namaAgentLength = Integer.parseInt(EmoneyConfiguration.getEmoneyProperties().getProperty("namaAgent.length"));
	           if(member.getBroker().getName().length() > namaAgentLength) {
	        	   		namaAgent = member.getBroker().getName().substring(0, namaAgentLength);
	           } else {
	        	   		namaAgent = member.getBroker().getName();
	           }
	           String smsFinalTemplate = smsTemplate.replace("{0}", namaAgent);
	           sms.setFree(true);
	           sms.setMember(member);
	           sms.setText(smsFinalTemplate);
	           LoggedUser.init(member.getUser());
	           smsMailingServiceLocal.send(sms);
	           LoggedUser.cleanup();
	       } catch (Exception e) {
	           e.getMessage();
	       }
	   }
	
	
}
