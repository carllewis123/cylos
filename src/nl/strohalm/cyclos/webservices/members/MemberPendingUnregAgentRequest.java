package nl.strohalm.cyclos.webservices.members;

import nl.strohalm.cyclos.webservices.model.SearchParameters;

public class MemberPendingUnregAgentRequest extends SearchParameters{
	String brokerId;

	public String getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	

}
