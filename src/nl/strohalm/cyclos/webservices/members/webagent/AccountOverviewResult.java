package nl.strohalm.cyclos.webservices.members.webagent;

import java.math.BigDecimal;

public class AccountOverviewResult {
	private BigDecimal        balance          = BigDecimal.ZERO;
    private BigDecimal        reservedAmount   = BigDecimal.ZERO;
    private BigDecimal        creditLimit      = BigDecimal.ZERO;
    private BigDecimal        upperCreditLimit = BigDecimal.ZERO;
    
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getReservedAmount() {
		return reservedAmount;
	}
	public void setReservedAmount(BigDecimal reservedAmount) {
		this.reservedAmount = reservedAmount;
	}
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	public BigDecimal getUpperCreditLimit() {
		return upperCreditLimit;
	}
	public void setUpperCreditLimit(BigDecimal upperCreditLimit) {
		this.upperCreditLimit = upperCreditLimit;
	}
	
    
}
