package nl.strohalm.cyclos.webservices.members.webagent;

import java.math.BigDecimal;

import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.services.transactions.TransactionSummaryVO;

public class AccountActivityResult {
	//private AccountStatus accountStatus;
	
	private AccountOverviewResult accOverview;
	private TransactionSummaryVO creditsAllTime;
    private TransactionSummaryVO debitsAllTime;
    private TransactionSummaryVO creditsLast30Days;
    private TransactionSummaryVO debitsLast30Days;
    private TransactionSummaryVO brokerCommission;
    private TransactionSummaryVO incomingInvoices;
    private TransactionSummaryVO remainingLoans;
    private BigDecimal           totalFeePercentage;

    
	public AccountOverviewResult getAccOverview() {
		return accOverview;
	}

	public void setAccOverview(AccountOverviewResult accOverview) {
		this.accOverview = accOverview;
	}

	public BigDecimal getTotalFeePercentage() {
		return totalFeePercentage;
	}

	public void setTotalFeePercentage(BigDecimal totalFeePercentage) {
		this.totalFeePercentage = totalFeePercentage;
	}

	public TransactionSummaryVO getCreditsAllTime() {
		return creditsAllTime;
	}

	public void setCreditsAllTime(TransactionSummaryVO creditsAllTime) {
		this.creditsAllTime = creditsAllTime;
	}

	public TransactionSummaryVO getDebitsAllTime() {
		return debitsAllTime;
	}

	public void setDebitsAllTime(TransactionSummaryVO debitsAllTime) {
		this.debitsAllTime = debitsAllTime;
	}

	public TransactionSummaryVO getCreditsLast30Days() {
		return creditsLast30Days;
	}

	public void setCreditsLast30Days(TransactionSummaryVO creditsLast30Days) {
		this.creditsLast30Days = creditsLast30Days;
	}

	public TransactionSummaryVO getDebitsLast30Days() {
		return debitsLast30Days;
	}

	public void setDebitsLast30Days(TransactionSummaryVO debitsLast30Days) {
		this.debitsLast30Days = debitsLast30Days;
	}

	public TransactionSummaryVO getBrokerCommission() {
		return brokerCommission;
	}

	public void setBrokerCommission(TransactionSummaryVO brokerCommission) {
		this.brokerCommission = brokerCommission;
	}

	public TransactionSummaryVO getIncomingInvoices() {
		return incomingInvoices;
	}

	public void setIncomingInvoices(TransactionSummaryVO incomingInvoices) {
		this.incomingInvoices = incomingInvoices;
	}

	public TransactionSummaryVO getRemainingLoans() {
		return remainingLoans;
	}

	public void setRemainingLoans(TransactionSummaryVO remainingLoans) {
		this.remainingLoans = remainingLoans;
	}
}
