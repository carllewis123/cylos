package nl.strohalm.cyclos.webservices.members.webagent;



import java.util.Calendar;
import java.util.Map;

import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.elements.ActivitiesVO;


public class LaporanActivityResult {
	
	public enum 			resultStatus{
		PROCESSED,
		UNKNOWN_ERROR,
		NOT_FOUND,
	};
	
	//private Member 			member;
	//private ActivitiesVO 	activities;
	private resultStatus						status;
	private Map<String, AccountActivityResult> 	accActivity;
	private String								sinceActive;
	private int									numberBrokeredMembers;
	
	
	
	public String getSinceActive() {
		return sinceActive;
	}
	public void setSinceActive(String sinceActive) {
		this.sinceActive = sinceActive;
	}
	public int getNumberBrokeredMembers() {
		return numberBrokeredMembers;
	}
	public void setNumberBrokeredMembers(int numberBrokeredMembers) {
		this.numberBrokeredMembers = numberBrokeredMembers;
	}
	public resultStatus getStatus() {
		return status;
	}
	public void setStatus(resultStatus status) {
		this.status = status;
	}
	public Map<String, AccountActivityResult> getAccActivity() {
		return accActivity;
	}
	public void setAccActivity(Map<String, AccountActivityResult> accActivity) {
		this.accActivity = accActivity;
	}
	
	
}
