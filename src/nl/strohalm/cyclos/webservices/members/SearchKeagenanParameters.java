package nl.strohalm.cyclos.webservices.members;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;

import javax.xml.bind.annotation.XmlType;

import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.utils.hibernate.HibernateHelper.QueryParameter;
import nl.strohalm.cyclos.utils.query.QueryParameters;
import nl.strohalm.cyclos.webservices.model.SearchParameters;

//@XmlType(name = "searchKeagenanParameters", namespace = "http://members.webservices.cyclos.strohalm.nl/")
public class SearchKeagenanParameters extends SearchParameters {
//public class SearchKeagenanParameters extends QueryParameters {
//public class SearchKeagenanParameters implements Serializable {
	private static final long serialVersionUID = 8412577489504496634L;
	
	private String 						brokerUsername;
	private String 						brokeredName;
	private String 						brokeredUsername;
	private String	 					startDate;
	private String 						groupId;

	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}	
	public String getBrokeredName() {
		return brokeredName;
	}
	public void setBrokeredName(String brokeredName) {
		this.brokeredName = brokeredName;
	}
	public String getBrokerUsername() {
		return brokerUsername;
	}
	public void setBrokerUsername(String brokerUsername) {
		this.brokerUsername = brokerUsername;
	}
	public String getBrokeredUsername() {
		return brokeredUsername;
	}
	public void setBrokeredUsername(String brokeredUsername) {
		this.brokeredUsername = brokeredUsername;
	}	
}
