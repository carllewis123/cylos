package nl.strohalm.cyclos.webservices.members;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

public class UpgradeMemberParameters implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4637906480906264536L;
	private String 						   agentUsername;
    private Long                           id;
    private String                         principalType;
    private String                         principal;
    private String                         name;
    private String                         email;
    private List<RegistrationFieldValueVO> fields;
    private Long						   groupId;
    private String 						   field1;
    private String 						   field2;
    private String 						   field3;
    private String 						   field4;
    private String 						   field5;
	
	public String getAgentUsername() {
		return agentUsername;
	}
	public void setAgentUsername(String agentUsername) {
		this.agentUsername = agentUsername;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPrincipalType() {
		return principalType;
	}
	public void setPrincipalType(String principalType) {
		this.principalType = principalType;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<RegistrationFieldValueVO> getFields() {
		return fields;
	}
	public void setFields(List<RegistrationFieldValueVO> fields) {
		this.fields = fields;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	
	@Override
	public String toString() {
		return "UpgradeMemberParameters [agentUsername=" + agentUsername
				+ ", id=" + id + ", principalType=" + principalType
				+ ", principal=" + principal + ", name=" + name + ", email="
				+ email + ", fields=" + fields + ", groupId=" + groupId
				+ ", field1=" + field1 + ", field2=" + field2 + ", field3="
				+ field3 + ", field4=" + field4 + ", field5=" + field5 + "]";
	}

}
