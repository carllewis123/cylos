package nl.strohalm.cyclos.webservices.members;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import nl.strohalm.cyclos.webservices.model.HistoryUpgradeLayananVO;
import nl.strohalm.cyclos.webservices.model.KeagenanMemberVO;
import nl.strohalm.cyclos.webservices.model.ResultPage;

public class HistoryUpgradeLayananResultPage extends ResultPage<HistoryUpgradeLayananVO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 833333895499240171L;

	public HistoryUpgradeLayananResultPage() {}
	
	public HistoryUpgradeLayananResultPage(final int currentPage, final int pageSize, final int totalCount, final List<HistoryUpgradeLayananVO> historyUpgradeLayanan) {
        super(currentPage, pageSize, totalCount, historyUpgradeLayanan);
	}
	
	@JsonIgnore
    @XmlElement
    public List<HistoryUpgradeLayananVO> getHistoryUpgradeLayanan(){
		return getElements();
	}
	
	public void setHistoryUpgradeLayanan(final List<HistoryUpgradeLayananVO> historyUpgradeLayanan) {
		setElements(historyUpgradeLayanan);
	}
   
}
