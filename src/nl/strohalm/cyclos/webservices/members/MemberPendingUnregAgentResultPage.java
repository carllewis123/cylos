package nl.strohalm.cyclos.webservices.members;

import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import org.codehaus.jackson.annotate.JsonIgnore;
import nl.strohalm.cyclos.webservices.model.PendingMemberVO;
import nl.strohalm.cyclos.webservices.model.ResultPage;


public class MemberPendingUnregAgentResultPage extends ResultPage<PendingMemberVO> {

	private static final long serialVersionUID = -7863816774337808168L;

	public MemberPendingUnregAgentResultPage() {
    }

    public MemberPendingUnregAgentResultPage(final int currentPage, final int pageSize, final int totalCount, final List<PendingMemberVO> elements) {
        super(currentPage, pageSize, totalCount, elements);
    }

    @JsonIgnore
    @XmlElement
    public List<PendingMemberVO> getMembers() {
        return getElements();
    }

    public void setMembers(final List<PendingMemberVO> members) {
        setElements(members);
    }

	
}
