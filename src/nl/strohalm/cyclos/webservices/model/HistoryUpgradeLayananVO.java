package nl.strohalm.cyclos.webservices.model;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlType;

import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan.Nature;
import nl.strohalm.cyclos.entities.members.remarks.BrokerRemark;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.entities.members.remarks.Remark;
import nl.strohalm.cyclos.utils.StringValuedEnum;

@XmlType(name = "historyUpgradeLayananList")
public class HistoryUpgradeLayananVO extends EntityVO{
	
	
	
	
    public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	private static final long serialVersionUID = 3495444657368796399L;
    private Calendar          date;
    private String 			  username;
    private String			  name;
    private String 			  action;
    
  
    public Calendar getDate() {
        return date;
    }
  
    public void setDate(final Calendar date) {
        this.date = date;
    }

    public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	@Override
    public String toString() {
        return getId() + " - Remark for ";
    }


}
