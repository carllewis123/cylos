package nl.strohalm.cyclos.webservices.model;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "keagenanMember")
public class KeagenanMemberVO extends EntityVO {
	private static final long serialVersionUID = -1209821115052866312L;

	private Long userId;
	private String username;
	private String name;
	private Long brokerId;
	private String brokerName;
	private Calendar joinDate;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Long brokerId) {
		this.brokerId = brokerId;
	}

	public String getBrokerName() {
		return brokerName;
	}

	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}

	public Calendar getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Calendar joinDate) {
		this.joinDate = joinDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
