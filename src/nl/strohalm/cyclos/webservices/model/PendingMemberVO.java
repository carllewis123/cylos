package nl.strohalm.cyclos.webservices.model;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "pendingMember")
public class PendingMemberVO extends EntityVO{
	
	private static final long serialVersionUID = 2962376873793292108L;
		private String            name;
	    private String            username;
	    private String            email;
	    private Long              groupId;
	    private Calendar		  groupChangesAt;
	    
	    
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Long getGroupId() {
			return groupId;
		}
		public void setGroupId(Long groupId) {
			this.groupId = groupId;
		}
		public Calendar getGroupChangesAt() {
			return groupChangesAt;
		}
		public void setGroupChangesAt(Calendar groupChangesAt) {
			this.groupChangesAt = groupChangesAt;
		}
		@Override
		public String toString() {
			return "PendingMemberVO [name=" + name + ", username=" + username
					+ ", email=" + email + ", groupId=" + groupId
					+ ", groupChangesAt=" + groupChangesAt + "]";
		}
	    
	    

}
