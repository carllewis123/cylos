package nl.strohalm.cyclos.webservices.utils;

import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.webservices.model.PaymentTransferVO;

public class PaymentTransferHelper {
	
	public PaymentTransferVO toVO(final PaymentTransfer paymentTransfer, String errorCode) {
        final PaymentTransferVO vo = new PaymentTransferVO();
        vo.setErrorCode(errorCode);
        vo.setAmount(paymentTransfer.getAmount());
        vo.setAmountUnique(paymentTransfer.getAmountUnique());
        vo.setCreatedDate(paymentTransfer.getCreatedDate());
        vo.setDescription(paymentTransfer.getDescription());
        vo.setEcashRefNo(paymentTransfer.getEcashRefNo());
        vo.setExpiredDate(paymentTransfer.getExpiredDate());
        vo.setFee(paymentTransfer.getFee());
        vo.setMid(paymentTransfer.getMid());
        vo.setOrderId(paymentTransfer.getOrderId());
        vo.setPaymentTicket(paymentTransfer.getPaymentTicket());
        vo.setPtKey(paymentTransfer.getPtKey());
        vo.setPtNumber(paymentTransfer.getPtNumber());
        vo.setPtNumberUnique(paymentTransfer.getPtNumberUnique());
        vo.setPtType(paymentTransfer.getPtType());
	vo.setTimelimit(Integer.toString(paymentTransfer.getTimelimit()));
	vo.setUniqueNo((Integer.toString(paymentTransfer.getUniqueNo())));
        vo.setUrlCallback(paymentTransfer.getUrlCallback());
        
        
        return vo;
    }

}
