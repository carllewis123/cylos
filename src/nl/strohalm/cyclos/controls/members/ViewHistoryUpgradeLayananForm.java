package nl.strohalm.cyclos.controls.members;

import org.apache.struts.action.ActionForm;

public class ViewHistoryUpgradeLayananForm extends ActionForm {

	private static final long serialVersionUID = -6492144304134566667L;
	private long remarkId;
	private long memberId;
	private String status;
	private String comments;
	
	public long getRemarkId() {
		return remarkId;
	}
	public void setRemarkId(long remarkId) {
		this.remarkId = remarkId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
