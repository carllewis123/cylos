package nl.strohalm.cyclos.controls.members;

import nl.strohalm.cyclos.controls.BaseQueryForm;
import nl.strohalm.cyclos.utils.binding.MapBean;

public class HistoryUpgradeLayananForm extends BaseQueryForm {
	
	private static final long serialVersionUID = -1597521414607050456L;
	private long              memberId;
    private long              adminId;

    public HistoryUpgradeLayananForm() {
    	setQuery("period", new MapBean("begin", "end"));
    }
    
    public long getAdminId() {
        return adminId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setAdminId(final long adminId) {
        this.adminId = adminId;
    }

    public void setMemberId(final long memberId) {
        this.memberId = memberId;
    }

}
