package nl.strohalm.cyclos.controls.members;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.services.accounts.AccountService;
import nl.strohalm.cyclos.services.accounts.CreditLimitDTO;
import nl.strohalm.cyclos.services.elements.BrokeringService;
import nl.strohalm.cyclos.services.elements.ChangeBrokerDTO;
import nl.strohalm.cyclos.services.elements.exceptions.ChangeMemberGroupException;
import nl.strohalm.cyclos.services.fetch.FetchService;
import nl.strohalm.cyclos.services.sms.SmsMailingService;
import nl.strohalm.cyclos.utils.ActionHelper;
import nl.strohalm.cyclos.utils.EntityHelper;
import nl.strohalm.cyclos.utils.ResponseHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.transaction.CurrentTransactionData;
import nl.strohalm.cyclos.utils.validation.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;

import ptdam.emoney.EmoneyConfiguration;

public class ApproveUpgradeLayananAction extends ApproveUpgradeChangeGroupAction {
	
	 private BrokeringService brokeringService;
	 private SmsMailingService smsMailingService;
	 private AccountService accountService;
	 private FetchService fetchService;
	 private final Log logger = LogFactory.getLog(ApproveUpgradeLayananAction.class);
	 
	 @Inject
	 public void setBrokeringService(final BrokeringService brokeringService) {
	    this.brokeringService = brokeringService;
	 }
	 
	 @Inject
	 public void setSmsMailingService(SmsMailingService smsMailingService) {
	    this.smsMailingService = smsMailingService;
	 }
	 
	 @Inject
	 public void setAccountService(final AccountService accountService) {
	    this.accountService = accountService;
	 }
	 
	 @Inject
	 public void setFetchService(final FetchService fetchService) {
	    this.fetchService = fetchService;
	 }
	
	@Override
    protected ActionForward handleSubmit(final ActionContext context) throws Exception {
		try{
			final ChangeMemberGroupForm form = context.getForm();
            String status ="";
            Member member;
            member = elementService.load(form.getElementId());
            MemberGroup newGroup = new MemberGroup();
            List<? extends Group> possibleNewGroups = elementService.getPossibleNewGroups(member);
            String RegisteredAgent = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Registered.Agent.group.name");
            String RegisteredAgentRekhape = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Registered.Agent.Rekhape.group.name");
            
            for (int i = 0; i < possibleNewGroups.size(); i++)
            {
                String newGroupName = possibleNewGroups.get(i).getName();
                if (newGroupName.equals(RegisteredAgent) || newGroupName.equals(RegisteredAgentRekhape))
                {
                	newGroup = (MemberGroup)possibleNewGroups.get(i);
                	status = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.status.accept");
                    CreditLimitDTO limits = accountService.getCreditLimits(member);
                    Map<? extends AccountType, BigDecimal> upperLimitPerType = limits.getUpperLimitPerType();
                    Map<? extends AccountType, BigDecimal> cashInDailyLimitPerType = limits.getCashInPerDayLimitPerType();
                    Map<? extends AccountType, BigDecimal> cashInMonthlyLimitPerType = limits.getCashInPerMonthLimitPerType();
                    Map<? extends AccountType, BigDecimal> cashOutDailyLimitPerType = limits.getCashOutPerDayLimitPerType();
                    Map<? extends AccountType, BigDecimal> cashOutMonthlyLimitPerType = limits.getTrxPerMonthLimitPerType();


                    final Map<AccountType, BigDecimal> newUpperLimitPerType = new HashMap<AccountType, BigDecimal>();
                    final Map<AccountType, BigDecimal> newCashInDailyLimitPerType = new HashMap<AccountType, BigDecimal>();
                    final Map<AccountType, BigDecimal> newCashInMonthlyLimitPerType = new HashMap<AccountType, BigDecimal>();
                    final Map<AccountType, BigDecimal> newCashOutDailyLimitPerType = new HashMap<AccountType, BigDecimal>();
                    final Map<AccountType, BigDecimal> newCashOutMonthlyLimitPerType = new HashMap<AccountType, BigDecimal>();
                    
                    if (upperLimitPerType != null && cashInDailyLimitPerType != null && cashInMonthlyLimitPerType != null & cashOutDailyLimitPerType != null & cashOutMonthlyLimitPerType != null) {
                        for (AccountType accountType : upperLimitPerType.keySet()) {
                            final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultUpperCreditLimit();
                            accountType = fetchService.fetch(accountType);
                            newUpperLimitPerType.put(accountType, limit);
                        }
                        
                        for (AccountType accountType : cashInDailyLimitPerType.keySet()) {
                            final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashInPerDayLimit();
                            accountType = fetchService.fetch(accountType);
                            newCashInDailyLimitPerType.put(accountType, limit);
                        }
                        
                        for (AccountType accountType : cashInMonthlyLimitPerType.keySet()) {
                            final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashInPerMonthLimit();
                            accountType = fetchService.fetch(accountType);
                            newCashInMonthlyLimitPerType.put(accountType, limit);
                    }

                        for (AccountType accountType : cashOutDailyLimitPerType.keySet()) {
                            final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashOutPerDayLimit();
                            accountType = fetchService.fetch(accountType);
                            newCashOutDailyLimitPerType.put(accountType, limit);
                        }
                        
                        for (AccountType accountType : cashOutMonthlyLimitPerType.keySet()) {
                            final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultTrxPerMonthLimit();
                            accountType = fetchService.fetch(accountType);
                            newCashOutMonthlyLimitPerType.put(accountType, limit);
                        }
                        
                    }

                  
                    limits.setUpperLimitPerType(newUpperLimitPerType);
                    limits.setCashInPerDayLimitPerType(newCashInDailyLimitPerType);
                    limits.setCashInPerMonthLimitPerType(newCashInMonthlyLimitPerType);
                    limits.setCashOutPerDayLimitPerType(newCashOutDailyLimitPerType);
                    limits.setTrxPerMonthLimitPerType(newCashOutMonthlyLimitPerType);
                    
                    accountService.setCreditLimitExtended(member, limits);
                	break;
                }
            }
            
            final ActionForward forward = ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", form.getMemberId());
            String key = "upgradelayanan.message.approve";
            
            if (CurrentTransactionData.hasMailError()) {
                key += ".mailError";
            }
            context.sendMessage(key);
            
            sendSMS(member, status);
            return forward;
		}catch(final ChangeMemberGroupException e){
			return context.sendError(e.getErrorKey(), e.getErrorArgument());
		}
	}
	
	@Override
    protected ActionForward handleValidation(final ActionContext context) {
        try {
            validateForm(context);
            final ChangeMemberGroupForm form = context.getForm();
            Long UNREGISTERED_PENDING_AGENT = 0L;
			try {
				UNREGISTERED_PENDING_AGENT = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgent.group.id"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            final Group newGroup = groupService.load(UNREGISTERED_PENDING_AGENT);
            if (newGroup.getStatus() == Group.Status.REMOVED) {
                final Map<String, Object> fields = new HashMap<String, Object>();
                fields.put("confirmationMessage", context.message("changeGroup.confirmRemove", newGroup.getName()));
                responseHelper.writeStatus(context.getResponse(), ResponseHelper.Status.SUCCESS, fields);
            } else {
                responseHelper.writeValidationSuccess(context.getResponse());
            }
        } catch (final ValidationException e) {
            responseHelper.writeValidationErrors(context.getResponse(), e);
        }
        return null;
    }
	
	 @Override
	    protected void prepareForm(final ActionContext context) throws Exception {
	        super.prepareForm(context);
	        final HttpServletRequest request = context.getRequest();
	        final Member member = (Member) request.getAttribute("element");
	        final boolean isActive = member.getActivationDate() != null;
	        request.setAttribute("canRemove", !isActive && member.getGroup().getStatus().isEnabled());
	        request.setAttribute("member", member);
	    }
	    
	    public void sendSMS(Member member, String status){
	        
	        try {
	           final SmsMailing sms = new SmsMailing();
	           String smsTemplate = "";
	           if (status.equals(EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.status.accept"))){
	        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.activation.sms.accpeted");
	           }else{
	        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.activation.sms.refused");
	           }
	           sms.setFree(true);
	           sms.setMember(member);
	           sms.setText(smsTemplate);
//	           LoggedUser.init(member.getUser());
	           smsMailingService.send(sms);
	           LoggedUser.cleanup();
	       } catch (Exception e) {
	           logger.error(e);
	       }
	    }
}
