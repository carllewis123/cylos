/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.controls.members;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.mobile.MobileWebServiceImpl;
import com.ptdam.emoney.webservices.utils.OpenOTPStatus;
import com.ptdam.emoney.webservices.utils.OpenOTPTicketVO;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseAjaxAction;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.ChannelPrincipal;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.AdminGroup;
import nl.strohalm.cyclos.entities.members.AdminQuery;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.access.ChannelService;
import nl.strohalm.cyclos.services.access.ChannelServiceLocal;
import nl.strohalm.cyclos.services.access.exceptions.BlockedCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCredentialsException;
import nl.strohalm.cyclos.services.elements.ElementService;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.elements.MemberService;
import nl.strohalm.cyclos.services.groups.GroupService;
import nl.strohalm.cyclos.services.settings.SettingsService;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.JSONBuilder;
import nl.strohalm.cyclos.utils.binding.BeanCollectionBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.binding.DataBinderHelper;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;
import ptdam.emoney.EmoneyConfiguration;

/**
 * Generate OPEN OTP and returns the result as an JSON
 * @author Adhi Wicaksono
 */
public class AutoSweepAjaxAction extends BaseAjaxAction {

    private final Log                     logger              = LogFactory.getLog(AutoSweepAjaxAction.class);
	
    private DataBinder<?> dataBinder;
    private ChannelHelper                 channelHelper;
    private WebServiceHelper              webServiceHelper;
    private CoreWebServiceFactory         coreWSFactory;
    protected ChannelService   			  channelService;
    protected MemberService   			  memberService;

    private String     CUSTOM_FIELD_MOBILE_PHONE = "mobilePhone";

    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP };

 
    public DataBinder<?> getDataBinder() {
        if (dataBinder == null) {
            dataBinder = BeanCollectionBinder.instance(DataBinderHelper.simpleElementBinder());
        }
        return dataBinder;
    }

    @Inject
    public void setWebServiceHelper(WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
  
    @Inject
    public void setChannelHelper(ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
    
    @Inject
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }
    
    @Inject
    public final void setMemberService(final MemberService memberService) {
        this.memberService = memberService;
    }	
    
    @Override
    protected ContentType contentType() {
        return ContentType.JSON;
    }

    @Override
    protected void renderContent(final ActionContext context) throws Exception {
    	
          final AutoSweepAjaxForm form = context.getForm();
          
    	  final PrincipalType principalType = channelHelper.resolvePrincipalTypeCustom("USER");
          OpenOTPStatus status = null;
          List<FieldValueVO>  fields;
          FieldValueVO fieldVO = null;
          final JSONBuilder json = new JSONBuilder();

          try {
              final Member fromMember = elementService.loadByPrincipal(principalType, form.getUsername(), FETCH);
              

              if (status == null) {
                  
                  fieldVO = new FieldValueVO();
                  fieldVO.setInternalName(CUSTOM_FIELD_MOBILE_PHONE);
                  fieldVO.setValue(form.getUsername());
                  fields = Arrays.asList(fieldVO);
      
                  final MemberVO memberVo = toVO(fromMember);
                  memberVo.setName(fromMember.getName());
                  memberVo.setFields(fields);
                  memberVo.setId(fromMember.getId());
      
                  OpenOTPTicketVO ticket = new OpenOTPTicketVO();
                  ticket.setFromMember(memberVo);
                  ticket.setFromChannel("web");
          
                  // generate non trx otp
                  status = coreWSFactory.getOpenOTPWebService().generateNonTrxOTP((OpenOTPTicketVO) ticket);
                  String message = EmoneyConfiguration.getEmoneyProperties().getProperty("autosweep.otpmessage."+status.toString());
                  json.set("status", status);
                  json.set("message",message);
              }

          } catch (final InvalidCredentialsException e) {
              webServiceHelper.error(e);
              status = OpenOTPStatus.OTP_REQUEST_FAILED_INVALID_CREDENTIALS;

          } catch (final BlockedCredentialsException e) {
              webServiceHelper.error(e);
              status = OpenOTPStatus.OTP_REQUEST_FAILED_BLOCKED_CREDENTIALS;
          } catch (final EntityNotFoundException e) {
              webServiceHelper.error(e);
              logger.info(form.getUsername() + " belum terdaftar di mandiri e-cash.");
              status = OpenOTPStatus.OTP_REQUEST_FAILED_MEMBER_NOT_FOUND;
          } catch (final Exception e) {
              webServiceHelper.error(e);
              logger.error(e, e);
              status = OpenOTPStatus.UNKNOWN_ERROR;
          } finally {
              if (status == null) {
                  logger.error("unhandled error");
                  status = OpenOTPStatus.UNKNOWN_ERROR;
              }
          }
          String message = EmoneyConfiguration.getEmoneyProperties().getProperty("autosweep.otpmessage."+status.toString());
          json.set("status", status.toString());
          json.set("message",message);
          responseHelper.writeJSON(context.getResponse(), json);
    }
    
    private MemberVO toVO(final Member member) {
        return toVO(MemberVO.class, member);
    }
    
    private <VO extends MemberVO> VO toVO(final Class<VO> voType, final Member member) {
        if (member == null) {
            return null;
        }
        VO vo;
        try {
            vo = voType.newInstance();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        vo.setId(member.getId());
        vo.setName(member.getName());
        vo.setUsername(member.getUsername());
        vo.setEmail(member.getEmail());
        vo.setGroupId(member.getGroup().getId());
        final List<FieldValueVO> empty = Collections.emptyList();
        vo.setFields(empty);
     
        return vo;
    }
}
