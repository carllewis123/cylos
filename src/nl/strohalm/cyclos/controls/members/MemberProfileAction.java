package nl.strohalm.cyclos.controls.members;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.access.AdminMemberPermission;
import nl.strohalm.cyclos.access.BrokerPermission;
import nl.strohalm.cyclos.access.MemberPermission;
import nl.strohalm.cyclos.access.Permission;
import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.elements.ProfileAction;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.access.User.Relationships;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField.Access;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.groups.AdminGroup;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.Group.Status;
import nl.strohalm.cyclos.entities.groups.GroupFilter;
import nl.strohalm.cyclos.entities.groups.GroupFilterQuery;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.groups.MemberGroupSettings;
import nl.strohalm.cyclos.entities.members.Administrator;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.Operator;
import nl.strohalm.cyclos.entities.members.PendingEmailChange;
import nl.strohalm.cyclos.entities.members.Reference.Nature;
import nl.strohalm.cyclos.entities.members.remarks.Remark;
import nl.strohalm.cyclos.entities.settings.AccessSettings;
import nl.strohalm.cyclos.entities.settings.AccessSettings.UsernameGeneration;
import nl.strohalm.cyclos.exceptions.MailSendingException;
import nl.strohalm.cyclos.exceptions.PermissionDeniedException;
import nl.strohalm.cyclos.services.access.AccessService;
import nl.strohalm.cyclos.services.access.exceptions.NotConnectedException;
import nl.strohalm.cyclos.services.accounts.AccountService;
import nl.strohalm.cyclos.services.customization.ImageService;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldService;
import nl.strohalm.cyclos.services.elements.ElementService;
import nl.strohalm.cyclos.services.elements.MemberRecordService;
import nl.strohalm.cyclos.services.elements.ReferenceService;
import nl.strohalm.cyclos.services.elements.WhenSaving;
import nl.strohalm.cyclos.services.groups.GroupFilterService;
import nl.strohalm.cyclos.services.groups.GroupService;
import nl.strohalm.cyclos.services.permissions.PermissionService;
import nl.strohalm.cyclos.services.settings.SettingsService;
import nl.strohalm.cyclos.utils.ActionHelper;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.ImageHelper.ImageType;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.binding.BeanBinder;
import nl.strohalm.cyclos.utils.binding.BeanCollectionBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.binding.PropertyBinder;
import nl.strohalm.cyclos.utils.validation.ValidationException;

import org.apache.struts.action.ActionForward;
import org.apache.struts.upload.FormFile;

import ptdam.emoney.EmoneyConfiguration;

public class MemberProfileAction extends ProfileAction<Member>
{
  private static final Relationship[] FETCH = { 
    RelationshipHelper.nested(new Relationship[] { 
    User.Relationships.ELEMENT, Element.Relationships.GROUP }), 
    RelationshipHelper.nested(new Relationship[] { 
    User.Relationships.ELEMENT, Member.Relationships.BROKER }), 
    RelationshipHelper.nested(new Relationship[] { 
    User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES }) };
  private AccountService accountService;
  private MemberCustomFieldService memberCustomFieldService;
  private GroupFilterService groupFilterService;
  private ImageService imageService;
  private MemberRecordService memberRecordService;
  private ReferenceService referenceService;
  private CustomFieldHelper customFieldHelper;

  @Inject
  public void setAccountService(AccountService accountService)
  {
    this.accountService = accountService;
  }

  @Inject
  public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
    this.customFieldHelper = customFieldHelper;
  }

  @Inject
  public void setGroupFilterService(GroupFilterService groupFilterService) {
    this.groupFilterService = groupFilterService;
  }

  @Inject
  public void setImageService(ImageService imageService) {
    this.imageService = imageService;
  }

  @Inject
  public void setMemberCustomFieldService(MemberCustomFieldService memberCustomFieldService) {
    this.memberCustomFieldService = memberCustomFieldService;
  }

  @Inject
  public void setMemberRecordService(MemberRecordService memberRecordService) {
    this.memberRecordService = memberRecordService;
  }

  @Inject
  public void setReferenceService(ReferenceService referenceService) {
    this.referenceService = referenceService;
  }

  protected <CFV extends CustomFieldValue> Class<CFV> getCustomFieldValueClass()
  {
    return (Class<CFV>) MemberCustomFieldValue.class;
  }

  protected Class<Member> getElementClass()
  {
    return Member.class;
  }

  protected <G extends Group> Class<G> getGroupClass()
  {
    return (Class<G>) MemberGroup.class;
  }

  protected <U extends User> Class<U> getUserClass()
  {
    return (Class<U>) MemberUser.class;
  }

  protected ActionForward handleDisplay(ActionContext context)
    throws Exception
  {
    MemberProfileForm form = (MemberProfileForm)context.getForm();
    boolean profileOfBrokered = false;
    boolean myProfile = false;
    boolean profileOfOtherMember = false;
    boolean operatorCanViewReports = false;
    MemberUser memberUser = null;
    HttpServletRequest request = context.getRequest();

    Element loggedElement = context.getElement();

    if ((form.getMemberId() > 0L) && (form.getMemberId() != loggedElement.getId().longValue())) {
      User loaded = this.elementService.loadUser(Long.valueOf(form.getMemberId()), FETCH);
      if ((loaded instanceof MemberUser)) {
        memberUser = (MemberUser)loaded;
        profileOfOtherMember = true;
      }
      if (context.isAdmin())
        try {
          request.setAttribute("isLoggedIn", Boolean.valueOf(this.accessService.isLoggedIn(memberUser)));
        }
        catch (NotConnectedException localNotConnectedException)
        {
        }
      if (context.isOperator()) {
        Operator operator = (Operator)context.getElement();
        if (!memberUser.getMember().equals(operator.getMember()))
        {
          operatorCanViewReports = this.permissionService.hasPermission(new Permission[] { MemberPermission.REPORTS_VIEW });
        }
      }
    }
    if ((memberUser == null) && (context.isMember())) {
      memberUser = (MemberUser)this.elementService.loadUser(context.getUser().getId(), FETCH);
      myProfile = true;
    }
    if (memberUser == null) {
      throw new ValidationException();
    }

    Member member = memberUser.getMember();
    if (!loggedElement.equals(member)) {
      if ((loggedElement instanceof Administrator))
      {
        AdminGroup group = (AdminGroup)this.groupService.load(context.getGroup().getId(), new Relationship[] { AdminGroup.Relationships.MANAGES_GROUPS });
        if (!group.getManagesGroups().contains(member.getGroup()))
          throw new PermissionDeniedException();
      }
      else
      {
        MemberGroup group = (MemberGroup)this.groupService.load(((Member)context.getAccountOwner()).getGroup().getId(), new Relationship[] { MemberGroup.Relationships.CAN_VIEW_PROFILE_OF_GROUPS });
        if (!group.getCanViewProfileOfGroups().contains(member.getGroup()))
        {
          if (!context.isBrokerOf(member)) {
            throw new PermissionDeniedException();
          }
        }
      }
    }

    boolean memberCanAccessExternalChannels = false;
    MemberGroup group = (MemberGroup)this.groupService.load(member.getMemberGroup().getId(), new Relationship[] { MemberGroup.Relationships.CHANNELS });
    for (Channel current : group.getChannels()) {
      if (!"web".equals(current.getInternalName())) {
        memberCanAccessExternalChannels = true;
      }
    }
    request.setAttribute("memberCanAccessExternalChannels", Boolean.valueOf(memberCanAccessExternalChannels));

    Collection referenceNatures = this.referenceService.getNaturesByGroup(member.getMemberGroup());
    boolean hasTransactionFeedbacks = referenceNatures.contains(Nature.TRANSACTION);
    request.setAttribute("hasTransactionFeedbacks", Boolean.valueOf(hasTransactionFeedbacks));

    if (context.isAdmin()) {
      AdminGroup adminGroup = (AdminGroup)context.getGroup();
      adminGroup = (AdminGroup)this.groupService.load(adminGroup.getId(), new Relationship[] { AdminGroup.Relationships.MANAGES_GROUPS });
      if (!adminGroup.getManagesGroups().contains(member.getGroup())) {
        throw new PermissionDeniedException();
      }
    }

    getReadDataBinder(context).writeAsString(form.getMember(), member);

    if (context.isMember()) {
      GroupFilterQuery groupFilterQuery = new GroupFilterQuery();
      groupFilterQuery.setGroup(memberUser.getMember().getMemberGroup());
      Collection<GroupFilter> groupFilters = this.groupFilterService.search(groupFilterQuery);
      if (groupFilters.size() > 0) {
        StringBuilder groupFiltersStr = new StringBuilder();
        for (GroupFilter groupFilter : groupFilters) {
          if (groupFilter.isShowInProfile()) {
            if (!"".equals(groupFiltersStr.toString())) {
              groupFiltersStr.append(", ");
            }
            groupFiltersStr.append(groupFilter.getName());
          }
        }
        if (!"".equals(groupFiltersStr.toString())) {
          request.setAttribute("groupFilters", groupFiltersStr.toString());
        }
      }

    }

    List images = this.imageService.listByOwner(member);
    MemberGroupSettings groupSettings = member.getMemberGroup().getMemberSettings();
    boolean maxImages = groupSettings == null;

    boolean usernameGenerated = this.settingsService.getAccessSettings().getUsernameGeneration() != AccessSettings.UsernameGeneration.NONE;
    boolean editable = myProfile;
    boolean byBroker = false;
    boolean canChangeName = false;
    boolean canChangeUsername = false;
    boolean canChangeEmail = false;
    boolean removed = member.getGroup().getStatus() == Group.Status.REMOVED;
    if (!myProfile) {
      boolean canViewRecords = false;
      if (context.isAdmin())
      {
        editable = this.permissionService.hasPermission(new Permission[] { AdminMemberPermission.MEMBERS_CHANGE_PROFILE });
        canViewRecords = this.permissionService.hasPermission(new Permission[] { AdminMemberPermission.RECORDS_VIEW });
        if (editable);
        canChangeName = this.permissionService.hasPermission(new Permission[] { AdminMemberPermission.MEMBERS_CHANGE_NAME });
        if (editable);
        canChangeEmail = this.permissionService.hasPermission(new Permission[] { AdminMemberPermission.MEMBERS_CHANGE_EMAIL });
        if ((!usernameGenerated) && (editable));
        canChangeUsername = this.permissionService.hasPermission(new Permission[] { AdminMemberPermission.MEMBERS_CHANGE_USERNAME });
      }
      else {
        byBroker = context.isBrokerOf(member);
        if (byBroker) {
          editable = this.permissionService.hasPermission(new Permission[] { BrokerPermission.MEMBERS_CHANGE_PROFILE });
          canViewRecords = this.permissionService.hasPermission(new Permission[] { BrokerPermission.MEMBER_RECORDS_VIEW });
          if (editable);
          canChangeName = this.permissionService.hasPermission(new Permission[] { BrokerPermission.MEMBERS_CHANGE_NAME });
          if (editable);
          canChangeEmail = this.permissionService.hasPermission(new Permission[] { BrokerPermission.MEMBERS_CHANGE_EMAIL });
          if ((!usernameGenerated) && (editable));
          canChangeUsername = this.permissionService.hasPermission(new Permission[] { BrokerPermission.MEMBERS_CHANGE_USERNAME });
        }
      }
      if (canViewRecords)
        request.setAttribute("countByRecordType", this.memberRecordService.countByType(member));
    }
    else {
      canChangeName = this.permissionService.hasPermission(new Permission[] { MemberPermission.PROFILE_CHANGE_NAME });
      canChangeEmail = this.permissionService.hasPermission(new Permission[] { MemberPermission.PROFILE_CHANGE_EMAIL });
      if (!usernameGenerated);
      canChangeUsername = this.permissionService.hasPermission(new Permission[] { MemberPermission.PROFILE_CHANGE_USERNAME });
    }

    Group loggedGroup = context.getGroup();

    List allFields = this.memberCustomFieldService.list();
    List customFields;
    if (removed)
    {
      customFields = allFields;
    }
    else customFields = this.customFieldHelper.onlyForGroup(allFields, member.getMemberGroup());

    Map editableFields = new HashMap();
    for (Iterator it = customFields.iterator(); it.hasNext(); ) {
      MemberCustomField field = (MemberCustomField)it.next();

      String cifHiddenPref = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.hidden");
      MemberCustomField.Access visibility = field.getVisibilityAccess();
      if ((visibility != null) && (!visibility.granted(loggedGroup, myProfile, byBroker, false, false)))
        it.remove();
      else if (field.getInternalName().contains(cifHiddenPref)) {
        it.remove();
      }

      String cif = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.cif_custom_fieldname");
      if (field.getInternalName().equals(cif)) {
        editableFields.put(field, Boolean.valueOf(false));
      } else {
        MemberCustomField.Access update = field.getUpdateAccess();
        editableFields.put(field, Boolean.valueOf((update != null) && (update.granted(loggedGroup, myProfile, byBroker, false, false))));
      }

    }

    boolean hasCardType = false;
    if (member.getMemberGroup().getCardType() != null) {
      hasCardType = true;
    }

    PendingEmailChange pendingEmailChange = null;
    if (editable) {
      pendingEmailChange = this.elementService.getPendingEmailChange(member);
    }
    
    String unregPendAgent = EmoneyConfiguration.getEmoneyProperties().getProperty("agent.activation.newgrup");
    String unregPendAgentRekhape = EmoneyConfiguration.getEmoneyProperties().getProperty("agent.activation.newgrup.rekhape");

    // current logged in group id
    String groupId = String.valueOf(context.getElement().getGroup().getId());
    request.setAttribute("groupId", groupId);
    
    request.setAttribute("member", member);
    request.setAttribute("removed", Boolean.valueOf(member.getGroup().getStatus() == Group.Status.REMOVED));
    request.setAttribute("hasAccounts", Boolean.valueOf(this.accountService.hasAccounts(member)));
    request.setAttribute("disabledLogin", Boolean.valueOf(this.accessService.isLoginBlocked(member.getUser())));
    request.setAttribute("customFields", this.customFieldHelper.buildEntries(customFields, member.getCustomValues()));
    request.setAttribute("editableFields", editableFields);
    request.setAttribute("canChangeName", Boolean.valueOf(canChangeName));
    request.setAttribute("canChangeEmail", Boolean.valueOf(canChangeEmail));
    request.setAttribute("canChangeUsername", Boolean.valueOf(canChangeUsername));
    request.setAttribute("pendingEmailChange", pendingEmailChange);
    request.setAttribute("images", images);
    request.setAttribute("maxImages", Boolean.valueOf(maxImages));
    request.setAttribute("editable", Boolean.valueOf(editable));
    request.setAttribute("byBroker", Boolean.valueOf(byBroker));
    request.setAttribute("myProfile", Boolean.valueOf(myProfile));
    request.setAttribute("profileOfOtherMember", Boolean.valueOf(profileOfOtherMember));
    request.setAttribute("profileOfBrokered", Boolean.valueOf(false));
    request.setAttribute("operatorCanViewReports", Boolean.valueOf(operatorCanViewReports));
    request.setAttribute("hasCardType", Boolean.valueOf(hasCardType));
    request.setAttribute("isUnregPendAgent", Boolean.valueOf(member.getGroup().getName().equals(unregPendAgent)));
    request.setAttribute("isUnregPendAgentRekhape", Boolean.valueOf(member.getGroup().getName().equals(unregPendAgentRekhape)));

    if (editable) {
      return context.getInputForward();
    }
    return context.findForward("view");
  }

  protected ActionForward handleSubmit(ActionContext context)
    throws Exception
  {
    MemberProfileForm form = (MemberProfileForm)context.getForm();

    Member member = resolveMember(context);
    
    Long kodeCabangId = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.kodecabangmikro.field.id"));
    String branchGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.kodecabangmikro.group");
    
    for (final Iterator<MemberCustomFieldValue> it = member.getCustomValues().iterator(); it.hasNext();){
    	final MemberCustomFieldValue field = it.next();
    	if(field.getValue()!=null && field.getValue().length()>0){
    		if(field.getField().getId().equals(kodeCabangId)){
    			try{
		    		User branch = this.elementService.loadUser(field.getValue(), FETCH);
		    		if(branch!=null){
			    		if(!branch.getElement().getGroup().getName().equals(branchGroup)){
			    			context.sendMessage("branchcode.notfound");
			    			return ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", member.getId());
			    		}
		    		}else{
		    			context.sendMessage("branchcode.notfound");
		    			return ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", member.getId());
		    		}
    			}catch(Exception e){
    				context.sendMessage("branchcode.notfound");
	    			return ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", member.getId());
    			}
    		}
    	}
    }
    
    Member currentMember;
    try
    {
      currentMember = (Member)this.elementService.load(member.getId(), new Relationship[] { Member.Relationships.BROKER });
    }
    catch (ClassCastException e)
    {
      throw new ValidationException();
    }
    Member broker = currentMember.getBroker();
    member.setBroker(broker);

    if (member.isTransient()) {
      throw new ValidationException();
    }

    boolean hadPendingEmailChange = this.elementService.getPendingEmailChange(member) != null;
    try {
      member.setOldCustomValues(currentMember.getCustomValues());
      member = (Member)this.elementService.changeProfile(member);
    } catch (MailSendingException e) {
      return context.sendError("profile.error.changeEmailValidationFailed", new Object[0]);
    }
    PendingEmailChange pendingEmailChange = this.elementService.getPendingEmailChange(member);

    FormFile upload = form.getPicture();
    if ((upload != null) && (upload.getFileSize() > 0)) {
      try {
        this.imageService.save(member, form.getPictureCaption(), ImageType.getByContentType(upload.getContentType()), upload.getFileName(), upload.getInputStream());
      } finally {
        upload.destroy();
      }
    }

    if ((!hadPendingEmailChange) && (pendingEmailChange != null))
      context.sendMessage("profile.modified.emailPending", new Object[] { pendingEmailChange.getNewEmail() });
    else {
      context.sendMessage("profile.modified", new Object[0]);
    }

    return ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", member.getId());
  }

  protected DataBinder<Member> initDataBinderForRead(ActionContext context)
  {
    BeanBinder dataBinder = (BeanBinder)super.initDataBinderForRead(context);
    dataBinder.registerBinder("hideEmail", PropertyBinder.instance(Boolean.TYPE, "hideEmail"));
    return dataBinder;
  }

  protected DataBinder<Member> initDataBinderForWrite(ActionContext context)
  {
    BeanBinder dataBinder = (BeanBinder)super.initDataBinderForWrite(context);
    dataBinder.registerBinder("hideEmail", PropertyBinder.instance(Boolean.TYPE, "hideEmail"));

    BeanBinder userBinder = BeanBinder.instance(getUserClass(), "user");
    userBinder.registerBinder("username", PropertyBinder.instance(String.class, "username"));
    dataBinder.registerBinder("user", userBinder);

    BeanCollectionBinder collectionBinder = (BeanCollectionBinder)dataBinder.getMappings().get("customValues");
    BeanBinder elementBinder = (BeanBinder)collectionBinder.getElementBinder();
    elementBinder.registerBinder("hidden", PropertyBinder.instance(Boolean.TYPE, "hidden"));

    return dataBinder;
  }

  protected void validateForm(ActionContext context)
  {
    Member member = resolveMember(context);
    this.elementService.validate(member, WhenSaving.PROFILE, false);
  }

  private Member resolveMember(ActionContext context) {
    MemberProfileForm form = (MemberProfileForm)context.getForm();
    return (Member)getWriteDataBinder(context).readFromString(form.getMember());
  }
}
