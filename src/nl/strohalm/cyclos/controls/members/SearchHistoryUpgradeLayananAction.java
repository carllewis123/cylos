package nl.strohalm.cyclos.controls.members;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.entities.members.Administrator;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.entities.settings.events.LocalSettingsEvent;
import nl.strohalm.cyclos.utils.RequestHelper;
import nl.strohalm.cyclos.utils.binding.BeanBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.binding.DataBinderHelper;
import nl.strohalm.cyclos.utils.binding.PropertyBinder;
import nl.strohalm.cyclos.utils.query.QueryParameters;



public class SearchHistoryUpgradeLayananAction extends BaseHistoryUpgradeLayananSearchAction {
	private DataBinder<HistoryUpgradeLayananQuery> dataBinder;
	
	private DataBinder<HistoryUpgradeLayananQuery> getDataBinder() {
        if (dataBinder == null) {
            final BeanBinder<HistoryUpgradeLayananQuery> binder = BeanBinder.instance(HistoryUpgradeLayananQuery.class);
            final LocalSettings localSettings = settingsService.getLocalSettings();
            binder.registerBinder("period", DataBinderHelper.periodBinder(localSettings, "period"));
            binder.registerBinder("member", PropertyBinder.instance(Member.class, "member"));
            binder.registerBinder("action", PropertyBinder.instance(HistoryUpgradeLayanan.Action.class, "action"));
            binder.registerBinder("pageParameters", DataBinderHelper.pageBinder());
            dataBinder = binder;
        }
        return dataBinder;
    }
	
	 @Override
	    public void onLocalSettingsUpdate(final LocalSettingsEvent event) {
	        super.onLocalSettingsUpdate(event);
	        dataBinder = null;
	    }
	
	@Override
	protected void executeQuery(ActionContext context, QueryParameters queryParameters) {
		final HistoryUpgradeLayananQuery query = (HistoryUpgradeLayananQuery) queryParameters;
		query.setWriter(context.getUser().getElement());
        final List<HistoryUpgradeLayanan> authorizations = historyUpgradeLayananService.searchAuthorizations(query);
        context.getRequest().setAttribute("authorizations", authorizations);
		
	}

	@Override
	protected QueryParameters prepareForm(final ActionContext context) {
		final HistoryUpgradeLayananForm form = context.getForm();
		final HttpServletRequest request = context.getRequest();
		
		final HistoryUpgradeLayananQuery query = getDataBinder().readFromString(form.getQuery());
        
		if (query.getMember() != null) {
            final Member member = elementService.load(query.getMember().getId(), Element.Relationships.USER);
            query.setMember(member);
        }
        
        RequestHelper.storeEnum(request, HistoryUpgradeLayanan.Action.class, "actions");

        
		return query;
		
	}
	
	@Override
    protected boolean willExecuteQuery(final ActionContext context, final QueryParameters queryParameters) throws Exception {
        return true;
    }

	
}
