package nl.strohalm.cyclos.controls.members;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.services.elements.BrokeringService;
import nl.strohalm.cyclos.services.elements.ChangeBrokerDTO;
import nl.strohalm.cyclos.services.elements.exceptions.ChangeMemberGroupException;
import nl.strohalm.cyclos.services.sms.SmsMailingService;
import nl.strohalm.cyclos.utils.ActionHelper;
import nl.strohalm.cyclos.utils.ResponseHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.transaction.CurrentTransactionData;
import nl.strohalm.cyclos.utils.validation.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;

import ptdam.emoney.EmoneyConfiguration;

public class RejectUpgradeLayananAction extends RejectUpgradeChangeGroupAction {
	
	 private BrokeringService brokeringService;
	 private SmsMailingService smsMailingService;
	 private final Log logger = LogFactory.getLog(RejectUpgradeLayananAction.class);
	 
	 @Inject
	 public void setBrokeringService(final BrokeringService brokeringService) {
	    this.brokeringService = brokeringService;
	 }
	 
	 @Inject
	    public void setSmsMailingService(SmsMailingService smsMailingService) {
	        this.smsMailingService = smsMailingService;
	    }
	
	@Override
    protected ActionForward handleSubmit(final ActionContext context) throws Exception {
		try{
			final ChangeMemberGroupForm form = context.getForm();
            String status ="";
            Member member;
            member = elementService.load(form.getElementId());
            
            status = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.status.refuse");
        	ChangeBrokerDTO dto = new ChangeBrokerDTO();
        	dto.setMember(member);
        	dto.setSuspendCommission(true);
        	dto.setComments("updated by admin");
        	brokeringService.doRemoveBroker(dto);
            
            final ActionForward forward = ActionHelper.redirectWithParam(context.getRequest(), super.handleSubmit(context), "memberId", form.getMemberId());
            String key = "upgradelayanan.message.refuse";
            
            if (CurrentTransactionData.hasMailError()) {
                key += ".mailError";
            }
            context.sendMessage(key);
            
            sendSMS(member, status, form.getComments());
            return forward;
		}catch(final ChangeMemberGroupException e){
			return context.sendError(e.getErrorKey(), e.getErrorArgument());
		}
	}
	
	@Override
    protected ActionForward handleValidation(final ActionContext context) {
        try {
            validateForm(context);
            final ChangeMemberGroupForm form = context.getForm();
            Long UNREGISTERED_PENDING_AGENT = 0L;
			try {
				UNREGISTERED_PENDING_AGENT = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgent.group.id"));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            final Group newGroup = groupService.load(UNREGISTERED_PENDING_AGENT);
            if (newGroup.getStatus() == Group.Status.REMOVED) {
                final Map<String, Object> fields = new HashMap<String, Object>();
                fields.put("confirmationMessage", context.message("changeGroup.confirmRemove", newGroup.getName()));
                responseHelper.writeStatus(context.getResponse(), ResponseHelper.Status.SUCCESS, fields);
            } else {
                responseHelper.writeValidationSuccess(context.getResponse());
            }
        } catch (final ValidationException e) {
            responseHelper.writeValidationErrors(context.getResponse(), e);
        }
        return null;
    }
	
	 @Override
	    protected void prepareForm(final ActionContext context) throws Exception {
	        super.prepareForm(context);
	        final HttpServletRequest request = context.getRequest();
	        final Member member = (Member) request.getAttribute("element");
	        final boolean isActive = member.getActivationDate() != null;
	        request.setAttribute("canRemove", !isActive && member.getGroup().getStatus().isEnabled());
	        request.setAttribute("member", member);
	    }
	    
	    public void sendSMS(Member member, String status, String Comments){
	        
	        try {
	           final SmsMailing sms = new SmsMailing();
	           String smsTemplate = "";
	           String smsFinalTemplate = "";
	           if (status.equals(EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.status.accept"))){
	        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.activation.sms.accpeted");
	           }else{
	        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("approvalUpgradeLayanan.activation.sms.refused");
	        	   smsFinalTemplate = smsTemplate.replace("{0}", Comments);
	           }
	           sms.setFree(true);
	           sms.setMember(member);
	           sms.setText(smsFinalTemplate);
//	           LoggedUser.init(member.getUser());
	           smsMailingService.send(sms);
	           LoggedUser.cleanup();
	       } catch (Exception e) {
	           logger.error(e);
	       }
	    }
}
