package nl.strohalm.cyclos.controls.members;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import ptdam.emoney.EmoneyConfiguration;
import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseFormAction;
import nl.strohalm.cyclos.controls.elements.ChangeElementGroupForm;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.services.elements.RemarkService;
import nl.strohalm.cyclos.utils.EntityHelper;
import nl.strohalm.cyclos.utils.validation.RequiredError;
import nl.strohalm.cyclos.utils.validation.ValidationError;
import nl.strohalm.cyclos.utils.validation.ValidationException;

public class RejectUpgradeChangeGroupAction extends BaseFormAction {
	private RemarkService remarkService;

    public RemarkService getRemarkService() {
        return remarkService;
    }

    @Inject
    public void setRemarkService(final RemarkService remarkService) {
        this.remarkService = remarkService;
    }

    @Override
    protected void formAction(final ActionContext context) throws Exception {
        final ChangeElementGroupForm form = context.getForm();
        final String comments = form.getComments();
        final Long UNREGISTERED = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.Unregistered.group.id"));
        final Long UNREGISTERED_PENDING_AGENT = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgent.group.id"));
        final Long UNREGISTERED_REKHAPE = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredRekhape.group.id"));
        final Long UNREGISTERED_PENDING_AGENT_REKHAPE = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.UnregisteredPendingAgentRekhape.group.id"));
        
        final Element element = elementService.load(form.getElementId());
        Group newGroup = null;
        if(element.getGroup().getId().equals(UNREGISTERED_PENDING_AGENT)){
        	newGroup = EntityHelper.reference(Group.class, UNREGISTERED);
       	}else if(element.getGroup().getId().equals(UNREGISTERED_PENDING_AGENT_REKHAPE)){
        	newGroup = EntityHelper.reference(Group.class, UNREGISTERED_REKHAPE);
        }
        elementService.changeGroup(element, newGroup, comments);
    }
    
    @Override
    protected void validateForm(final ActionContext context) {
        final ChangeElementGroupForm form = context.getForm();
        final ValidationException val = new ValidationException();
        val.setPropertyKey("comments", "remark.comments");
        if (StringUtils.isEmpty(form.getComments())) {
            val.addPropertyError("comments", new RequiredError());
        }
        val.throwIfHasErrors();
    }
    
    @Override
    protected void prepareForm(final ActionContext context) throws Exception {
    	final HttpServletRequest request = context.getRequest();
        final ChangeElementGroupForm form = context.getForm();
        Element element = null;
        try {
            element = elementService.load(form.getElementId(), Element.Relationships.GROUP);
            final Element loggedElement = context.getElement();
            if (loggedElement.equals(element)) {
                throw new Exception();
            }
        } catch (final Exception e) {
            element = null;
        }
        if (element == null) {
            throw new ValidationException();
        }
        
        request.setAttribute("element", element);
    }
    
    
}
