package nl.strohalm.cyclos.controls.members;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseFormAction;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.remarks.Remark;
import nl.strohalm.cyclos.services.elements.MemberService;
import nl.strohalm.cyclos.services.elements.RemarkService;

public class ViewHistoryUpgradeLayananAction extends BaseFormAction{
	public static final Relationship[]     FETCH = {Remark.Relationships.SUBJECT};
	private MemberService memberService;
	private RemarkService remarkService;
	
	public MemberService getMemberService() {
		return memberService;
	}

	@Inject
	public final void setMemberService(final MemberService memberService) {
		this.memberService = memberService;
	}
	
	@Inject
	public final void setRemarkService(final RemarkService remarkService) {
		this.remarkService = remarkService;
	}
	
	@Override
    protected void prepareForm(final ActionContext context) throws Exception {
        final HttpServletRequest request = context.getRequest();
        
        final ViewHistoryUpgradeLayananForm form = context.getForm();
        
        final Remark remark = remarkService.load(form.getRemarkId(),FETCH);
        
        form.setComments(remark.getComments());
        
        
        Long memberId = remark.getSubject().getId();
        final Member member = elementService.load(memberId, FETCH);
        
        final Collection<MemberCustomFieldValue> customValues = ((Member) elementService.load(memberId, Member.Relationships.CUSTOM_VALUES)).getCustomValues();
        request.setAttribute("remark", remark);
        request.setAttribute("member", member);
        request.setAttribute("customFields", customValues);
	}
	
}
