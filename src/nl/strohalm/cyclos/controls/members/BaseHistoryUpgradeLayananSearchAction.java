package nl.strohalm.cyclos.controls.members;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.BaseQueryAction;
import nl.strohalm.cyclos.services.elements.HistoryUpgradeLayananService;

public abstract class BaseHistoryUpgradeLayananSearchAction extends BaseQueryAction {
	
	protected HistoryUpgradeLayananService historyUpgradeLayananService;
	
	public BaseHistoryUpgradeLayananSearchAction(){
		super();
	}
	 
	 @Inject
	public void setHistoryUpgradeLayananService(final HistoryUpgradeLayananService historyUpgradeLayananService) {
		this.historyUpgradeLayananService = historyUpgradeLayananService;
	}
	
	

}
