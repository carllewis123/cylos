/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.controls.payments.request;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceException;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseFormAction;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.Currency;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentRequestTicket;
import nl.strohalm.cyclos.entities.accounts.transactions.TicketQuery;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.exceptions.UnexpectedEntityException;
import nl.strohalm.cyclos.entities.groups.Group.Status;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.entities.settings.events.LocalSettingsChangeListener;
import nl.strohalm.cyclos.entities.settings.events.LocalSettingsEvent;
import nl.strohalm.cyclos.services.access.ChannelService;
import nl.strohalm.cyclos.services.accounts.CurrencyService;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldService;
import nl.strohalm.cyclos.services.transactions.TicketService;
import nl.strohalm.cyclos.services.transactions.exceptions.AuthorizedPaymentInPastException;
import nl.strohalm.cyclos.services.transactions.exceptions.CreditsException;
import nl.strohalm.cyclos.services.transactions.exceptions.InvalidChannelException;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.RequestHelper;
import nl.strohalm.cyclos.utils.binding.BeanBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.binding.PropertyBinder;
import nl.strohalm.cyclos.utils.validation.ValidationException;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.ecomm.service.ws.ECommConfirmRequest;
import com.ptdam.emoney.ecomm.service.ws.EcommConfirmResponse;
import com.ptdam.emoney.ecomm.webservices.transaction.EcommConfirmationWebService;
import com.ptdam.emoney.ecomm.webservices.transaction.EcommResponse;

/**
 * Action used by a member to request a payment to be confirmed by other channel
 * @author luis
 */
public class AgentTarikTunaiAction extends BaseFormAction implements LocalSettingsChangeListener {
    final Log                                logger              = LogFactory.getLog(AgentTarikTunaiAction.class);
    private ChannelService                   channelService;
    private CurrencyService                  currencyService;
    private TicketService                    ticketService;
    private DataBinder<PaymentRequestTicket> dataBinder;
    private ReadWriteLock                    lock                = new ReentrantReadWriteLock(true);
    private String                           urlEcommConfirmAdapter;
    private EcommConfirmationWebService      eCommConfirmationWebService;
    private MemberCustomFieldService         memberCustomFieldService;
    private static final boolean             NOT_MERCHANT_MEMBER = false;
    private static final String              MOBILE_PHONE        = "mobilePhone";

    @Override
    public void onLocalSettingsUpdate(final LocalSettingsEvent event) {
        try {
            lock.writeLock().lock();
            dataBinder = null;
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Inject
    public void setChannelService(final ChannelService channelService) {
        this.channelService = channelService;
    }

    @Inject
    public void setCurrencyService(final CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @Inject
    public void setTicketService(final TicketService ticketService) {
        this.ticketService = ticketService;
        // FIXME: inelegant
        try {
            urlEcommConfirmAdapter = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.agent.urlConfirmUsingEcomm");

        } catch (IOException e) {
            logger.error(e);

        }
    }

    @Override
    protected void formAction(final ActionContext context) throws Exception {
        final RequestPaymentForm form = context.getForm();
        // if contain otp bypass normal process
        if (form.getTicket("otp") == null) {
            PaymentRequestTicket ticket = resolveTicket(context);
            try {
                ticket = ticketService.generate(ticket);
                context.sendMessage("paymentRequest.sent", ticket.getFrom().getName());
            } catch (final CreditsException e) {
                throw new ValidationException(actionHelper.resolveErrorKey(e), actionHelper.resolveParameters(e));
            } catch (final InvalidChannelException e) {
                throw new ValidationException("paymentRequest.error.invalidChannel", e.getUsername(), e.getChannelName());
            } catch (final UnexpectedEntityException e) {
                throw new ValidationException("payment.error.invalidTransferType");
            } catch (final AuthorizedPaymentInPastException e) {
                throw new ValidationException("payment.error.authorizedInPast");
            }
        } else {
            //PaymentRequestTicket ticket = resolveTicket(context);
            
            ECommConfirmRequest request = new ECommConfirmRequest();
            request.setMobileNo(form.getTicket("otpfrom").toString());
            request.setOtp(form.getTicket("otp").toString());

            EcommConfirmResponse response = confirm(request);
            
            PaymentRequestTicket ticket = resolveTicket(context);
            

            if ((response.getRespCode()).equalsIgnoreCase("PROCESSED")){
                context.sendMessage("otp.confirmation.succes", request.getMobileNo());
            }
            throw new ValidationException("otp.confirmation."+response.getRespCode()); 

        }
    }

    private EcommConfirmResponse confirm(ECommConfirmRequest ecConfirmReq) {
        EcommConfirmResponse eCommConfirmResponse = new EcommConfirmResponse();
        try {

            com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest eConfirmRequest = new com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest();
            // //FIXME: lazy approach pass trough?
            BeanUtils.copyProperties(eConfirmRequest, ecConfirmReq);

            try {
                String mobilePhone = userToPhone(eConfirmRequest.getMobileNo(), NOT_MERCHANT_MEMBER);
                eConfirmRequest.setMobileNo(mobilePhone);

            } catch (Exception e) {
                e.printStackTrace();
                eCommConfirmResponse.setRespCode("2002");
                eCommConfirmResponse.setRespMsg("ACCES_DENIED");
                return eCommConfirmResponse;
            }

            EcommResponse payResponse = getEcommConfirmWebService().confirmPayment(eConfirmRequest);

            BeanUtils.copyProperties(eCommConfirmResponse, payResponse);

        } catch (WebServiceException e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2998");
            eCommConfirmResponse.setRespMsg("CONNECTION_ERROR");
        } catch (Exception e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2999");
            eCommConfirmResponse.setRespMsg("UNKOWN_ERROR");
        }
        return eCommConfirmResponse;
    }

    private EcommConfirmationWebService getEcommConfirmWebService() {

        if (eCommConfirmationWebService == null) {
            try {
                eCommConfirmationWebService = ExternalWebServiceHelper.proxyFor(EcommConfirmationWebService.class, urlEcommConfirmAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return eCommConfirmationWebService;
    }

    /**
     * @param eTrxRequest
     * @throws Exception
     */
    private String userToPhone(String username, boolean isMerchant) throws Exception {
        User user;

        user = elementService.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception();
        }

        Member member = ((MemberUser) user).getMember();
        if (!member.isActive() || member.getGroup().getStatus() == Status.REMOVED) {
            throw new EntityNotFoundException();
        }
        List<MemberCustomField> fields = memberCustomFieldService.list();

        // get field id mobile
        int size = fields.size();
        String iNameMobile = null;
        for (int i = 0; i < size; i++) {
            if (MOBILE_PHONE.equals(fields.get(i).getInternalName())) {
                iNameMobile = fields.get(i).getName();
                break;
            }
        }

        // //get field id merchant
        // String iNameMerchant=null;
        // for (int i = 0; i < size; i++) {
        // if (ECOMMMERCHANT.equals(fields.get(i).getInternalName())){
        // iNameMerchant=fields.get(i).getName();
        // break;
        // }
        // }

        final Collection<MemberCustomFieldValue> customValues = ((Member) elementService.load(member.getId(), Member.Relationships.CUSTOM_VALUES)).getCustomValues();

        List list = new ArrayList<MemberCustomFieldValue>(customValues);

        size = list.size();
        MemberCustomFieldValue temp = null;
        String test = null;
        String mobilPhone = null;
        boolean isFlaggedMerchant = false;
        for (int i = 0; i < size; i++) {
            try {
                temp = (MemberCustomFieldValue) list.get(i);
                test = temp.toString();
                // Retrieve mobile phone
                if (test != null && iNameMobile != null && test.contains(iNameMobile)) {
                    mobilPhone = temp.getValue();
                }

                // //Retrieve merchant flag
                // if (test != null && iNameMerchant!=null && test.contains(iNameMerchant)) {
                // isFlaggedMerchant = "true".equals(temp.getStringValue());
                // }

            } catch (Exception e) {
                logger.debug(e, e);
            }
        }

        // if (isMerchant!=isFlaggedMerchant){
        // throw new Exception("Username:"+username+" is not Merchant member");
        // }
        return (mobilPhone);

    }

    @Override
    protected void prepareForm(final ActionContext context) throws Exception {
        final HttpServletRequest request = context.getRequest();
        final Member member = (Member) context.getAccountOwner();
        request.setAttribute("toMember", member);
        final MemberGroup group = groupService.load(member.getMemberGroup().getId(), MemberGroup.Relationships.REQUEST_PAYMENT_BY_CHANNELS);

        // Get the possible currencies
        final List<Currency> currencies = currencyService.listByMemberGroup(group);
        if (currencies.isEmpty()) {
            throw new ValidationException("payment.error.noTransferType");
        }
        if (currencies.size() == 1) {
            request.setAttribute("singleCurrency", currencies.iterator().next());
        }
        request.setAttribute("currencies", currencies);

        // Get the possible channels
        final Collection<Channel> channels = new ArrayList<Channel>(group.getRequestPaymentByChannels());
        for (final Iterator<Channel> it = channels.iterator(); it.hasNext();) {
            if (!it.next().isPaymentRequestSupported()) {
                it.remove();
            }
        }
        if (channels.isEmpty()) {
            throw new ValidationException("paymentRequest.error.noChannels");
        }
        if (channels.size() == 1) {
            request.setAttribute("singleChannel", channels.iterator().next());
        }
        request.setAttribute("channels", channels);

        RequestHelper.storeEnum(request, TicketQuery.GroupedStatus.class, "status");
    }

    @Override
    protected void validateForm(final ActionContext context) {
        final RequestPaymentForm form = context.getForm();
        // if contain otp bypass validation
        if (form.getTicket("otp") == null) {
            final PaymentRequestTicket ticket = resolveTicket(context);
            ticketService.validate(ticket);

        }
    }

    private DataBinder<PaymentRequestTicket> getDataBinder() {
        try {
            lock.readLock().lock();
            if (dataBinder == null) {
                final LocalSettings localSettings = settingsService.getLocalSettings();
                final BeanBinder<PaymentRequestTicket> binder = BeanBinder.instance(PaymentRequestTicket.class);
                binder.registerBinder("from", PropertyBinder.instance(Member.class, "from"));
                binder.registerBinder("amount", PropertyBinder.instance(BigDecimal.class, "amount", localSettings.getNumberConverter()));
                binder.registerBinder("currency", PropertyBinder.instance(Currency.class, "currency"));
                binder.registerBinder("toChannel", PropertyBinder.instance(Channel.class, "toChannel"));
                binder.registerBinder("description", PropertyBinder.instance(String.class, "description"));
                dataBinder = binder;
            }
            return dataBinder;
        } finally {
            lock.readLock().unlock();
        }
    }

    private PaymentRequestTicket resolveTicket(final ActionContext context) {
        final RequestPaymentForm form = context.getForm();
        final PaymentRequestTicket ticket = getDataBinder().readFromString(form.getTicket());
        ticket.setTo((Member) context.getAccountOwner());
        ticket.setFromChannel(channelService.loadByInternalName(Channel.WEB));
        return ticket;
    }

    @Inject
    public void setMemberCustomFieldService(MemberCustomFieldService memberCustomFieldService) {
        this.memberCustomFieldService = memberCustomFieldService;
    }

}
