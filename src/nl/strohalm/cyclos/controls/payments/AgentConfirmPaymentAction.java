/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package nl.strohalm.cyclos.controls.payments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceException;

import nl.strohalm.cyclos.access.OperatorPermission;
import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.payments.SchedulingType;
import nl.strohalm.cyclos.controls.posweb.BasePosWebPaymentAction;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.Channel.Principal;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group.Status;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.services.access.ChannelService;
import nl.strohalm.cyclos.services.access.exceptions.BlockedCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCardException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidUserForChannelException;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldService;
import nl.strohalm.cyclos.services.elements.ElementService;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.ProjectionDTO;
import nl.strohalm.cyclos.services.transactions.ScheduledPaymentDTO;
import nl.strohalm.cyclos.services.transactions.TransactionContext;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.RequestHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.binding.BeanBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.conversion.CoercionHelper;
import nl.strohalm.cyclos.utils.validation.InvalidError;
import nl.strohalm.cyclos.utils.validation.PositiveNonZeroValidation;
import nl.strohalm.cyclos.utils.validation.RequiredError;
import nl.strohalm.cyclos.utils.validation.TodayValidation;
import nl.strohalm.cyclos.utils.validation.ValidationError;
import nl.strohalm.cyclos.utils.validation.ValidationException;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.ecomm.service.ws.ECommConfirmRequest;
import com.ptdam.emoney.ecomm.service.ws.EcommConfirmResponse;
import com.ptdam.emoney.ecomm.webservices.transaction.EcommConfirmationWebService;
import com.ptdam.emoney.ecomm.webservices.transaction.EcommResponse;

/**
 * Action for the posweb member to receive a OTP payment
 * @author luis
 */

public class AgentConfirmPaymentAction extends BasePosWebPaymentAction {
    final Log                           logger       = LogFactory.getLog(AgentConfirmPaymentAction.class);

    private static final String         MOBILE_PHONE = "mobilePhone";
    private String                      urlEcommConfirmAdapter;
    private ChannelService              channelService;
    private MemberCustomFieldService    memberCustomFieldService;
    private EcommConfirmationWebService eCommConfirmationWebService;

    @Inject
    public void setChannelService(final ChannelService channelService) {
        this.channelService = channelService;
    }

    @Override
    protected Payment doPayment(final ActionContext context, final DoPaymentDTO dto) {
        final Member from;
        try {
            // Load the member that is making the payment
            from = elementService.load(((Member) dto.getFrom()).getId(), Element.Relationships.USER);
            if (from == null) {
                throw new Exception();
            }
        } catch (final Exception e) {
            throw new ValidationException();
        }

        // run as the member, to ensure the model layer will test permissions as that member
        return LoggedUser.runAs(from.getUser(), new Callable<Payment>() {
            @Override
            public Payment call() throws Exception {
                // Check if the member has access to the "posweb" channel;
//                if (!accessService.isChannelEnabledForMember(Channel.POSWEB, from)) {
//                    throw new InvalidUserForChannelException(from.getUsername());
//                }

                // Perform the payment itself
                return confirm(context);
            }
        });
    }

    @Override
    protected OperatorPermission getPermission() {
        return OperatorPermission.PAYMENTS_POSWEB_RECEIVE_PAYMENT;
    }

    @Override
    protected DataBinder<DoPaymentDTO> initDataBinder() {
        final BeanBinder<DoPaymentDTO> binder = (BeanBinder<DoPaymentDTO>) super.initDataBinder();
        // As we will handle from member by principal, remove from the regular binder
        binder.getMappings().remove("from");
        return binder;
    }

    @Override
    protected void prepareForm(final ActionContext context) throws Exception {
        super.prepareForm(context);
        final AgentConfirmPaymentForm form = context.getForm();
        final HttpServletRequest request = context.getRequest();
        final Channel channel = posWebChannel();
        // final Credentials credentials = channel.getCredentials();
        boolean numericCredentials = false;
        boolean uppercasedCredentials = false;
        // switch (credentials) {
        // case PIN:
        // case CARD_SECURITY_CODE:
        // numericCredentials = true;
        // break;
        // case LOGIN_PASSWORD:
        // numericCredentials = settingsService.getAccessSettings().isNumericPassword();
        // break;
        // case TRANSACTION_PASSWORD:
        // numericCredentials = StringUtils.containsOnly(settingsService.getAccessSettings().getTransactionPasswordChars(), "0123456789");
        // uppercasedCredentials = true;
        // break;
        // }
        final PrincipalType selectedPrincipalType = channelService.resolvePrincipalType(channel.getInternalName(), form.getPrincipalType());
        form.setPrincipalType(selectedPrincipalType.toString());
        final Map<String, PrincipalType> principalTypes = new TreeMap<String, PrincipalType>();
        for (final PrincipalType principalType : channel.getPrincipalTypes()) {
            principalTypes.put(principalTypeLabel(principalType), principalType);
        }
        request.setAttribute("principalTypes", principalTypes);
        request.setAttribute("selectedPrincipalType", selectedPrincipalType);
        request.setAttribute("selectedPrincipalLabel", principalTypeLabel(selectedPrincipalType));
        // request.setAttribute("credentials", credentials);
        request.setAttribute("numericCredentials", numericCredentials);
        request.setAttribute("uppercasedCredentials", uppercasedCredentials);
        request.setAttribute("credentialsKey", getCredentialsKey());
        final LocalSettings localSettings = settingsService.getLocalSettings();
        request.setAttribute("today", localSettings.getRawDateConverter().toString(Calendar.getInstance()));
        RequestHelper.storeEnum(request, SchedulingType.class, "schedulingTypes");
    }

    @Override
    protected DoPaymentDTO resolvePaymentDTO(final ActionContext context) {

        final Member member = (Member) context.getAccountOwner();
        final AgentConfirmPaymentForm form = context.getForm();
        final DoPaymentDTO payment = getDataBinder().readFromString(form);

//        try {
            final Member fromMember = CoercionHelper.coerce(Member.class, form.getFrom());
            
            payment.setFrom(fromMember);
//        } catch (final EntityNotFoundException e) {
//            // Leave null - will fail validation
//        }

        payment.setChannel(Channel.WEB);
        payment.setContext(TransactionContext.PAYMENT);
        payment.setTo(member);
        if (context.isOperator()) {
            payment.setReceiver(context.getElement());
        }
        final LocalSettings localSettings = settingsService.getLocalSettings();

        // Handle scheduling
        SchedulingType schedulingType = CoercionHelper.coerce(SchedulingType.class, form.getSchedulingType());
        if (schedulingType == null) {
            schedulingType = SchedulingType.IMMEDIATELY;
        }
        List<ScheduledPaymentDTO> installments = null;
        switch (schedulingType) {
            case SINGLE_FUTURE:
                final ScheduledPaymentDTO installment = new ScheduledPaymentDTO();
                installment.setAmount(payment.getAmount());
                installment.setDate(localSettings.getRawDateConverter().valueOf(form.getScheduledFor()));
                installments = Collections.singletonList(installment);
                break;
            case MULTIPLE_FUTURE:
                final int paymentCount = CoercionHelper.coerce(int.class, form.getPaymentCount());
                final Calendar firstPaymentDate = localSettings.getRawDateConverter().valueOf(form.getFirstPaymentDate());
                if (paymentCount > 0 && firstPaymentDate != null) {
                    final ProjectionDTO projection = new ProjectionDTO();
                    projection.setTransferType(payment.getTransferType());
                    projection.setAmount(payment.getAmount());
                    projection.setFirstExpirationDate(firstPaymentDate);
                    projection.setPaymentCount(paymentCount);
                    installments = getPaymentService().calculatePaymentProjection(projection);
                }
                break;
        }
        payment.setPayments(installments);

        return payment;
    }

    @Override
    protected void validateForm(final ActionContext context) {
        final DoPaymentDTO dto = resolvePaymentDTO(context);
        dto.setPayments(null);

        ValidationException validation = new ValidationException();
        final AgentConfirmPaymentForm form = context.getForm();
        final LocalSettings localSettings = settingsService.getLocalSettings();

        final AccountOwner from = dto.getFrom();
        if (from instanceof Member) {
            validation.setPropertyKey("paymentCount", "transfer.paymentCount");
            validation.setPropertyKey("firstPaymentDate", "transfer.firstPaymentDate");
            validation.setPropertyKey("scheduledFor", "transfer.scheduledFor");
            validation.setPropertyKey("_credentials", getCredentialsKey());

            SchedulingType schedulingType = CoercionHelper.coerce(SchedulingType.class, form.getSchedulingType());
            if (schedulingType == null) {
                schedulingType = SchedulingType.IMMEDIATELY;
            }

            switch (schedulingType) {
                case SINGLE_FUTURE:
                    // Validate the scheduled for date
                    Calendar scheduledFor = null;
                    try {
                        scheduledFor = localSettings.getRawDateConverter().valueOf(form.getScheduledFor());
                        ValidationError error = null;
                        if (scheduledFor == null) {
                            error = new RequiredError();
                        } else {
                            error = TodayValidation.future().validate(null, null, scheduledFor);
                        }
                        if (error != null) {
                            validation.addPropertyError("scheduledFor", error);
                        }
                    } catch (final Exception e) {
                        validation.addPropertyError("scheduledFor", new InvalidError());
                    }
                    break;
                case MULTIPLE_FUTURE:
                    // Validate the payment count
                    Integer paymentCount;
                    try {
                        paymentCount = CoercionHelper.coerce(Integer.class, form.getPaymentCount());
                        ValidationError error = null;
                        if (paymentCount == null) {
                            error = new RequiredError();
                        } else {
                            error = PositiveNonZeroValidation.instance().validate(null, null, paymentCount);
                        }
                        if (error != null) {
                            validation.addPropertyError("paymentCount", new InvalidError());
                        }
                    } catch (final Exception e) {
                        validation.addPropertyError("paymentCount", new InvalidError());
                    }

                    // Validate the first payment date
                    Calendar firstPaymentDate = null;
                    try {
                        firstPaymentDate = localSettings.getRawDateConverter().valueOf(form.getFirstPaymentDate());
                        ValidationError error = null;
                        if (firstPaymentDate == null) {
                            error = new RequiredError();
                        } else {
                            error = TodayValidation.futureOrToday().validate(null, null, firstPaymentDate);
                        }
                        if (error != null) {
                            validation.addPropertyError("firstPaymentDate", error);
                        }
                    } catch (final Exception e) {
                        validation.addPropertyError("firstPaymentDate", new InvalidError());
                    }
                    break;
            }

            // Validate the credentials
            if (StringUtils.isEmpty(form.getCredentials())) {
                validation.addPropertyError("_credentials", new RequiredError());
            }
        }
        validation.throwIfHasErrors();
    }

    private Payment confirm(ActionContext context) {
        final AgentConfirmPaymentForm form = context.getForm();
        final String credentials = form.getCredentials();

        ECommConfirmRequest ecConfirmReq = new ECommConfirmRequest();
        ecConfirmReq.setMobileNo(form.getTo());
        ecConfirmReq.setOtp(credentials);
        ecConfirmReq.setTrxID("");

        // FIXME: Quick fix using ecomm ws
        EcommConfirmResponse eCommConfirmResponse = new EcommConfirmResponse();
        try {

            com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest eConfirmRequest = new com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest();
            // //FIXME: lazy approach pass trough?
            BeanUtils.copyProperties(eConfirmRequest, ecConfirmReq);

            try {
                String mobilePhone = userToPhone(eConfirmRequest.getMobileNo(), false);
                eConfirmRequest.setMobileNo(mobilePhone);

            } catch (Exception e) {
                logger.error(e);
                logger.debug(e, e);

            }

            EcommResponse payResponse = getEcommConfirmWebService().confirmPayment(eConfirmRequest);

            BeanUtils.copyProperties(eCommConfirmResponse, payResponse);

        } catch (WebServiceException e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2998");
            eCommConfirmResponse.setRespMsg("CONNECTION_ERROR");
        } catch (Exception e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2999");
            eCommConfirmResponse.setRespMsg("UNKOWN_ERROR");
        }

        return null;
    }

    private EcommConfirmationWebService getEcommConfirmWebService() {

        if (eCommConfirmationWebService == null) {
            try {
                eCommConfirmationWebService = ExternalWebServiceHelper.proxyFor(EcommConfirmationWebService.class, urlEcommConfirmAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return eCommConfirmationWebService;
    }

    private String userToPhone(String username, boolean isMerchant) throws Exception {
        User user;

        user = elementService.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception("Not recognized user: " + user);
        }

        Member member = ((MemberUser) user).getMember();
        if (!member.isActive() || member.getGroup().getStatus() == Status.REMOVED) {
            throw new EntityNotFoundException();
        }
        List<MemberCustomField> fields = memberCustomFieldService.list();

        // get field id mobile
        int size = fields.size();
        String iNameMobile = null;
        for (int i = 0; i < size; i++) {
            if (MOBILE_PHONE.equals(fields.get(i).getInternalName())) {
                iNameMobile = fields.get(i).getName();
                break;
            }
        }

        final Collection<MemberCustomFieldValue> customValues = ((Member) elementService.load(member.getId(), Member.Relationships.CUSTOM_VALUES)).getCustomValues();

        List list = new ArrayList<MemberCustomFieldValue>(customValues);

        size = list.size();
        MemberCustomFieldValue temp = null;
        String test = null;
        String mobilPhone = null;
        boolean isFlaggedMerchant = false;
        for (int i = 0; i < size; i++) {
            try {
                temp = (MemberCustomFieldValue) list.get(i);
                test = temp.toString();
                // Retrieve mobile phone
                if (test != null && iNameMobile != null && test.contains(iNameMobile)) {
                    mobilPhone = temp.getValue();
                }

            } catch (Exception e) {
                logger.debug(e, e);
            }
        }
        return (mobilPhone);

    }

    private ValidationException checkCredentials(final ActionContext context, final Member member) {
        final String cardFormProperty = "_card";
        final String credentialsFormProperty = "_credentials";

        final ValidationException validation = new ValidationException();
        validation.setPropertyKey(cardFormProperty, "posweb.client.card");
        validation.setPropertyKey(credentialsFormProperty, getCredentialsKey());

        final AgentConfirmPaymentForm form = context.getForm();
        final String credentials = form.getCredentials();

        // Validate the credentials
        if (StringUtils.isEmpty(credentials)) {
            // Missing
            validation.addPropertyError(credentialsFormProperty, new RequiredError());
        }
        // else {
        // // Check it
        // final Member relatedMember = (Member) context.getAccountOwner();
        // try {
        // final MemberUser payer = CoercionHelper.coerce(MemberUser.class, form.getFrom());
        // accessService.checkCredentials(posWebChannel(), payer, credentials, context.getRequest().getRemoteAddr(), relatedMember);
        // } catch (final InvalidCardException e) {
        // validation.addPropertyError(cardFormProperty, new InvalidError());
        // } catch (final InvalidCredentialsException e) {
        // validation.addPropertyError(credentialsFormProperty, new InvalidError());
        // } catch (final BlockedCredentialsException e) {
        // String key;
        // switch (e.getCredentialsType()) {
        // case TRANSACTION_PASSWORD:
        // key = "transactionPassword.error.blockedByTrials";
        // break;
        // case PIN:
        // key = "pin.error.blocked";
        // break;
        // case CARD_SECURITY_CODE:
        // key = "cardSecurityCode.error.blocked";
        // break;
        // default:
        // key = "login.error.blocked";
        // break;
        // }
        // validation.addGeneralError(new ValidationError(key));
        // }
        // }

        return validation;
    }

    private String getCredentialsKey() {
        String credentialsKey = "posweb.client.otp";

        return credentialsKey;
    }

    private Channel posWebChannel() {
        final Channel channel = channelService.loadByInternalName(Channel.WEB);
        return channel;
    }

    private String principalTypeLabel(final PrincipalType principalType) {
        final Principal principal = principalType.getPrincipal();
        switch (principal) {
            case USER:
                return messageHelper.message("posweb.client.username");
            case CUSTOM_FIELD:
                return principalType.getCustomField().getName();
            default:
                return messageHelper.message(principal.getKey());
        }
    }

    public void setMemberCustomFieldService(MemberCustomFieldService memberCustomFieldService) {
        this.memberCustomFieldService = memberCustomFieldService;
        try {

            // FIXME:
            urlEcommConfirmAdapter = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.ecomm.urlEcommConfirmAdapter");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
