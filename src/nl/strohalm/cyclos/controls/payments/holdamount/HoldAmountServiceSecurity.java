package nl.strohalm.cyclos.controls.payments.holdamount;

import java.math.BigDecimal;

import javax.transaction.RollbackException;

import nl.strohalm.cyclos.entities.accounts.TransferAuthorizationAmountReservation;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.services.BaseServiceSecurity;

public class HoldAmountServiceSecurity extends BaseServiceSecurity implements
		HoldAmountService {

	private HoldAmountService holdAmountService;

	public HoldAmountService getHoldAmountService() {
		return holdAmountService;
	}

	public void setHoldAmountService(HoldAmountService holdAmountService) {
		this.holdAmountService = holdAmountService;
	}

	@Override
	public Transfer deny(TransferAuthorizationDTO dto) {
		// TODO Auto-generated method stub
		return holdAmountService.deny(dto);
	}

	@Override
	public Transfer authorize(HoldAmountRequestParameter request,
			TransferAuthorizationDTO dto, boolean newTransaction,
			boolean automaticallyAuthorize) {
		// TODO Auto-generated method stub
		return holdAmountService.authorize(request, dto, newTransaction, automaticallyAuthorize);
	}

	@Override
	public Transfer updateAmountTransfer(TransferAuthorizationDTO dto,
			BigDecimal amount) throws RollbackException {
		// TODO Auto-generated method stub
		return holdAmountService.updateAmountTransfer(dto, amount);
	}

	@Override
	public TransferAuthorizationAmountReservation loadAuthorization(
			Transfer transfer) {
		// TODO Auto-generated method stub
		return holdAmountService.loadAuthorization(transfer);
	}

	@Override
	public boolean deleteAndUpdateAuthorization(Long idTransferReservation,
			Transfer transfer) throws RollbackException {
		// TODO Auto-generated method stub
		return false;
	}

}
