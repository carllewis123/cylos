package nl.strohalm.cyclos.controls.payments.holdamount;

import java.io.Serializable;
import java.math.BigDecimal;

public class HoldAmountRequestParameter implements Serializable {

	private static final long serialVersionUID = 0L;
	private BigDecimal amount;
	private String traceNumber;
	private String from;
	private String to;
	private String credentials;

	public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

}
