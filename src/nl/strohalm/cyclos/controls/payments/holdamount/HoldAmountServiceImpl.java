package nl.strohalm.cyclos.controls.payments.holdamount;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.transaction.RollbackException;

import nl.strohalm.cyclos.dao.accounts.transactions.TransferAuthorizationDAO;
import nl.strohalm.cyclos.dao.accounts.transactions.TransferDAO;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.TransferAuthorizationAmountReservation;
import nl.strohalm.cyclos.entities.accounts.transactions.AuthorizationLevel;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorization;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.entities.exceptions.UnexpectedEntityException;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.accounts.rates.RatesToSave;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.services.transactions.exceptions.AlreadyAuthorizedException;
import nl.strohalm.cyclos.utils.RelationshipHelper;

public class HoldAmountServiceImpl implements HoldAmountService {

	private TransferDAO transferDao;
	private AccountServiceLocal accountService;
	private TransferAuthorizationDAO transferAuthorizationDao;
	private FetchServiceLocal fetchService;
	private ElementServiceLocal elementServiceLocal;

	public void setFetchServiceLocal(final FetchServiceLocal fetchService) {
		this.fetchService = fetchService;
	}

	public void setElementServiceLocal(ElementServiceLocal elementServiceLocal) {
		this.elementServiceLocal = elementServiceLocal;
	}

	public TransferAuthorizationDAO getTransferAuthorizationDao() {
		return transferAuthorizationDao;
	}

	public void setAccountServiceLocal(final AccountServiceLocal accountService) {
		this.accountService = accountService;
	}
	
	public void setTransferAuthorizationDao(
			TransferAuthorizationDAO transferAuthorizationDao) {
		this.transferAuthorizationDao = transferAuthorizationDao;
	}

	public TransferDAO getTransferDao() {
		return transferDao;
	}

	public void setTransferDao(TransferDAO transferDao) {
		this.transferDao = transferDao;
	}

		@Override
	public Transfer authorize(HoldAmountRequestParameter request, TransferAuthorizationDTO dto,
			final boolean newTransaction, final boolean automaticallyAuthorize) {
		User user;

		Transfer transfer;
		user = elementServiceLocal.loadUser(request.getFrom(),
				RelationshipHelper.nested(User.Relationships.ELEMENT,
						Element.Relationships.GROUP), RelationshipHelper
						.nested(User.Relationships.ELEMENT,
								Member.Relationships.CUSTOM_VALUES),
				RelationshipHelper.nested(User.Relationships.ELEMENT,
						Member.Relationships.IMAGES));

		validateAuthorization(dto.getTransfer());

		for (final TransferAuthorization authorization : dto.getTransfer()
				.getAuthorizations()) {
			if (user.getElement().equals(authorization.getBy())) {
				throw new AlreadyAuthorizedException();
			}
		}

		final AuthorizationLevel authorizationLevel = dto.getTransfer()
				.getNextAuthorizationLevel();
		final String comments = "authorize by " + user.getUsername();
		final boolean showToMember = dto.isShowToMember();

		transfer = fetchService.reload(dto.getTransfer(),
				Transfer.Relationships.SCHEDULED_PAYMENT,
				Transfer.Relationships.AUTHORIZATIONS);

		RatesToSave rates = new RatesToSave();
		Calendar processDate = (rates.getFromRates() == null) ? Calendar
				.getInstance() : rates.getFromRates().getDate();
		transfer.setProcessDate(processDate);
		
		// returnReservation will do this for the source account
				accountService.removeClosedBalancesAfter(transfer.getTo(), processDate);
				transfer = transferDao.updateAuthorizationData(transfer.getId(),
						Transfer.Status.PROCESSED, null, transfer.getProcessDate(),
						rates);
		
		final TransferAuthorization authorization = createAuthorization(
				transfer, user, TransferAuthorization.Action.AUTHORIZE,
				authorizationLevel, comments, showToMember);

		

		// Return the reserved amount
		accountService.returnReservation(authorization, transfer);
	
		return transfer;
	}

	private void validateAuthorization(final Transfer transfer) {
		// Can't cancel a nested transfer, and it must be pending authorization
		if (!transfer.isRoot()
				|| transfer.getStatus() == Transfer.Status.PROCESSED) {
			throw new AlreadyAuthorizedException();
		} else if (!transfer.isRoot()
				|| transfer.getStatus() != Transfer.Status.PENDING) {
			throw new UnexpectedEntityException();
		}
	}

	private TransferAuthorization createAuthorization(final Transfer transfer,
			final User user, final TransferAuthorization.Action action,
			final AuthorizationLevel level, final String comments,
			final boolean showToMember) {
		TransferAuthorization transferAuthorization = new TransferAuthorization();
		transferAuthorization.setTransfer(transfer);
		transferAuthorization.setLevel(level);
		transferAuthorization.setBy(user.getElement());
		transferAuthorization.setDate(Calendar.getInstance());
		transferAuthorization.setAction(action);
		transferAuthorization.setComments(comments);
		transferAuthorization.setShowToMember(showToMember);
		transferAuthorization = transferAuthorizationDao
				.insert(transferAuthorization);

		return transferAuthorization;
	}

	@Override
	public Transfer deny(TransferAuthorizationDTO dto) {
		Transfer transfer = new Transfer();

		User user;
		user = elementServiceLocal.loadUser(dto.getTransfer().getTo().getOwnerName(),
				RelationshipHelper.nested(User.Relationships.ELEMENT,
						Element.Relationships.GROUP), RelationshipHelper
						.nested(User.Relationships.ELEMENT,
								Member.Relationships.CUSTOM_VALUES),
				RelationshipHelper.nested(User.Relationships.ELEMENT,
						Member.Relationships.IMAGES));

		validateAuthorization(dto.getTransfer());

		
		transfer = fetchService.fetch(dto.getTransfer());
		validateAuthorization(transfer);
		
		final String comments = "deny";
		final boolean showToMember = dto.isShowToMember();
		
		// Update transfer
        final AuthorizationLevel authorizationLevel = transfer.getNextAuthorizationLevel();

		transfer = transferDao.updateAuthorizationData(dto
				.getTransfer().getId(), Transfer.Status.DENIED, null, null,
				null);

		// Create the transfer authorization object
        final TransferAuthorization authorization = createAuthorization(transfer, user, TransferAuthorization.Action.DENY, authorizationLevel, comments, showToMember);

        // Return the reserved amount
        accountService.returnReservation(authorization, transfer);

        
		
		return transfer;
	}

	@Override
	public TransferAuthorizationAmountReservation loadAuthorization(
			Transfer transfer) {
		// TODO Auto-generated method stub
		return accountService.loadReservation(transfer);
	}

	@Override
	public Transfer updateAmountTransfer(TransferAuthorizationDTO dto,
			BigDecimal amount) throws RollbackException {
		// TODO Auto-generated method stub
		Transfer transfer = new Transfer();

		try {
			transfer = transferDao.updateAmount(dto.getTransfer().getId(),amount);
			
		} catch (Exception e) {
			throw new RollbackException();
		}
		
		return transfer;
	
	}

	@Override
	public boolean deleteAndUpdateAuthorization(Long idTransferReservation, Transfer transfer) throws RollbackException {
		// TODO Auto-generated method stub
		try {
			accountService.reservePending(transfer);
			accountService.deleteReservation(idTransferReservation);
			
		} catch (Exception e) {
			throw new RollbackException();
		}
		
		return true;
		
	}
}