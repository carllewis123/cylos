package nl.strohalm.cyclos.controls.payments.holdamount;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.transaction.RollbackException;

@WebService
public interface HoldAmountWebService {

	@WebMethod
	@WebResult(name = "holdAmountResult")
	public HoldAmountResponseParameters deny(
			@WebParam(name = "request") HoldAmountRequestParameter request);
	
	@WebMethod
	public HoldAmountResponseParameters Authorize(
			@WebParam(name = "request") HoldAmountRequestParameter request) throws RollbackException;
	
}

