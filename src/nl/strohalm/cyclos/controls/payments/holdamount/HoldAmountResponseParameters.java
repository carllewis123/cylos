package nl.strohalm.cyclos.controls.payments.holdamount;

public class HoldAmountResponseParameters {

	private boolean isError;
	private String status;

	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
