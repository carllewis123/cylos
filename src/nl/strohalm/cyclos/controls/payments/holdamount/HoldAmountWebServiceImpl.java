package nl.strohalm.cyclos.controls.payments.holdamount;

import java.math.BigDecimal;

import javax.jws.WebService;
import javax.transaction.RollbackException;
import nl.strohalm.cyclos.dao.accounts.transactions.TraceNumberDAO;
import nl.strohalm.cyclos.dao.accounts.transactions.TransferDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.Account;
import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.TransferAuthorizationAmountReservation;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.ScheduledPayment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.utils.RelationshipHelper;

@WebService(name = "holdAmountWebService", serviceName = "holdAmountWebService")
public class HoldAmountWebServiceImpl implements HoldAmountWebService {

	public static final Relationship[] FETCH = {
			Transfer.Relationships.CHILDREN,
			Payment.Relationships.CUSTOM_VALUES,
			RelationshipHelper.nested(Payment.Relationships.FROM,
					MemberAccount.Relationships.MEMBER,
					Element.Relationships.GROUP),
			RelationshipHelper.nested(Payment.Relationships.FROM,
					Account.Relationships.TYPE),
			RelationshipHelper.nested(Payment.Relationships.TO,
					MemberAccount.Relationships.MEMBER,
					Element.Relationships.GROUP),
			RelationshipHelper.nested(Payment.Relationships.TO,
					Account.Relationships.TYPE),
			RelationshipHelper.nested(Payment.Relationships.TYPE,
					TransferType.Relationships.TO),
			RelationshipHelper.nested(Transfer.Relationships.SCHEDULED_PAYMENT,
					ScheduledPayment.Relationships.TRANSFERS),
			Payment.Relationships.BY, Transfer.Relationships.RECEIVER,
			Transfer.Relationships.NEXT_AUTHORIZATION_LEVEL,
			Transfer.Relationships.AUTHORIZATIONS,
			Transfer.Relationships.CHARGEBACK_OF,
			Transfer.Relationships.CHARGED_BACK_BY };
	private HoldAmountService holdAmountService;
	private TransferDAO transferDao;
	private PaymentServiceLocal paymentServiceLocal;
	private TraceNumberDAO traceNumberDao;
	private HoldAmountResponseParameters response = new HoldAmountResponseParameters();
	private final TransferAuthorizationDTO transferAuthorizationDto = new TransferAuthorizationDTO();

	public TraceNumberDAO getTraceNumberDao() {
		return traceNumberDao;
	}

	public void setTraceNumberDao(TraceNumberDAO traceNumberDao) {
		this.traceNumberDao = traceNumberDao;
	}

	public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
		paymentServiceLocal = paymentService;
	}

	public void setHoldAmountService(HoldAmountService holdAmountService) {
		this.holdAmountService = holdAmountService;
	}

	public TransferDAO getTransferDao() {
		return transferDao;
	}

	public void setTransferDao(TransferDAO transferDao) {
		this.transferDao = transferDao;
	}

	@Override
	public HoldAmountResponseParameters deny(HoldAmountRequestParameter request) {
		Transfer transfer = transferDao.loadTransferByTraceNumber(
				request.getTraceNumber(), null);
		transferAuthorizationDto.setTransfer(transfer);

		if (transfer != null) {
			response.setError(true);
			response.setStatus("Data not found");

			transfer = holdAmountService.deny(transferAuthorizationDto);

			response.setError(false);
			response.setStatus("SUCCESS");

		} else {
			response.setError(true);
			response.setStatus("DATA NOT FOUND");
		}
		return response;
	}

	@Override
	public HoldAmountResponseParameters Authorize(
			HoldAmountRequestParameter request) throws RollbackException {
		Transfer transfer = transferDao.loadTransferByTraceNumber(
				request.getTraceNumber(), null);
		
		if (transfer != null) {
			transfer = paymentServiceLocal.load(transfer.getId(),
					Payment.Relationships.FROM, Payment.Relationships.TO,
					Payment.Relationships.TYPE,
					Payment.Relationships.CUSTOM_VALUES);
			transferAuthorizationDto.setTransfer(transfer);

			TransferAuthorizationAmountReservation amountReservation = holdAmountService.loadAuthorization(transfer);
			amountReservation.setTransfer(transfer);

			BigDecimal holdAmount = transfer.getAmount();

			// check if holdAmount equal with request amount
			if (holdAmount.subtract(request.getAmount()).compareTo(
					BigDecimal.ZERO) == 0) {

					transfer = holdAmountService.authorize(request,
							transferAuthorizationDto, true, false);	
				
				if (transfer.getStatus().toString().equals("PROCESSED")) {
					response.setError(false);
					response.setStatus("Payment Authorize");

					return response;
					// }
				} else {
					response.setError(true);
					response.setStatus("please try again later");
				}
			} else {
				response.setError(true);
				response.setStatus("INVALID AMOUNT");
			}
		} else {
			response.setError(false);
			response.setStatus("tracenumber not found");
		}
		return response;
	}
}
