package nl.strohalm.cyclos.controls.payments.holdamount;

import java.math.BigDecimal;

import javax.transaction.RollbackException;

import nl.strohalm.cyclos.entities.accounts.TransferAuthorizationAmountReservation;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.services.Service;

public interface HoldAmountService extends Service {
	
	Transfer deny (final TransferAuthorizationDTO dto);
	
	Transfer updateAmountTransfer (final TransferAuthorizationDTO dto, final BigDecimal amount) throws RollbackException;
	
	Transfer authorize (HoldAmountRequestParameter request, TransferAuthorizationDTO dto, final boolean newTransaction, final boolean automaticallyAuthorize);

	TransferAuthorizationAmountReservation loadAuthorization(Transfer transfer); 
	
	boolean deleteAndUpdateAuthorization(Long idTransferReservation, Transfer transfer) throws RollbackException;
}
