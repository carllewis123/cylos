/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package nl.strohalm.cyclos.controls.payments;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseFormAction;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.ScheduledPayment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.CustomField;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldPossibleValue;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.UnexpectedEntityException;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldService;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.PaymentService;
import nl.strohalm.cyclos.services.transactions.ScheduledPaymentDTO;
import nl.strohalm.cyclos.services.transactions.exceptions.AuthorizedPaymentInPastException;
import nl.strohalm.cyclos.services.transactions.exceptions.CreditsException;
import nl.strohalm.cyclos.services.transfertypes.TransactionFeePreviewDTO;
import nl.strohalm.cyclos.services.transfertypes.TransactionFeeService;
import nl.strohalm.cyclos.services.transfertypes.TransferTypeService;
import nl.strohalm.cyclos.utils.ActionHelper;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.CustomFieldHelper.Entry;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.conversion.CoercionHelper;
import nl.strohalm.cyclos.utils.validation.RequiredError;
import nl.strohalm.cyclos.utils.validation.ValidationException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.BillInfoList;
import ptdam.emoney.webservice.client.BillItemList;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.ResponseHeaderExtended;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.client.UBPCInqMsgRequest;
import ptdam.emoney.webservice.client.UBPCInqMsgResponse;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

/**
 * Action used to display a message and optionally request the transaction password in order to make a payment
 * @author luis
 */
public class BillInquiryResultAction extends BaseFormAction {

    private PaymentService            paymentService;
    private TransferTypeService       transferTypeService;
    private TransactionFeeService     transactionFeeService;
    private PaymentCustomFieldService paymentCustomFieldService;
    private final Log               logger    = LogFactory.getLog(BillInquiryResultAction.class);

    private CustomFieldHelper         customFieldHelper;
//    private UBPChannelWebService      wsClient;
    private CoreWebServiceFactory     coreWSFactory;
    
    @Inject
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    @Inject
    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    @Inject
    public void setPaymentCustomFieldService(final PaymentCustomFieldService paymentCustomFieldService) {
        this.paymentCustomFieldService = paymentCustomFieldService;
    }

    @Inject
    public void setPaymentService(final PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Inject
    public void setTransactionFeeService(final TransactionFeeService transactionFeeService) {
        this.transactionFeeService = transactionFeeService;
    }

    @Inject
    public void setTransferTypeService(final TransferTypeService transferTypeService) {
        this.transferTypeService = transferTypeService;
    }


    @Override
    protected ActionForward handleSubmit(final ActionContext context) throws Exception {
        final BillInquiryResultForm form = context.getForm();
        final DoPaymentDTO paymentDTO = validatePayment(context);
        // Validate the transaction password if needed
        if (shouldValidateTransactionPassword(context, paymentDTO)) {
            context.checkTransactionPassword(form.getTransactionPassword());
        }
        // Perform the actual payment
        Payment payment;
        try {
            payment = paymentService.doPayment(paymentDTO);
            context.getSession().removeAttribute("payment");
        } catch (final CreditsException e) {
            return context.sendError(actionHelper.resolveErrorKey(e), actionHelper.resolveParameters(e));
        } catch (final UnexpectedEntityException e) {
            return context.sendError("payment.error.invalidTransferType");
        } catch (final AuthorizedPaymentInPastException e) {
            return context.sendError("payment.error.authorizedInPast");
        }
        // Redirect to the next action
        final Map<String, Object> params = new HashMap<String, Object>();
        ActionForward forward;
        if (payment instanceof Transfer) {
            params.put("transferId", payment.getId());
            forward = context.getSuccessForward();
        } else if (payment instanceof ScheduledPayment) {
            params.put("paymentId", payment.getId());
            forward = context.findForward("scheduledPayment");
        } else {
            throw new IllegalStateException("Unknown payment type: " + payment);
        }
        params.put("selectMember", form.getSelectMember());
        params.put("from", form.getFrom());
        return ActionHelper.redirectWithParams(context.getRequest(), forward, params);
    }

    @Override
    protected void prepareForm(final ActionContext context) throws Exception {
        final DoPaymentDTO oripayment = validatePayment(context);
        
        DoPaymentDTO payment = inquiry(oripayment);
        //DoPaymentDTO payment = inquirySimulator(oripayment);
        
        if (payment == null) {
            payment = oripayment;
        }
        
        // Clear the from when the same as logged owner
        if (payment.getFrom() != null && context.getAccountOwner().equals(payment.getFrom())) {
            payment.setFrom(null);
        }

        // Check for transaction password
        final HttpServletRequest request = context.getRequest();
        final boolean requestTransactionPassword = shouldValidateTransactionPassword(context, payment);
        if (requestTransactionPassword) {
            context.validateTransactionPassword();
        }

        final boolean wouldRequireAuthorization = paymentService.wouldRequireAuthorization(payment);
        request.setAttribute("requestTransactionPassword", requestTransactionPassword);
        request.setAttribute("wouldRequireAuthorization", wouldRequireAuthorization);

        if (wouldRequireAuthorization && payment.getDate() != null) {
            throw new ValidationException("payment.error.authorizedInPast");
        }

        // Fetch related data
        AccountOwner from = payment.getFrom();
        AccountOwner to = payment.getTo();
        final TransferType transferType = transferTypeService.load(payment.getTransferType().getId(), RelationshipHelper.nested(TransferType.Relationships.FROM, AccountType.Relationships.CURRENCY), TransferType.Relationships.TO);
        final BigDecimal amount = payment.getAmount();
        if (from instanceof Member) {
            from = (Member) elementService.load(((Member) from).getId());
            request.setAttribute("fromMember", from);
            payment.setFrom(from);
        }
        if (to instanceof Member) {
            to = (Member) elementService.load(((Member) to).getId());
            request.setAttribute("toMember", to);
            payment.setTo(to);
        }
        // request.setAttribute("relatedMember", from != null ? from.g : to);
        payment.setTransferType(transferType);
        request.setAttribute("unitsPattern", transferType.getFrom().getCurrency().getPattern());

        // Store the transaction fees
        final TransactionFeePreviewDTO preview = transactionFeeService.preview(from, to, transferType, amount);
        request.setAttribute("finalAmount", preview.getFinalAmount());
        request.setAttribute("fees", preview.getFees());

        // Show the total amount when the original amount has changed (there where fees which deducted from it)
        if (!preview.getAmount().equals(preview.getFinalAmount())) {
            request.setAttribute("totalAmount", preview.getAmount());
        }

        // Calculate the transaction fees for every scheduled payment
        final List<ScheduledPaymentDTO> payments = payment.getPayments();
        final boolean isScheduled = CollectionUtils.isNotEmpty(payments);
        if (isScheduled) {
            for (final ScheduledPaymentDTO current : payments) {
                final TransactionFeePreviewDTO currentPreview = transactionFeeService.preview(from, to, transferType, current.getAmount());
                current.setFinalAmount(currentPreview.getFinalAmount());
            }
        }
        request.setAttribute("isScheduled", isScheduled);

        // Return the custom field values
        final Collection<PaymentCustomFieldValue> customValues = payment.getCustomValues();
        if (customValues != null) {
            final List<PaymentCustomField> customFields = paymentCustomFieldService.list(transferType, false);
            final Collection<Entry> entries = customFieldHelper.buildEntries(customFields, customValues);
            // Load the value for enumerated values, since this collection was built from direct databinding with ids only
            for (final Entry entry : entries) {
                final CustomField field = entry.getField();
                final CustomFieldValue fieldValue = entry.getValue();
                if (field.getType() == CustomField.Type.ENUMERATED) {
                    Long possibleValueId;
                    final CustomFieldPossibleValue possibleValue = fieldValue.getPossibleValue();
                    if (possibleValue != null) {
                        possibleValueId = possibleValue.getId();
                    } else {
                        possibleValueId = CoercionHelper.coerce(Long.class, fieldValue.getValue());
                    }
                    if (possibleValueId != null) {
                        fieldValue.setPossibleValue(paymentCustomFieldService.loadPossibleValue(possibleValueId));
                    }
                } else if (field.getType() == CustomField.Type.MEMBER) {
                    final Long memberId = CoercionHelper.coerce(Long.class, fieldValue.getValue());
                    if (memberId != null) {
                        final Element element = elementService.load(memberId);
                        if (element instanceof Member) {
                            fieldValue.setMemberValue((Member) element);
                        }
                    }
                }
            }
            request.setAttribute("customFields", entries);
        }
    }

    @Override
    protected void validateForm(final ActionContext context) {
        if (shouldValidateTransactionPassword(context, validatePayment(context))) {
            final BillInquiryResultForm form = context.getForm();
            if (StringUtils.isEmpty(form.getTransactionPassword())) {
                throw new ValidationException("_transactionPassword", "login.transactionPassword", new RequiredError());
            }
        }
    }

    private boolean shouldValidateTransactionPassword(final ActionContext context, final DoPaymentDTO payment) {
        if (payment.getFrom() == null) {
            // When a logged member performing payments from himself
            final TransferType transferType = transferTypeService.load(payment.getTransferType().getId(), TransferType.Relationships.FROM);
            return context.isTransactionPasswordEnabled(transferType.getFrom());
        } else {
            return context.isTransactionPasswordEnabled();
        }
    }

    private DoPaymentDTO validatePayment(final ActionContext context) {
        final DoPaymentDTO payment = (DoPaymentDTO) context.getSession().getAttribute("payment");
        if (payment == null) {
            throw new ValidationException();
        }
        return payment;
    }
    
    @SuppressWarnings("unused")
    private DoPaymentDTO inquiry(DoPaymentDTO paymentInfo) {
        
        try {

            final UBPCInqMsgRequest request = new UBPCInqMsgRequest();

            // Get Params
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.channelId");
            final String billkey1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey1");
            final String billkey2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey2");
            final String billkey3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey3");
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.glaccount");
            final String debitAccountType = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.customerAccountType");
            final String cardAcceptorTermId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.cardAcceptorTermId");
            final String traceNumberLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.traceNumberLength");
            final String track2DataLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.track2DataLength");
            final String languageCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.language_code.ind");
            final String currencyCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.currencyCode");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String timeStampPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format_prefix");
            final String transactionCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.transactionCode");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.tellerId");
            final String journalSequenceLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.journalSequenceLength");
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.extIdlength");
            final String companyCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.company_code_prefix");
            final String paymentCustomFieldPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("pcf.sisf_prefix");
            final String validBillkeysPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkeys_prefix");
            final String ubpAdvicePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.advice_prefix");
            final String ubpBillTypePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type_prefix");
            final String billTypeOpen = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.open");
            final String billTypeClose = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.close");
            final String skipChar = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.skip_label_char");
            final String labelValueSep = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.label_value_sep");

            
            // Get Bill Keys
            final String companyCode1Val = getCustomLabel(companyCodePrefix, paymentInfo); //EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.companycode." + entityId);
            final String billKey1Val = getCustomValue(paymentCustomFieldPrefix + billkey1 + "_" + companyCode1Val, paymentInfo);
            final String validBillkeys = getCustomLabel(validBillkeysPrefix, paymentInfo);
            
            final String ubpBillType = getCustomLabel(ubpBillTypePrefix, paymentInfo);
            
            request.setBdyBillKey1(billKey1Val);
            if (ubpBillType.equals(billTypeOpen)) {
                if (validBillkeys.contains(billkey2)) {
                    BigDecimal amount = paymentInfo.getAmount();
                    amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
                    request.setBdyBillKey2(amount.longValue());
                } else {
                    request.setBdyBillKey2(null);
                    paymentInfo.setAmount(BigDecimal.ONE);
                }
            }
            
            
            //final String companyCode1Val = getCustomLabel(companyCodePrefix, paymentInfo); //EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.companycode." + entityId);
            if (companyCode1Val != null) {
                request.setBdyCompanyCode(Long.parseLong(companyCode1Val));
            } else {
                logger.error("Company code is not setup correctly.");
                throw new ExternalException("CCINSC");
            }
            
            request.setBdyChannelId(channelId);
            request.setBdyCustomerAccountNumber(Long.parseLong(debitAccount));
            request.setBdyCustomerAccountType(debitAccountType);
            request.setBdyCardAcceptorTermId(cardAcceptorTermId);

            final String traceNumber= RandomStringUtils.randomNumeric(Integer.parseInt(traceNumberLength));
            request.setBdyTraceNumber(Long.parseLong(traceNumber));
            
            final String track2Data = RandomStringUtils.randomNumeric(Integer.parseInt(track2DataLength));
            request.setBdyTrack2data(Long.parseLong(track2Data));
            
            request.setBdyLanguageCode(Long.parseLong(languageCode));
            request.setBdyCurrencyCode(currencyCode);
    
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(Calendar.getInstance().getTime());

            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.reqSecurityHeader"));
            
            RequestHeader header = new RequestHeader();
            header.setHdrChannelId(request.getBdyChannelId());
            header.setHdrTellerId(Long.parseLong(tellerId));

            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
            header.setHdrExternalId(extId);
            
            header.setHdrTransactionCode(transactionCode);
            header.setHdrTimestamp(sTimestamp);
            
            final String journalSequence = RandomStringUtils.randomNumeric(Integer.parseInt(journalSequenceLength));
            header.setHdrJournalSequence(Long.parseLong(journalSequence));

            SecurityHeader securityHeader = new SecurityHeader();
            request.setSecurityHeader(securityHeader);
            
            request.setHeader(header);                
            final UBPCInqMsgResponse response = coreWSFactory.getUBPChannelWebService().inquiry(request);
            
            String responseMsg = "";
            
            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()){
                        case 1: 
                            BillInfoList[] billInfo = response.getBdyBillInfos();
                            String text = "";
                            for (int i=0 ; i<billInfo.length; i++) {
                                String label = billInfo[i].getLabel();
                                String val = billInfo[i].getValue();
                                
                                if (label.equals(skipChar)) {
                                    text = text + val.trim() + "\r\n";
                                } else {
                                    text = text + label.trim() + " " + val.trim() + "\r\n";
                                }
                                
                            }
                            paymentInfo.setDescription(text);
                            if (ubpBillType.equals(billTypeClose)) {
                                BillItemList[] items = response.getBdyBillItems();
                                if (items.length > 0) {
                                    paymentInfo.setAmount(items[0].getBillAmount());
                                } else {
                                    paymentInfo.setAmount(BigDecimal.ZERO);
                                }
                            }
                            break;
                        default:
                            responseMsg = response.getResponseHeader().getHdrResponseMessage();
                            final String errorCode = response.getResponseHeader().getHdrErrorNumber().trim();
                            String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.inquiry.error_code." + companyCode1Val + "." +errorCode);
                            logger.error("Host Error (" + errorCode + ") : " + responseMsg);
                            if (mappingErrorMsg == null) {
                                mappingErrorMsg = responseMsg;
                            }
                            throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }

        } catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
        return paymentInfo;
    }
    
    private String getCustomValue(final String internalName, final DoPaymentDTO paymentInfo) {
        final List<PaymentCustomField> customFields = paymentCustomFieldService.list(paymentInfo.getTransferType(), false);
        final Collection<PaymentCustomFieldValue> customValues = paymentInfo.getCustomValues();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                for (PaymentCustomFieldValue customValue: customValues) {
                    if (customValue.getField().getId().compareTo(customField.getId()) == 0) {
                        return customValue.getValue();
                    }
                }
            }
        }
        return null;
    }
    
    private String getCustomLabel(final String internalName, final DoPaymentDTO paymentInfo) {
        final List<PaymentCustomField> customFields = paymentCustomFieldService.list(paymentInfo.getTransferType(), false);
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    private Collection<PaymentCustomFieldValue> setCustomValue(final String internalName, final DoPaymentDTO paymentInfo, final String value) {
        final List<PaymentCustomField> customFields = paymentCustomFieldService.list(paymentInfo.getTransferType(), false);
        final Collection<PaymentCustomFieldValue> customValues = paymentInfo.getCustomValues();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().equals(internalName)) {
                PaymentCustomFieldValue customValue = new PaymentCustomFieldValue();
                customValue.setField(customField);
                customValue.setValue(value);
                customValues.add(customValue);
                break;
            }
        }
        
        return customValues;
    }
    
    @SuppressWarnings("unused")
    private DoPaymentDTO inquirySimulator(DoPaymentDTO paymentInfo) {
        
        try {

            final UBPCInqMsgRequest request = new UBPCInqMsgRequest();

            // Get Params
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.channelId");
            final String billkey1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey1");
            final String billkey2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey2");
            final String billkey3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey3");
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.glaccount");
            final String debitAccountType = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.customerAccountType");
            final String cardAcceptorTermId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.cardAcceptorTermId");
            final String traceNumberLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.traceNumberLength");
            final String track2DataLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.track2DataLength");
            final String languageCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.language_code.ind");
            final String currencyCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.currencyCode");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String timeStampPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format_prefix");
            final String transactionCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.transactionCode");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.tellerId");
            final String journalSequenceLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.journalSequenceLength");
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.extIdlength");
            final String companyCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.company_code_prefix");
            final String paymentCustomFieldPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("pcf.sisf_prefix");
            final String validBillkeysPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkeys_prefix");
            final String ubpAdvicePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.advice_prefix");
            final String ubpBillTypePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type_prefix");
            final String billTypeOpen = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.open");
            final String billTypeClose = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.close");
            final String skipChar = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.skip_label_char");
            final String labelValueSep = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.label_value_sep");

            
            // Get Bill Keys
            final String companyCode1Val = getCustomLabel(companyCodePrefix, paymentInfo); //EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.companycode." + entityId);
            final String billKey1Val = getCustomValue(paymentCustomFieldPrefix + billkey1 + "_" + companyCode1Val, paymentInfo);
            final String validBillkeys = getCustomLabel(validBillkeysPrefix, paymentInfo);
            
            final String ubpBillType = getCustomLabel(ubpBillTypePrefix, paymentInfo);
            
            request.setBdyBillKey1(billKey1Val);
            if (ubpBillType.equals(billTypeOpen)) {
                if (validBillkeys.contains(billkey2)) {
                    BigDecimal amount = paymentInfo.getAmount();
                    amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
                    request.setBdyBillKey2(amount.longValue());
                } else {
                    request.setBdyBillKey2(null);
                    paymentInfo.setAmount(BigDecimal.ONE);
                }
            }
            
            
            //final String companyCode1Val = getCustomLabel(companyCodePrefix, paymentInfo); //EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.companycode." + entityId);
            if (companyCode1Val != null) {
                request.setBdyCompanyCode(Long.parseLong(companyCode1Val));
            } else {
                logger.error("Company code is not setup correctly.");
                throw new ExternalException("CCINSC");
            }
            
            request.setBdyChannelId(channelId);
            request.setBdyCustomerAccountNumber(Long.parseLong(debitAccount));
            request.setBdyCustomerAccountType(debitAccountType);
            request.setBdyCardAcceptorTermId(cardAcceptorTermId);

            final String traceNumber= RandomStringUtils.randomNumeric(Integer.parseInt(traceNumberLength));
            request.setBdyTraceNumber(Long.parseLong(traceNumber));
            
            final String track2Data = RandomStringUtils.randomNumeric(Integer.parseInt(track2DataLength));
            request.setBdyTrack2data(Long.parseLong(track2Data));
            
            request.setBdyLanguageCode(Long.parseLong(languageCode));
            request.setBdyCurrencyCode(currencyCode);
    
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(Calendar.getInstance().getTime());

            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.reqSecurityHeader"));
            
            RequestHeader header = new RequestHeader();
            header.setHdrChannelId(request.getBdyChannelId());
            header.setHdrTellerId(Long.parseLong(tellerId));

            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
            header.setHdrExternalId(extId);
            
            header.setHdrTransactionCode(transactionCode);
            header.setHdrTimestamp(sTimestamp);
            
            final String journalSequence = RandomStringUtils.randomNumeric(Integer.parseInt(journalSequenceLength));
            header.setHdrJournalSequence(Long.parseLong(journalSequence));

            SecurityHeader securityHeader = new SecurityHeader();
            request.setSecurityHeader(securityHeader);
            
            request.setHeader(header);                
//            final UBPCInqMsgResponse response = coreWSFactory.getUBPChannelWebService().inquiry(request);
            UBPCInqMsgResponse response = new UBPCInqMsgResponse();
            ResponseHeaderExtended responseHeader = new ResponseHeaderExtended();
            
            BillInfoList[] bdyReceiptInfo = new BillInfoList[7];
            // purchase advice
            BigDecimal amount = paymentInfo.getAmount();
            amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
            amount = amount.add(new BigDecimal("2500").negate());
	            
            bdyReceiptInfo[0] = new BillInfoList();
            bdyReceiptInfo[0].setLabel("-");
            bdyReceiptInfo[0].setValue("82ED847839A7D47560DC");
            
            bdyReceiptInfo[1] = new BillInfoList();
            bdyReceiptInfo[1].setLabel("-");
            bdyReceiptInfo[1].setValue("SETIAWAN");
            
            bdyReceiptInfo[2] = new BillInfoList();
            bdyReceiptInfo[2].setLabel("-");
            bdyReceiptInfo[2].setValue("14234567890");
            
            bdyReceiptInfo[3] = new BillInfoList();
            bdyReceiptInfo[3].setLabel("-");
            bdyReceiptInfo[3].setValue("124444444445");
            
            bdyReceiptInfo[4] = new BillInfoList();
            bdyReceiptInfo[4].setLabel("-");
            bdyReceiptInfo[4].setValue("DK/7700VA");
            
            bdyReceiptInfo[5] = new BillInfoList();
            bdyReceiptInfo[5].setLabel("Rp");
            bdyReceiptInfo[5].setValue(amount.toString());
            
            bdyReceiptInfo[6] = new BillInfoList();
            bdyReceiptInfo[6].setLabel("kWh");
            bdyReceiptInfo[6].setValue(amount.divide(new BigDecimal("700"), 2, RoundingMode.HALF_UP).toString());
            
            BillItemList[] billItemList = new BillItemList[1];
            billItemList[0] = new BillItemList();
            billItemList[0].setBillCode("00");
            billItemList[0].setBillName("TOTAL");
            billItemList[0].setBillAmount(paymentInfo.getAmount());
            billItemList[0].setBillCustomerChargeAmount(BigDecimal.ZERO);
            billItemList[0].setBillCompanyChargeAmount(BigDecimal.ZERO);
            billItemList[0].setBillPaidFlag("0");
            billItemList[0].setBillCurrency("IDR");
            
            responseHeader.setHdrResponseCode(1);
            
            response.setResponseHeader(responseHeader);
            response.setBdyBillInfos(bdyReceiptInfo);
            response.setBdyBillItems(billItemList);
            
            String responseMsg = "";
            
            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()){
                        case 1: 
                            BillInfoList[] billInfo = response.getBdyBillInfos();
                            String text = "";
                            for (int i=0 ; i<billInfo.length; i++) {
                                String label = billInfo[i].getLabel();
                                String val = billInfo[i].getValue();
                                
                                if (label.equals(skipChar)) {
                                    text = text + val.trim() + "\r\n";
                                } else {
                                    text = text + label.trim() + " " + val.trim() + "\r\n";
                                }
                                
                            }
                            paymentInfo.setDescription(text);
                            if (ubpBillType.equals(billTypeClose)) {
                                BillItemList[] items = response.getBdyBillItems();
                                if (items.length > 0) {
                                    paymentInfo.setAmount(items[0].getBillAmount());
                                } else {
                                    paymentInfo.setAmount(BigDecimal.ZERO);
                                }
                            }
                            break;
                        default:
                            responseMsg = response.getResponseHeader().getHdrResponseMessage();
                            final String errorCode = response.getResponseHeader().getHdrErrorNumber().trim();
                            String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.inquiry.error_code." + companyCode1Val + "." +errorCode);
                            logger.error("Host Error (" + errorCode + ") : " + responseMsg);
                            if (mappingErrorMsg == null) {
                                mappingErrorMsg = responseMsg;
                            }
                            throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }

        } catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
        return paymentInfo;
    }

}
