package nl.strohalm.cyclos.entities.members;

import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.utils.query.QueryParameters;

public class HistoryUpgradeLayananQuery extends QueryParameters{
    /**
	 * 
	 */
	private static final long serialVersionUID = -9111837887684025676L;
	private Period                       period;
    private Member                       member;
    
    private HistoryUpgradeLayanan.Action action;

    
    private Element writer;
    private Element subject;
    private String  comments;

    public String getComments() {
        return comments;
    }

    public Element getSubject() {
        return subject;
    }

    public Element getWriter() {
        return writer;
    }

    public void setComments(final String comments) {
        this.comments = comments;
    }

    public void setSubject(final Element subject) {
        this.subject = subject;
    }

    public void setWriter(final Element writer) {
        this.writer = writer;
    }
    
//    public Element getBy() {
//        return by;
//    }

    public Member getMember() {
        return member;
    }

    public Period getPeriod() {
        return period;
    }
    
    public HistoryUpgradeLayanan.Action getAction() {
        return action;
    }

    public void setAction(final HistoryUpgradeLayanan.Action action) {
        this.action = action;
    }


    public void setMember(final Member member) {
        this.member = member;
    }

    public void setPeriod(final Period period) {
        this.period = period;
    }
    
}
