package nl.strohalm.cyclos.entities.members;

import java.util.Calendar;

import nl.strohalm.cyclos.entities.Entity;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.members.remarks.BrokerRemark;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.entities.members.remarks.Remark;
import nl.strohalm.cyclos.utils.StringValuedEnum;

/**
 * An authorization given to a pending authorized transfer
 * @author luis, Jefferson Magno
 */
public abstract class HistoryUpgradeLayanan extends Entity {

	public static enum Action implements StringValuedEnum {
        APPROVED("A"), REJECTED("R");
        private final String value;

        private Action(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
	
	public static enum Nature implements StringValuedEnum {
        BROKER("B"), GROUP("G");

        private final String value;

        private Nature(final String value) {
            this.value = value;
        }

        public Class<? extends Remark> getType() {
            return this == BROKER ? BrokerRemark.class : GroupRemark.class;
        }

        public String getValue() {
            return value;
        }
    }

    public static enum Relationships implements Relationship {
        WRITER("writer"), SUBJECT("subject");
        private final String name;

        private Relationships(final String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static final long serialVersionUID = 3495444657368796399L;
    private Element           writer;
    private Element           subject;
    private Calendar          date;
    private String            comments;

    public String getComments() {
        return comments;
    }

    public Calendar getDate() {
        return date;
    }

    public abstract Nature getNature();

    public Element getSubject() {
        return subject;
    }

    public Element getWriter() {
        return writer;
    }

    public void setComments(final String comment) {
        comments = comment;
    }

    public void setDate(final Calendar date) {
        this.date = date;
    }

    public void setSubject(final Element subject) {
        this.subject = subject;
    }

    public void setWriter(final Element writer) {
        this.writer = writer;
    }

    @Override
    public String toString() {
        return getId() + " - Remark for " + subject;
    }

}
