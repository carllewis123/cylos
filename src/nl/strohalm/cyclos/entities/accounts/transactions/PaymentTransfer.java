package nl.strohalm.cyclos.entities.accounts.transactions;

import java.math.BigDecimal;
import java.util.Calendar;

import nl.strohalm.cyclos.entities.Entity;

public class PaymentTransfer extends Entity {

	private static final long serialVersionUID = 6444677633848215304L;

	private String ptNumber;
	private String ptNumberUnique;
	private String orderId;
	private String ptType;
	private String ptKey;
	private String mid;
	private String urlCallback;
	private String description;
	private String paymentTicket;
	private int uniqueNo;
	private int timelimit;
	private BigDecimal amount;
	private BigDecimal fee;
	private BigDecimal amountUnique;
	private Calendar createdDate;
	private Calendar expiredDate;
	private String ecashRefNo;
	private String status;

	public String getPtNumber() {
		return ptNumber;
	}

	public void setPtNumber(String ptNumber) {
		this.ptNumber = ptNumber;
	}

	public String getPtNumberUnique() {
		return ptNumberUnique;
	}

	public void setPtNumberUnique(String ptNumberUnique) {
		this.ptNumberUnique = ptNumberUnique;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPtType() {
		return ptType;
	}

	public void setPtType(String ptType) {
		this.ptType = ptType;
	}

	public String getPtKey() {
		return ptKey;
	}

	public void setPtKey(String ptKey) {
		this.ptKey = ptKey;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentTicket() {
		return paymentTicket;
	}

	public void setPaymentTicket(String paymentTicket) {
		this.paymentTicket = paymentTicket;
	}

	public int getUniqueNo() {
		return uniqueNo;
	}

	public void setUniqueNo(int uniqueNo) {
		this.uniqueNo = uniqueNo;
	}

	public int getTimelimit() {
		return timelimit;
	}

	public void setTimelimit(int timelimit) {
		this.timelimit = timelimit;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getAmountUnique() {
		return amountUnique;
	}

	public void setAmountUnique(BigDecimal amountUnique) {
		this.amountUnique = amountUnique;
	}

	public Calendar getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public Calendar getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Calendar expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getEcashRefNo() {
		return ecashRefNo;
	}

	public void setEcashRefNo(String ecashRefNo) {
		this.ecashRefNo = ecashRefNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PaymentTransfer [ptNumber=" + ptNumber + ", ptNumberUnique="
				+ ptNumberUnique + ", orderId=" + orderId + ", ptType="
				+ ptType + ", ptKey=" + ptKey + ", mid=" + mid
				+ ", urlCallback=" + urlCallback + ", description="
				+ description + ", paymentTicket=" + paymentTicket
				+ ", uniqueNo=" + uniqueNo + ", timelimit=" + timelimit
				+ ", amount=" + amount + ", fee=" + fee + ", amountUnique="
				+ amountUnique + ", createdDate=" + createdDate
				+ ", expiredDate=" + expiredDate + ", ecashRefNo=" + ecashRefNo
				+ ", status=" + status + "]";
	}

}
