package nl.strohalm.cyclos.entities.accounts.transactions;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import nl.strohalm.cyclos.entities.Entity;

public class PaymentTransferHistories extends Entity{
	
	private static final long serialVersionUID = -4214727166097246143L;
	
	private String orderId;
	private String ptType;
	private String ptKey;
	private BigDecimal amount;
	private BigDecimal fee;
	private int uniqueNo;
	private BigDecimal amountUnique;
	private String ptNumber;
	private String ptNumberUnique;
	private int timelimit;
	private String mid;
	private String urlCallback;
	private String description;
	private Calendar createdDate;
	private Calendar expiredDate;
	private String ecashRefNo;
	private String status;
	private String statusDesc;
	private String paymentTicket;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPtType() {
		return ptType;
	}
	public void setPtType(String ptType) {
		this.ptType = ptType;
	}
	public String getPtKey() {
		return ptKey;
	}
	public void setPtKey(String ptKey) {
		this.ptKey = ptKey;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public int getUniqueNo() {
		return uniqueNo;
	}
	public void setUniqueNo(int uniqueNo) {
		this.uniqueNo = uniqueNo;
	}
	public BigDecimal getAmountUnique() {
		return amountUnique;
	}
	public void setAmountUnique(BigDecimal amountUnique) {
		this.amountUnique = amountUnique;
	}
	public String getPtNumber() {
		return ptNumber;
	}
	public void setPtNumber(String ptNumber) {
		this.ptNumber = ptNumber;
	}
	public String getPtNumberUnique() {
		return ptNumberUnique;
	}
	public void setPtNumberUnique(String ptNumberUnique) {
		this.ptNumberUnique = ptNumberUnique;
	}
	public int getTimelimit() {
		return timelimit;
	}
	public void setTimelimit(int timelimit) {
		this.timelimit = timelimit;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getUrlCallback() {
		return urlCallback;
	}
	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Calendar getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}
	public Calendar getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Calendar expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getEcashRefNo() {
		return ecashRefNo;
	}
	public void setEcashRefNo(String ecashRefNo) {
		this.ecashRefNo = ecashRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getPaymentTicket() {
		return paymentTicket;
	}
	public void setPaymentTicket(String paymentTicket) {
		this.paymentTicket = paymentTicket;
	}
	
	@Override
	public String toString() {
		return "PaymentTransferHistories [orderId=" + orderId + ", ptType="
				+ ptType + ", ptKey=" + ptKey + ", amount=" + amount + ", fee="
				+ fee + ", uniqueNo=" + uniqueNo + ", amountUnique="
				+ amountUnique + ", ptNumber=" + ptNumber + ", ptNumberUnique="
				+ ptNumberUnique + ", timelimit=" + timelimit + ", mid=" + mid
				+ ", urlCallback=" + urlCallback + ", description="
				+ description + ", createdDate=" + createdDate
				+ ", expiredDate=" + expiredDate + ", ecashRefNo=" + ecashRefNo
				+ ", status=" + status + ", statusDesc=" + statusDesc
				+ ", paymentTicket=" + paymentTicket + "]";
	}
	
}
