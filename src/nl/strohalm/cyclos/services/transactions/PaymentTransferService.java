package nl.strohalm.cyclos.services.transactions;

import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferProcessingRequest;

import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.services.Service;

public interface PaymentTransferService extends Service {

	PaymentTransfer generate(PaymentTransfer request);
	
	
	void move(PaymentTransferProcessingRequest request);
	
	
	PaymentTransfer load(String ptNumberUnique, String paymenTicket);
	
}
