package nl.strohalm.cyclos.services.transactions;

import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferProcessingRequest;
import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferRequest;

import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransferHistories;
import nl.strohalm.cyclos.services.BaseServiceSecurity;

public class PaymentTransferServiceSecurity extends BaseServiceSecurity
		implements PaymentTransferService {

	private PaymentTransferService paymentTransferServiceSecurity;

	@Override
	public PaymentTransfer generate(PaymentTransfer request) {
		return paymentTransferServiceSecurity.generate(request);
	}

	@Override
	public void move(PaymentTransferProcessingRequest request) {
		paymentTransferServiceSecurity.move(request);
	}

	@Override
	public PaymentTransfer load(String ptNumberUnique, String paymentTicket) {
		return paymentTransferServiceSecurity.load(ptNumberUnique, paymentTicket);
	}

}
