package nl.strohalm.cyclos.services.transactions.exceptions;

import java.math.BigDecimal;
import nl.strohalm.cyclos.exceptions.ApplicationException;

public class MinAmountTransactionException extends ApplicationException{
	private static final long serialVersionUID = -3857833713671383524L;
	private BigDecimal amount;

	  public MinAmountTransactionException(BigDecimal amount)
	  {
	    this.amount = amount;
	  }

	  public BigDecimal getAmount() {
	    return this.amount;
	  }
	  public void setAmount(BigDecimal amount) {
	    this.amount = amount;
	  }
	}