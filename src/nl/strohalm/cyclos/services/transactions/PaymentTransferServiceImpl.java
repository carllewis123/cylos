package nl.strohalm.cyclos.services.transactions;

import java.io.IOException;
import java.math.BigDecimal;
import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferProcessingRequest;
import nl.strohalm.cyclos.dao.accounts.transactions.PaymentTransferDAO;
import nl.strohalm.cyclos.dao.accounts.transactions.PaymentTransferHistoriesDAO;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransferHistories;
import nl.strohalm.cyclos.entities.exceptions.DaoException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

public class PaymentTransferServiceImpl implements PaymentTransferService {

	private static final int TICKET_SIZE = 32;
	private static final String POSSIBLE_TICKET_CHARS = "0123456879ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static int UNIQUE_NO_DIGIT =1;
	private static final String POSSIBLE_UNIQUE_NO_CHARS = "0123456789";
	private final Log logger = LogFactory.getLog(PaymentTransferServiceImpl.class);
	private PaymentTransferDAO paymentTransferDao;
	private PaymentTransferHistoriesDAO paymentTransferHistoriesDao;

	public PaymentTransferServiceImpl(){
		try{
			UNIQUE_NO_DIGIT = Integer.parseInt(EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"digit.generate.ptnumber"));
		} catch (IOException e) {
			logger.error(e);
		}
	}

	@Override
	public PaymentTransfer generate(final PaymentTransfer request) 
	{

		PaymentTransfer paymentTransfer = new PaymentTransfer();
		BigDecimal amountUnique = request.getAmountUnique();
		String ptNumberUnique = request.getPtNumberUnique();
		int uniqueNo = 0;
		int maxUniqueNo = (int) Math.pow(10, UNIQUE_NO_DIGIT);
		boolean retry=false;
		int count=0;
		
		while (!retry && count <= maxUniqueNo) {
		try {
			

			while (paymentTransferDao.existPtNumber(ptNumberUnique)) {

				if (paymentTransferDao.allPtNumberExist(request.getPtKey(),
							request.getAmount()) >= maxUniqueNo) {
					paymentTransfer.setStatus("full");
					
					return paymentTransfer;
				} else {
					uniqueNo = Integer.parseInt(RandomStringUtils.random(UNIQUE_NO_DIGIT,POSSIBLE_UNIQUE_NO_CHARS));
					amountUnique = request.getAmountUnique().add(
							new BigDecimal(uniqueNo));
					ptNumberUnique = (request.getPtType().concat((request
							.getPtKey()).concat(amountUnique.toString())));

				}
			}
			paymentTransfer.setAmountUnique(amountUnique);
			paymentTransfer.setPtNumberUnique(ptNumberUnique);
			paymentTransfer.setUniqueNo(uniqueNo);
			paymentTransfer.setPtNumber(request.getPtNumber());
			paymentTransfer.setOrderId(request.getOrderId());
			paymentTransfer.setPtType(request.getPtType());
			paymentTransfer.setPtKey(request.getPtKey());
			paymentTransfer.setMid(request.getMid());
			paymentTransfer.setUrlCallback(request.getUrlCallback());
			paymentTransfer.setDescription(request.getDescription());
			paymentTransfer.setTimelimit(request.getTimelimit());
			paymentTransfer.setAmount(request.getAmount());
			paymentTransfer.setFee(request.getFee());
			paymentTransfer.setCreatedDate(request.getCreatedDate());
			paymentTransfer.setExpiredDate(request.getExpiredDate());
			paymentTransfer.setPaymentTicket(generateTicket());
			paymentTransferDao.insert(paymentTransfer);
			
			retry=true;
			count++;
			
		}catch(DaoException e){
			Throwable th1 = e.getCause();
			Throwable th2 = th1.getCause();
			Throwable th3 = th2.getCause();
			 if(th1 !=null && th1 instanceof DataIntegrityViolationException){
				 if(th2 !=null && th2 instanceof ConstraintViolationException){
					 if(th3 !=null && th3.getMessage().toString().contains("Duplicate entry")){
						 retry=false;
						 paymentTransfer=null;
					 }
				 }
			 }
			 else{
				 retry=true;
				 paymentTransfer=null;
			 }
		
		}catch (Exception e) {
			retry=true;
			paymentTransfer=null;
		}
	}
		return paymentTransfer;

	}

	private String generateTicket() {
		String paymentTicket = null;
		while (paymentTicket == null
				|| paymentTransferDao.existTicket(paymentTicket)) {
			paymentTicket = RandomStringUtils.random(TICKET_SIZE,
					POSSIBLE_TICKET_CHARS);
		}
		return paymentTicket;
	}
	
	@Override
	public void move(PaymentTransferProcessingRequest request) {
	
		PaymentTransferHistories histories = new PaymentTransferHistories();
		PaymentTransfer payment = load(request.getPtNumberUnique(), null);
		
		try {
			histories.setOrderId(payment.getOrderId());
			histories.setAmount(payment.getAmount());
			histories.setFee(payment.getFee());
			histories.setPtKey(payment.getPtKey());
			histories.setPtType(payment.getPtType());
			histories.setPtNumber(payment.getPtNumber());
			histories.setPtNumberUnique(payment.getPtNumberUnique());
			histories.setDescription(payment.getDescription());
			histories.setUrlCallback(payment.getUrlCallback());
			histories.setUniqueNo(payment.getUniqueNo());
			histories.setAmountUnique(payment.getAmountUnique());
			histories.setEcashRefNo(payment.getEcashRefNo());
			histories.setPaymentTicket(payment.getPaymentTicket());
			histories.setTimelimit(payment.getTimelimit());
			histories.setCreatedDate(payment.getCreatedDate());
			histories.setExpiredDate(payment.getExpiredDate());
			histories.setMid(payment.getMid());
			histories.setStatus(request.getStatus());
			histories.setStatusDesc(request.getStatusDesc());
			
			
			paymentTransferHistoriesDao.insert(histories);
			paymentTransferDao.delete(payment.getId());
		}
		catch(Exception e) {
			throw e;
		}
		
	}
	
	@Override
	public PaymentTransfer load(String ptNumberUnique, String paymentTicket) {		
		return paymentTransferDao.load(ptNumberUnique, paymentTicket);
	}
	
	
	public void setPaymentTransferDao(PaymentTransferDAO paymentTransferDao) {
		this.paymentTransferDao = paymentTransferDao;
	}

	public void setPaymentTransferHistoriesDao(
			PaymentTransferHistoriesDAO paymentTransferHistoriesDao) {
		this.paymentTransferHistoriesDao = paymentTransferHistoriesDao;
	}

}
