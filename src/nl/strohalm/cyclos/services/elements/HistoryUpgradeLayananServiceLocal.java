package nl.strohalm.cyclos.services.elements;

import java.util.List;

import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;

public interface HistoryUpgradeLayananServiceLocal extends HistoryUpgradeLayananService {
	HistoryUpgradeLayanan load (Long id, Relationship... fetcch) throws EntityNotFoundException;
}
