package nl.strohalm.cyclos.services.elements;

import java.util.List;

import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.services.Service;

public interface HistoryUpgradeLayananService extends Service {
	
	List<HistoryUpgradeLayanan> searchAuthorizations(HistoryUpgradeLayananQuery query);
	
	List<GroupRemark> searchByAgent(HistoryUpgradeLayananQuery query);

}
