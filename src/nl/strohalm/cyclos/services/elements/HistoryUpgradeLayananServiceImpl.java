package nl.strohalm.cyclos.services.elements;

import java.util.List;

import nl.strohalm.cyclos.dao.members.HistoryUpgradeLayananDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayanan;
import nl.strohalm.cyclos.entities.members.HistoryUpgradeLayananQuery;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.remarks.GroupRemark;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.utils.access.LoggedUser;

public class HistoryUpgradeLayananServiceImpl implements HistoryUpgradeLayananServiceLocal{
	
	private FetchServiceLocal            fetchService;
	private HistoryUpgradeLayananDAO	 historyUpgradeLayananDao;
	
	@Override
	public List<HistoryUpgradeLayanan> searchAuthorizations(
			HistoryUpgradeLayananQuery query) {
		
		return historyUpgradeLayananDao.search(query);
	}

	@Override
	public HistoryUpgradeLayanan load(Long id, Relationship... fetcch)
			throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setFetchServiceLocal(final FetchServiceLocal fetchService) {
		this.fetchService = fetchService;
    }
	
	public void setHistoryUpgradeLayananDao(final HistoryUpgradeLayananDAO historyUpgradeLayananDao) {
        this.historyUpgradeLayananDao = historyUpgradeLayananDao;
    }

	@Override
	public List<GroupRemark> searchByAgent(HistoryUpgradeLayananQuery query) {
		// TODO Auto-generated method stub
		return historyUpgradeLayananDao.searchByAgent(query);
	}

}
