/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.customfieldvalues.validations;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import nl.strohalm.cyclos.dao.customizations.CustomFieldValueDAO;
import nl.strohalm.cyclos.utils.CustomFieldsContainer;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.customization.fields.CustomField;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.utils.validation.PropertyValidation;
import nl.strohalm.cyclos.utils.validation.ValidationError;

public class MaxDuplicateValueValidation implements PropertyValidation {

    private static final long serialVersionUID = 9086596157675900136L;

    @Override
    public ValidationError validate(Object object, Object data, Object value) {
        if (!(object instanceof CustomFieldsContainer<?, ?>)) {
            return null;
        }
        if (object instanceof Transfer && ((Transfer) object).getScheduledPayment() != null) {
            // We cannot validate unique on scheduled payment installments, as all installments share the same values, and would always fail
            return null;
        }
        final CustomField field = (CustomField) data;
        final String string = (String) value;
        if (StringUtils.isNotEmpty(string)) {
            // Build a field value
            CustomFieldValue fieldValue;
            try {
                fieldValue = field.getNature().getValueType().newInstance();
            } catch (final Exception e) {
                throw new RuntimeException(e);
            }
            fieldValue.setField(field);
            fieldValue.setOwner(object);
            fieldValue.setStringValue(string);

            String params = field.getDescription();
            Integer maxDuplicateLimit = Integer.parseInt("2");
            if (params.contains("com.ptdam.customfieldvalues.validations.MaxDuplicateValueValidation=")) {
                try {
                    maxDuplicateLimit = Integer.parseInt(params.replace("com.ptdam.customfieldvalues.validations.MaxDuplicateValueValidation=", ""));
                }
                catch (final Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            // Check uniqueness
            Member member = (Member) object;
            if (isModifiedValue(member.getOldCustomValues(), fieldValue)) {
                if (customFieldValueDao.numberOfDuplicateValue(fieldValue) >= maxDuplicateLimit) {
                    return new MaxValueDuplicateError(fieldValue.getStringValue(), Integer.toString(maxDuplicateLimit));
                }
            }
        }
        return null; 
    }
    
    protected CustomFieldValueDAO         customFieldValueDao;

    public final void setCustomFieldValueDao(final CustomFieldValueDAO customFieldValueDao) {
        this.customFieldValueDao = customFieldValueDao;
    }

    private boolean isModifiedValue(Collection<MemberCustomFieldValue> oldFieldValues, CustomFieldValue newValue) {
        
        if (oldFieldValues != null) {
            for (MemberCustomFieldValue oldValue : oldFieldValues) {
                if (oldValue.getField().getInternalName().equals(newValue.getField().getInternalName())) { 
                    if (oldValue.getStringValue().equals(newValue.getStringValue())) {
                        return false;
                    }
                    break;
                }
            }
        }
        return true;
    }
    
}
