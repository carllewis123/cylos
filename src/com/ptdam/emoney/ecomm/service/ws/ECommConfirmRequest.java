/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.ecomm.service.ws;

import java.io.Serializable;

public class ECommConfirmRequest implements Serializable {

    private static final long serialVersionUID = -8862490239706688085L;

    String                    mercID;
    String                    mobileNo;
    String                    otp;
    String                    trxID;
    public String getMercID() {
        return mercID;
    }
    public void setMercID(String mercID) {
        this.mercID = mercID;
    }
    public String getMobileNo() {
        return mobileNo;
    }
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
    public String getOtp() {
        return otp;
    }
    public void setOtp(String otp) {
        this.otp = otp;
    }
    public String getTrxID() {
        return trxID;
    }
    public void setTrxID(String trxID) {
        this.trxID = trxID;
    }
    
}
