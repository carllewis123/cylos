/*
]
\    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.ecomm.service.ws;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import nl.strohalm.cyclos.dao.accounts.transactions.TicketDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentRequestTicket;
import nl.strohalm.cyclos.entities.accounts.transactions.Ticket;
import nl.strohalm.cyclos.entities.accounts.transactions.WebShopTicket;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.DaoException;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group.Status;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.access.ChannelServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.TicketServiceLocal;
import nl.strohalm.cyclos.webservices.payments.*;
import nl.strohalm.cyclos.services.access.exceptions.*;
import nl.strohalm.cyclos.services.transactions.exceptions.PaymentException;
import nl.strohalm.cyclos.services.transfertypes.TransferTypeServiceLocal;
import nl.strohalm.cyclos.utils.HashHandler;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;
import nl.strohalm.cyclos.webservices.model.PaymentRequestTicketVO;
import nl.strohalm.cyclos.webservices.model.WebShopTicketVO;
import nl.strohalm.cyclos.webservices.utils.TicketHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;
import nl.strohalm.cyclos.webservices.webshop.GenerateWebShopTicketParams;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.ecomm.webservices.transaction.EcommConfirmationWebService;
import com.ptdam.emoney.ecomm.webservices.transaction.EcommResponse;

@WebService(name = "ecommTransaction", serviceName = "ecommTransaction")
public class ECommTransactionImpl implements ECommTransaction {

    private static final String PIPE_REGEX = "\\|";
    private static final String           PIPE_SEP            = "|";
    private static final String           KEYWORD_REF         = "ref:";
    final Log                             logger              = LogFactory.getLog(ECommTransactionImpl.class);
    private static final boolean          NOT_MERCHANT_MEMBER = false;

    private static final Relationship[]   FETCH               = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private static final String           ALGO                = "AES";
    private String                        scrambled;

    private byte[]                        keyValue            = new byte[] { 'T', 'h', 'e', 'B', 'e', 's', 't',
                                                              'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y' };

    private static final String           MOBILE_PHONE        = "mobilePhone";
    private String                        urlEcommConfirmAdapter;
    private EcommConfirmationWebService   eCommConfirmationWebService;
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private HashHandler                   hashHandler;
    private TicketServiceLocal            ticketServiceLocal;
    private WebServiceHelper              webServiceHelper;
    private TicketHelper                  ticketHelper;
    protected TransferTypeServiceLocal    transferTypeServiceLocal;
    private TicketDAO                     ticketDao;
    private ChannelServiceLocal           channelServiceLocal;                                                                                                                                ;
    private String                        eCommChannelTo;
    private String                        eCommChannelFrom;

    public ECommTransactionImpl() {
        try {

            urlEcommConfirmAdapter = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.ecomm.urlEcommConfirmAdapter");
            eCommChannelTo = EmoneyConfiguration.getEmoneyProperties().getProperty("ecomm.channel.id.to", "auto");
            eCommChannelFrom = EmoneyConfiguration.getEmoneyProperties().getProperty("ecomm.channel.id.from", Channel.WEB);

        } catch (IOException e) {
            this.webServiceHelper.error(e); 
        }
    }

    @Override
    public EcommPaymentResponse payment(ECommPaymentRequest ecPayReq) {
        EcommPaymentResponse eCommPaymentResponse = new EcommPaymentResponse();
        // load ticket
        WebShopTicket wst = ticketServiceLocal.load(ecPayReq.getTicketvo(), FETCH);

        try {

            try {

                //validatePassword(ecPayReq.getMobileNo(), decrypt(ecPayReq.getPassword()));
                String mobilePhone = userToPhone(ecPayReq.getMobileNo(), NOT_MERCHANT_MEMBER);
                ecPayReq.setMobileNo(mobilePhone);

            } catch (Exception e) {
                logger.error(e);
                this.webServiceHelper.error(e);
                eCommPaymentResponse.setRespCode("2002");
                eCommPaymentResponse.setRespMsg("ACCES_DENIED");
                return eCommPaymentResponse;
            }

            eCommPaymentResponse = requestPayment(wst, ecPayReq);

        } catch (PaymentException e) {
            logger.error(e, e);
            this.webServiceHelper.error(e);
            eCommPaymentResponse.setRespMsg(e.getClass().getName());
        } catch (WebServiceException e) {
            logger.error(e, e);
            this.webServiceHelper.error(e);
            eCommPaymentResponse.setRespCode("2998");
            eCommPaymentResponse.setRespMsg("CONNECTION_ERROR");
        } catch (Exception e) {
            logger.error(e, e);
            this.webServiceHelper.error(e);
            eCommPaymentResponse.setRespCode("2999");
            eCommPaymentResponse.setRespMsg("UNKOWN_ERROR");
        }

        return eCommPaymentResponse;
    }

    private EcommPaymentResponse requestPayment(WebShopTicket wst, ECommPaymentRequest ecPayReq) throws Exception {

        EcommPaymentResponse result = new EcommPaymentResponse();

        // payment request
        wst.setClientAddress(ecPayReq.getClientAddress());
        Member member = toMember(ecPayReq.getMobileNo());
        //wst.setFrom(member);
        PaymentRequestTicket prTicket = new PaymentRequestTicket();

        prTicket.setAmount(wst.getAmount());
        prTicket.setDescription(wst.getDescription());
        prTicket.setFrom(member);
        prTicket.setTo(wst.getTo());

        prTicket.setFromChannel(channelServiceLocal.loadByInternalName(eCommChannelFrom));

        if ("auto".equals(eCommChannelTo)) {
            prTicket.setToChannel(channelServiceLocal.getSmsChannel());
        } else {
            prTicket.setToChannel(channelServiceLocal.loadByInternalName(eCommChannelTo));
        }

        prTicket = ticketServiceLocal.generate(prTicket);
        // expect description in this format: description|trid
        String data[] = wst.getDescription().split(PIPE_REGEX);
        // ref pr ticket|original desc|trace number ecash|no hp pelanggan|trxid merc|status
        String descWST = KEYWORD_REF + prTicket.getTicket();
        descWST = descWST + PIPE_SEP + data[0];
        descWST = descWST + PIPE_SEP + prTicket.getId();
        descWST = descWST + PIPE_SEP + prTicket.getFrom().getUsername();
        descWST = descWST + PIPE_SEP + data[1];
        descWST = descWST + PIPE_SEP;
        wst.setDescription(descWST);
        wst.setStatus(Ticket.Status.EXPIRED);

        final WebShopTicket persistentTicket = ticketDao.update(wst);

        BeanUtils.copyProperties(result, persistentTicket);

        result.setRespCode("00");
        result.setTicketID(wst.getTicket());
        result.setRespMsg(prTicket.getStatus().getValue());
        result.setTrxID(ecPayReq.trxID);
        result.setTicketPR(prTicket.getTicket());

        return result;
    }

    public Member toMember(String userName) throws Exception {
        if (userName == null || userName.isEmpty()) {
            return null;
        }

        User user;

        user = elementServiceLocal.loadUser(userName,
                RelationshipHelper.nested(User.Relationships.ELEMENT,
                        Element.Relationships.GROUP), RelationshipHelper
                        .nested(User.Relationships.ELEMENT,
                                Member.Relationships.CUSTOM_VALUES),
                RelationshipHelper.nested(User.Relationships.ELEMENT,
                        Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception("Not recognized user: " + user);
        }

        Member member = ((MemberUser) user).getMember();
        if (!member.isActive()
                || member.getGroup().getStatus() == Status.REMOVED) {
            throw new EntityNotFoundException();
        }
        return member;
    }

    /**
     * @param eTrxRequest
     * @throws Exception
     */
    private void validatePassword(String username, String custPassword) throws Exception {

        User user = elementServiceLocal.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception();
        }

        // password
        final String password = hashHandler.hash(user.getSalt(), custPassword);
        final String userPassword = user.getPassword();
        if (userPassword == null || !userPassword.equalsIgnoreCase(StringUtils.trimToNull(password))) {
            throw new Exception("access denied");
        }

    }

    /**
     * @param eTrxRequest
     * @throws Exception
     */
    private String userToPhone(String username, boolean isMerchant) throws Exception {
        User user;

        user = elementServiceLocal.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception();
        }

        Member member = ((MemberUser) user).getMember();
        if (!member.isActive() || member.getGroup().getStatus() == Status.REMOVED) {
            throw new EntityNotFoundException();
        }
        List<MemberCustomField> fields = memberCustomFieldServiceLocal.list();

        // get field id mobile
        int size = fields.size();
        String iNameMobile = null;
        for (int i = 0; i < size; i++) {
            if (MOBILE_PHONE.equals(fields.get(i).getInternalName())) {
                iNameMobile = fields.get(i).getName();
                break;
            }
        }

        final Collection<MemberCustomFieldValue> customValues = ((Member) elementServiceLocal.load(member.getId(), Member.Relationships.CUSTOM_VALUES)).getCustomValues();

        List<MemberCustomFieldValue> list = new ArrayList<MemberCustomFieldValue>(customValues);

        size = list.size();
        MemberCustomFieldValue temp = null;
        String test = null;
        String mobilPhone = null;
        for (int i = 0; i < size; i++) {
            try {
                temp = (MemberCustomFieldValue) list.get(i);
                test = temp.toString();
                // Retrieve mobile phone
                if (test != null && iNameMobile != null && test.contains(iNameMobile)) {
                    mobilPhone = temp.getValue();
                }

            } catch (Exception e) {
                logger.debug(e, e);
            }
        }

        return (mobilPhone);

    }

    @Override
    public EcommConfirmResponse confirm(ECommConfirmRequest ecConfirmReq) {
        EcommConfirmResponse eCommConfirmResponse = new EcommConfirmResponse();
        try {

            com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest eConfirmRequest = new com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest();

            BeanUtils.copyProperties(eConfirmRequest, ecConfirmReq);

            try {
                String mobilePhone = userToPhone(eConfirmRequest.getMobileNo(), NOT_MERCHANT_MEMBER);
                eConfirmRequest.setMobileNo(mobilePhone);
    	

            } catch (Exception e) {
                e.printStackTrace();
                eCommConfirmResponse.setRespCode("2002");
                eCommConfirmResponse.setRespMsg("ACCES_DENIED");
                return eCommConfirmResponse;
            }

            EcommResponse payResponse = getEcommConfirmWebService().confirmPayment(eConfirmRequest);

            BeanUtils.copyProperties(eCommConfirmResponse, payResponse);
            eCommConfirmResponse.setTrxID(payResponse.getTransactionNumber());


        } catch (WebServiceException e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2998");
            eCommConfirmResponse.setRespMsg("CONNECTION_ERROR");
        } catch (Exception e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2999");
            eCommConfirmResponse.setRespMsg("UNKOWN_ERROR");
        }
        return eCommConfirmResponse;
    }

    
    @Override
    public EcommConfirmResponse confirmAgent(ECommConfirmRequest ecConfirmReq) {
    EcommConfirmResponse eCommConfirmResponse = new EcommConfirmResponse();
    try {

        com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest eConfirmRequest = new com.ptdam.emoney.ecomm.webservices.transaction.ECommConfirmRequest();
        // //FIXME: lazy approach pass trough?
        BeanUtils.copyProperties(eConfirmRequest, ecConfirmReq);

        try {
            String mobilePhone = userToPhone(eConfirmRequest.getMobileNo(), NOT_MERCHANT_MEMBER);
            eConfirmRequest.setMobileNo(mobilePhone);

        } catch (Exception e) {
            e.printStackTrace();
            eCommConfirmResponse.setRespCode("2002");
            eCommConfirmResponse.setRespMsg("ACCES_DENIED");
            return eCommConfirmResponse;
        }

        EcommResponse payResponse = getEcommConfirmWebService().confirmPaymentAgent(eConfirmRequest);

        BeanUtils.copyProperties(eCommConfirmResponse, payResponse);

    } catch (WebServiceException e) {
        e.printStackTrace();
        eCommConfirmResponse.setRespCode("2998");
        eCommConfirmResponse.setRespMsg("CONNECTION_ERROR");
    } catch (Exception e) {
        e.printStackTrace();
        eCommConfirmResponse.setRespCode("2999");
        eCommConfirmResponse.setRespMsg("UNKOWN_ERROR");
    }
    return eCommConfirmResponse;
}

    @Override
    public EcommValidateResponse ticketValidation(String ticket) {
        EcommValidateResponse validResp = new EcommValidateResponse();

       // try {

            WebShopTicket ticketObj = ticketServiceLocal.load(ticket);

            // expect ticket webshop
            if (ticketObj instanceof WebShopTicket) {
                final WebShopTicket object = (WebShopTicket) ticketObj;

                // Check the member restriction
                final Member restricted = WebServiceContext.getMember();
                if (restricted != null && !restricted.equals(object.getTo())) {
                    throw new IllegalArgumentException("Error getting webshop ticket: the target member ('to') of the webshop ticket is not the restricted one");
                }

                WebShopTicketVO ticketVO = ticketHelper.toVO(object, WebServiceContext.getChannel().getPrincipalCustomFields());

                validResp.setCode("2999");
                String status = "UNKNOWN STATUS";

                if (ticketVO.getAwaitingAuthorization()) {
                    validResp.setCode("2999");
                    status = "Awaiting Authorization".toUpperCase();
                } else if (ticketVO.getCancelled()) {
                    validResp.setCode("2999");
                    status = "Cancelled".toUpperCase();

                } else if (ticketVO.getExpired()) {
                    validResp.setCode("2999");
                    status = "Expired".toUpperCase();

                } else if (ticketVO.getPending()) {
                    validResp.setCode("2999");
                    status = "Pending".toUpperCase();

                } else if (ticketVO.getOk()) {
                    validResp.setCode("00");
                    status = "SUCCESS";

                }
                validResp.setStatus(status);
                validResp.setTicket(ticketVO.getTicket());
            }

            // load ref ticket payment request
            String data[] = ticketObj.getDescription().split(PIPE_REGEX);
            String prticket = data[0].replace(KEYWORD_REF,"");
            
            PaymentRequestTicket ticketPr;
			try {
				ticketPr = ticketServiceLocal.load(prticket);
			      if (ticketPr instanceof PaymentRequestTicket) {
		                PaymentRequestTicket ticketData = (PaymentRequestTicket) ticketPr;
		                validResp.setPrcode(ticketData.getStatus().getValue());
		                validResp.setPrmsg(ticketData.getStatus().name());
		                validResp.setPrstatus(ticketData.getStatus().getValue());
		                validResp.setPrticket(ticketData.getTicket());
			      }
			} catch (final EntityNotFoundException e) {
                validResp.setPrcode("null");
                validResp.setPrmsg("null");
                validResp.setPrstatus("null");
                validResp.setPrticket("null");
				//e.printStackTrace();
			}
	        return validResp;
            }
//        } catch (final Exception e) {
//            e.printStackTrace();
//            validResp.setCode("2999");
//            validResp.setMsg("Unknown Error");
//
//        }
//    }

    private EcommConfirmationWebService getEcommConfirmWebService() {

        if (eCommConfirmationWebService == null) {
            try {
                eCommConfirmationWebService = ExternalWebServiceHelper.proxyFor(EcommConfirmationWebService.class, urlEcommConfirmAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return eCommConfirmationWebService;
    }

    /**
     * @see com.ptdam.ecomm.gateway.service.ServiceSecurity#decrypt(java.lang.String)
     */
    private String decrypt(String encryptedData) throws Exception {
        if ("false".equals(scrambled)) {
            return encryptedData;
        }
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.decodeBase64(encryptedData.getBytes());
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private Key generateKey() throws Exception {
        String text1 = new String(keyValue, "UTF-8");
        char[] chars = text1.toCharArray();
        java.util.Date date = new java.util.Date();
        String keyStr = Integer.toString(date.getYear()) + Integer.toString(date.getMonth()) + Integer.toString(date.getDate());
        int n = 16 - keyStr.length();
        if (keyStr.length() < 16) {
            for (int i = 0; i < n; i++) {
                keyStr = keyStr + chars[i];
            }
        }
        Key key = new SecretKeySpec(keyStr.getBytes(), ALGO);
        return key;
    }

    public void setScrambled(String scrambled) {
        this.scrambled = scrambled;
    }

    /**
     * Generate webshop ticket: expect paramas description in this format: trid|description expect params original description replace | to _ provide
     * description with merchant name in this order : trid|description
     */
    @Override
    public String generate(GenerateEcommTicketParams params) {
        try {
            GenerateWebShopTicketParams gwt = new GenerateWebShopTicketParams();
            BeanUtils.copyProperties(gwt, params);
            final WebShopTicket ticket = ticketHelper.toTicket(gwt);
            ticket.setMemberAddress(params.getMemberAddress());
            final WebShopTicket wst = ticketServiceLocal.generate(ticket);
            return wst.getTicket();
        } catch (final Exception e) {
            webServiceHelper.error(e);
            return null;
        }
    }

    @Override
    public ECommTicketVO get(String id) {
        try {

            Object ot = ticketServiceLocal.load(id);

            if (ot instanceof WebShopTicket) {
                final WebShopTicket ticketWo = (WebShopTicket) ot;
                // Check the member restriction
                final Member restricted = WebServiceContext.getMember();
                if (restricted != null && !restricted.equals(ticketWo.getTo())) {
                    throw new IllegalArgumentException("Error getting webshop ticket: the target member ('to') of the webshop ticket is not the restricted one");
                }

                WebShopTicketVO wvo = ticketHelper.toVO(ticketWo, WebServiceContext.getChannel().getPrincipalCustomFields());
                ECommTicketVO dest = new ECommTicketVO();
                BeanUtils.copyProperties(dest, wvo);
                return dest;
            } else if (ot instanceof PaymentRequestTicket) {

                PaymentRequestTicket prTicket = (PaymentRequestTicket) ot;

                PaymentRequestTicketVO prvo = ticketHelper.toVO(prTicket, null);

                ECommTicketVO dest = new ECommTicketVO();
                BeanUtils.copyProperties(dest, prvo);
                return dest;

            }
        } catch (final Exception e) {
            webServiceHelper.error(e);

        }
        return null;
    }

    @Override
    public boolean validUser(UserInfo userInfo) {
        boolean result = false;

        try {
            validatePassword(userInfo.user, decrypt(userInfo.password));
            result = true;
        } catch (Exception e) {
            this.webServiceHelper.error(userInfo.user + ":" + e.getMessage());
            logger.error(userInfo.user + ":" + e.getMessage(), e);
        }

        return result;
    }

    public void setUrlEcommConfirmAdapter(String urlEcommConfirmAdapter) {
        this.urlEcommConfirmAdapter = urlEcommConfirmAdapter;
    }

    public void setMemberCustomFieldServiceLocal(MemberCustomFieldServiceLocal memberCustomFieldServiceLocal) {
        this.memberCustomFieldServiceLocal = memberCustomFieldServiceLocal;
    }

    public void setElementServiceLocal(ElementServiceLocal elementServiceLocal) {
        this.elementServiceLocal = elementServiceLocal;
    }

    public void setWebServiceHelper(WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }

    public void setKeyValue(byte[] keyValue) {
        this.keyValue = keyValue;
    }

    public void seteCommConfirmationWebService(EcommConfirmationWebService eCommConfirmationWebService) {
        this.eCommConfirmationWebService = eCommConfirmationWebService;
    }

    public void setHashHandler(HashHandler hashHandler) {
        this.hashHandler = hashHandler;
    }

    public void setTicketServiceLocal(TicketServiceLocal ticketServiceLocal) {
        this.ticketServiceLocal = ticketServiceLocal;
    }

    public void setTicketHelper(TicketHelper ticketHelper) {
        this.ticketHelper = ticketHelper;
    }

    public void setTransferTypeServiceLocal(TransferTypeServiceLocal transferTypeServiceLocal) {
        this.transferTypeServiceLocal = transferTypeServiceLocal;
    }

    public void setTicketDao(TicketDAO ticketDao) {
        this.ticketDao = ticketDao;
    }

    public void setChannelServiceLocal(ChannelServiceLocal channelServiceLocal) {
        this.channelServiceLocal = channelServiceLocal;
    }
    
	private String cleanRef(String description) {
		// clean up ref
		String result = description.replace(KEYWORD_REF, "");

		return result;

	}
	@Override
	public boolean expire(String id) {
		boolean result = false;
		String pticket = new String();
        try {
			WebShopTicket wst = ticketServiceLocal.load(id, FETCH);
			if(wst.getStatus() == Ticket.Status.EXPIRED){
		        String desc[] = wst.getDescription().split(PIPE_REGEX);
		        // ref pr ticket|original desc|trac e number ecash|no hp pelanggan|trxid merc|status
				pticket = cleanRef(desc[0]);
				PaymentRequestTicket PRticket = ticketServiceLocal.load(pticket, FETCH);
				if(PRticket.getStatus()==Ticket.Status.PENDING){
					wst.setStatus(Ticket.Status.CANCELLED);
					ticketDao.update(wst);
					PRticket.setStatus(Ticket.Status.EXPIRED);
					ticketDao.update(PRticket);
					result = true;
				}else if(PRticket.getStatus()==Ticket.Status.EXPIRED){
					result = false;
				}
			}
			else if(wst.getStatus() == Ticket.Status.CANCELLED){
				result = false;
			}
			else if(wst.getStatus() == Ticket.Status.PENDING){
				wst.setStatus(Ticket.Status.CANCELLED);
				ticketDao.update(wst);
				result = true;
			}

		} catch (EntityNotFoundException e) {
            webServiceHelper.error(e);
		} catch (DaoException e) {
            webServiceHelper.error(e);
		}
        

		return result;
	}

}
