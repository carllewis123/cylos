/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.ecomm.service.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;
import nl.strohalm.cyclos.webservices.model.WebShopTicketVO;

@WebService
public interface ECommTransaction {

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    @WebResult(name = "paymentResult")
    public EcommPaymentResponse payment(@WebParam(name = "request") ECommPaymentRequest ecPayReq);

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    @WebResult(name = "confirmResult")
    public EcommConfirmResponse confirm(@WebParam(name = "request") ECommConfirmRequest ecConfirmReq);

    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    @WebResult(name = "confirmResult")
    public EcommConfirmResponse confirmAgent(@WebParam(name = "request") ECommConfirmRequest ecConfirmReq);
    

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    public EcommValidateResponse ticketValidation(@WebParam(name = "ticket") String ticket);

    /**
     * Generates a ticket using the specified parameters
     */
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    public String generate(@WebParam(name = "params") GenerateEcommTicketParams params);

    /**
     * Returns a ticket, using it's value
     */
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    public ECommTicketVO get(@WebParam(name = "id") String id);

    /**
     * validate user
     */
    @Permission({ ServiceOperation.MEMBERS, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    public boolean validUser(@WebParam(name="params") UserInfo userInfo);
    
    /**
     * expire ticket
     */
    @Permission({ ServiceOperation.MEMBERS, ServiceOperation.RECEIVE_PAYMENT })
    @WebMethod
    public boolean expire(@WebParam(name="id") String id);
}
