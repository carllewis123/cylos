/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
/**
 * 
 */
package com.ptdam.emoney.ecomm.service.ws;

import java.io.Serializable;

/**
 * @author dragon
 */
public class EcommValidateResponse implements Serializable {

    private static final long serialVersionUID = -7560334403971369398L;

    private String            ticket;
    private String            status;
    private String            code;
    private String            msg;
    private String            prticket;
    private String            prstatus;
    private String            prcode;
    private String            prmsg;

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getPrticket() {
        return prticket;
    }

    public void setPrticket(String prticket) {
        this.prticket = prticket;
    }

    public String getPrstatus() {
        return prstatus;
    }

    public void setPrstatus(String prstatus) {
        this.prstatus = prstatus;
    }

    public String getPrcode() {
        return prcode;
    }

    public void setPrcode(String prcode) {
        this.prcode = prcode;
    }

    public String getPrmsg() {
        return prmsg;
    }

    public void setPrmsg(String prmsg) {
        this.prmsg = prmsg;
    }

}
