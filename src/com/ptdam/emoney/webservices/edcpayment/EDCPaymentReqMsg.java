/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.edcpayment;

import java.io.Serializable;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class EDCPaymentReqMsg implements Serializable {

    private static final long serialVersionUID = 8273890530290782176L;

    private String OTP;
    private String edcTrxRefNo;
    private String edcTrxDateTime;
    private String edcTrxAmount;
    private String edcCashOutAmount;
    private String edcId;
    private String merchantId;
    private String pId;
    private String offlineMerchantId;
    
    private String reserved1;
    private String reserved2;
    private String reserved3;
    private String reserved4;
    private String reserved5;
    private String reserved6;
    private String reserved7;
    private String reserved8;
    private String reserved9;
    private String reserved10;
    /**
     * Getter for property oTP
     */
    public String getOTP() {
        return OTP;
    }
    /**
     * Setter for property oTP
     */
    public void setOTP(String oTP) {
        OTP = oTP;
    }
    /**
     * Getter for property edcTrxRefNo
     */
    public String getEdcTrxRefNo() {
        return edcTrxRefNo;
    }
    /**
     * Setter for property edcTrxRefNo
     */
    public void setEdcTrxRefNo(String edcTrxRefNo) {
        this.edcTrxRefNo = edcTrxRefNo;
    }
    /**
     * Getter for property edcTrxDateTime
     */
    public String getEdcTrxDateTime() {
        return edcTrxDateTime;
    }
    /**
     * Setter for property edcTrxDateTime
     */
    public void setEdcTrxDateTime(String edcTrxDateTime) {
        this.edcTrxDateTime = edcTrxDateTime;
    }
    /**
     * Getter for property edcTrxAmount
     */
    public String getEdcTrxAmount() {
        return edcTrxAmount;
    }
    /**
     * Setter for property edcTrxAmount
     */
    public void setEdcTrxAmount(String edcTrxAmount) {
        this.edcTrxAmount = edcTrxAmount;
    }
    /**
     * Getter for property edcId
     */
    public String getEdcId() {
        return edcId;
    }
    /**
     * Setter for property edcId
     */
    public void setEdcId(String edcId) {
        this.edcId = edcId;
    }
    /**
     * Getter for property merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }
    /**
     * Setter for property merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
    /**
     * Getter for property pId
     */
    public String getpId() {
        return pId;
    }
    /**
     * Setter for property pId
     */
    public void setpId(String pId) {
        this.pId = pId;
    }
    /**
     * Getter for property reserved1
     */
    public String getReserved1() {
        return reserved1;
    }
    /**
     * Setter for property reserved1
     */
    public void setReserved1(String reserved1) {
        this.reserved1 = reserved1;
    }
    /**
     * Getter for property reserved2
     */
    public String getReserved2() {
        return reserved2;
    }
    /**
     * Setter for property reserved2
     */
    public void setReserved2(String reserved2) {
        this.reserved2 = reserved2;
    }
    /**
     * Getter for property reserved3
     */
    public String getReserved3() {
        return reserved3;
    }
    /**
     * Setter for property reserved3
     */
    public void setReserved3(String reserved3) {
        this.reserved3 = reserved3;
    }
    /**
     * Getter for property reserved4
     */
    public String getReserved4() {
        return reserved4;
    }
    /**
     * Setter for property reserved4
     */
    public void setReserved4(String reserved4) {
        this.reserved4 = reserved4;
    }
    /**
     * Getter for property reserved5
     */
    public String getReserved5() {
        return reserved5;
    }
    /**
     * Setter for property reserved5
     */
    public void setReserved5(String reserved5) {
        this.reserved5 = reserved5;
    }
    /**
     * Getter for property reserved6
     */
    public String getReserved6() {
        return reserved6;
    }
    /**
     * Setter for property reserved6
     */
    public void setReserved6(String reserved6) {
        this.reserved6 = reserved6;
    }
    /**
     * Getter for property reserved7
     */
    public String getReserved7() {
        return reserved7;
    }
    /**
     * Setter for property reserved7
     */
    public void setReserved7(String reserved7) {
        this.reserved7 = reserved7;
    }
    /**
     * Getter for property reserved8
     */
    public String getReserved8() {
        return reserved8;
    }
    /**
     * Setter for property reserved8
     */
    public void setReserved8(String reserved8) {
        this.reserved8 = reserved8;
    }
    /**
     * Getter for property reserved9
     */
    public String getReserved9() {
        return reserved9;
    }
    /**
     * Setter for property reserved9
     */
    public void setReserved9(String reserved9) {
        this.reserved9 = reserved9;
    }
    /**
     * Getter for property reserved10
     */
    public String getReserved10() {
        return reserved10;
    }
    /**
     * Setter for property reserved10
     */
    public void setReserved10(String reserved10) {
        this.reserved10 = reserved10;
    }
    /**
     * Getter for property edcCashOutAmount
     */
    public String getEdcCashOutAmount() {
        return edcCashOutAmount;
    }
    /**
     * Setter for property edcCashOutAmount
     */
    public void setEdcCashOutAmount(String edcCashOutAmount) {
        this.edcCashOutAmount = edcCashOutAmount;
    }
    /**
     * Getter for property offlineMerchantId
     */
    public String getOfflineMerchantId() {
        return offlineMerchantId;
    }
    /**
     * Setter for property offlineMerchantId
     */
    public void setOfflineMerchantId(String offlineMerchantId) {
        this.offlineMerchantId = offlineMerchantId;
    }
    
    
    
    
}
