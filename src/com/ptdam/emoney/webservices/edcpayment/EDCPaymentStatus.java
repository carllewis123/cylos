/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.edcpayment;

public enum EDCPaymentStatus {
    
    /**
     * The otp was successfully validated
     */
    OTP_VALIDATION_PROCESSED,

    /**
     * The given otp was not found
     */
    OTP_VALIDATION_FAILED_INVALID_OTP,
    
    /**
     * The otp is already expired
     */
    OTP_VALIDATION_FAILED_OTP_ALREADY_EXPIRED,
    
    /**
     * The otp validation already used
     */
    OTP_VALIDATION_FAILED_OTP_ALREADY_VALIDATED,
            
    /**
     * The given otp was a non transaction otp (otp untuk buka profile tidak diperbolehkan digunakan untuk operasi trx otp)
     */
    OTP_VALIDATION_FAILED_NON_TRX_OTP_IS_USED,
    
    /**
     * The given amount was exceeded the request amount
     */
    OTP_VALIDATION_FAILED_EXCEEDED_REQUEST_AMOUNT,
    
    /**
     * The given merchant is not allowed
     */
    OTP_VALIDATION_FAILED_MERCHANT_NOT_ALLOWED,
    
    /**
     * The given merchant is not found
     */
    OTP_VALIDATION_FAILED_MERCHANT_NOT_FOUND,
    
    OTP_VALIDATION_FAILED_PAYMENT_NOT_ALLOWED,
    
    OTP_VALIDATION_FAILED_NOT_ENOUGH_CREDITS,
    
    OTP_VALIDATION_FAILED_MAX_MONTH_AMOUNT_EXCEEDED,
    
    OTP_VALIDATION_FAILED_MAX_DAILY_AMOUNT_EXCEEDED,
    
    OTP_VALIDATION_FAILED_GENERAL_INVALID_PARAMETERS,
    
    OTP_VALIDATOIN_FAILED_PAYER_NOT_FOUND,
    
    OTP_VALIDATION_FAILED_NON_EDC_OTP,
    
    
    /**
     * The given request parameters are invalid
     */
    OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT,
    OTP_VALIDATION_FAILED_INVALID_CASHOUT_AMOUNT,
    OTP_VALIDATION_FAILED_INVALID_TRX_REF_NO,
    OTP_VALIDATION_FAILED_INVALID_TRX_DATE,
    OTP_VALIDATION_FAILED_INVALID_MERCHANT_ID,
    OTP_VALIDATION_FAILED_INVALID_PID,
    OTP_VALIDATION_FAILED_INVALID_EDC_ID,
    
    
    /**
     * The given transaction was successfully reversed.
     */
    OTP_VALIDATION_REVERSAL_PROCESSED,
    
    /**
     * The reversal process was failed.
     */
    OTP_VALIDATION_REVERSAL_FAILED,
    
    
    
    EDC_INQUIRY_PROCESSED,
    
    EDC_INQUIRY_FAILED_TRX_NOT_FOUND,
    
    EDC_INQUIRY_FAILED_MERCHANT_NOT_FOUND,
    
    /**
     * Any other unexpected error will fall in this category
     */
    UNKNOWN_ERROR;

}
