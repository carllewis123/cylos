/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.edcpayment;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import javax.jws.WebService;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ptdam.emoney.EmoneyConfiguration;
import com.ptdam.emoney.webservices.CoreWebServiceFactory;

/**
 * Web service implementation
 * @author ptdam
 */
@WebService(name = "edcpayment", serviceName = "edcpayment")
public class EDCPaymentWebServiceImpl implements EDCPaymentWebService {

    private static final Relationship[]     FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal             elementServiceLocal;
    private ChannelHelper                   channelHelper;
    private AccountHelper                   accountHelper;
    private WebServiceHelper                webServiceHelper;
    private PaymentServiceLocal             paymentServiceLocal;
    private CoreWebServiceFactory           coreWSFactory;
    private final Log                       logger = LogFactory.getLog(EDCPaymentWebServiceImpl.class);
    private AccessServiceLocal              accessServiceLocal;
    private PaymentCustomFieldServiceLocal  paymentCustomFieldServiceLocal;

    private String OFFLINE_MERCHANT_ID = "";

    public EDCPaymentWebServiceImpl() {

        try {
            OFFLINE_MERCHANT_ID = EmoneyConfiguration.getEmoneyProperties().getProperty("edc.offline_merchant");
        } catch (IOException e) {
            logger.error(e);
        }
    
    }
    
    @Override
    public String echoTest(final String tx) {
        return tx;
    }

    @Override
    public EDCPaymentRespMsg validatePayment(final EDCPaymentReqMsg request) {
        EDCPaymentRespMsg status = new EDCPaymentRespMsg();
        BigDecimal paymentAmount = null;
        BigDecimal cashOutAmount = null;
        String merchantId = null;
        Member merchant = null;

        logger.info("Enters validatePayment");
        
        try {

            // Validate Mandatory Parameters
            // 1. TrxAmount
            if (request.getEdcTrxAmount() == null || request.getEdcTrxAmount().trim().length() == 0) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
            } else if (status.getEdcPaymentStatus() == null) {
                try {
                    paymentAmount = new BigDecimal(request.getEdcTrxAmount());
                    if (paymentAmount.compareTo(BigDecimal.ZERO) <= 0) {
                        status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
                    }
                } catch (final NumberFormatException e) {
                    webServiceHelper.error(e);
                    status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
                }
            }
            
            // 2. CashOut Amount
            if (status.getEdcPaymentStatus() == null && (request.getEdcCashOutAmount() == null || request.getEdcCashOutAmount().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_CASHOUT_AMOUNT);
            } else if (status.getEdcPaymentStatus() == null) {
                try {
                    cashOutAmount = new BigDecimal(request.getEdcCashOutAmount());
                    if (cashOutAmount.compareTo(BigDecimal.ZERO) < 0) {
                        status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_CASHOUT_AMOUNT);
                    }
                } catch (final NumberFormatException e) {
                    webServiceHelper.error(e);
                    status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_CASHOUT_AMOUNT);
                }
            }
            
            // 3. EDC Id
            if (status.getEdcPaymentStatus() == null && (request.getEdcId() == null || request.getEdcId().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_EDC_ID);
            }
           
            // 4. Merchant Id
            if (status.getEdcPaymentStatus() == null && (request.getMerchantId() == null || request.getMerchantId().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_MERCHANT_ID);
            }
            
            // 5. TrxDate
            if (status.getEdcPaymentStatus() == null && (request.getEdcTrxDateTime() == null || request.getEdcTrxDateTime().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_TRX_DATE);
            }
            
            // 6. PId
            if (status.getEdcPaymentStatus() == null && (request.getpId() == null || request.getpId().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PID);
            }
            
            // 7. Trx Ref No
            if (status.getEdcPaymentStatus() == null && (request.getEdcTrxRefNo() == null || request.getEdcTrxRefNo().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_TRX_REF_NO);
            }
            
            // 8. OTP
            if (status.getEdcPaymentStatus() == null && (request.getOTP() == null || request.getOTP().trim().length() == 0)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_INVALID_OTP);
            }
            
            
            // get merchant info
            if (status.getEdcPaymentStatus() == null) {
                final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
                merchant = elementServiceLocal.loadByPrincipal(principalType, request.getMerchantId(), FETCH);
            }
            
            // Check the channel immediately, as needed by SMS controller
            if (status.getEdcPaymentStatus() == null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), merchant)) {
                status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_MERCHANT_NOT_ALLOWED);
            }
            
            request.setOfflineMerchantId(OFFLINE_MERCHANT_ID);

            if (status.getEdcPaymentStatus() == null) {
                // get payment amount from request
                status = coreWSFactory.getOTPValidateWebService().validatePayment(request);
            }
            
        } 
        catch (final EntityNotFoundException e) {
            webServiceHelper.error(e);
            logger.info(merchantId + " belum terdaftar sebagai merchant di mandiri e-cash.");
            status.setEdcPaymentStatus(EDCPaymentStatus.OTP_VALIDATION_FAILED_MERCHANT_NOT_FOUND);
        }
        catch (final Exception e) {
            webServiceHelper.error(e);
            logger.error(e, e);
            status.setEdcPaymentStatus(EDCPaymentStatus.UNKNOWN_ERROR);
        } finally {
            if (status.getEdcPaymentStatus() == null) {
                logger.error("Unhandled error during edc validate payment");
                status.setEdcPaymentStatus(EDCPaymentStatus.UNKNOWN_ERROR);
            }
        }
        
        return status;

    }
        
    public EDCPaymentRespMsg reversePayment(final EDCPaymentReqMsg request) {
        EDCPaymentRespMsg status = new EDCPaymentRespMsg();
        
        try {
            logger.info("Enters reversePayment");
            status = coreWSFactory.getOTPValidateWebService().reversePayment(request);
        } catch (Exception e) {
            logger.error(e, e);
            status.setEdcPaymentStatus(EDCPaymentStatus.UNKNOWN_ERROR);
        } finally {
            if (status.getEdcPaymentStatus() == null) {
                logger.error("Unhandled error during edc validate payment");
                status.setEdcPaymentStatus(EDCPaymentStatus.UNKNOWN_ERROR);
            }
        }
        
        return status;
    }
    
    @Override
    public EDCPaymentRespMsg inquiryPayment(final EDCPaymentReqMsg request) {
        // TODO Auto-generated method stub
        final EDCPaymentRespMsg status = new EDCPaymentRespMsg();
        AccountHistoryTransferVO transferVO = new AccountHistoryTransferVO();
        EDCPaymentStatus edcStatus = null;

        try {
            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            final Member member = elementServiceLocal.loadByPrincipal(principalType, request.getMerchantId(), FETCH);
            final TransferQuery query = new TransferQuery();

            query.setTransactionNumber(request.getReserved1());
            query.setMember(member);
            
            final List<Transfer> transfers = paymentServiceLocal.search(query);

            final AccountOwner owner = member;
            
            if (!transfers.isEmpty()) {
         
                final Transfer transfer = transfers.iterator().next();
                final List<PaymentCustomField> customFields = paymentCustomFieldServiceLocal.list(transfer.getType(), true);
                transferVO = accountHelper.toVO(owner, transfer, customFields);
                edcStatus = EDCPaymentStatus.EDC_INQUIRY_PROCESSED;
                
            } else {
                
                edcStatus = EDCPaymentStatus.EDC_INQUIRY_FAILED_TRX_NOT_FOUND;
                
            }
        }
        catch (final EntityNotFoundException ex) {
            edcStatus = EDCPaymentStatus.EDC_INQUIRY_FAILED_MERCHANT_NOT_FOUND;
        }
        catch (final Exception ex) {
            logger.error(ex, ex);
            edcStatus = EDCPaymentStatus.UNKNOWN_ERROR;
        }
        finally {
            if (edcStatus == null) {
                edcStatus = EDCPaymentStatus.UNKNOWN_ERROR;
            }
            status.setEdcPaymentStatus(edcStatus);
            status.setTransferVO(transferVO);
        }
        return status;
    }
    
    
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
        
    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }
    
    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    /**
     * Setter for property coreWSFactory
     */
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    /**
     * Setter for property accessServiceLocal
     */
    public void setAccessServiceLocal(AccessServiceLocal accessServiceLocal) {
        this.accessServiceLocal = accessServiceLocal;
    }

    /**
     * Setter for property paymentCustomFieldServiceLocal
     */
    public void setPaymentCustomFieldServiceLocal(PaymentCustomFieldServiceLocal paymentCustomFieldServiceLocal) {
        this.paymentCustomFieldServiceLocal = paymentCustomFieldServiceLocal;
    }


    
    
}