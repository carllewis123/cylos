/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.edcpayment;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;

/**
 * Web service interface for EDC OTP Validation
 * @author ptdam
 */
@WebService
public interface OTPValidateWebService {

    @WebMethod
    @WebResult(name = "edcEcho")
    String echoTest(@WebParam(name = "tx") String tx);
     
    @WebMethod
    @WebResult(name = "validateResult")
    EDCPaymentRespMsg validatePayment(@WebParam(name = "request") EDCPaymentReqMsg request);
 
    @WebMethod
    @WebResult(name = "reversalResult")
    EDCPaymentRespMsg reversePayment(@WebParam(name = "request") EDCPaymentReqMsg request);

}
