/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.hosttohost;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;
import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.payments.PaymentParameters;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;

/**
 * Web service interface for HosttoHost OTP Validation
 * @author ptdam
 */
@WebService
public interface HosttoHostPaymentWebService {

    @WebMethod
    @WebResult(name = "host2hostEcho")
    String echoTest(@WebParam(name = "tx") String tx);
    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT})
    @WebMethod
    @WebResult(name = "validateResult")
    HosttoHostPaymentRespMsg validatePayment(@WebParam(name = "request") HosttoHostPaymentReqMsg request);
    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT})
    @WebMethod
    @WebResult(name = "paymentResult")
    PaymentResult_Extended doPaymentH2H_Extended(PaymentParameters params);

    @Permission({ ServiceOperation.CHARGEBACK})
    @WebMethod
    @WebResult(name = "reversalResult")
    HosttoHostPaymentRespMsg reversePayment(@WebParam(name = "request") String request);
}
