/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.hosttohost;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.jws.WebService;

import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.webservices.payments.PaymentParameters;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

/**
 * Web service implementation
 * @author ptdam
 */
@WebService(name = "hosttohostpayment", serviceName = "hosttohostpayment")
public class HosttoHostPaymentWebServiceImpl implements HosttoHostPaymentWebService {

    private static final Relationship[]     FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal             elementServiceLocal;
    private ChannelHelper                   channelHelper;
    private AccountHelper                   accountHelper;
    private WebServiceHelper                webServiceHelper;
    private PaymentServiceLocal             paymentServiceLocal;
    private CoreWebServiceFactory           coreWSFactory;
    private final Log                       logger = LogFactory.getLog(HosttoHostPaymentWebServiceImpl.class);
    private AccessServiceLocal              accessServiceLocal;
    private PaymentCustomFieldServiceLocal  paymentCustomFieldServiceLocal;

    private String HOST2HOST_MERCHANT_ID = "";
    private String validatephoneno = "";


    public HosttoHostPaymentWebServiceImpl() {

        try {
            HOST2HOST_MERCHANT_ID = EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.offline_merchant");
            validatephoneno = EmoneyConfiguration.getEmoneyProperties().getProperty("hosttohost.checkPhoneNo");

        } catch (IOException e) {
            logger.error(e);
        }
    
    }
    
    @Override
    public String echoTest(final String tx) {
        return tx;
    }

	@Override
	public HosttoHostPaymentRespMsg reversePayment(String request) {
		HosttoHostPaymentRespMsg status = new HosttoHostPaymentRespMsg();
		try {
            logger.info("Host 2 Host Enters reversePayment ");
            status = coreWSFactory.getOTPHosttoHostValidateWebService().reversePayment(request); 
        } catch (Exception e) {
            logger.error(e, e);
            status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
        } finally {
            if (status.getHosttoHostPaymentStatus() == null) {
                logger.error("Unhandled error during edc validate payment");
                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
            }
        }
        
        return status;
	}

    @Override
	public HosttoHostPaymentRespMsg validatePayment(HosttoHostPaymentReqMsg request) {
			HosttoHostPaymentRespMsg status = new HosttoHostPaymentRespMsg();
	        BigDecimal paymentAmount = null;
	        String merchantId = null;
	        Member merchant = null;

	        logger.info("Enters validatePayment H2H");
	        if(request.getReserved1()==null||request.getReserved1().equals("0")){
	        	 try {

	 	            // Validate Mandatory Parameters
	 	            // 1. TrxAmount
	 	            if (request.getHosttohostTrxAmount() == null || request.getHosttohostTrxAmount().trim().length() == 0) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	            } else if (status.getHosttoHostPaymentStatus() == null) {
	 	                try {
	 	                    paymentAmount = new BigDecimal(request.getHosttohostTrxAmount());
	 	                    if (paymentAmount.compareTo(BigDecimal.ZERO) <= 0) {
	 	                        status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	                    }
	 	                } catch (final NumberFormatException e) {
	 	                    webServiceHelper.error(e);
	 	                    status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	                }
	 	            }
	 	           
	 	            // 2. Merchant Id
	 	            if (status.getHosttoHostPaymentStatus() == null && (request.getMerchantId() == null || request.getMerchantId().trim().length() == 0)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_MERCHANT_ID);
	 	            }
	 	            
	 	            // 3. Trx Trace No
	 	            if (status.getHosttoHostPaymentStatus() == null && (request.getHosttohostTraceNo() == null || request.getHosttohostTraceNo().trim().length() == 0)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_TRACE_NO);
	 	            }
	 	            
	 	            // 4. Trx Desc.
	 	            if (status.getHosttoHostPaymentStatus() ==null &&(request.getHosttohostDescription() == null || request.getHosttohostDescription().trim().length() == 0)){
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_DESCRIPTION);	         
	 	            }
	 	            // 5. OTP
	 	            if (status.getHosttoHostPaymentStatus() == null && (request.getOTP() == null || request.getOTP().trim().length() == 0)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_OTP);
	 	            }
	 	            // 6. VALIDATE PHONE NO
	 	            if (!request.getOfflineMerchantId().equals(HOST2HOST_MERCHANT_ID)){
	 	            	if(validatephoneno.equals("false")){
		 	            if (status.getHosttoHostPaymentStatus() == null && (request.getPhoneNo() ==null || request.getPhoneNo().trim().length() == 0 ))
			 	            {
			 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PHONE_NO);
			 	            }           		
	 	            	}
	 	            }
	 
	 	            // get merchant info
	 	            if (status.getHosttoHostPaymentStatus() == null) {
	 	                final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
	 	                merchant = elementServiceLocal.loadByPrincipal(principalType, request.getMerchantId(), FETCH);
	 	            }
	 	            
	 	            // Check the channel immediately, as needed by SMS controller
	 	            if (status.getHosttoHostPaymentStatus() == null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), merchant)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_MERCHANT_NOT_ALLOWED);
	 	            }
	 	            

	 	            if (status.getHosttoHostPaymentStatus() == null) {
	 	                // get payment amount from request
	 	                status = coreWSFactory.getOTPHosttoHostValidateWebService().validatePayment(request);
	 	            }
	 	            
	 	        } 
	 	        catch (final EntityNotFoundException e) {
	 	            webServiceHelper.error(e);
	 	            logger.info(merchantId + " belum terdaftar sebagai merchant di mandiri e-cash.");
	 	            status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_MERCHANT_NOT_FOUND);
	 	        }
	 	        catch (final Exception e) {
	 	            webServiceHelper.error(e);
	 	            logger.error(e, e);
	 	            status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
	 	        } finally {
	 	            if (status.getHosttoHostPaymentStatus() == null) {
	 	                logger.error("Unhandled error during h2h validate payment");
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
	 	            }
	 	        }
	 	        return status;
	        }else if(request.getReserved1().equals("1")){
	       	 try {

	 	            // Validate Mandatory Parameters
	 	            // 1. TrxAmount
	 	            if (request.getHosttohostTrxAmount() == null || request.getHosttohostTrxAmount().trim().length() == 0) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	            } else if (status.getHosttoHostPaymentStatus() == null) {
	 	                try {
	 	                    paymentAmount = new BigDecimal(request.getHosttohostTrxAmount());
	 	                    if (paymentAmount.compareTo(BigDecimal.ZERO) <= 0) {
	 	                        status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	                    }
	 	                } catch (final NumberFormatException e) {
	 	                    webServiceHelper.error(e);
	 	                    status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PAYMENT_AMOUNT);
	 	                }
	 	            }
	 	            
	 	            // 3. Trx Trace No
	 	            if (status.getHosttoHostPaymentStatus() == null && (request.getHosttohostTraceNo() == null || request.getHosttohostTraceNo().trim().length() == 0)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_TRACE_NO);
	 	            }
	 	            
	 	            // 4. Trx Desc.
	 	            if (status.getHosttoHostPaymentStatus() ==null &&(request.getHosttohostDescription() == null || request.getHosttohostDescription().trim().length() == 0)){
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_DESCRIPTION);	         
	 	            }
	 	            // 5. OTP
	 	            if (status.getHosttoHostPaymentStatus() == null && (request.getOTP() == null || request.getOTP().trim().length() == 0)) {
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_OTP);
	 	            }
	 	            
	 	            // 6. VALIDATE PHONE NO
	 	           if (request.getOfflineMerchantId().equals(HOST2HOST_MERCHANT_ID)){
		 	            if (status.getHosttoHostPaymentStatus() == null && (request.getPhoneNo() ==null || request.getPhoneNo().trim().length() == 0))
		 	            {
		 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.OTP_VALIDATION_FAILED_INVALID_PHONE_NO);
		 	            }
	 	            }
	 	            if (status.getHosttoHostPaymentStatus() == null) {
	 	                // get payment amount from request
	 	                status = coreWSFactory.getOTPHosttoHostValidateWebService().validatePayment(request);
	 	            }
	 	            
	 	        } 
	 	        catch (final Exception e) {
	 	            webServiceHelper.error(e);
	 	            logger.error(e, e);
	 	            status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
	 	        } finally {
	 	            if (status.getHosttoHostPaymentStatus() == null) {
	 	                logger.error("Unhandled error during edc validate payment");
	 	                status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
	 	            }
	 	        }
	 	        return status;	
	        }else{
	            status.setHosttoHostPaymentStatus(HosttoHostPaymentStatus.UNKNOWN_ERROR);
	 	        return status;	
	        }
	       
	}
   

    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
        
    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }
    
    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    /**
     * Setter for property coreWSFactory
     */
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    /**
     * Setter for property accessServiceLocal
     */
    public void setAccessServiceLocal(AccessServiceLocal accessServiceLocal) {
        this.accessServiceLocal = accessServiceLocal;
    }

    /**
     * Setter for property paymentCustomFieldServiceLocal
     */
    public void setPaymentCustomFieldServiceLocal(PaymentCustomFieldServiceLocal paymentCustomFieldServiceLocal) {
        this.paymentCustomFieldServiceLocal = paymentCustomFieldServiceLocal;
    }

	@Override
	public PaymentResult_Extended doPaymentH2H_Extended(PaymentParameters params) {
		PaymentResult_Extended status = new PaymentResult_Extended();
        logger.info("Enters dopayment");
        
       try{
                // get payment amount from request
                status = coreWSFactory.getOTPHosttoHostValidateWebService().doPaymentH2H_Extended(params);
       }
        catch (final Exception e) {
            webServiceHelper.error(e);
            logger.error(e, e);
        }
        return status;
	}

}
