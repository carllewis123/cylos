/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.hosttohost;

import java.io.Serializable;

import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.payments.PaymentResult;
import nl.strohalm.cyclos.webservices.payments.PaymentStatus;

/**
 * Parameters used to update members
 * 
 * @author ptdam
 */
public class HosttoHostPaymentRespMsg implements Serializable {

	private static final long serialVersionUID = 8815719359313691295L;
	/**
	 * 
	 */
	private HosttoHostPaymentStatus         HosttoHostPaymentStatus;
    private AccountHistoryTransferVO transferVO;
 

    public HosttoHostPaymentStatus getHosttoHostPaymentStatus() {
		return HosttoHostPaymentStatus;
	}
	public void setHosttoHostPaymentStatus(
		HosttoHostPaymentStatus hosttoHostPaymentStatus) {
		HosttoHostPaymentStatus = hosttoHostPaymentStatus;
	}
	public AccountHistoryTransferVO getTransferVO() {
		return transferVO;
	}
	public void setTransferVO(AccountHistoryTransferVO transferVO) {
		this.transferVO = transferVO;
	}
	
    
     
}
