/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.hosttohost;

import java.io.Serializable;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class HosttoHostPaymentReqMsg implements Serializable {

	private static final long serialVersionUID = 2766459210510890259L;
	private String OTP;
	private String PhoneNo;
	private String hosttohostTraceNo;
	private String hosttohostTrxAmount;
    private String hosttohostDescription;
    private String merchantId;
    private String offlineMerchantId;
    
    private String reserved1;
    private String reserved2;
    private String reserved3;
    private String reserved4;
    private String reserved5;
    private String reserved6;
    private String reserved7;
    private String reserved8;
    private String reserved9;
    private String reserved10;
    
	public String getOTP() {
		return OTP;
	}
	public void setOTP(String oTP) {
		OTP = oTP;
	}
	public String getHosttohostTraceNo() {
		return hosttohostTraceNo;
	}
	public void setHosttohostTraceNo(String hosttohostTraceNo) {
		this.hosttohostTraceNo = hosttohostTraceNo;
	}
	public String getHosttohostTrxAmount() {
		return hosttohostTrxAmount;
	}
	public void setHosttohostTrxAmount(String hosttohostTrxAmount) {
		this.hosttohostTrxAmount = hosttohostTrxAmount;
	}

	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getOfflineMerchantId() {
		return offlineMerchantId;
	}
	public void setOfflineMerchantId(String offlineMerchantId) {
		this.offlineMerchantId = offlineMerchantId;
	}
    public String getHosttohostDescription() {
		return hosttohostDescription;
	}
	public void setHosttohostDescription(String hosttohostDescription) {
		this.hosttohostDescription = hosttohostDescription;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
	public String getReserved10() {
		return reserved10;
	}
	public void setReserved10(String reserved10) {
		this.reserved10 = reserved10;
	}
    public String getPhoneNo() {
		return PhoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    

 
}
