package com.ptdam.emoney.webservices.paymenttransfer;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;
import nl.strohalm.cyclos.webservices.accounts.LoadTransferParameters;
import nl.strohalm.cyclos.webservices.model.PaymentTransferVO;
import ptdam.emoney.webservice.member.Status;

@WebService
public interface PaymentTransferWebService {

	@WebMethod
	@WebResult(name = "paymentTransferResult")
	public PaymentTransferResponse generate(
			@WebParam(name = "request") PaymentTransferRequest request);
	
	@WebMethod
	public Status processing(
			@WebParam(name = "request") PaymentTransferProcessingRequest request);
	
	@WebMethod
	PaymentTransferVO loadPTDetail(
			@WebParam(name = "ptNumberUnique") String ptNumberUnique,
			@WebParam(name = "paymentTicket") String paymentTicket);
	
	@Permission(ServiceOperation.ACCOUNT_DETAILS)
    @WebMethod
    Boolean IsChargebackTransfer(@WebParam(name = "params") LoadTransferParameters params);
}

