package com.ptdam.emoney.webservices.paymenttransfer;

import java.io.Serializable;

public class PaymentTransferRequest implements Serializable {

	private static final long serialVersionUID = 3564749556634453044L;

	private String orderId;
	private String ptKey;
	private String amount;
	private String feeTrx;
	private String mid;
	private String merchantToken;
	private String urlCallback;
	private String batasBayar;
	private String expiredDate;
	private String description;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPtKey() {
		return ptKey;
	}

	public void setPtKey(String ptKey) {
		this.ptKey = ptKey;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFeeTrx() {
		return feeTrx;
	}

	public void setFeeTrx(String feeTrx) {
		this.feeTrx = feeTrx;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMerchantToken() {
		return merchantToken;
	}

	public void setMerchantToken(String merchantToken) {
		this.merchantToken = merchantToken;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	public String getBatasBayar() {
		return batasBayar;
	}

	public void setBatasBayar(String batasBayar) {
		this.batasBayar = batasBayar;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PaymentTransferRequest [orderId=" + orderId + ", ptKey="
				+ ptKey + ", amount=" + amount + ", feeTrx=" + feeTrx
				+ ", mid=" + mid + ", merchantToken=" + merchantToken
				+ ", urlCallback=" + urlCallback + ", batasBayar=" + batasBayar
				+ ", expiredDate=" + expiredDate + ", description="
				+ description + "]";
	}

}
