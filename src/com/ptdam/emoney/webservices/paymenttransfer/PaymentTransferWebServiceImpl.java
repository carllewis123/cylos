package com.ptdam.emoney.webservices.paymenttransfer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentTransferService;
import nl.strohalm.cyclos.webservices.accounts.LoadTransferParameters;
import nl.strohalm.cyclos.webservices.model.PaymentTransferVO;
import nl.strohalm.cyclos.webservices.utils.PaymentTransferHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;
import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.member.Status;

@WebService(name = "paymentTransferWebService", serviceName = "paymentTransferWebService")
public class PaymentTransferWebServiceImpl implements PaymentTransferWebService {

	private PaymentTransferService paymentTransferService;
	private PaymentTransferHelper paymentTransferHelper;
	private PaymentServiceLocal            paymentServiceLocal;
	private WebServiceHelper               webServiceHelper;

	private String ptType = "";

	public PaymentTransferWebServiceImpl() {
		try {

			ptType = EmoneyConfiguration.getEmoneyProperties().getProperty(
					"prefix.merchant.payment.transfer");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public PaymentTransferResponse generate(PaymentTransferRequest request) {

		PaymentTransferResponse response = new PaymentTransferResponse();
		try {
			BigDecimal amount = new BigDecimal(request.getAmount());
			BigDecimal fee = new BigDecimal(request.getFeeTrx());
			final Calendar createdDate = Calendar.getInstance();

			PaymentTransfer pt = new PaymentTransfer();
			pt.setOrderId(request.getOrderId());
			pt.setPtKey(request.getPtKey());
			pt.setMid(request.getMid());
			pt.setUrlCallback(request.getUrlCallback());
			pt.setDescription(request.getDescription());
			pt.setAmount(amount);
			pt.setFee(fee);

			pt.setPtType(ptType);
			pt.setCreatedDate(createdDate);

			BigDecimal amountUnique = amount.add(fee);
			pt.setAmountUnique(amountUnique);
			Calendar expiredDate = Calendar.getInstance();

			if (StringUtils.isNotEmpty(request.getBatasBayar())) {

				int timelimit = Integer.parseInt(request.getBatasBayar());
				pt.setTimelimit(timelimit);
				expiredDate.setTimeInMillis(createdDate.getTimeInMillis());
				expiredDate.add(Calendar.MINUTE, timelimit);
				pt.setExpiredDate(expiredDate);

			} else {

				SimpleDateFormat formatter = new SimpleDateFormat(
						"yyyyMMdd HH:mm:ss");
				String expired = (request.getExpiredDate() + " 00:00:00");
				Date date = formatter.parse(expired);
				expiredDate.setTime(date);
				pt.setExpiredDate(expiredDate);
			}

			pt.setPtNumber(ptType.concat(request.getPtKey()));

			pt.setPtNumberUnique(ptType.concat((request.getPtKey())
					.concat(amountUnique.toString())));

			PaymentTransfer payment = paymentTransferService.generate(pt);
			if (payment != null) {

				if (payment.getStatus() == "full") {
					response.setErrorCode("FULL_GENERATE_PTNO");

				} else {
					response.setErrorCode("SUCCESS");

					response.setOrderId(payment.getOrderId());
					response.setPtNumber(payment.getPtNumber());
					response.setDescription(payment.getDescription());
					response.setAmountTotal(payment.getAmountUnique()
							.toString());
					response.setAmountOrigin(payment.getAmount().toString());
					response.setTimelimit(payment.getExpiredDate().getTime()
							.toString());
					response.setPaymentTicket(payment.getPaymentTicket());
				}
			} else {
				response.setErrorCode("INVALID_PARAMETER");
			}
		} catch (Exception e) {
			response.setErrorCode("INVALID_PARAMETER");
		}

		return response;
	}

	@Override
	public Status processing(PaymentTransferProcessingRequest request) {
		Status status = new Status();

		try {
			paymentTransferService.move(request);
			status.setErrorCode("SUCCESS");
		} catch (Exception e) {
			status.setErrorCode("FAILED");
		}

		return status;
	}

	@Override
	public PaymentTransferVO loadPTDetail(String ptNumberUnique,
			String paymentTicket) {
		PaymentTransfer paymentTransfer;
		PaymentTransferVO paymentTransferVO= new PaymentTransferVO();
		String errorCode;
		try {

			paymentTransfer = paymentTransferService.load(ptNumberUnique,
					paymentTicket);
			if(paymentTransfer == null){
				paymentTransferVO.setErrorCode("INVALID_PTNO");
			} else {
			errorCode = "SUCCESS";
				paymentTransferVO = paymentTransferHelper.toVO(paymentTransfer,
						errorCode);
			}
		} catch (Exception e) {
			paymentTransferVO.setErrorCode("INVALID_PTNO");
		}
		
		return paymentTransferVO;
	}

	@Override
	public Boolean IsChargebackTransfer(LoadTransferParameters params) { 
		Transfer transfer;
    try {
        transfer = paymentServiceLocal.load(params.getTransferId(), Payment.Relationships.FROM, Payment.Relationships.TO, Payment.Relationships.TYPE, Payment.Relationships.CUSTOM_VALUES);
    } catch (final RuntimeException e) {
        webServiceHelper.error(e);
        throw e;
    }
    Boolean result = null;
    
    if (transfer.getChargedBackBy() != null) {
	result = true; 
	}else{
		result = false;
	}
    
	return result;
	}
	

	public void setPaymentTransferService(
			PaymentTransferService paymentTransferService) {
		this.paymentTransferService = paymentTransferService;
	}

	public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
	
	public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }

	public void setPaymentTransferHelper(
			PaymentTransferHelper paymentTransferHelper) {
		this.paymentTransferHelper = paymentTransferHelper;
	}

}
