package com.ptdam.emoney.webservices.paymenttransfer;

import java.io.Serializable;

public class PaymentTransferProcessingRequest implements Serializable {

	private static final long serialVersionUID = -1310412472852853899L;

	private String ptNumberUnique;
	private String status;
	private String statusDesc;

	public String getPtNumberUnique() {
		return ptNumberUnique;
	}

	public void setPtNumberUnique(String ptNumberUnique) {
		this.ptNumberUnique = ptNumberUnique;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	@Override
	public String toString() {
		return "PaymentTransferProcessingRequest [ptNumberUnique="
				+ ptNumberUnique + ", status=" + status + ", statusDesc="
				+ statusDesc + "]";
	}

}
