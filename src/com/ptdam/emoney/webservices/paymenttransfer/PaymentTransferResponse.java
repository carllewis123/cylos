package com.ptdam.emoney.webservices.paymenttransfer;

import java.io.Serializable;

import ptdam.emoney.webservice.member.Status;

public class PaymentTransferResponse implements Serializable {

	private static final long serialVersionUID = -5827643745909545932L;

	private String orderId;	
	private String ptNumber;
	private String description;
	private String mid;
	private String amountTotal;
	private String amountOrigin;
	private String timelimit;
	private String paymentTicket;

	private String errorCode;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPtNumber() {
		return ptNumber;
	}

	public void setPtNumber(String ptNumber) {
		this.ptNumber = ptNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getAmountTotal() {
		return amountTotal;
	}

	public void setAmountTotal(String amountTotal) {
		this.amountTotal = amountTotal;
	}

	public String getAmountOrigin() {
		return amountOrigin;
	}

	public void setAmountOrigin(String amountOrigin) {
		this.amountOrigin = amountOrigin;
	}

	public String getTimelimit() {
		return timelimit;
	}

	public void setTimelimit(String timelimit) {
		this.timelimit = timelimit;
	}

	public String getPaymentTicket() {
		return paymentTicket;
	}

	public void setPaymentTicket(String paymentTicket) {
		this.paymentTicket = paymentTicket;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "PaymentTransferResponse [orderId=" + orderId + ", ptNumber="
				+ ptNumber + ", description=" + description + ", mid=" + mid
				+ ", amountTotal=" + amountTotal + ", amountOrigin="
				+ amountOrigin + ", timelimit=" + timelimit
				+ ", paymentTicket=" + paymentTicket + ", errorCode="
				+ errorCode + "]";
	}

}
