/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.mobile;

import java.io.IOException;
import java.util.List;

import javax.jws.WebService;

import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferTypeQuery;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.services.transactions.TransactionContext;
import nl.strohalm.cyclos.services.transfertypes.TransferTypeService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;


@WebService(name = "paymentpurchaseservice", serviceName = "paymentpurchaseservice")
public class PaymentPurchaseWebServiceImpl implements PaymentPurchaseWebService {

    private final Log   logger = LogFactory.getLog(PaymentPurchaseWebService.class);
    private TransferTypeService transferTypeService;
    

    public void setTransferTypeService(TransferTypeService transferTypeService) {
        this.transferTypeService = transferTypeService;
    }

    @Override
    public InquiryResponseMsg inquiry(final InquiryRequestMsg input) {

        InquiryResponseMsg response = new InquiryResponseMsg();

        TransferTypeQuery ttQuery = new TransferTypeQuery();
        ttQuery.setContext(TransactionContext.PAYMENT);
        ttQuery.setChannel(input.getChannel());
        
        final String trxType = input.getTrxType();
        Long lTrxType = 0L;
        
        try {
            lTrxType = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(""));
        } catch (NumberFormatException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        
        
        try {
            List<TransferType> types = transferTypeService.search(ttQuery);
            List<PaymentCustomField> customFields = null;
            
            for (TransferType type : types) {
                if (type.getId().compareTo(lTrxType) == 0) {
                    customFields = (List<PaymentCustomField>) type.getCustomFields();
                    break;
                }
            }
            
            
            
            
        } catch (final Exception e) {
            logger.error(e);
        }
        
        return response;
    }

    @Override
    public PaymentResponseMsg payment(final PaymentRequestMsg input) {

        PaymentResponseMsg response = new PaymentResponseMsg();
        
        try {
            
        } catch (final Exception e) {
            logger.error(e);
        }
        
        return response;
    }
    

    private void UBPInquiry() throws IOException {
        // Get Params
        final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.channelId");
        final String billkey1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey1");
        final String billkey2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey2");
        final String billkey3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey3");
        final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.glaccount");
        final String debitAccountType = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.customerAccountType");
        final String cardAcceptorTermId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.cardAcceptorTermId");
        final String traceNumberLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.traceNumberLength");
        final String track2DataLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.track2DataLength");
        final String languageCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.language_code.ind");
        final String currencyCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.currencyCode");
        final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
        final String timeStampPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format_prefix");
        final String transactionCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.transactionCode");
        final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.tellerId");
        final String journalSequenceLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.journalSequenceLength");
        final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.extIdlength");
        final String companyCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.company_code_prefix");
        final String paymentCustomFieldPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("pcf.sisf_prefix");
        final String validBillkeysPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkeys_prefix");
        final String ubpAdvicePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.advice_prefix");
        final String ubpBillTypePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type_prefix");
        final String billTypeOpen = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.open");
        final String billTypeClose = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.bill_type.close");
        
        

    }
    
//    private String getCustomValue(final String internalName, final List<PaymentCustomField> customFields, final Collection<PaymentCustomFieldValue> customValues) {
//        
//        for (PaymentCustomField customField: customFields) {
//            if (customField.getInternalName().contains(internalName)) {
//                for (PaymentCustomFieldValue customValue: customValues) {
//                    if (customValue.getField().getId().compareTo(customField.getId()) == 0) {
//                        return customValue.getValue();
//                    }
//                }
//            }
//        }
//        return null;
//    }


}
