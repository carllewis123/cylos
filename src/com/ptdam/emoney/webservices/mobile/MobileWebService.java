/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.mobile;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ptdam.emoney.webservices.utils.OpenOTPStatus;
import com.ptdam.emoney.webservices.utils.OpenOTPTicketExtVO;
import com.ptdam.emoney.webservices.utils.OpenOTPTicketVO;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;
import nl.strohalm.cyclos.webservices.payments.PaymentResult;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.payments.PaymentStatus;

@WebService
public interface MobileWebService {

    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "inquiryResult")
    InqPreviewResponseMsg mobileInqMemberPaymentPreview(@WebParam(name = "request") InqPreviewRequestMsg input) throws Exception;
    
    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "inquiryResult")
    InqPreviewResponseMsg ussdInqMemberPaymentPreview(@WebParam(name = "request") InqPreviewRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS})
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended mobileMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended mobileMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended ussdMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended ussdMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    
    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "inquiryResult")
    NotificationResponseMsg mobileNotification(@WebParam(name = "request") NotificationRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "inquiryResult")
    NotificationResponseMsg ussdNotification(@WebParam(name = "request") NotificationRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "inquiryResult")
    ResetPswdResponseMsg mobileResetPassword(@WebParam(name = "request") ResetPswdRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "inquiryResult")
    ResetPswdResponseMsg ussdResetPassword(@WebParam(name = "request") ResetPswdRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "inquiryResult")
    CifAcctInqResponseMsg cifAcctInq(@WebParam(name = "request") CifAcctInqRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "inviteOther")
    InviteResponseMsg mobileInviteOther(@WebParam(name = "request") InviteRequestMsg input) throws Exception;
    
    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "regIfNotExist")
    PaymentStatus registerIfNotExits(@WebParam(name = "request") PaymentRequestMsg input);
    

    @Permission({ ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "generateNonTrxOTP")
    OpenOTPStatus generateNonTrxOTP(@WebParam(name = "request") OpenOTPTicketExtVO ticket);

    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended ussdMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended ussdMemberPaymentExternal(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended ussdMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended mobileMemberPaymentExternal(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS})
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended mobileMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended mobileMemberPaymentH2H(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
   
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.MEMBERS })
    @WebMethod
    @WebResult(name = "paymentResult")
    //PaymentResult_Extended ussdMemberPayment(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
    PaymentResult_Extended mobileMemberPaymentExternalH2H(@WebParam(name = "request") PaymentRequestMsg input) throws Exception;
}
