/**
 * 
 */
package com.ptdam.emoney.webservices.mobile;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import nl.strohalm.cyclos.dao.access.UserDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.Account;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.accounts.SystemAccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferTypeQuery;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.exceptions.UnexpectedEntityException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.Group.Status;
import nl.strohalm.cyclos.entities.groups.GroupQuery;
import nl.strohalm.cyclos.entities.members.Administrator;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.messages.Message;
import nl.strohalm.cyclos.entities.members.messages.MessageBox;
import nl.strohalm.cyclos.entities.members.messages.MessageQuery;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountDateDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.elements.MessageServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupServiceLocal;
import nl.strohalm.cyclos.services.services.ServiceClientServiceLocal;
import nl.strohalm.cyclos.services.settings.SettingsServiceLocal;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.TransactionContext;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerDayExceededException;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerMonthExceededException;
import nl.strohalm.cyclos.services.transactions.exceptions.UpperCreditLimitReachedException;
import nl.strohalm.cyclos.services.transfertypes.TransactionFeePreviewDTO;
import nl.strohalm.cyclos.services.transfertypes.TransactionFeeServiceLocal;
import nl.strohalm.cyclos.services.transfertypes.TransferTypeServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.HashHandler;
import nl.strohalm.cyclos.utils.MailHandler;
import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.conversion.CoercionHelper;
import nl.strohalm.cyclos.utils.query.PageParameters;
import nl.strohalm.cyclos.utils.query.QueryParameters.ResultType;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.accounts.AccountHistorySearchParameters;
import nl.strohalm.cyclos.webservices.members.MemberWebServiceImpl;
import nl.strohalm.cyclos.webservices.members.RegisterMemberParameters;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.CurrencyHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.QueryHelper;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.BillInfoList;
import ptdam.emoney.webservice.client.BillItemList;
import ptdam.emoney.webservice.client.FTInquiryMsgRequest;
import ptdam.emoney.webservice.client.FTInquiryMsgResponse;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.client.UBPCInqMsgRequest;
import ptdam.emoney.webservice.client.UBPCInqMsgResponse;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

/**
 * @author issotiyo
 * 
 */
public class MobileServiceLocalImpl {
	private static final float                PRECISION_DELTA    = 0.0001F;

    private static final String            REFERED_BY                = "referedBy";
    private final Log                      logger                    = LogFactory.getLog(MobileServiceLocalImpl.class);
    private static final String            PSWD_KEY_TEMPLATE         = "$pswd$";
    //FIXME: parameterized
    private static final boolean           NOT_FORCE_CHANGE          = false;
    private static final boolean           FORCECHANGE               = true;
    private static final Relationship[]    FETCH                     = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private String                         CUSTOM_FIELD_MOBILE_PHONE = "mobilePhone";
    private String                         INIT_PWD_PREFIX           = "hidden_initpwd";
    private String                         DEVICE_PAIR               = "hidden_devicepairingflag";
    private static final String            MOBILE                    = "mobile";
    private static final String            USSD                      = "ussd";

    private String                         NEW_ACCOUNT               = "Nasabah Baru";
    private GroupServiceLocal              groupServiceLocal;
    private String                         UNREGISTERED              = "UNREGISTERED";
    protected TransferTypeServiceLocal     transferTypeServiceLocal;
    private TransactionFeeServiceLocal     transactionFeeServiceLocal;
    private ElementServiceLocal            elementServiceLocal;
    private CoreWebServiceFactory          coreWSFactory;
    private PaymentCustomFieldServiceLocal paymentCustomFieldServiceLocal;
    private ChannelHelper                  channelHelper;
    private ServiceClientServiceLocal      serviceClientServiceLocal;
    private CurrencyHelper                 currencyHelper;
    private MessageServiceLocal            messageServiceLocal;
    private AccessServiceLocal             accessServiceLocal;
    private MailHandler                    mailHandler;
    private SmsMailingServiceLocal         smsMailingServiceLocal;
    private HashHandler                    hashHandler;
    private UserDAO                        userDao;
    private MemberHelper                   memberHelper;
    private CustomFieldHelper              customFieldHelper;
    private QueryHelper                    queryHelper;
    private PaymentServiceLocal            paymentServiceLocal;
    private AccountServiceLocal            accountService;

    public InqPreviewResponseMsg inquiryPreview(InqPreviewRequestMsg request,
            String typeMobile) throws Exception {
        InqPreviewResponseMsg result = new InqPreviewResponseMsg();

        // check host
        hostInquiry(result, request, typeMobile);

        // calculate fee
        proccess(result, request, typeMobile);

        return result;
    }

    public void hostInquiry(InqPreviewResponseMsg result,
            InqPreviewRequestMsg request, String typeMobile) throws IOException {
        // check is host inquiry type
     
        final String trxID = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(
                        typeMobile + ".transferID."
                                + request.getTrxtype().toLowerCase());

        if (trxID == null || !checkTransferTypeCode(trxID, "transferID.inquiry.host")){
            return;
        }

        if (result == null) {
            result = new InqPreviewResponseMsg();
        }

        DoPaymentDTO paymentInfo = new DoPaymentDTO();
        paymentInfo.setAmount(toBigDecimal(request.getAmount()));
        final String channelID = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(typeMobile + ".channelID.member_payment");

        paymentInfo.setChannel(channelID);
        paymentInfo.setTransferType(toTransferType(request, typeMobile,
                channelID));

        // paymentInfo.setFrom(toAccInfo(request.getFrom()));
        // paymentInfo.setTo(toAccInfo(request.getTo()));
        DoPaymentDTO data = null;
        if(request.getTrxtype().toLowerCase().equals("p60") || request.getTrxtype().toLowerCase().equals("p61")){
        	data = inquiryTrfBank(request, paymentInfo);
        	if(data != null && (data.getDescription() != null || data.getDescription().length() > 0 )){
        		if(data.getDescription().split("--").length == 1){
        			result.setField2(data.getDescription().split("--")[0]);
        		}else 
        		if(data.getDescription().split("--").length == 2){
        			result.setField2(data.getDescription().split("--")[0]);
        			result.setField3(data.getDescription().split("--")[1]);
        		}
        	}
        	
        }else{
        	data = inquiry(request, paymentInfo);
        	request.setAmount(data.getAmount().toPlainString());
            result.setDesc(data.getDescription());
        }
        

    }

    public Member toMember(String userName) throws Exception {
        if (userName == null || userName.isEmpty()) {
            return null;
        }

        User user;

        user = elementServiceLocal.loadUser(userName,
                RelationshipHelper.nested(User.Relationships.ELEMENT,
                        Element.Relationships.GROUP), RelationshipHelper
                        .nested(User.Relationships.ELEMENT,
                                Member.Relationships.CUSTOM_VALUES),
                RelationshipHelper.nested(User.Relationships.ELEMENT,
                        Member.Relationships.IMAGES));
        if (!(user instanceof MemberUser)) {
            throw new Exception("Not recognized user: " + user);
        }

        Member member = ((MemberUser) user).getMember();
        if (!member.isActive()
                || member.getGroup().getStatus() == Status.REMOVED) {
            throw new EntityNotFoundException();
        }
        return member;
    }

    public void proccess(InqPreviewResponseMsg result,
            InqPreviewRequestMsg request, String typeMobile) throws Exception {
        if (result == null) {
            result = new InqPreviewResponseMsg();
        }
        
        if(!checkTransferTypeCode(request.getTrxtype().toLowerCase(), "validate.p8")) {
        	result.setFrom(request.getFrom());
        }
        result.setTo(request.getTo());

        // Fetch related data
        String transferID = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(
                        typeMobile + ".transferID."
                                + request.getTrxtype().toLowerCase());

        BigDecimal amount = toBigDecimal(request.getAmount());
        
        Member memberFrom = null;
        if(!checkTransferTypeCode(request.getTrxtype().toLowerCase(), "validate.p8")) {
        	memberFrom = toMember(request.getFrom());
        }
        
        Member memberTo = null; // toMember(request.getTo());
        //Update toMember - 22 Oktober 2014
		if (checkTransferTypeCode(request.getTrxtype().toLowerCase(),"validate.toMember")) {
        	memberTo = toMember(request.getTo());
        }else if(checkTransferTypeCode(request.getTrxtype().toLowerCase(),"validate.p8")){
        	memberTo = toMember(request.getTo());
        	
        	try {
        		
            	String [] listTrxTypes = new String[10];
            	BigDecimal MonthlyTotalAmount = BigDecimal.ZERO;
            	BigDecimal todayTotalAmount = BigDecimal.ZERO;
            	String strMaxLimitPerMonth = "0";
            	String strMaxLimitPerDay = "0";
            	TransferType transferType = new TransferType();
            	transferType.setId(Long.parseLong(transferID));
                DoPaymentDTO transfer = new DoPaymentDTO();
                transfer.setTransferType(transferType);
                
            	if(EmoneyConfiguration.getEmoneyProperties().getProperty("p8.debit").contains(request.getTrxtype().toLowerCase())){
            		listTrxTypes = EmoneyConfiguration.getEmoneyProperties().getProperty("p8.debit").split(",");
            		final String maxLimitDebitPerMonthIntName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.debit.max.limit.per.month.internal.name");
                	strMaxLimitPerMonth = getCustomLabel(maxLimitDebitPerMonthIntName, transfer);
                	
                	final String maxLimitDebitPerDayIntName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.debit.max.limit.per.day.internal.name");
                	strMaxLimitPerDay = getCustomLabel(maxLimitDebitPerDayIntName, transfer);
                	
            	}else if(EmoneyConfiguration.getEmoneyProperties().getProperty("p8.credit").contains(request.getTrxtype().toLowerCase())){
            		listTrxTypes = EmoneyConfiguration.getEmoneyProperties().getProperty("p8.credit").split(",");
            		final String maxLimitCreditPerMonthIntName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.credit.max.limit.per.month.internal.name");
                	strMaxLimitPerMonth = getCustomLabel(maxLimitCreditPerMonthIntName, transfer);
                	
                	final String maxLimitCreditPerDayIntName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.credit.max.limit.per.day.internal.name");
                	strMaxLimitPerDay = getCustomLabel(maxLimitCreditPerDayIntName, transfer);
            	}
            	
            	BigDecimal maxLimitPerDay = (new BigDecimal(strMaxLimitPerDay)).setScale(2);
                BigDecimal maxLimitPerMonth = (new BigDecimal(strMaxLimitPerMonth)).setScale(2);
                
                Account account = null;
            	
            	for(int i = 0; i < listTrxTypes.length; i++){
            		String strTopupCCTrfTypeId = EmoneyConfiguration.getEmoneyProperties().getProperty(listTrxTypes[i]);
            		if(strTopupCCTrfTypeId.equals("") || strTopupCCTrfTypeId.length() == 0){
            			continue;
            		}
                	
            		Long topupCCTrfTypeId = Long.parseLong(strTopupCCTrfTypeId);
                	
                	String strEmoneyAccountTypeId = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.account.type.id");
                	Long emoneyAccountTypeId = Long.parseLong(strEmoneyAccountTypeId);
                	
                    Member member = customFieldHelper.loadMember(memberTo.getId());
                    AccountHistorySearchParameters params = new AccountHistorySearchParameters();
                    params.setPrincipal(memberTo.getUsername());
                    params.setAccountTypeId(emoneyAccountTypeId);
                    params.setPageSize(100);
                    Calendar beginDate = Calendar.getInstance();
                    beginDate.set(beginDate.get(Calendar.YEAR),beginDate.get(Calendar.MONTH),1,0,0,0);
                    params.setBeginDate(beginDate);
                    Calendar endDate = Calendar.getInstance();
                    params.setEndDate(endDate);
                    
                      // Get the query and account type
                    final TransferQuery query = new TransferQuery();
                    queryHelper.fill(params, query);
                    AccountType accountType = CoercionHelper.coerce(AccountType.class, params.getAccountTypeId());
                    query.setOwner(member);
                    account = accountService.getAccount(new AccountDTO(member, accountType), Account.Relationships.TYPE);
                    query.setType(account.getType());
                    if (params.getBeginDate() != null || params.getEndDate() != null) {
                        final Period period = new Period(params.getBeginDate(), params.getEndDate());
                        query.setPeriod(period);
                    }
                    
                    TransferType trfType = new TransferType();
                    trfType.setId(topupCCTrfTypeId);
                    query.setTransferType(trfType);

                      final List<Transfer> transfers = paymentServiceLocal.search(query);
                      Iterator it = transfers.iterator();
                      
                      Calendar today = Calendar.getInstance();
                      
                      while(it.hasNext()){
                    	  Transfer trf = (Transfer) it.next();
                    	  Calendar processDate = trf.getProcessDate();
                    	  
                    	  if(isSameDay(today,processDate)){
                    		  todayTotalAmount = todayTotalAmount.add((BigDecimal) trf.getAmount());
                    	  }

                    	  if(isSameMonth(today,processDate)){
                    		  MonthlyTotalAmount = MonthlyTotalAmount.add((BigDecimal) trf.getAmount());
                    	  } 
                    	  
                      }
                     
            	}
            	System.out.println("todayTotalAmount" + todayTotalAmount);
            	System.out.println("MonthlyTotalAmount : " + MonthlyTotalAmount);
                
            	this.paymentServiceLocal.validateUpperCreditLimit(amount, account);
            	if((todayTotalAmount.add(amount)).compareTo(maxLimitPerDay) > 0){
              	  throw new MaxTopupCCPerDayExceededException("MaxTopupCCPerDayExceededException");
                }else if((MonthlyTotalAmount.add(amount)).compareTo(maxLimitPerMonth) > 0){
           	  		throw new MaxTopupCCPerMonthExceededException("MaxTopupCCPerMonthExceededException");
                }
            	
                	  
            } catch (final MaxTopupCCPerDayExceededException e) {
            	logger.error(e);
            	throw e;    
            } catch (final MaxTopupCCPerMonthExceededException e) {
            	logger.error(e);
            	throw e;  
            } catch (final UpperCreditLimitReachedException e) {
            	logger.error(e);
            	throw e;    
            } catch (final ExternalException e) {
            	logger.error(e);
            	throw e;
            } catch (final Exception e) {
                logger.error(e);
                throw new ExternalException("GE");
            } 
        }
		
        Long lTrxType = 0L;
        
        logger.info(transferID);
        
        try {
            lTrxType = Long.parseLong(transferID);
        } catch (NumberFormatException e1) {
            logger.error(e1);
            logger.debug(e1, e1);
        }
        // Fetch related data
        AccountOwner from = null;
        AccountOwner to = null;
        TransferType tType = transferTypeServiceLocal.load(lTrxType,
                RelationshipHelper.nested(TransferType.Relationships.FROM,
                        AccountType.Relationships.CURRENCY),
                TransferType.Relationships.TO);

		logger.info(tType.getName());
		if(checkTransferTypeCode(request.getTrxtype().toLowerCase(),"validate.p8")){
			from = SystemAccountOwner.instance();
		}else if(memberFrom !=null && memberFrom instanceof Member) {
            // from = (Member) elementServiceLocal.load(((Member)
            // from).getId());
            from = memberFrom.getAccountOwner();
        }
        if (memberTo instanceof Member) {
            // to = (Member) elementServiceLocal.load(((Member) to).getId());
            to = memberTo.getAccountOwner();

            // Try to find a default transfer type
            DoPaymentDTO dto = new DoPaymentDTO();
            dto.setFrom(from);
            dto.setTo(to);
            dto.setCurrency(currencyHelper.loadByIdOrSymbol(null, "IDR"));
            final Collection<TransferType> possibleTypes = listPossibleTypes(dto);
            if (tType == null) {
	            if (possibleTypes.isEmpty()) {
	                throw new UnexpectedEntityException();
	            }
	            tType = possibleTypes.iterator().next();
            }
        }

        // for certain case to is predefined to system account (GL)
        if (tType.isToSystem()) {
            to = SystemAccountOwner.instance();

        }

		logger.info(tType.getName());

        // // Store the transaction fees
        TransactionFeePreviewDTO preview = transactionFeeServiceLocal.preview(
                from, to, tType, amount);


        result.setAmount(preview.getAmount().toPlainString());
        result.setTotalFee(preview.getTotalFeeAmount().toPlainString());
        result.setFinalAmount(preview.getFinalAmount().toPlainString());//.add(preview.getTotalFeeAmount()).toPlainString());
   


    }

    public TransferType toTransferType(InqPreviewRequestMsg request,
            String typeMobile, final String channelID) throws IOException {
        String transferID = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(
                        typeMobile + ".transferID."
                                + request.getTrxtype().toLowerCase());

        TransferTypeQuery ttQuery = new TransferTypeQuery();
        ttQuery.setContext(TransactionContext.PAYMENT);
        ttQuery.setChannel(channelID);

        Long lTrxType = 0L;

        try {
            lTrxType = Long.parseLong(transferID);
        } catch (NumberFormatException e1) {
            logger.error(e1);
            logger.debug(e1, e1);
        }

        TransferType tType = null;
        try {
            List<TransferType> types = transferTypeServiceLocal.search(ttQuery);

            for (TransferType type : types) {
                if (type.getId().compareTo(lTrxType) == 0) {
                    tType = type;
                    break;
                }
            }

        } catch (final Exception e) {
            logger.error(e);
            logger.debug(e, e);
        }
        return tType;
    }

    public BigDecimal toBigDecimal(String amount) {

        try {
            return new BigDecimal(amount);
        } catch (Exception e) {
            logger.error("unable to parse amount", e);
            return null;
        }

    }

    @SuppressWarnings("unused")
    public DoPaymentDTO inquiry(InqPreviewRequestMsg inqReq,
            DoPaymentDTO paymentInfo) {

        try {

            final UBPCInqMsgRequest request = new UBPCInqMsgRequest();

            // Get Params
            final String channelId = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.ubpc.channelId");
            final String billkey1 = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.ubpc.billkey1");
            final String billkey2 = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.ubpc.billkey2");
            final String billkey3 = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.ubpc.billkey3");
            final String debitAccount = EmoneyConfiguration
                    .getEmoneyProperties().getProperty("core.ubpc.glaccount");
            final String debitAccountType = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.customerAccountType");
            final String cardAcceptorTermId = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.cardAcceptorTermId");
            final String traceNumberLength = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.traceNumberLength");
            final String track2DataLength = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.track2DataLength");
            final String languageCode = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.language_code.ind");
            final String currencyCode = EmoneyConfiguration
                    .getEmoneyProperties()
                    .getProperty("core.ubpc.currencyCode");
            final String timeStampFormat = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.timestamp_format");
            final String timeStampPrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.timestamp_format_prefix");
            final String transactionCode = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.transactionCode");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.ubpc.tellerId");
            final String journalSequenceLength = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.journalSequenceLength");
            final String extIdLength = EmoneyConfiguration
                    .getEmoneyProperties().getProperty("core.ubpc.extIdlength");
            final String companyCodePrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.company_code_prefix");
            final String paymentCustomFieldPrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty("pcf.sisf_prefix");
            final String validBillkeysPrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.billkeys_prefix");
            final String ubpAdvicePrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.advice_prefix");
            final String ubpBillTypePrefix = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.bill_type_prefix");
            final String billTypeOpen = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.bill_type.open");
            final String billTypeClose = EmoneyConfiguration
                    .getEmoneyProperties().getProperty(
                            "core.ubpc.bill_type.close");
            final String skipChar = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.skip_label_char");

            // Get Bill Keys
            final String companyCode1Val = getCustomLabel(companyCodePrefix,
                    paymentInfo);

            final String validBillkeys = getCustomLabel(validBillkeysPrefix,
                    paymentInfo);

            final String ubpBillType = getCustomLabel(ubpBillTypePrefix,
                    paymentInfo);

            request.setBdyBillKey1(inqReq.getField1());
            if (ubpBillType.equals(billTypeOpen)) {
                if (validBillkeys.contains(billkey2)) {
                    BigDecimal amount = toBigDecimal(inqReq.getAmount());
                    amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
                    request.setBdyBillKey2(amount.longValue());
                } else {
                    request.setBdyBillKey2(null);
                    paymentInfo.setAmount(BigDecimal.ONE);
                }
            }

            if (companyCode1Val != null) {
                request.setBdyCompanyCode(Long.parseLong(companyCode1Val));
            } else {
                logger.error("Company code is not setup correctly.");
                throw new ExternalException(
                        "CCINSC");
            }

            request.setBdyChannelId(channelId);
            request.setBdyCustomerAccountNumber(Long.parseLong(debitAccount));
            request.setBdyCustomerAccountType(debitAccountType);
            request.setBdyCardAcceptorTermId(cardAcceptorTermId);

            final String traceNumber = RandomStringUtils.randomNumeric(Integer
                    .parseInt(traceNumberLength));
            request.setBdyTraceNumber(Long.parseLong(traceNumber));

            final String track2Data = RandomStringUtils.randomNumeric(Integer
                    .parseInt(track2DataLength));
            request.setBdyTrack2data(Long.parseLong(track2Data));

            request.setBdyLanguageCode(Long.parseLong(languageCode));
            request.setBdyCurrencyCode(currencyCode);

            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(
                    timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(Calendar
                    .getInstance().getTime());

            final boolean reqSecHeader = Boolean
                    .parseBoolean(EmoneyConfiguration.getEmoneyProperties()
                            .getProperty("core.ubpc.reqSecurityHeader"));

            RequestHeader header = new RequestHeader();
            header.setHdrChannelId(request.getBdyChannelId());
            header.setHdrTellerId(Long.parseLong(tellerId));

            final String extId = RandomStringUtils.randomNumeric(Integer
                    .parseInt(extIdLength));
            header.setHdrExternalId(extId);

            header.setHdrTransactionCode(transactionCode);
            header.setHdrTimestamp(sTimestamp);

            final String journalSequence = RandomStringUtils
                    .randomNumeric(Integer.parseInt(journalSequenceLength));
            header.setHdrJournalSequence(Long.parseLong(journalSequence));

            SecurityHeader securityHeader = new SecurityHeader();
            request.setSecurityHeader(securityHeader);

            request.setHeader(header);
            final UBPCInqMsgResponse response = coreWSFactory
                    .getUBPChannelWebService().inquiry(request);

            String responseMsg = "";

            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()) {
                        case 1:
                            BillInfoList[] billInfo = response.getBdyBillInfos();
                            String text = "";
                            for (int i = 0; i < billInfo.length; i++) {
                                String label = billInfo[i].getLabel();
                                String val = billInfo[i].getValue();

                                if (label.equals(skipChar)) {
                                    text = text + val.trim() + "\r\n";
                                } else {
                                    text = text + label.trim() + " " + val.trim() + "\r\n";
                                }

                            }
                            paymentInfo.setDescription(text);
                            if (ubpBillType.equals(billTypeClose)) {
                                BillItemList[] items = response.getBdyBillItems();
                                if (items.length > 0) {
                                    paymentInfo.setAmount(items[0].getBillAmount());
                                } else {
                                    paymentInfo.setAmount(BigDecimal.ZERO);
                                }
                            }
                            break;
                        default:
                            responseMsg = response.getResponseHeader()
                                    .getHdrResponseMessage();
                            final String errorCode = response.getResponseHeader()
                                    .getHdrErrorNumber().trim();
                            String mappingErrorMsg = EmoneyConfiguration
                                    .getEmoneyProperties().getProperty(
                                            "core.ubpc.inquiry.error_code."
                                                    + companyCode1Val + "."
                                                    + errorCode);
                            logger.error("Host Error (" + errorCode + ") : "
                                    + responseMsg);
                            if (mappingErrorMsg == null) {
                                mappingErrorMsg = responseMsg;
                            }
                            throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }

        } catch (final ExternalException e) {
            throw e;
        } catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        } catch (final Exception e) {
            logger.error(e);
            throw new ExternalException(
                    "GE");
        }

        return paymentInfo;
    }

    public String getCustomValue(final String internalName,
            final DoPaymentDTO paymentInfo) {
        final List<PaymentCustomField> customFields = paymentCustomFieldServiceLocal
                .list(paymentInfo.getTransferType(), false);
        final Collection<PaymentCustomFieldValue> customValues = paymentInfo
                .getCustomValues();

        for (PaymentCustomField customField : customFields) {
            if (customField.getInternalName().contains(internalName)) {
                for (PaymentCustomFieldValue customValue : customValues) {
                    if (customValue.getField().getId()
                            .compareTo(customField.getId()) == 0) {
                        return customValue.getValue();
                    }
                }
            }
        }
        return null;
    }

    public String getCustomLabel(final String internalName,
            final DoPaymentDTO paymentInfo) {
        final List<PaymentCustomField> customFields = paymentCustomFieldServiceLocal
                .list(paymentInfo.getTransferType(), false);

        for (PaymentCustomField customField : customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }

    public NotificationResponseMsg searchMessage(NotificationRequestMsg request, String type) throws Exception {

        NotificationResponseMsg response = new NotificationResponseMsg();

        MessageQuery query = new MessageQuery();
        // TODO: Parameterized INBOX
        query.setMessageBox(MessageBox.INBOX);
        query.setResultType(ResultType.PAGE);
        Member member = toMember(request.getMobilePhone());

        query.setGetter(member);

        if (query.getGetter() instanceof Administrator) {
            throw new Exception("notify.admin.denied");
        }

        try {
            int pageSize = (request.getPageSize() == 0) ? 10 : request.getPageSize();

            query.setPageParameters(new PageParameters(pageSize, request.getCurrentPage()));

            // FIXME: check group permission to view message, pin required
            List<Message> messages = messageServiceLocal.search(query);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // String TimeStop_Str = sdf.format(TimeStop.getTime());
            List<UserMessage> listMsg = new ArrayList<>();
            for (Message message : messages) {
                UserMessage msg = new UserMessage();
                msg.setBody(message.getBody());
                msg.setDate(sdf.format(message.getDate().getTime()));
                msg.setFrom((message.getFromMember() == null) ? "System" : message.getFromMember().getName());
                msg.setMsgBox(message.getMessageBox().toString());
                msg.setSubjectExt(message.getSubject());
                msg.setId(message.getId().toString());
                listMsg.add(msg);

            }

            response.setMessages(listMsg);
        } catch (Exception e) {
            logger.error(e);
            logger.debug(e, e);
            throw new Exception("notify.message.not.found");
        }

        return response;
    }

    public Collection<TransferType> listPossibleTypes(final DoPaymentDTO dto) {
        final String channel = channelHelper.restricted();
        if (StringUtils.isEmpty(channel)) {
            return Collections.emptyList();
        }

        final Member member = WebServiceContext.getMember();
        final ServiceClient client = WebServiceContext.getClient();

        // First, we need a list of existing TTs for the payment
        final TransferTypeQuery query = new TransferTypeQuery();
        query.setResultType(ResultType.LIST);
        query.setContext(TransactionContext.PAYMENT);
        query.setChannel(channel);
        query.setFromOwner(dto.getFrom());
        query.setToOwner(dto.getTo());
        query.setCurrency(dto.getCurrency());
        final List<TransferType> transferTypes = transferTypeServiceLocal.search(query);

        // Then, restrict according to the web service client permissions
        boolean doPayment = true;
        if (member != null) {
            doPayment = member.equals(dto.getFrom());
        }
        Collection<TransferType> possibleTypes;
        if (doPayment) {
            possibleTypes = serviceClientServiceLocal.load(client.getId(), ServiceClient.Relationships.DO_PAYMENT_TYPES).getDoPaymentTypes();
        } else {
            possibleTypes = serviceClientServiceLocal.load(client.getId(), ServiceClient.Relationships.RECEIVE_PAYMENT_TYPES).getReceivePaymentTypes();
        }
        transferTypes.retainAll(possibleTypes);

        return transferTypes;
    }

    public ResetPswdResponseMsg resetPassword(ResetPswdRequestMsg input, String mobileType) throws Exception {

        ResetPswdResponseMsg result = new ResetPswdResponseMsg();

        // check mobilephone
        Member member = checkMobilePhone(input.getMobilePhone(), mobileType);

        //check user blocked after 3x attempt
        checkBlockedUser(member, mobileType);
        
        // check dob
        checkDOB(member, input.getDateOfBirth(), mobileType);

        // check secret answer
        checkSecretAnswer(member, input.getSecretAnswer(), mobileType);

        // check secret answer
        checkName(member, input.getName(), mobileType);
        

        // all fine up to now, all checked
        resetPasswordMember(member);

        return result;
    }

    private void checkName(Member member, String name, String mobileType) throws Exception{

        final String dobCustValName = EmoneyConfiguration.getEmoneyProperties()
                .getProperty("mobile.field.value.name.fullname", "name");
        final String validate = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(mobileType + ".resetpass.validate", "");

        if (!validate.contains(dobCustValName)) {
            return;
        }

        boolean match = member.getName().equalsIgnoreCase(name);

        if (!match){
            throw new Exception(mobileType+".name.notmatch");
               
        }
    }

    private void checkBlockedUser(Member member, String mobileType) throws Exception {

       boolean blocked = accessServiceLocal.isLoginBlocked(member.getUser());
       
       if (blocked) {
            throw new Exception(mobileType+".reset.user.blocked");
       } 
        
    }

    private void resetPasswordMember(Member member) throws Exception {

        // reject if user is admin
        Element element = member;
        if (element instanceof Administrator) {
            throw new Exception("notify.admin.denied");
        }

        logger.warn("===WARNING==== RESET PASSWORD USER:" + member.getUsername());

        // Generate new PIN
        String newPassword = RandomStringUtils.randomNumeric(6);

        // send new PIN by SMS
        boolean smsReset = false;
        try {
            SmsMailing sms = new SmsMailing();
            sms.setFree(true);
            sms.setMember(member);

            String templateResetPwd = EmoneyConfiguration.getEmoneyProperties().getProperty("mobile.template.message.reset.pswd");
            sms.setText(templateResetPwd.replace(PSWD_KEY_TEMPLATE, newPassword));
            LoggedUser.init(member.getUser());
            smsMailingServiceLocal.send(sms);
            LoggedUser.cleanup();
            smsReset = true;
        } catch (Exception e) {
            logger.error("===WARNING==== RESET PASSWORD USER:" + member.getUsername() + "unable to send SMS, reason" + e);
        }

        // if both fail throw error
        if (!smsReset) {
            throw new Exception("mobile.reset.password.done.unable.to.notify");
        }

        // Reset user's PIN if SMS sending is success and force change
        User user = member.getUser();
        user.setPassword(hashHandler.hash(user.getSalt(), newPassword));
        user.setPasswordDate(Calendar.getInstance());
        userDao.update(user);

        logger.warn("===WARNING==== RESET PASSWORD USER:" + member.getUsername() + " is success.");
    }

    private void checkSecretAnswer(Member member, String secretAnswer, String mobileType) throws Exception {

        final String secretValName = EmoneyConfiguration.getEmoneyProperties()
                .getProperty("mobile.custom.value.name.secret");
        final String validate = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(mobileType + ".resetpass.validate", "");

        if (!validate.contains(secretValName)) {
            return;
        }
        

        final Collection<MemberCustomFieldValue> customValues = ((Member) elementServiceLocal.load(member.getId(), Member.Relationships.CUSTOM_VALUES)).getCustomValues();

        for (MemberCustomFieldValue memberCustomFieldValue : customValues) {
            if (secretValName.equals(memberCustomFieldValue.getField().getInternalName())) {
                if (secretAnswer.equals(memberCustomFieldValue.getValue())) {
                    // equals
                    return;
                } else {
                    throw new Exception(mobileType+".secret.notmatch");
                }

            }
        }
        throw new Exception(mobileType+".secret.field.missing");

    }

    private void checkDOB(Member member, String dateOfBirth, String mobileType) throws Exception {

        final String dobCustValName = EmoneyConfiguration.getEmoneyProperties()
                .getProperty("mobile.custom.value.name.dob", "dateofbirth");
        final String validate = EmoneyConfiguration.getEmoneyProperties()
                .getProperty(mobileType + ".resetpass.validate", "");

        // update by fikri: dob bypassed for ussd
        // FIXME: parameterized
        if (!validate.contains(dobCustValName)) {
            return;
        }

        final Collection<MemberCustomFieldValue> customValues = ((Member) elementServiceLocal.load(member.getId(), Member.Relationships.CUSTOM_VALUES)).getCustomValues();

        for (MemberCustomFieldValue memberCustomFieldValue : customValues) {
            if (dobCustValName.equals(memberCustomFieldValue.getField().getInternalName())) {
                if (dateOfBirth.equals(memberCustomFieldValue.getValue())) {
                    // equals
                    return;
                } else {
                    throw new Exception(mobileType+".dob.notmatch");
                }

            }
        }
        throw new Exception(mobileType+".dob.field.missing");

    }

    private Member checkMobilePhone(String mobilePhone, String mobileType) throws Exception {

        // Simple conversion from user name to Member
        try {
            return toMember(mobilePhone);
        } catch (Exception e) {
            throw new Exception(mobileType+".member.not.found");
        }

    }

    public InviteResponseMsg inviteOther(InviteRequestMsg input, String mobile) throws Exception {
        InviteResponseMsg result = null;

        // check To not exist
        checkExistTo(input.getTo());

        // create To
        result = createTo(input);

        return result;
    }

    private InviteResponseMsg createTo(InviteRequestMsg params) throws Exception {
        InviteResponseMsg result = null;
        final String emoneyAccountNo = params.getTo();
        Member member = null;

        try {

            MemberWebServiceImpl newAccountService = new MemberWebServiceImpl();
            newAccountService.setElementServiceLocal(elementServiceLocal);
            newAccountService.setMemberHelper(memberHelper);

            final String init = RandomStringUtils.randomNumeric(6);

            RegisterMemberParameters registrationParams = new RegisterMemberParameters();
            RegistrationFieldValueVO customField = new RegistrationFieldValueVO(CUSTOM_FIELD_MOBILE_PHONE, emoneyAccountNo, false);
            RegistrationFieldValueVO customField1 = new RegistrationFieldValueVO(INIT_PWD_PREFIX, init, false);
            RegistrationFieldValueVO customField2 = new RegistrationFieldValueVO(DEVICE_PAIR, "false", false);
            RegistrationFieldValueVO customField3 = new RegistrationFieldValueVO(REFERED_BY, "false", false);
            List<RegistrationFieldValueVO> fieldValueVOs = Arrays.asList(customField, customField1, customField2, customField3);
            registrationParams.setUsername(emoneyAccountNo);
            registrationParams.setName(NEW_ACCOUNT);
            registrationParams.setLoginPassword(init);
            registrationParams.setFields(fieldValueVOs);

            Long groupId = new Long(0);
            final GroupQuery query = new GroupQuery();
            query.setNatures(Group.Nature.MEMBER);

            final List<? extends Group> possibleNewGroups = groupServiceLocal.search(query);

            if (possibleNewGroups != null) {
                for (int i = 0; i < possibleNewGroups.size(); i++)
                {
                    final Group group = possibleNewGroups.get(i);
                    if (group.getName().equals(UNREGISTERED))
                    {
                        groupId = group.getId();
                        break;
                    }
                }
            }

            if (groupId > 0) {
                registrationParams.setGroupId(groupId);
            }

            newAccountService.registerMember(registrationParams);
            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);

            result = new InviteResponseMsg();
            result.setMember(memberHelper.toVO(member));

            return result;
        } catch (Exception e) {
            logger.error(e, e);
            throw new Exception("mobile.invite.error.create.to.unknown");
        }

    }

    private void checkExistTo(String to) throws Exception {

        boolean existingAcc = true;
        final PrincipalType principalType = channelHelper.resolvePrincipalType(null);

        try {
            elementServiceLocal.loadByPrincipal(principalType, to, FETCH);
        } catch (EntityNotFoundException e) {
            existingAcc = false;
        }

        if (existingAcc) {
            throw new Exception("mobile.invite.error.to.exist");
        }

    }

    public void setTransferTypeServiceLocal(
            TransferTypeServiceLocal transferTypeServiceLocal) {
        this.transferTypeServiceLocal = transferTypeServiceLocal;
    }

    public void setTransactionFeeServiceLocal(
            TransactionFeeServiceLocal transactionFeeServiceLocal) {
        this.transactionFeeServiceLocal = transactionFeeServiceLocal;
    }

    public void setElementServiceLocal(ElementServiceLocal elementServiceLocal) {
        this.elementServiceLocal = elementServiceLocal;
    }

    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    public void setPaymentCustomFieldServiceLocal(
            PaymentCustomFieldServiceLocal paymentCustomFieldServiceLocal) {
        this.paymentCustomFieldServiceLocal = paymentCustomFieldServiceLocal;
    }

    public void setChannelHelper(ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    public void setServiceClientServiceLocal(ServiceClientServiceLocal serviceClientServiceLocal) {
        this.serviceClientServiceLocal = serviceClientServiceLocal;
    }

    public void setCurrencyHelper(CurrencyHelper currencyHelper) {
        this.currencyHelper = currencyHelper;
    }

    public void setMessageServiceLocal(MessageServiceLocal messageServiceLocal) {
        this.messageServiceLocal = messageServiceLocal;
    }

    public void setAccessServiceLocal(AccessServiceLocal accessServiceLocal) {
        this.accessServiceLocal = accessServiceLocal;
    }

    public void setMailHandler(MailHandler mailHandler) {
        this.mailHandler = mailHandler;
    }

    public void setSmsMailingServiceLocal(SmsMailingServiceLocal smsMailingServiceLocal) {
        this.smsMailingServiceLocal = smsMailingServiceLocal;
    }

    public void setHashHandler(HashHandler hashHandler) {
        this.hashHandler = hashHandler;
    }

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    public void setGroupServiceLocal(GroupServiceLocal groupServiceLocal) {
        this.groupServiceLocal = groupServiceLocal;
    }

    public void setMemberHelper(MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    @SuppressWarnings("unused")
    public DoPaymentDTO inquiryTrfBank(InqPreviewRequestMsg inqReq,
            DoPaymentDTO paymentInfo) {

        try {
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.timestamp_format");
            final String transactionCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.transactionCode");
            final String journalSequence = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.journalSequence");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.tellerId");
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.extIdlength");
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.inquiry.channelId");
            String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(Calendar.getInstance().getTime());
            
            
        	FTInquiryMsgRequest request = new FTInquiryMsgRequest();
        	RequestHeader header = new RequestHeader();
        	header.setHdrChannelId(channelId);
        	header.setHdrExternalId(extId);
        	header.setHdrJournalSequence(Long.parseLong(journalSequence));
        	header.setHdrTellerId(Long.parseLong(tellerId));
            header.setHdrTransactionCode(transactionCode);
            header.setHdrTimestamp(sTimestamp);

        	request.setHeader(header);
        	request.setBdyAccount(inqReq.getField1());
        	
            final FTInquiryMsgResponse response = coreWSFactory.getFTInquiryWebService().getAccountInq(request);
            
            String responseMsg = "";

            if (response != null) {
                if (response.getSoaHeader() != null) {
                	switch (response.getSoaHeader().getHdrResponseCode()) {
                		case 1 :
                			String name = response != null && (response.getBdyShortName() != null ||response.getBdyShortName().length() >0) ? response.getBdyShortName() : "";
                			String accountType = response != null && (response.getBdyAccountType() != null ||response.getBdyAccountType().length() >0) ? response.getBdyAccountType() : "";
                			paymentInfo.setDescription(name + "--" + accountType);
                			break;
                		default :
                			responseMsg = response.getSoaHeader().getHdrResponseMessage();
                			String errorCode = response.getSoaHeader().getHdrErrorNumber().trim();
                			String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code."+ errorCode);
                			logger.error("inquiry Fund Transfer, Host Error (" + errorCode + ") : " + responseMsg);
			                  if (mappingErrorMsg == null) {
			                      mappingErrorMsg = responseMsg;
			                  }
			                  throw new ExternalException(errorCode);
                	}
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }

        } catch (final ExternalException e) {
            throw e;
        } 
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        } catch (final Exception e) {
            logger.error(e);
            throw new ExternalException(
                    "GE");
        }

        return paymentInfo;
    }
    
    //update 22 Oktober 2014
    public boolean checkTransferTypeCode(String transferTypeCode, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(transferTypeCode)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(transferTypeCode)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    public boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            return false;
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) 
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    
    public boolean isSameMonth(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            return false;
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
            && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountService = accountService;
    }
    
    public void setQueryHelper(final QueryHelper queryHelper) {
        this.queryHelper = queryHelper;
    }
}
