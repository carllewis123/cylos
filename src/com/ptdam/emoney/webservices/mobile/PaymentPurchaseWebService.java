/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.mobile;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;

@WebService
public interface PaymentPurchaseWebService {
    
    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "inquiryResult")
    InquiryResponseMsg inquiry(@WebParam(name = "request") InquiryRequestMsg input);
    
    @Permission({ ServiceOperation.DO_PAYMENT })
    @WebMethod
    @WebResult(name = "paymentResult")
    PaymentResponseMsg payment(@WebParam(name = "request") PaymentRequestMsg input);
    
}
