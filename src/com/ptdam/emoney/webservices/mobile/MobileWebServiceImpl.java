/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.mobile;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.accounts.Account.Relationships;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.entities.accounts.LockedAccountsOnPayments;
import nl.strohalm.cyclos.entities.accounts.SystemAccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferAuthorizationDTO;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.GroupQuery;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.exceptions.PermissionDeniedException;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.access.exceptions.BlockedCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCredentialsException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.application.ApplicationServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupServiceLocal;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.TransferAuthorizationServiceLocal;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerDayExceededException;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerMonthExceededException;
import nl.strohalm.cyclos.services.transactions.exceptions.MinAmountTransactionException;
import nl.strohalm.cyclos.services.transactions.exceptions.UpperCreditLimitReachedException;
import nl.strohalm.cyclos.utils.HashHandler;
import nl.strohalm.cyclos.utils.Pair;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.notifications.MemberNotificationHandler;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.members.MemberWebServiceImpl;
import nl.strohalm.cyclos.webservices.members.RegisterMemberParameters;
import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.model.AccountStatusVO;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.payments.ApplicationContextProvider;
import nl.strohalm.cyclos.webservices.payments.ChargebackResult;
import nl.strohalm.cyclos.webservices.payments.ChargebackStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentParameters;
import nl.strohalm.cyclos.webservices.payments.PaymentParametersExtended;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.payments.PaymentStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;
import nl.strohalm.cyclos.webservices.payments.commandActiveMQ;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.PaymentHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.AccountDetail;
import ptdam.emoney.webservice.client.AcctInfoInquiryRequest;
import ptdam.emoney.webservice.client.AcctInfoInquiryResponse;
import ptdam.emoney.webservice.client.ArrayOfAccounts;
import ptdam.emoney.webservice.client.DataHeaderMBase;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.OpenOTPStatus;
import com.ptdam.emoney.webservices.utils.OpenOTPTicketExtVO;
import com.ptdam.emoney.webservices.utils.OpenOTPTicketVO;

@WebService(name = "mobileadapterwebservice", serviceName = "mobileadapterwebservice")
public class MobileWebServiceImpl implements MobileWebService {

    private final Log                     logger              = LogFactory.getLog(MobileWebServiceImpl.class);

    private static final String           UNKNOWN_ERROR       = "UNKNOWN ERROR";
    private static final String           PREFIX_ERR_MOBILE   = "mobile.";
    private static final String           MOBILE_MSG_PREFIX         = "mobile";

    private static final String           ERROR                     = "ERROR: ";

    private static final String           SUCCESS                   = "SUCCESS";

    private static final String           ERROR_UNKNOWN_ERROR       = "ERROR: Unknown Error ";

    private static final String           CONFIGURATION_ERROR       = "Configuration Error";

    private static final String           MOBILE              = "mobile";
    private static final String           USSD                = "ussd";

    private MobileServiceLocalImpl        mobileServiceLocalImpl;

    private AccessServiceLocal            accessServiceLocal;
    private ApplicationServiceLocal       applicationServiceLocal;
    private PaymentServiceLocal           paymentServiceLocal;
    private ElementServiceLocal           elementServiceLocal;
    private PaymentHelper                 paymentHelper;
    private WebServiceHelper              webServiceHelper;
    private AccountHelper                 accountHelper;
    private ChannelHelper                 channelHelper;
    private AccountServiceLocal           accountServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private CoreWebServiceFactory         coreWSFactory;
    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP };

    
    private MemberHelper                  memberHelper;
    private GroupServiceLocal             groupServiceLocal;

    private String     CUSTOM_FIELD_MOBILE_PHONE = "mobilePhone";
    private String     INIT_PWD_PREFIX = "hidden_initpwd";
    private String     DEVICE_PAIR = "hidden_devicepairingflag";
    private String     NEW_ACCOUNT = "Nasabah Baru";
    private String     UNREGISTERED = "UNREGISTERED";
    private String	   CIF_NAME = "nama_cif";

    private HashHandler                hashHandler;
    private SmsMailingServiceLocal         smsMailingServiceLocal;
    
    //Modification for co-branding
    private String[]   prefixNumber;
    private String regexPrefix = "";
    private String groupUnregMember = "";
    private String     VALID_NUMBERS = "";
    
    //Modification for agen tarik tunai - 26 Juni 2014
    private String TRX_AGENT = "";
    
    private PaymentCustomFieldServiceLocal 		paymentCustomFieldServiceLocal;
    
    private TransferAuthorizationServiceLocal 	transferAuthorizationServiceLocal;
    
    private MemberNotificationHandler         	memberNotificationHandler;
    
       
    public MemberNotificationHandler getMemberNotificationHandler() {
		return memberNotificationHandler;
	}

	public void setMemberNotificationHandler(
			MemberNotificationHandler memberNotificationHandler) {
		this.memberNotificationHandler = memberNotificationHandler;
	}

	public TransferAuthorizationServiceLocal getTransferAuthorizationServiceLocal() {
		return transferAuthorizationServiceLocal;
	}

	public void setTransferAuthorizationServiceLocal(
			TransferAuthorizationServiceLocal transferAuthorizationServiceLocal) {
		this.transferAuthorizationServiceLocal = transferAuthorizationServiceLocal;
	}

	public PaymentCustomFieldServiceLocal getPaymentCustomFieldServiceLocal() {
		return paymentCustomFieldServiceLocal;
	}

	public void setPaymentCustomFieldServiceLocal(
			PaymentCustomFieldServiceLocal paymentCustomFieldServiceLocal) {
		this.paymentCustomFieldServiceLocal = paymentCustomFieldServiceLocal;
	}
    
    @Inject
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    @Override
    public InqPreviewResponseMsg mobileInqMemberPaymentPreview(
            InqPreviewRequestMsg input) throws Exception {
        InqPreviewResponseMsg result = null;
        String resp = SUCCESS;
        PaymentStatus status = PaymentStatus.UNKNOWN_ERROR;
        String code = "";
        try {
            status = validatePreview(input);
            if (status == null || status == PaymentStatus.PROCESSED) {
                result = mobileServiceLocalImpl.inquiryPreview(input, MOBILE);
                status = PaymentStatus.PROCESSED;
                resp = status.toString();
            } else {
                resp = status.toString();
            }
        } catch (IOException ioe) {
            logger.error(CONFIGURATION_ERROR, ioe);
            status = PaymentStatus.INVALID_PARAMETERS;
            resp = status.toString();
        } catch (final ExternalException e) {
            logger.error(e);
            status = PaymentStatus.UNKNOWN_ERROR;
            code = e.getMessage();
            resp = status.toString();
        } catch (final EntityNotFoundException e) {
            logger.error(e);
            status = PaymentStatus.INVALID_PARAMETERS;
            resp = status.toString();
        } catch(final MaxTopupCCPerDayExceededException e){
        	logger.error(e);
            status = PaymentStatus.EXCEED_MAX_LIMIT_TOPUP_PER_DAY;
            resp = status.toString();
        } catch(final MaxTopupCCPerMonthExceededException e){
        	logger.error(e);
            status = PaymentStatus.EXCEED_MAX_LIMIT_TOPUP_PER_MONTH;
            resp = status.toString();
        } catch(final UpperCreditLimitReachedException e){
        	logger.error(e);
            status = PaymentStatus.RECEIVER_UPPER_CREDIT_LIMIT_REACHED;
            resp = status.toString();
        }catch (Exception e) {
            logger.error(e, e);
            status = PaymentStatus.UNKNOWN_ERROR;
            resp = status.toString();
        } finally {
            if (result == null)
                result = new InqPreviewResponseMsg();
            result.setStatus(resp);
        }

        result.setField1(resolveMessage(MOBILE, status, code, input.getTrxtype()));

        return result;
    }

    @Override
    public InqPreviewResponseMsg ussdInqMemberPaymentPreview(
            InqPreviewRequestMsg input) throws Exception {

        InqPreviewResponseMsg result = null;
        String resp = SUCCESS;
        PaymentStatus status = PaymentStatus.UNKNOWN_ERROR;
        String code = "";
        try {
            status = validatePreview(input);
            if (status == null || status == PaymentStatus.PROCESSED) {
                result = mobileServiceLocalImpl.inquiryPreview(input, USSD);
                status = PaymentStatus.PROCESSED;
                resp = status.toString();
            } else {
                resp = status.toString();
            }
        } catch (IOException ioe) {
            logger.error(CONFIGURATION_ERROR, ioe);
            status = PaymentStatus.INVALID_PARAMETERS;
            resp = status.toString();
        } catch (final ExternalException e) {
            logger.error(e);
            status = PaymentStatus.UNKNOWN_ERROR;
            code = e.getMessage();
            resp = status.toString();
        } catch (final EntityNotFoundException e) {
            logger.error(e);
            status = PaymentStatus.INVALID_PARAMETERS;
            resp = status.toString();
        } catch (Exception e) {
            logger.error(e, e);
            status = PaymentStatus.UNKNOWN_ERROR;
            resp = status.toString();
        } finally {
            if (result == null)
                result = new InqPreviewResponseMsg();
            result.setStatus(resp);
        }

        result.setField1(resolveMessage(USSD, status, code, input.getTrxtype()));

        return result;

    }

    public void setMobileServiceLocalImpl(
            MobileServiceLocalImpl mobileServiceLocalImpl) {
        this.mobileServiceLocalImpl = mobileServiceLocalImpl;
    }

    public PaymentResult_Extended mobileMemberPayment(final PaymentRequestMsg input) throws Exception {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();

        try {
            // Original Member
            paymentParams.setFromMember(input.getFrom());

            // Destination Member (if null or empty, it means the destination is System Account, such as: PLN, Telco)
//            if (!(input.getTrxtype().toLowerCase().equals("p1") || input.getTrxtype().toLowerCase().equals("p2"))) {
            //Update toMember - 2 Oktober 2014
            if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
            paymentParams.setToSystem(true);
            } else {
                paymentParams.setToMember(input.getTo());
            }

            // Transaction Amount
            paymentParams.setAmount(new BigDecimal(input.getAmount()));

            // Transaction Memo
            if (input.getDesc() != null) {
                paymentParams.setDescription(input.getDesc());
            }

            try {
//                if (paymentParams.getToSystem()) {
                    paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(MOBILE + ".transferID." + input.getTrxtype().toLowerCase())));
//                }
            } catch (final IOException e) {
                logger.error(e);
            }

            String customFieldName = "";

            try {
                customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name");
            } catch (final IOException e) {
                logger.error(e);
            }

            if (customFieldName != null && !customFieldName.isEmpty()) {

                FieldValueVO customFieldValue = new FieldValueVO(customFieldName, input.getField1());
                List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
                paymentParams.setCustomValues(customValues);

            }

            paymentParams.setCredentials(input.getCredential());
            
            // Untuk reversal purpose
            StringBuffer revKey = new StringBuffer();
            revKey.append("MOBILE");
            revKey.append(input.getTrxtype());
            revKey.append(RandomStringUtils.randomAlphanumeric(20));
            paymentParams.setTraceNumber(revKey.toString());

            payment.setWebServiceHelper(webServiceHelper);
            payment.setPaymentHelper(paymentHelper);
            payment.setPaymentServiceLocal(paymentServiceLocal);
            payment.setAccountHelper(accountHelper);
            payment.setApplicationServiceLocal(applicationServiceLocal);
            payment.setElementServiceLocal(elementServiceLocal);
            payment.setChannelHelper(channelHelper);
            payment.setAccessServiceLocal(accessServiceLocal);

            // result = payment.doPayment(paymentParams);
            result = doPayment(paymentParams);

        } catch (final ExternalException e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        } catch (final Exception e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        }

        String errorMessage = resolveMessage(MOBILE, result.getStatus(), result.getField1(), input.getTrxtype());

        if ((result.getStatus().equals(PaymentStatus.MIN_AMOUNT_TRANSACTION)) && (errorMessage.contains("{amount}"))) {
        	DecimalFormat format = new DecimalFormat("#,###.##");
         	String formattedMinAmount = format.format(new BigDecimal(result.getField1()));
            errorMessage = errorMessage.replace("{amount}", formattedMinAmount);
          }
        
        result.setField1(errorMessage);

        return result;
    }

    @Override
    public PaymentResult_Extended ussdMemberPayment(final PaymentRequestMsg input) {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        PaymentResponseMsg response = new PaymentResponseMsg();
        String tempAgent = "";

            try {
	        	//Add new logic for agen tarik tunai - 26 Juni 2014
	        	TRX_AGENT = EmoneyConfiguration.getEmoneyProperties().getProperty("agen.tarik_tunai");
	        	//input.getField1 adalah temp member agen untuk tarik tunai agent - 26 Juni 2014
	        	tempAgent = input.getField1();
//	        	if(tempAgent != null && tempAgent.equals(TRX_AGENT)) {
//	        		// get payment amount from request
//	        		response = coreWSFactory.getOTPAgentValidateWebService().validatePayment(input);
//	        		
//	        		result.setStatus(response.getStatus());
//	        		result.setField1(response.getErrocCode());
//	        		
//	        	} else {
	        		// Original Member
		            paymentParams.setFromMember(input.getFrom());
		
		            // Destination Member (if null or empty, it means the destination is System Account, such as: PLN, Telco)
		            //modification for co-branding 10 Juni 2014
		            //SEBELUM
		            //if (!(input.getTrxtype().toLowerCase().equals("p1") || input.getTrxtype().toLowerCase().equals("p2"))) {
		            //SESUDAH
		            if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
		            paymentParams.setToSystem(true);
		            } else {
		                paymentParams.setToMember(input.getTo());
		            }
		
		            // Transaction Amount
		            paymentParams.setAmount(new BigDecimal(input.getAmount()));
		
		            // Transaction Memo
		            if (input.getDesc() != null) {
		                paymentParams.setDescription(input.getDesc());
		            }
		
		            try {
		//                if (paymentParams.getToSystem()) {
		                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(USSD + ".transferID." + input.getTrxtype().toLowerCase())));
		//                }
		            } catch (final IOException e) {
		                logger.error(e);
		            }
		
		            String customFieldName = "";
		            String customFieldName2 = "";
		            String trxtypeidDepositSettlement = "";
		
		            try {
		                customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name");
		                customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name2");
		            } catch (final IOException e) {
		                logger.error(e);
		            }
		
		            if (customFieldName != null && customFieldName2 != null && !customFieldName.isEmpty()) {
		
		                FieldValueVO customFieldValue = new FieldValueVO(customFieldName, input.getField1());
		                List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
		                FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, input.getField9());

		                ArrayList addCustomValues = new ArrayList(customValues);
		                addCustomValues.add(customFieldValue2);
		                paymentParams.setCustomValues(addCustomValues);
		                
		                System.out.println("CUSTOM FROM MobileWebServiceImpl : " + customFieldValue);
			            System.out.println("CUSTOM FROM MobileWebServiceImpl : " + customFieldValue2);
		
		            } else if (customFieldName != null && !customFieldName.isEmpty()){
		            	FieldValueVO customFieldValue = new FieldValueVO(customFieldName, input.getField1());
		                List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
		                paymentParams.setCustomValues(customValues);
		            }
		
		            paymentParams.setCredentials(input.getCredential());
		
		            payment.setWebServiceHelper(webServiceHelper);
		            payment.setPaymentHelper(paymentHelper);
		            payment.setPaymentServiceLocal(paymentServiceLocal);
		            payment.setAccountHelper(accountHelper);
		            payment.setApplicationServiceLocal(applicationServiceLocal);
		            payment.setElementServiceLocal(elementServiceLocal);
		            payment.setChannelHelper(channelHelper);
		            payment.setAccessServiceLocal(accessServiceLocal);
		
		            result = doPayment(paymentParams);
//	        	}
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
    
            String errorMessage = resolveMessage(USSD, result.getStatus(), result.getField1(), input.getTrxtype());
            
            if ((result.getStatus().equals(PaymentStatus.MIN_AMOUNT_TRANSACTION)) && (errorMessage.contains("{amount}"))) {
            	DecimalFormat format = new DecimalFormat("#,###.##");
             	String formattedMinAmount = format.format(new BigDecimal(result.getField1()));
                errorMessage = errorMessage.replace("{amount}", formattedMinAmount);
              }
            
            result.setField1(errorMessage);

        return result;
    }

    public void setPaymentHelper(PaymentHelper paymentHelper) {
        this.paymentHelper = paymentHelper;
    }

    public void setWebServiceHelper(WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }

    public void setPaymentServiceLocal(PaymentServiceLocal paymentServiceLocal) {
        this.paymentServiceLocal = paymentServiceLocal;
    }

    public void setApplicationServiceLocal(ApplicationServiceLocal applicationServiceLocal) {
        this.applicationServiceLocal = applicationServiceLocal;
    }

    public void setElementServiceLocal(ElementServiceLocal elementServiceLocal) {
        this.elementServiceLocal = elementServiceLocal;
    }

    public void setAccountHelper(AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }

    public void setAccessServiceLocal(AccessServiceLocal accessServiceLocal) {
        this.accessServiceLocal = accessServiceLocal;
    }

    public void setChannelHelper(ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    private PaymentResult_Extended doPayment(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";
        Member fromMember = null;

        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();
            
            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferType(dto)) {
                	fromMember = (Member) dto.getFrom();
                	String fromMemberId = fromMember.getGroup().getId().toString();
                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
                    } else {
                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    }
                	webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];
                    } else {
                    	fromMember = (Member) dto.getFrom();
                    	String fromMemberId = fromMember.getGroup().getId().toString();
                    	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
                    		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
                        } else {
                        	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                        }
                        webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            if ((e instanceof MinAmountTransactionException))
                errorCode = ((MinAmountTransactionException)e).getAmount().toString();
              else {
            	  errorCode = e.getMessage();
              }
        }
        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);

        result.setField1(errorCode);

        return result;
    }

    private static class PrepareParametersResult {
        private final PaymentStatus                 status;
        private final AccountOwner                  from;
        private final AccountOwner                  to;
        private final Collection<MemberCustomField> fromRequiredFields;
        private final Collection<MemberCustomField> toRequiredFields;

        public PrepareParametersResult(final PaymentStatus status, final AccountOwner from, final AccountOwner to, final Collection<MemberCustomField> fromRequiredFields, final Collection<MemberCustomField> toRequiredFields) {
            this.status = status;
            this.from = from;
            this.to = to;
            this.fromRequiredFields = fromRequiredFields;
            this.toRequiredFields = toRequiredFields;
        }

        public AccountOwner getFrom() {
            return from;
        }

        public Collection<MemberCustomField> getFromRequiredFields() {
            return fromRequiredFields;
        }

        public PaymentStatus getStatus() {
            return status;
        }

        public AccountOwner getTo() {
            return to;
        }

        public Collection<MemberCustomField> getToRequiredFields() {
            return toRequiredFields;
        }
    }

    private PrepareParametersResult prepareParameters(final PaymentParameters params) {

        final Member restricted = WebServiceContext.getMember();
        final boolean fromSystem = params.getFromSystem();
        final boolean toSystem = params.getToSystem();
        PaymentStatus status = null;
        Member fromMember = null;
        Member toMember = null;
        // Load the from member
        if (!fromSystem) {
            try {
                fromMember = paymentHelper.resolveFromMember(params);
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.FROM_NOT_FOUND;
            }
        }
        // Load the to member
        if (status == null && !toSystem) {
            try {
                toMember = paymentHelper.resolveToMember(params);
                
                String dormantGroup="";
            	try {
					dormantGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("dormant.group.name");
				} catch (IOException e) {
					dormantGroup = "DORMANT";
					e.printStackTrace();
				}
            	
            	if(toMember.getMemberGroup()!= null && toMember.getMemberGroup().getName().equalsIgnoreCase(dormantGroup)){
            		status = PaymentStatus.TO_MEMBER_IS_DORMANT;
            	}
            	
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.TO_NOT_FOUND;
            }
        }

        if (status == null) {
            if (restricted == null) {
                // Ensure has the do payment permission
                if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                    throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                }
                // Check the channel immediately, as needed by SMS controller
                if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                    status = PaymentStatus.INVALID_CHANNEL;
                }
            } else {
                // Enforce the restricted to member parameters
                if (fromSystem) {
                    // Restricted to member can't perform payment from system
                    status = PaymentStatus.FROM_NOT_FOUND;
                } else {
                    if (fromMember == null) {
                        fromMember = restricted;
                    } else if (toMember == null && !toSystem) {
                        toMember = restricted;
                    }
                }
                if (status == null) {
                    // Check make / receive payment permissions
                    if (fromMember.equals(restricted)) {
                        if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                        }
                    } else {
                        if (!WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                        }
                    }
                    // Ensure that either from or to member is the restricted one
                    if (!fromMember.equals(restricted) && !toMember.equals(restricted)) {
                        status = PaymentStatus.INVALID_PARAMETERS;
                        webServiceHelper.trace(status + ". Reason: Neither the origin nor the destination members are equal to the restricted: " + restricted);
                    }
                }
                if (status == null) {
                    // Enforce the permissions
                    if (restricted.equals(fromMember) && !WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                    } else if (restricted.equals(toMember) && !WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                    }
                }
            }
        }

        // Ensure both from and to member are present
        if (status == null) {
            if (fromMember == null && !fromSystem) {
                status = PaymentStatus.FROM_NOT_FOUND;
            } else if (toMember == null && !toSystem) {
                status = PaymentStatus.TO_NOT_FOUND;
            } else if (fromMember != null && toMember != null) {
                // Ensure the to member is visible by the from member
            	final Collection<MemberGroup> visibleGroups = fromMember.getMemberGroup().getCanViewProfileOfGroups();            	
            	if (CollectionUtils.isEmpty(visibleGroups) || !visibleGroups.contains(toMember.getGroup())) {
                    status = PaymentStatus.TO_NOT_FOUND;
                }
            }
        }

        // Ensure required CF are present ONLY for unrestricted client
        Collection<MemberCustomField> fromMemberfields = null, toMemberfields = null;
        if (status == null) {
            boolean hasFromRequired = CollectionUtils.isNotEmpty(params.getFromMemberFieldsToReturn());
            boolean hasToRequired = CollectionUtils.isNotEmpty(params.getToMemberFieldsToReturn());
            if (restricted != null && (hasFromRequired || hasToRequired) || hasFromRequired && fromSystem || hasToRequired && toSystem) {
                webServiceHelper.trace(restricted != null ? "Restricted web service clients are not allowed to require member custom field values" : "Can't require custom field values for a system payment");
                status = PaymentStatus.INVALID_PARAMETERS;
            }
            if (status == null && hasFromRequired) {
                fromMemberfields = getMemberCustomFields(fromMember, params.getFromMemberFieldsToReturn());
                status = fromMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }

            if (status == null && hasToRequired) {
                toMemberfields = getMemberCustomFields(toMember, params.getToMemberFieldsToReturn());
                status = toMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }
        }

        if (status == null) {
            // Check the channel
            if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
        }
        if (status == null) {
            // Check the credentials
            boolean checkCredentials;
            if (restricted != null) {
                checkCredentials = !fromMember.equals(restricted);
            } else {
                checkCredentials = !fromSystem && WebServiceContext.getClient().isCredentialsRequired();
            }
            if (checkCredentials) {
                try {
                    checkCredentials(fromMember, WebServiceContext.getChannel(), params.getCredentials());
                } catch (final InvalidCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
            }
        }

        // No error
        final AccountOwner fromOwner = fromSystem ? SystemAccountOwner.instance() : fromMember;
        final AccountOwner toOwner = toSystem ? SystemAccountOwner.instance() : toMember;
        return new PrepareParametersResult(status, fromOwner, toOwner, fromMemberfields, toMemberfields);
    }

    private boolean validateTransferType(final DoPaymentDTO dto) {
        final Collection<TransferType> possibleTypes = paymentHelper.listPossibleTypes(dto);
        return possibleTypes != null && possibleTypes.contains(dto.getTransferType());
    }

    private AccountStatusVO[] getAccountStatusesForPayment(final PaymentParameters params, final Transfer transfer) {
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS) && params.getReturnStatus()) {
            if (WebServiceContext.getMember() == null) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            } else if (WebServiceContext.getMember().equals(paymentHelper.resolveFromMember(params))) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
            } else {
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            }
        }
        return new AccountStatusVO[] {
                accountHelper.toVO(fromMemberStatus),
                accountHelper.toVO(toMemberStatus) };
    }

    private Collection<MemberCustomField> getMemberCustomFields(final Member member, final List<String> fieldInternalNames) {
        Collection<MemberCustomField> fields = new HashSet<MemberCustomField>();

        for (final String internalName : fieldInternalNames) {
            MemberCustomFieldValue mcfv = (MemberCustomFieldValue) CollectionUtils.find(member.getCustomValues(), new Predicate() {
                @Override
                public boolean evaluate(final Object object) {
                    MemberCustomFieldValue mcfv = (MemberCustomFieldValue) object;
                    return mcfv.getField().getInternalName().equals(internalName);
                }
            });
            if (mcfv == null) {
                webServiceHelper.trace(String.format("Required field '%1$s' was not found for member %2$s", internalName, member));
                return null;
            } else {
                fields.add(memberCustomFieldServiceLocal.load(mcfv.getField().getId()));
            }
        }

        return fields;
    }

    private void checkCredentials(Member member, final Channel channel, final String credentials) {
        if (member == null) {
            return;
        }
        final ServiceClient client = WebServiceContext.getClient();
        final Member restrictedMember = client.getMember();
        if (restrictedMember == null) {
            // Non-restricted clients use the flag credentials required
            if (!client.isCredentialsRequired()) {
                // No credentials should be checked
                throw new InvalidCredentialsException();
            }
        } else {
            // Restricted clients don't need check if is the same member
            if (restrictedMember.equals(member)) {
                throw new InvalidCredentialsException();
            }
        }
        if (StringUtils.isEmpty(credentials)) {
            throw new InvalidCredentialsException();
        }
        member = elementServiceLocal.load(member.getId(), Element.Relationships.USER);
        accessServiceLocal.checkCredentials(channel, member.getMemberUser(), credentials, WebServiceContext.getRequest().getRemoteAddr(), WebServiceContext.getMember());
    }

    private String resolveMessage(final String channel, final PaymentStatus status, final String code, final String trxType) {
        String errorKey = status.toString();
        String errorMessage = "";

        if (status == PaymentStatus.UNKNOWN_ERROR) {

//            if (!trxType.toLowerCase().equals("p1")) {
//                errorKey = errorKey + "." + trxType.toLowerCase().substring(0, 2) + "." + code;
//            }
        	//Update cek nomor - 2 Oktober 2014
        	if (!(checkTransferTypeCode(trxType, "validate.autoCreate"))) {
                errorKey = errorKey + "." + trxType.toLowerCase().substring(0, 2) + "." + code;
            }

        }

        try {
            if (channel.equals(MOBILE)) {
                errorMessage = EmoneyConfiguration.getMobileErrorMessage().getProperty(errorKey);
            } else {
                errorMessage = EmoneyConfiguration.getUSSDErrorMessage().getProperty(errorKey);
            }
        } catch (final IOException e) {
            logger.info(e);
        }

        return errorMessage;
    }

    @Override
    public NotificationResponseMsg mobileNotification(NotificationRequestMsg input) throws Exception {

        NotificationResponseMsg result = null;
        String resp = SUCCESS;
        try {
            result = mobileServiceLocalImpl.searchMessage(input, MOBILE);
        } catch (Exception e) {
        	logger.warn(e);
            logger.debug(e, e);
            resp = ERROR_UNKNOWN_ERROR + e.getMessage();
        } finally {
            if (result == null) {
                result = new NotificationResponseMsg();
            }
            result.setStatus(resp);
        }
        return result;
    }

    @Override
    public NotificationResponseMsg ussdNotification(NotificationRequestMsg input) throws Exception {
        NotificationResponseMsg result = null;
        String resp = SUCCESS;
        try {
            result = mobileServiceLocalImpl.searchMessage(input, USSD);
        } catch (Exception e) {
        	logger.warn(e);
            logger.debug(e, e);
            resp = ERROR_UNKNOWN_ERROR + e.getMessage();
        } finally {
            if (result == null) {
                result = new NotificationResponseMsg();
            }
            result.setStatus(resp);
        }
        return result;
    }

    @Override
    public ResetPswdResponseMsg mobileResetPassword(ResetPswdRequestMsg input) throws Exception {

        ResetPswdResponseMsg result = null;
        String resp = SUCCESS;
        try {
            result = mobileServiceLocalImpl.resetPassword(input, MOBILE);
        } catch (Exception e) {
        	logger.warn(e);
            logger.debug(e, e);
            resp = ERROR;
            if (e != null && e.getMessage() != null && e.getMessage().startsWith(MOBILE)) {
                resp = EmoneyConfiguration.getMobileErrorMessage().getProperty(e.getMessage(), UNKNOWN_ERROR);
            }
            ;

        } finally {
            if (result == null) {
                result = new ResetPswdResponseMsg();
            }
            result.setStatus(resp);
        }
        return result;
    }

    @Override
    public ResetPswdResponseMsg ussdResetPassword(ResetPswdRequestMsg input) throws Exception {
        ResetPswdResponseMsg result = null;
        String resp = SUCCESS;
        try {
            result = mobileServiceLocalImpl.resetPassword(input, USSD);
        } catch (Exception e) {
        	logger.warn(e);
            logger.debug(e, e);
            resp = ERROR;
            if (e != null && e.getMessage() != null && e.getMessage().startsWith(USSD)) {
                resp = EmoneyConfiguration.getUSSDErrorMessage().getProperty(e.getMessage(), UNKNOWN_ERROR);
            }
            ;
        } finally {
            if (result == null) {
                result = new ResetPswdResponseMsg();
            }
            result.setStatus(resp);
        }
        return result;
    }

    public PaymentStatus validatePreview(InqPreviewRequestMsg input) {
        Member toMember = null;
        Member fromMember = null;
        PaymentStatus status = null;
        PaymentParameters params = new PaymentParameters();
        boolean isNewAccount = false;

        // load the from Member, if transaction type = p8 no need to check to From member
        if(!checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.p8")) {
	        try {
	            if (input.getFrom().isEmpty() || input.getFrom() == null) {
	                status = PaymentStatus.FROM_NOT_FOUND;
	            } else {
	                params.setFromMember(input.getFrom());
	                fromMember = paymentHelper.resolveFromMember(params);
	            }
	        } catch (final EntityNotFoundException e) {
	            webServiceHelper.error(e);
	            status = PaymentStatus.FROM_NOT_FOUND;
	        }
        }
        // Load the to member
//        if (status == null && (input.getTrxtype().toLowerCase().equals("p1") || input.getTrxtype().toLowerCase().equals("p2"))) {
//            try {
//                if (input.getTo().isEmpty() || input.getTo() == null) {
//                    status = PaymentStatus.TO_NOT_FOUND;
//                } else {
//                    params.setToMember(input.getTo());
//                    toMember = paymentHelper.resolveToMember(params);
//                }
//            } catch (final EntityNotFoundException e) {
//                webServiceHelper.error(e);
//                status = PaymentStatus.TO_NOT_FOUND;
//                isNewAccount = true;
//            }
//        }
        
        //Update load toMember - 2 Oktober 2014
        if (status == null && (checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
            try {
                if (input.getTo().isEmpty() || input.getTo() == null) {
                    status = PaymentStatus.TO_NOT_FOUND;
                } else {
                    params.setToMember(input.getTo());
                    toMember = paymentHelper.resolveToMember(params);
                }
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.TO_NOT_FOUND;
                isNewAccount = true;
            }
        }
        
        if (isNewAccount) {
            status = checkForAutoCreate(input.getTo(), input.getTrxtype());
        }
        
        return status;
    }

    @Override
    public CifAcctInqResponseMsg cifAcctInq(
            @WebParam(name = "request") CifAcctInqRequestMsg input)
            throws Exception {
        CifAcctInqResponseMsg result = new CifAcctInqResponseMsg();
        Member member = mobileServiceLocalImpl.toMember(input.getMobilePhone());
        String cifInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.cif_custom_fieldname");
        String cifNumber = "";
        if (member != null) {
            Collection<MemberCustomFieldValue> memberCustomFields = member
                    .getCustomValues();
            Iterator itMember = memberCustomFields.iterator();
            // paymentParams.setToMember(member.getUser().getUsername());
            while (itMember.hasNext()) {
                MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember
                        .next();
                if (mCustomField.getField().getInternalName()
                        .equals(cifInternalName)) {
                    cifNumber = mCustomField.getValue();
                    break;
                }
            }
        }

        ArrayList acctNumbers = loadListRekening(cifNumber);
        result.setCustomerNumber(cifNumber);
        result.setAccountNumber(acctNumbers);
        return result;
    }

    public ArrayList loadListRekeningDummy(String cifNumber) throws IOException {
        ArrayList listRekeningNo = new ArrayList();
        listRekeningNo.add("1234567890");
        listRekeningNo.add("2345678900");
        listRekeningNo.add("3456789012");
        return listRekeningNo;
    }

    public ArrayList loadListRekening(String cifNumber) throws IOException {
        ArrayList listRekeningNo = new ArrayList();
        if (cifNumber != null && !cifNumber.equals("")) {
            ArrayOfAccounts arrAcct = new ArrayOfAccounts();
            AcctInfoInquiryResponse resp = getCIFAccountInquiry(cifNumber, null, null);
            if (resp != null && resp.getBdyAccountList() != null && resp.getBdyAccountList().getArrayOfAccounts() != null) {
                arrAcct = resp.getBdyAccountList();
                int noOfRecords = 0;
                try {
                    noOfRecords = Long.valueOf(EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.noofrecordstoretrieve")).intValue();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
                while (noOfRecords > 1 && resp.getDataHeader() != null && resp.getDataHeader().getMoreRecordsIndicator() != null && resp.getDataHeader().getMoreRecordsIndicator().equalsIgnoreCase("Y")) {
                    AccountDetail[] acctDetails = arrAcct.getArrayOfAccounts();
                    String accountNumber = resp.getBdyAccountList().getArrayOfAccounts()[noOfRecords - 1].getAccountNumber();
                    String accountType = resp.getBdyAccountList().getArrayOfAccounts()[noOfRecords - 1].getAccountType();
                    resp = null;
                    resp = getCIFAccountInquiry(cifNumber, accountNumber, accountType);
                    if (resp != null && resp.getBdyAccountList() != null && resp.getBdyAccountList().getArrayOfAccounts() != null) {
                        for (AccountDetail acctDet : resp.getBdyAccountList().getArrayOfAccounts()) {
                            if (!acctDet.getAccountNumber().equals(accountNumber)) {
                                acctDetails = append(acctDetails, acctDet);
                            }
                        }
                        arrAcct.setArrayOfAccounts(acctDetails);
                    }
                }
            }

            AccountDetail[] acctDetails = arrAcct.getArrayOfAccounts();
            if (acctDetails != null && acctDetails.length > 0) {
                for (AccountDetail accDet : acctDetails) {
                    if (check(accDet.getAccountType())) {
                        listRekeningNo.add(accDet.getAccountNumber());
                    }
                }
            }
        }
        return listRekeningNo;
    }
    
    public AcctInfoInquiryResponse getCIFAccountInquiry(String customerNumber,
            String accountNumber, String accountType) {
        AcctInfoInquiryRequest request = new AcctInfoInquiryRequest();
        AcctInfoInquiryResponse response = null;

        SimpleDateFormat sdfTimestamp = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.'0Z'");
        try {
            final String extId = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.cifacctinquiry.extId");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.cifacctinquiry.tellerId");
            final String journalSeq = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.cifacctinquiry.journalSequence");
            final String tranCode = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.cifacctinquiry.transactionCode");
            final String channelId = EmoneyConfiguration.getEmoneyProperties()
                    .getProperty("core.cifacctinquiry.channelId");
            final long noOfRecords = Long.valueOf(
                    EmoneyConfiguration.getEmoneyProperties().getProperty(
                            "core.cifacctinquiry.noofrecordstoretrieve"))
                    .longValue();

            request.setBdyCustomerNumber(customerNumber);
            if (accountNumber != null && accountType != null) {
                request.setBdyAccountNumber(accountNumber);
                request.setBdyAccountType(accountType);
            }

            Calendar actualDate = Calendar.getInstance();
            String sActualDate = sdfTimestamp.format(actualDate.getTime());

            RequestHeader header = new RequestHeader();
            header.setHdrExternalId(extId);
            header.setHdrTellerId(Long.parseLong(tellerId));
            header.setHdrJournalSequence(Long.parseLong(journalSeq));
            header.setHdrTransactionCode(tranCode);
            header.setHdrChannelId(channelId);
            header.setHdrTimestamp(sActualDate);

            DataHeaderMBase dataHeader = new DataHeaderMBase();
            dataHeader.setNoOfRecordsToRetrieve(noOfRecords);
            request.setDataHeader(dataHeader);

            SecurityHeader securityHeader = new SecurityHeader();
            securityHeader.setHdrAppliNo("1");
            securityHeader.setHdrChallenge1("1");
            securityHeader.setHdrChallenge2("1");
            securityHeader.setHdrChallenge3("1");
            securityHeader.setHdrChallenge4("1");
            securityHeader.setHdrChallengeType("1");
            securityHeader.setHdrCorporateId("1");
            securityHeader.setHdrCustomerAdditionalId("1");
            securityHeader.setHdrResponseToken("1");
            request.setSecurityHeader(securityHeader);
            request.setHeader(header);

            response = coreWSFactory.getAccountInquiryWebService().inquiry(
                    request);
            String responseMsg = "";
            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()) {
                    case 1:
                        logger.info("sukses get CIF account.");
                        // return response;
                        break;
                    default:
                        responseMsg = response.getResponseHeader()
                                .getHdrResponseMessage();
                        logger.error(responseMsg);
                        throw new Exception("System Error (Host Error) : "
                                + responseMsg);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new Exception(
                            "System Error (Response Header is empty).");
                }
            } else {
                logger.error("Response is empty.");
                throw new Exception("System Error (Response is empty).");
            }

        } catch (final SocketTimeoutException e) {
            logger.error(e);
        } catch (final Exception e) {
            logger.error(e);
        }

        return response;
    }
    
    static <T> T[] append(T[] arr, T element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }
    
    public boolean check(String a){
        String temp = "";
        try {
            temp = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.accounttype");
            while(true){
                if(temp.indexOf(",")!= -1){
                    if(temp.substring(0, temp.indexOf(",")).equals(a)){
                        return true;
                    }
                }else{
                    if(temp.substring(0, temp.length()).equals(a)){
                        return true;
                    }
                    break;
                }
                temp = temp.substring(temp.indexOf(",")+1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
        
    }

    @Override
    public InviteResponseMsg mobileInviteOther(InviteRequestMsg input) throws Exception {
        InviteResponseMsg result = null;
        String resp = SUCCESS;
        try {

            result = mobileServiceLocalImpl.inviteOther(input, MOBILE);

        } catch (IOException ioe) {
            logger.error(CONFIGURATION_ERROR, ioe);
            resp = CONFIGURATION_ERROR;
        } catch (Exception e) {
            logger.error(e, e);

            if (e != null && e.getMessage() != null && e.getMessage().startsWith(MOBILE)) {
                resp = EmoneyConfiguration.getMobileErrorMessage().getProperty(e.getMessage(), UNKNOWN_ERROR);
            } 

        } finally {
            if (result == null)
                result = new InviteResponseMsg();
            result.setStatus(resp);
        }
        return result;
    }
    
    
    private boolean paymentGroupCheck(final String fromMember, final TransferType type) {
        final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
        final Member member = elementServiceLocal.loadByPrincipal(principalType, fromMember, FETCH);
        boolean isPaymentAllow = false;

        if (member.getGroup().getTransferTypes().contains(type)) {
            isPaymentAllow = true;
        }
        return isPaymentAllow;
    }

    public void setMemberHelper(MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }

    public void setGroupServiceLocal(GroupServiceLocal groupServiceLocal) {
        this.groupServiceLocal = groupServiceLocal;
    }

    private PaymentStatus checkForAutoCreate(String to, String type) {
        PaymentStatus status = PaymentStatus.TO_NOT_FOUND;
        
//        // Auto create only for P2P Transfer
//        if (!type.toLowerCase().equals("p1")) {
//            return status;
//        }
        //Update Auto create only for P2P Transfer - 2 Oktober 2014
        if (!(checkTransferTypeCode(type, "validate.autoCreate"))) {
            return status;
        }
        
        Member member = null;
        boolean isNewAccount = false;
        final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
        
        try {
            member = elementServiceLocal.loadByPrincipal(principalType, to, FETCH);
            status = PaymentStatus.PROCESSED;
        } 
        catch (EntityNotFoundException e) {
            isNewAccount = true;
        }

        if (isNewAccount) {
        
        	try {
				CIF_NAME = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.nama_cif_custom_fieldname");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				System.out.println("can't find custom field : "+e1.getLocalizedMessage());
			}
            MemberWebServiceImpl newAccountService = new MemberWebServiceImpl();
            newAccountService.setElementServiceLocal(elementServiceLocal);
            newAccountService.setMemberHelper(memberHelper);
            
            String init = RandomStringUtils.randomNumeric(6);
//            init = hashHandler.newHash(HashHandler.SHA_256, hashHandler.newHash(HashHandler.MD5, init).toLowerCase());
            RegisterMemberParameters registrationParams = new RegisterMemberParameters();
            RegistrationFieldValueVO customField = new RegistrationFieldValueVO(CUSTOM_FIELD_MOBILE_PHONE, to, false);
            RegistrationFieldValueVO customField1 = new RegistrationFieldValueVO(INIT_PWD_PREFIX, init, false);
            RegistrationFieldValueVO customField2 = new RegistrationFieldValueVO(DEVICE_PAIR, "false", false);
            RegistrationFieldValueVO customField3 = new RegistrationFieldValueVO(CIF_NAME, NEW_ACCOUNT, false);
            List<RegistrationFieldValueVO> fieldValueVOs = Arrays.asList(customField, customField1, customField2, customField3);
            registrationParams.setUsername(to);
            registrationParams.setName(NEW_ACCOUNT);
            registrationParams.setLoginPassword(init);
            registrationParams.setFields(fieldValueVOs);
            
            Long groupId = new Long(0);
            final GroupQuery query = new GroupQuery();
            query.setNatures(Group.Nature.MEMBER);
            
            final List<? extends Group> possibleNewGroups = groupServiceLocal.search(query);
            
            //Modification for co-branding (add member's group for auto create by regex)
            if(validatePrefixNumber(registrationParams.getUsername())) {
	
	            if (possibleNewGroups != null) {
	                for (int i = 0; i < possibleNewGroups.size(); i++)
	                {
	                    final Group group = possibleNewGroups.get(i);
	                    if (group.getName().equals(UNREGISTERED))
	                    {
	                        groupId = group.getId();
	                        break;
	                    }
	                }
	            }
	            
	            if (groupId > 0) {
	                registrationParams.setGroupId(groupId);
	            }
	            
	            try {
	                newAccountService.registerMember(registrationParams);
	            } catch (final Exception e) {
	                logger.error(e);
	            }
	    
	            member = elementServiceLocal.loadByPrincipal(principalType, to, FETCH);
	            
	            if (member != null) {
	                status = PaymentStatus.PROCESSED; 
	                
	                // Send notification to member how to use e-cash
	                sendSMS(member);
	            }
            } else {
            	// e-cash number not valid - 05/09/2017
            	logger.info("e-cash number pattern not match (autocreate) for: " + to);
            	status = PaymentStatus.PATTERN_NOT_MATCH;
            }
        
        }
        
        return status;
    }
    
    @Override
    public PaymentStatus registerIfNotExits(final PaymentRequestMsg input) {
        return checkForAutoCreate(input.getTo(), input.getTrxtype());
    }
    
    @Override
    public OpenOTPStatus generateNonTrxOTP(final OpenOTPTicketExtVO reqTicket) {
        final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
        OpenOTPStatus status = null;
        List<FieldValueVO>  fields;
        FieldValueVO fieldVO = null;
        
        try {
            final Member fromMember = elementServiceLocal.loadByPrincipal(principalType, reqTicket.getFromMember().getUsername(), FETCH);
            
            // Check the webservice permission
            if (!WebServiceContext.hasPermission(ServiceOperation.MEMBERS)) {
                status = OpenOTPStatus.OTP_REQUEST_FAILED_PERMISSION_DENIED;
            }
            // Check the channel immediately, as needed by SMS controller
            if (status == null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                status = OpenOTPStatus.OTP_REQUEST_FAILED_MEMBER_CHANNEL_NOT_ALLOWED;
            }

            if (status == null) {
                // validate user credentials
                //checkCredentials(fromMember, WebServiceContext.getChannel(), reqTicket.getCredentials());
    
                
                fieldVO = new FieldValueVO();
                fieldVO.setInternalName(CUSTOM_FIELD_MOBILE_PHONE);
                fieldVO.setValue(reqTicket.getFromMember().getUsername());
                fields = Arrays.asList(fieldVO);
    
                final MemberVO member = reqTicket.getFromMember();
                member.setName(fromMember.getName());
                member.setFields(fields);
                member.setId(fromMember.getId());
    
                OpenOTPTicketVO ticket = new OpenOTPTicketVO();
                ticket.setFromMember(member);
                ticket.setFromChannel(reqTicket.getFromChannel());
                ticket.setDescription(reqTicket.getDescription());
        
                // generate non trx otp
                status = coreWSFactory.getOpenOTPWebService().generateNonTrxOTP((OpenOTPTicketVO) ticket);
            }

        } catch (final InvalidCredentialsException e) {
            webServiceHelper.error(e);
            status = OpenOTPStatus.OTP_REQUEST_FAILED_INVALID_CREDENTIALS;
        } catch (final BlockedCredentialsException e) {
            webServiceHelper.error(e);
            status = OpenOTPStatus.OTP_REQUEST_FAILED_BLOCKED_CREDENTIALS;
        } catch (final EntityNotFoundException e) {
            webServiceHelper.error(e);
            logger.info(reqTicket.getFromMember().getUsername() + " belum terdaftar di mandiri e-cash.");
            status = OpenOTPStatus.OTP_REQUEST_FAILED_MEMBER_NOT_FOUND;
        } catch (final Exception e) {
            webServiceHelper.error(e);
            logger.error(e, e);
            status = OpenOTPStatus.UNKNOWN_ERROR;
        } finally {
            if (status == null) {
                logger.error("unhandled error");
                status = OpenOTPStatus.UNKNOWN_ERROR;
            }
        }
        
        return status;
    }
    
    public void setHashHandler(final HashHandler hashHandler) {
        this.hashHandler = hashHandler;
    }

    public void setSmsMailingServiceLocal(SmsMailingServiceLocal smsMailingServiceLocal) {
        this.smsMailingServiceLocal = smsMailingServiceLocal;
    }

    public void sendSMS(Member member){
        try {
           final SmsMailing sms = new SmsMailing();
           final String smsTemplate;
           //Modification for co-branding (add member's group for auto create by regex)
           if (groupUnregMember == null) {
        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("topup.auto-create");
           } else {
        	   smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("topup.auto-create." + groupUnregMember);
           }
           sms.setFree(true);
           sms.setMember(member);
           sms.setText(smsTemplate);
           LoggedUser.init(member.getUser());
           smsMailingServiceLocal.send(sms);
           LoggedUser.cleanup();
       } catch (Exception e) {
           logger.error(e);
       }

    }
    
  //Modification for co-branding (add member's group for auto create by regex)
    private boolean validatePrefixNumber(final String number) {
        String regex;
        boolean isValid = false;
        String PREFIX_NUMBERS = "";
        
        try {
        	PREFIX_NUMBERS = EmoneyConfiguration.getEmoneyProperties().getProperty("valid.prefixNumber");
        } catch (IOException e) {
        	logger.error(e);
        }
        
        if (PREFIX_NUMBERS != null) {
        	prefixNumber = PREFIX_NUMBERS.split("[,]");
        }
        
            for (int i=0; i<prefixNumber.length; i++) {
                
                regex = "";
            
				// rule to prevent auto create
                try {
                        regex = EmoneyConfiguration.getEmoneyProperties().getProperty(prefixNumber[i].toString() + ".regex");
                        groupUnregMember = prefixNumber[i].toString();
                        
                    } catch (IOException e) {
                        logger.error(e);
                    }
                
                    if (regex.length() > 0) {
                        final Pattern validateEcashPattern = Pattern.compile(regex);
                        final Matcher validateEcashMatcher = validateEcashPattern.matcher(number);
                        isValid = validateEcashMatcher.find();
                    }

                    if (isValid) {
                    	try {
            				UNREGISTERED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.group.unregistered." + groupUnregMember);
            			} catch (IOException e) {
            				logger.error(e);
            			}
                    	break;
                }

            }
        
        return isValid;
    }
    
    public boolean checkTransferTypeCode(String transferTypeCode, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(transferTypeCode)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(transferTypeCode)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    public boolean checkGroup(String groupId, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(groupId)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(groupId)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    @Override
    public PaymentResult_Extended mobileMemberPaymentExternal(final PaymentRequestMsg input) {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        PaymentResponseMsg response = new PaymentResponseMsg();
        String tempAgent = "";

            try {
	        	TRX_AGENT = EmoneyConfiguration.getEmoneyProperties().getProperty("agen.tarik_tunai");
	        	tempAgent = input.getField1();
	            paymentParams.setFromMember(input.getFrom());
		        if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
		            paymentParams.setToSystem(true);
		        } else {
		            paymentParams.setToMember(input.getTo());
		        }
		
		        // Transaction Amount
		        paymentParams.setAmount(new BigDecimal(input.getAmount()));
		
		        // Transaction Memo
		        if (input.getDesc() != null) {
		           paymentParams.setDescription(input.getDesc());
		         }
		
		         try {
		                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(MOBILE + ".transferID." + input.getTrxtype().toLowerCase())));
		         } catch (final IOException e) {
		                logger.error(e);
		         }
		         
		         List<FieldValueVO> customValues = new ArrayList<FieldValueVO>();
		         TransferType transferType = new TransferType();
		         transferType.setId(paymentParams.getTransferTypeId());
		         List<PaymentCustomField> allowedFields = paymentCustomFieldServiceLocal.list(transferType, true);
		         
		         
		         HashMap<String, String> map = new HashMap<String, String>();
		         
		         for (int i=1; i<allowedFields.size(); i++){
		        	 String customFieldName = "a"+i;
			         try {
			        	 map.put(customFieldName, EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name_"+i));
			        	 
			             if (map.get(customFieldName) != null && !map.get(customFieldName).isEmpty()) {
			            	 if(i==1){
			            		 FieldValueVO customFieldValue = new FieldValueVO(map.get(customFieldName), input.getField1());
			            		 customValues.add(customFieldValue);
			            	 }else{
			            		 FieldValueVO customFieldValue2 = new FieldValueVO(map.get(customFieldName), input.getDesc());
			            		 customValues.add(customFieldValue2);
			            	 }
				        	 
				         }
			         } catch (final IOException e) {
			             logger.error(e);
			         }
			
		         }
		         paymentParams.setCustomValues(customValues);
		         paymentParams.setCredentials(input.getCredential());
		
		         payment.setWebServiceHelper(webServiceHelper);
		         payment.setPaymentHelper(paymentHelper);
		         payment.setPaymentServiceLocal(paymentServiceLocal);
		         payment.setAccountHelper(accountHelper);
		         payment.setApplicationServiceLocal(applicationServiceLocal);
		         payment.setElementServiceLocal(elementServiceLocal);
		         payment.setChannelHelper(channelHelper);
		         payment.setAccessServiceLocal(accessServiceLocal);
		
		         result = doPaymentExternal(paymentParams);
		         
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
    
            final String errorMessage = resolveMessage(USSD, result.getStatus(), result.getField1(), input.getTrxtype());
            
            result.setTransfer(result.getTransfer());
            result.setField1(errorMessage);

        return result;
    }
    
    @Override
    public PaymentResult_Extended ussdMemberPaymentExternal(final PaymentRequestMsg input) {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        PaymentResponseMsg response = new PaymentResponseMsg();
        String tempAgent = "";

            try {
	        	TRX_AGENT = EmoneyConfiguration.getEmoneyProperties().getProperty("agen.tarik_tunai");
	        	tempAgent = input.getField1();
	            paymentParams.setFromMember(input.getFrom());
		        if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
		            paymentParams.setToSystem(true);
		        } else {
		            paymentParams.setToMember(input.getTo());
		        }
		
		        // Transaction Amount
		        paymentParams.setAmount(new BigDecimal(input.getAmount()));
		
		        // Transaction Memo
		        if (input.getDesc() != null) {
		           paymentParams.setDescription(input.getDesc());
		         }
		
		         try {
		                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(USSD + ".transferID." + input.getTrxtype().toLowerCase())));
		         } catch (final IOException e) {
		                logger.error(e);
		         }
		         
		         List<FieldValueVO> customValues = new ArrayList<FieldValueVO>();
		         TransferType transferType = new TransferType();
		         transferType.setId(paymentParams.getTransferTypeId());
		         List<PaymentCustomField> allowedFields = paymentCustomFieldServiceLocal.list(transferType, true);
		         
		         
		         HashMap<String, String> map = new HashMap<String, String>();
		         
		         for (int i=1; i<allowedFields.size(); i++){
		        	 String customFieldName = "a"+i;
			         try {
			        	 map.put(customFieldName, EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name_"+i));
			        	 
			             if (map.get(customFieldName) != null && !map.get(customFieldName).isEmpty()) {
			            	 if(i==1){
			            		 FieldValueVO customFieldValue = new FieldValueVO(map.get(customFieldName), input.getField1());
			            		 customValues.add(customFieldValue);
			            	 }else{
			            		 FieldValueVO customFieldValue2 = new FieldValueVO(map.get(customFieldName), input.getDesc());
			            		 customValues.add(customFieldValue2);
			            	 }
				        	 
				         }
			         } catch (final IOException e) {
			             logger.error(e);
			         }
			
		         }
		         paymentParams.setCustomValues(customValues);
		         paymentParams.setCredentials(input.getCredential());
		
		         payment.setWebServiceHelper(webServiceHelper);
		         payment.setPaymentHelper(paymentHelper);
		         payment.setPaymentServiceLocal(paymentServiceLocal);
		         payment.setAccountHelper(accountHelper);
		         payment.setApplicationServiceLocal(applicationServiceLocal);
		         payment.setElementServiceLocal(elementServiceLocal);
		         payment.setChannelHelper(channelHelper);
		         payment.setAccessServiceLocal(accessServiceLocal);
		
		         result = doPaymentExternal(paymentParams);
		         
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
    
            final String errorMessage = resolveMessage(USSD, result.getStatus(), result.getField1(), input.getTrxtype());
            
            result.setTransfer(result.getTransfer());
            result.setField1(errorMessage);

        return result;
    }

	public PaymentResult_Extended doPaymentExternal(final PaymentParameters params) {
   	 AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";
        String prefixErrorKey = "";
        Member fromMember = null;
        
        try {
        	
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());
                
                // Validate the transfer type
                if (!validateTransferType(dto)) {
                	if(dto.getFrom() != null) {
	                	fromMember = (Member) dto.getFrom();
	                	String fromMemberId = fromMember.getGroup().getId().toString();
	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
	                    } else {
	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
	                    }
                	} else {
                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                	}
                	webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    if (params.getFromSystem() || paymentGroupCheck(params.getFromMember(), dto.getTransferType())) {
                    
                        transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                        status = paymentHelper.toStatus(transfer);
                        transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                        final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                        fromMemberStatus = statuses[0];
                        toMemberStatus = statuses[1];

                    } else {
                    	if(dto.getFrom() != null) {
    	                	fromMember = (Member) dto.getFrom();
    	                	String fromMemberId = fromMember.getGroup().getId().toString();
    	                	if ((checkGroup(fromMemberId, "list.registered.group.id"))) {
    	                		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED_FOR_REGISTERED;
    	                    } else {
    	                    	status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
    	                    }
                    	} else {
                    		status = PaymentStatus.TRANSFER_TYPE_NOT_ALLOWED;
                    	}
                    	webServiceHelper.trace(status + ". Reason: The member doesn't have permission to the specified transfer type: " + dto.getTransferType());
                    }
                }
            }
        }catch (final ExternalException e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            
            	
            	errorCode = e.getMessage();
        }catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
        }
        
        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }else{
		    	System.out.println("Result : " + result);
		    	try{
		    		Date nowDate = new Date();
		    		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		
		    		ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
		    		commandActiveMQ cAMQ = (commandActiveMQ) ctx.getBean("test");
		    		
		    		StringWriter sw = new StringWriter();
		    		JAXBContext jaxbContext = JAXBContext.newInstance(PaymentResult_Extended.class);
		    		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		    		jaxbMarshaller.marshal(result, sw);
		    		String xmlString = sw.toString();
		    		
		    		String queueEPL = EmoneyConfiguration.getEmoneyProperties().getProperty("queue.epl");
		    		String queueLog = EmoneyConfiguration.getEmoneyProperties().getProperty("queue.eplLog");
		    		
		    		logger.info("---Send To Active MQ---" +" Tanggal :"+ format.format(nowDate) + "From : "+ result.getTransfer().getFromMember().getUsername() + "Amount : "+result.getTransfer().getAmount());
		    		
		    		cAMQ.SendNoReply(queueEPL, xmlString);
		    		
		    		String hostname = "Unknown";
		    		
		    		try{
		    		    InetAddress addr;
		    		    addr = InetAddress.getLocalHost();
		    		    hostname = addr.getHostName();
		    		}catch (UnknownHostException ex){
		    		    System.out.println("Hostname can not be resolved");
		    		}
		    		
		    		String logsNotification = hostname + "/EPL-Module/"+ format.format(nowDate) + " : [EPL request : " + result.getTransfer().getTransactionNumber() + "]" + "{" + xmlString + "}";
		    		logger.info("---Successful Send Msg To Active MQ---" +" Logs :"+ logsNotification);
		    		cAMQ.SendNoReply(queueLog, logsNotification);
		    		//throw new JMSException("ExceptionJMS");
		    	} catch (Exception e){
		    		System.out.println(e);
		    		doChargeback(transfer);
		    	}
        }
   	
   	
   	String errorMessage = "";
       
       try {
       	String errorKey = "";
       	if(status.equals(PaymentStatus.UNKNOWN_ERROR)){
       		 errorKey = status + prefixErrorKey + errorCode;
       	}else{
       		errorKey = status.toString();
       	}
       	
//       	if(params.getMedia() != null && params.getMedia().length() > 0 ){
//       		switch (params.getMedia().toLowerCase()){ 
//       			case "mobile" :
//       				errorMessage = EmoneyConfiguration.getMobileErrorMessage().getProperty(errorKey);
//       				break;
//       			case "ussd" :
//       				errorMessage = EmoneyConfiguration.getUSSDErrorMessage().getProperty(errorKey);
//       				break;
//       			default :
//       				errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
//       				break;
//       		}
//       	}else{
//       		errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty(errorKey);
//       	}
       	
		} catch (Exception e) {
			e.printStackTrace();
		}
       
       result.setField1(errorMessage);

   	return result;
   }
	
	private ChargebackResult doChargeback(final Transfer transfer) {

        final Pair<ChargebackStatus, Transfer> preprocessResult = preprocessChargeback(transfer);
        ChargebackStatus status = preprocessResult.getFirst();
        Transfer chargebackTransfer = preprocessResult.getSecond();

        // Do the chargeback
        if (status == null) {
            chargebackTransfer = paymentServiceLocal.chargeback(transfer);
            status = ChargebackStatus.SUCCESS;
            if(chargebackTransfer != null)
            memberNotificationHandler.chargebackPaymentNotification(transfer, chargebackTransfer.getTransactionNumber());
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Chargeback result: " + status);
        }

        Member member = WebServiceContext.getMember();
        // Build the result
        if (status == ChargebackStatus.SUCCESS || status == ChargebackStatus.TRANSFER_ALREADY_CHARGEDBACK) {
            AccountHistoryTransferVO originalVO = null;
            AccountHistoryTransferVO chargebackVO = null;
            try {
                final AccountOwner owner = member == null ? transfer.getToOwner() : member;
                originalVO = accountHelper.toVO(owner, transfer, null);
                chargebackVO = accountHelper.toVO(owner, chargebackTransfer, null);
            } catch (Exception e) {
                webServiceHelper.error(e);
                if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                    // The chargeback is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                    // exception.
                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                }
                // When the locking method is not NONE, the chargeback is committed on a new transaction so we'll preserve the status.
            }
            return new ChargebackResult(status, originalVO, chargebackVO);
        } else {
            return new ChargebackResult(status, null, null);
        }
    }
	
	private Pair<ChargebackStatus, Transfer> preprocessChargeback(final Transfer transfer) {
        ChargebackStatus status = null;
        Transfer chargebackTransfer = null;

        // Check if the transfer can be charged back
        if (!paymentServiceLocal.canChargeback(transfer, false)) {
            if (transfer.getChargedBackBy() != null) {
                chargebackTransfer = transfer.getChargedBackBy();
                status = ChargebackStatus.TRANSFER_ALREADY_CHARGEDBACK;
            } else {
                if (transfer.getStatus() == Payment.Status.PENDING) {
                    final TransferAuthorizationDTO transferAuthorizationDto = new TransferAuthorizationDTO();
                    transferAuthorizationDto.setTransfer(transfer);
                    transferAuthorizationDto.setShowToMember(false);
                    chargebackTransfer = transferAuthorizationServiceLocal.cancel(transferAuthorizationDto);
                    status = ChargebackStatus.SUCCESS;
                } else {
                    status = ChargebackStatus.TRANSFER_CANNOT_BE_CHARGEDBACK;
                }
            }
        }

        return new Pair<ChargebackStatus, Transfer>(status, chargebackTransfer);
    }
    
	@Override
    public PaymentResult_Extended mobileMemberPaymentH2H(final PaymentRequestMsg input) throws Exception {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();

        try {
            // Original Member
            paymentParams.setFromMember(input.getFrom());

            // Destination Member (if null or empty, it means the destination is System Account, such as: PLN, Telco)
//            if (!(input.getTrxtype().toLowerCase().equals("p1") || input.getTrxtype().toLowerCase().equals("p2"))) {
            //Update toMember - 2 Oktober 2014
            if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
            paymentParams.setToSystem(true);
            } else {
                paymentParams.setToMember(input.getTo());
            }

            // Transaction Amount
            paymentParams.setAmount(new BigDecimal(input.getAmount()));

            // Transaction Memo
            if (input.getDesc() != null) {
                paymentParams.setDescription(input.getDesc());
            }

            try {
//                if (paymentParams.getToSystem()) {
                    paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(MOBILE + ".transferID." + input.getTrxtype().toLowerCase())));
//                }
            } catch (final IOException e) {
                logger.error(e);
            }

            String customFieldName = "";

            try {
                customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name");
            } catch (final IOException e) {
                logger.error(e);
            }

            if (customFieldName != null && !customFieldName.isEmpty()) {

                FieldValueVO customFieldValue = new FieldValueVO(customFieldName, input.getField1());
                List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
                paymentParams.setCustomValues(customValues);

            }

            // Payment Channel (Isi dengan merchant id dari partner terkait)
            FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", input.getField10());
  //          List<FieldValueVO> paymentChannelCustomValues = null; 
            ArrayList<FieldValueVO> addCustomValues = null;
            if (paymentParams.getCustomValues() == null) {
            	List<FieldValueVO> customValues = Arrays.asList(paymentChannelCustomFieldValue);
                paymentParams.setCustomValues(customValues);
            } else {
            	addCustomValues = new ArrayList<FieldValueVO>(paymentParams.getCustomValues());
                addCustomValues.add(paymentChannelCustomFieldValue);
                paymentParams.setCustomValues(addCustomValues);
                
                paymentParams.setCustomValues(addCustomValues);
            }



            
            paymentParams.setCredentials(input.getCredential());
            
            // Untuk reversal purpose
            StringBuffer revKey = new StringBuffer();
            revKey.append("MOBILE");
            revKey.append(input.getTrxtype());
            revKey.append(RandomStringUtils.randomAlphanumeric(20));
            paymentParams.setTraceNumber(revKey.toString());

            payment.setWebServiceHelper(webServiceHelper);
            payment.setPaymentHelper(paymentHelper);
            payment.setPaymentServiceLocal(paymentServiceLocal);
            payment.setAccountHelper(accountHelper);
            payment.setApplicationServiceLocal(applicationServiceLocal);
            payment.setElementServiceLocal(elementServiceLocal);
            payment.setChannelHelper(channelHelper);
            payment.setAccessServiceLocal(accessServiceLocal);

            // result = payment.doPayment(paymentParams);
            result = doPayment(paymentParams);

        } catch (final ExternalException e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        } catch (final Exception e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        }

        final String errorMessage = resolveMessage(MOBILE, result.getStatus(), result.getField1(), input.getTrxtype());

        result.setField1(errorMessage);

        return result;
    }

	@Override
    public PaymentResult_Extended mobileMemberPaymentExternalH2H(final PaymentRequestMsg input) {
        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        PaymentResponseMsg response = new PaymentResponseMsg();
        String tempAgent = "";

            try {
	        	TRX_AGENT = EmoneyConfiguration.getEmoneyProperties().getProperty("agen.tarik_tunai");
	        	tempAgent = input.getField1();
	            paymentParams.setFromMember(input.getFrom());
		        if(!(checkTransferTypeCode(input.getTrxtype().toLowerCase(), "validate.toMember"))) {
		            paymentParams.setToSystem(true);
		        } else {
		            paymentParams.setToMember(input.getTo());
		        }
		
		        // Transaction Amount
		        paymentParams.setAmount(new BigDecimal(input.getAmount()));

		        // Transaction Memo
		        if (input.getDesc() != null) {
		           paymentParams.setDescription(input.getDesc());
		         }
		
		         try {
		                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty(MOBILE + ".transferID." + input.getTrxtype().toLowerCase())));
		         } catch (final IOException e) {
		                logger.error(e);
		         }
		         
		         List<FieldValueVO> customValues = new ArrayList<FieldValueVO>();
		         TransferType transferType = new TransferType();
		         transferType.setId(paymentParams.getTransferTypeId());
		         List<PaymentCustomField> allowedFields = paymentCustomFieldServiceLocal.list(transferType, true);
		         
		         
		         HashMap<String, String> map = new HashMap<String, String>();
		         
		         for (int i=1; i<allowedFields.size(); i++){
		        	 String customFieldName = "a"+i;
			         try {
			        	 map.put(customFieldName, EmoneyConfiguration.getEmoneyProperties().getProperty(input.getTrxtype().toLowerCase() + ".custom_field_name_"+i));
			        	 
			             if (map.get(customFieldName) != null && !map.get(customFieldName).isEmpty()) {
			            	 if(i==1){
			            		 FieldValueVO customFieldValue = new FieldValueVO(map.get(customFieldName), input.getField1());
			            		 customValues.add(customFieldValue);
			            	 }else{
			            		 FieldValueVO customFieldValue2 = new FieldValueVO(map.get(customFieldName), input.getDesc());
			            		 customValues.add(customFieldValue2);
			            	 }
				        	 
				         }
			         } catch (final IOException e) {
			             logger.error(e);
			         }
			
		         }
		         
		      // Payment Channel (Isi dengan merchant id dari partner terkait)
		            FieldValueVO paymentChannelCustomFieldValue = new FieldValueVO("pcf_payment_origin", input.getField10());
		            customValues.add(paymentChannelCustomFieldValue);

		         
		         paymentParams.setCustomValues(customValues);
		         paymentParams.setCredentials(input.getCredential());
		
		         payment.setWebServiceHelper(webServiceHelper);
		         payment.setPaymentHelper(paymentHelper);
		         payment.setPaymentServiceLocal(paymentServiceLocal);
		         payment.setAccountHelper(accountHelper);
		         payment.setApplicationServiceLocal(applicationServiceLocal);
		         payment.setElementServiceLocal(elementServiceLocal);
		         payment.setChannelHelper(channelHelper);
		         payment.setAccessServiceLocal(accessServiceLocal);
		
		         result = doPaymentExternal(paymentParams);
		         
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
    
            final String errorMessage = resolveMessage(USSD, result.getStatus(), result.getField1(), input.getTrxtype());
            
            result.setTransfer(result.getTransfer());
            result.setField1(errorMessage);

        return result;
    }
}
