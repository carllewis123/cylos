/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.utils;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "openOTPResponse")
public class OpenOTPResponse {
    
    private OpenOTPStatus openOTPStatus;
    private OpenOTPTicketVO openOTPTicketVO;
    
    public OpenOTPStatus getOpenOTPStatus() {
        return openOTPStatus;
    }
    public OpenOTPTicketVO getOpenOTPTicketVO() {
        return openOTPTicketVO;
    }
    public void setOpenOTPStatus(OpenOTPStatus openOTPStatus) {
        this.openOTPStatus = openOTPStatus;
    }
    public void setOpenOTPTicketVO(OpenOTPTicketVO openOTPTicketVO) {
        this.openOTPTicketVO = openOTPTicketVO;
    }

}
