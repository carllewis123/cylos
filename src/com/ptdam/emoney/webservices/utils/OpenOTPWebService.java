/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package com.ptdam.emoney.webservices.utils;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import nl.strohalm.cyclos.webservices.model.MemberVO;

/**
 * Web Service interface that should be implemented externally in order to request payment confirmations
 * @author luis
 */
@WebService
public interface OpenOTPWebService {

    /**
     * Requests a payment confirmation
     */
    @WebMethod
    @WebResult(name = "requestOTPResult")
    OpenOTPStatus generateNonTrxOTP(@WebParam(name = "requestOTPTicket") OpenOTPTicketVO ticket);

    @WebMethod
    @WebResult(name = "validateOTPResult")
    OpenOTPStatus validateNonTrxOTP(@WebParam(name = "validateOTPTicket") OpenOTPTicketVO ticket);

    @WebMethod
    @WebResult(name = "validateExtendedOTPResult")
    OpenOTPResponse validateNonTrxOTP_Extended(@WebParam(name = "validateOTPTicket") OpenOTPTicketVO ticket);

}
