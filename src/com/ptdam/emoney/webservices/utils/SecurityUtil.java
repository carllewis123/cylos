/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hazelcast.util.Base64;

import ptdam.emoney.EmoneyConfiguration;

public class SecurityUtil {
    private final Log               logger    = LogFactory.getLog(SecurityUtil.class);
    private String keyStoreLocation = "";
    private String keyStorePassword = "cikarang00";
    private String signatureAlgorithm = "SHA1withRSA";
    private String keyStoreAlias = "moku_key";
    private KeyStore ks = null;
    private char[] pwd = null;
    private Signature instance = null;
    
    public String getSignature(final String rawSignature) {
        final PrivateKey key = getPrivateKey();
        String signatureText = "";
        
        try {
            if (rawSignature.isEmpty()) {
                throw new Exception("Raw Signature Text is empty. Signature generation cannot be proceed.");
            }
            
            if (instance != null && key != null) {
                instance.initSign(key);
                instance.update(rawSignature.getBytes());
                final byte[] signature = instance.sign();
                signatureText = new String(Base64.encode(signature));
            }
            
        } catch (final Exception e) {
            logger.error(e);
        }
        
        return signatureText;
    }

    public boolean verifySignature(final String rawSignature, final String signature) {
        boolean isVerified = false;
        final PublicKey key = getPublicKey();
        
        try {
            
            if (rawSignature.isEmpty() || signature.isEmpty()) {
                throw new Exception("Raw Signature or Signature Text is empty. Verify signature cannot be proceed.");
            }
            
            if (instance != null && key != null) {
                instance.initVerify(key);
                byte[] byteRawSignature = rawSignature.getBytes();
                byte[] byteSignature = Base64.decode(signature.getBytes());
                instance.update(byteRawSignature, 0, byteRawSignature.length);
                isVerified = instance.verify(byteSignature);
            }
        } catch (final Exception e) {
            logger.error(e);
        }
        
        return isVerified;
    }
    
    public SecurityUtil() {
        init();
    }
    
    private void init() {
        try {
            keyStoreLocation = EmoneyConfiguration.getEmoneyProperties().getProperty("keystore.location");
            keyStorePassword = EmoneyConfiguration.getEmoneyProperties().getProperty("keystore.password");
            keyStoreAlias = EmoneyConfiguration.getEmoneyProperties().getProperty("keystore.alias");
            signatureAlgorithm = EmoneyConfiguration.getEmoneyProperties().getProperty("signature.algorithm");
            
            if (ks == null) {
                ks = KeyStore.getInstance(KeyStore.getDefaultType());
            }
            
            if (instance == null) {
                instance = Signature.getInstance(signatureAlgorithm);
            }
            
            if (keyStoreLocation.isEmpty()) {
                throw new Exception("KeyStore Location is not define.");
            }
            
            if (keyStorePassword.isEmpty()) {
                throw new Exception("KeyStore Password is not define.");
            }
            
            if (keyStoreAlias.isEmpty()) {
                throw new Exception("KeyStore Alias is not define.");
            }
            
            pwd = keyStorePassword.toCharArray();
            final FileInputStream fis = new FileInputStream(new File(keyStoreLocation));
            ks.load(fis, pwd);
            fis.close();
            
        } catch (final Exception e) {
            logger.error(e);
        }
    }
        
    private PrivateKey getPrivateKey() {
        PrivateKey key = null;
        try {
            if (ks != null) {
                key = (PrivateKey) ks.getKey(keyStoreAlias, pwd);
            } else {
                throw new Exception("KeyStore instance is empty.");
            }
        }
        catch (final Exception e) {
            logger.error(e);
        }

        return key;
    }
    
    private PublicKey getPublicKey() {
        PublicKey key = null;
        try {
            if (ks != null) {
                X509Certificate certificate = (X509Certificate) ks.getCertificate(keyStoreAlias);
                key = (PublicKey) certificate.getPublicKey();
            } else {
                throw new Exception("KeyStore instance is empty.");
            }
        } catch (final Exception e) {
            logger.error(e);
        }
        
        return key;
    }
}
