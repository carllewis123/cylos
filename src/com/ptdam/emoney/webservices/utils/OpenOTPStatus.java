/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices.utils;

public enum OpenOTPStatus {
    
    /**
     * The request otp was successfully processed
     */
    OTP_REQUEST_PROCESSED,
    
    /**
     * The given ecash number was not found
     */
    OTP_REQUEST_FAILED_MEMBER_NOT_FOUND,
    
    /**
     * The given channel is not allowed
     */
    OTP_REQUEST_FAILED_MEMBER_CHANNEL_NOT_ALLOWED,
    
    /**
     * The given credential for request was invalid
     */
    OTP_REQUEST_FAILED_INVALID_CREDENTIALS,
    
    /**
     * The given credential for request was blocked
     */
    OTP_REQUEST_FAILED_BLOCKED_CREDENTIALS,
    
    /**
     * The otp request is not allowed (web service settings are not allowed)
     */
    OTP_REQUEST_FAILED_PERMISSION_DENIED,

    
    
    
    /**
     * The otp was successfully validated
     */
    OTP_VALIDATION_PROCESSED,

    /**
     * The given otp was not found
     */
    OTP_VALIDATION_FAILED_INVALID_OTP,
    
    /**
     * The otp is already expired
     */
    OTP_VALIDATION_FAILED_OTP_ALREADY_EXPIRED,
    
    /**
     * The otp validation already used
     */
    OTP_VALIDATION_FAILED_OTP_ALREADY_VALIDATED,
    
    /**
     * The member who validate is not matched with member who request the otp
     */
    OTP_VALIDATION_FAILED_WRONG_VALIDATE_MEMBER,
        
    /**
     * The given otp was a transaction otp (otp untuk pembayaran dan tarik tunai tidak diperbolehkan digunakan untuk operasi non trx otp)
     */
    OTP_VALIDATION_FAILED_TRX_OTP_IS_USED,
    
    
    /**
     * Any other unexpected error will fall in this category
     */
    UNKNOWN_ERROR;

}
