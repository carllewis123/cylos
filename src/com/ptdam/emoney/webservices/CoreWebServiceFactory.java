/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package com.ptdam.emoney.webservices;

import java.io.IOException;

import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ptdam.emoney.webservices.edcpayment.OTPValidateWebService;
import com.ptdam.emoney.webservices.hosttohost.HosttoHostOTPValidateWebService;
import com.ptdam.emoney.webservices.hosttohost.HosttoHostPaymentWebService;
import com.ptdam.emoney.webservices.utils.OpenOTPWebService;

import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.payments.PaymentWebService;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;
import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.AccountInquiryWebService;
import ptdam.emoney.webservice.client.CIFInquiryWebService;
import ptdam.emoney.webservice.client.FTInquiryWebService;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.TelcoPaymentWebService;
import ptdam.emoney.webservice.client.UBPChannelWebService;

import com.ptdam.emoney.webservices.edcpayment.OTPValidateWebService;
import com.ptdam.emoney.webservices.utils.OpenOTPWebService;

public class CoreWebServiceFactory {
    
    private final Log   logger = LogFactory.getLog(CoreWebServiceFactory.class);

    public FundTransferWebService getFundTransferWebService() {
        FundTransferWebService wsClient = null;

        try {
              final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.fundtransfer");
              wsClient = ExternalWebServiceHelper.proxyFor(FundTransferWebService.class, url);
        } catch (final IOException e) {
              logger.error(e);
        }

        return wsClient;
    }

    public UBPChannelWebService getUBPChannelWebService() {
        UBPChannelWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.ubpchannel");
            wsClient = ExternalWebServiceHelper.proxyFor(UBPChannelWebService.class, url);
        } catch (final IOException e) {
            logger.error(e);
        }

        return wsClient;
    }
    
    public TelcoPaymentWebService getTelcoPaymentWebService() {
        TelcoPaymentWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.telco");
            wsClient = ExternalWebServiceHelper.proxyFor(TelcoPaymentWebService.class, url);
        } catch (final IOException e) {
            logger.error(e);
        }
        
        return wsClient;
    }

    public CIFInquiryWebService getCIFInquiryWebService() {
        CIFInquiryWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.cifinquiry");
            wsClient = ExternalWebServiceHelper.proxyFor(CIFInquiryWebService.class, url);
        } catch (IOException e) {
        	logger.error(e);
        }
        
        return wsClient;
    }
    
    public AccountInquiryWebService getAccountInquiryWebService() {
        AccountInquiryWebService wsClient = null;
        
        try {
              final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.cifacctinquiry");
              wsClient = ExternalWebServiceHelper.proxyFor(AccountInquiryWebService.class, url);
        } catch (IOException e) {
        	logger.error(e);
        }
        return wsClient;
    }
    
    public OpenOTPWebService getOpenOTPWebService() {
        OpenOTPWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.openotp");
            wsClient = ExternalWebServiceHelper.proxyFor(OpenOTPWebService.class, url);
        } catch (IOException e) {
            logger.error(e);
        }
      return wsClient;

    }
    
    public OTPValidateWebService getOTPValidateWebService() {
        OTPValidateWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.edc_otp_validate");
            wsClient = ExternalWebServiceHelper.proxyFor(OTPValidateWebService.class, url);
        } catch (IOException e) {
            logger.error(e);
        }
        return wsClient;

    }
    
    public FTInquiryWebService getFTInquiryWebService() {
    	FTInquiryWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.fundtransfer.inquiry");
            wsClient = ExternalWebServiceHelper.proxyFor(FTInquiryWebService.class, url);
        } catch (IOException e) {
            logger.error(e);
        }
        return wsClient;

    }
//Modification for h2h tarik tunai - 23 Juni 2014
    public HosttoHostOTPValidateWebService getOTPHosttoHostValidateWebService() {
    	HosttoHostOTPValidateWebService wsClient = null;
        
        try {
            final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.h2h_otp_validate");
            wsClient = ExternalWebServiceHelper.proxyFor(HosttoHostOTPValidateWebService.class, url);
        } catch (IOException e) {
            logger.error(e);
        }
        return wsClient;
    }
    
    public FundTransferWebService getReversalFundTransferWebService() {
        FundTransferWebService wsClient = null;

        try {
              final String url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.reversal.fundtransfer");
              wsClient = ExternalWebServiceHelper.proxyFor(FundTransferWebService.class, url);
        } catch (final IOException e) {
              logger.error(e);
        }

        return wsClient;
    }
}
