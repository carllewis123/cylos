/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Returns the cyclos properties
 * 
 * @author luis
 */
public class EmoneyConfiguration {

    public static final String  PTDAM_CONFIGURATION               = "/ptdam.properties";
    public static final String  HOST_TERMINAL_TABLE                  = "/hostterminals.properties";
    private static final String MOBILE_ERROR_MAPPING    ="/mobile_error.properties";
    private static final String USSD_ERROR_MAPPING = "/ussd_error.properties";
    public static Properties ptdamProperties = null;
    public static Properties hostTerminalProperties = null;
    public static Properties mobileErrorProperties = null;
    public static Properties ussdErrorProperties = null;
    
    public static Properties getEmoneyProperties() throws IOException{
		if(ptdamProperties == null){
			ptdamProperties = new Properties();
			InputStream propin= EmoneyConfiguration.class.getResourceAsStream(PTDAM_CONFIGURATION);
			ptdamProperties.load(propin);
			try{
				propin.close(); 
			}catch(IOException ioe){
			
			}
		}
		return ptdamProperties;
	}


    public static Properties getTerminalIDs() throws IOException {
    	if(hostTerminalProperties == null){
    		hostTerminalProperties = new Properties();
    		InputStream propin= EmoneyConfiguration.class.getResourceAsStream(HOST_TERMINAL_TABLE);
			hostTerminalProperties.load(propin);
			try{
				propin.close(); 
			}catch(IOException ioe){
			
			}
		}
		return hostTerminalProperties;
		
    }
    
    public static Properties getMobileErrorMessage() throws IOException {
    	if(mobileErrorProperties == null){
    		mobileErrorProperties = new Properties();
    		InputStream propin= EmoneyConfiguration.class.getResourceAsStream(MOBILE_ERROR_MAPPING);
    		mobileErrorProperties.load(propin);
    		try{
				propin.close(); 
			}catch(IOException ioe){
			
			}
		}
		return mobileErrorProperties;
    }

    public static Properties getUSSDErrorMessage() throws IOException {
    	if(ussdErrorProperties == null){
    		ussdErrorProperties = new Properties();
    		InputStream propin= EmoneyConfiguration.class.getResourceAsStream(USSD_ERROR_MAPPING);
    		ussdErrorProperties.load(propin);
    		try{
				propin.close(); 
			}catch(IOException ioe){
			
			}
		}
		return ussdErrorProperties;
    }

}
