package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerMonthExceededException;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;

public class FundTransferHostToHostListener extends TransferListenerAdapter{

    private final Log               logger    = LogFactory.getLog(FundTransferHostToHostListener.class);
    private CustomFieldHelper 		customFieldHelper;
    private ElementServiceLocal     elementServiceLocal;
    
    @SuppressWarnings("unused")
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
    	String remark3="";
    	Long toAccountTypeId = 0L;
    	
    	try {
			toAccountTypeId = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.account_type_id_emoney"));
		} catch (NumberFormatException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    	
    	
        try {
        	
        	final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
        	
            String trxtypeCustomFieldName=null;
            String fromagentCustomFieldName=null;
            String toMemberCustomFieldName=null;
            String refNoCustomFieldName=null;
            String traceNumberST=null;
            String tracenum=null;
            String traceNumberTT=null;
			
            
            if(transfer.getTo().getType().getId().equals(toAccountTypeId)){
            	try {
            		//Setor Tunai Indomaret
    				trxtypeCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.customfield_trxtype_ST");
    				fromagentCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.from_agent_ST");
    				toMemberCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.to_Member_ST");
    				refNoCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.refNo_ST");
    				traceNumberST = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.rekon.tracenumber.internalname.st");
    				
    				final PaymentCustomFieldValue traceNumberValueST = customFieldHelper.getValue(traceNumberST, customValues);
    				tracenum = traceNumberValueST.getValue();
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}	
        	}else{
        		try {
        			//Tarik Tunai Indomaret
    				trxtypeCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.customfield_trxtype_TT");
    				fromagentCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.from_agent_TT");
    				toMemberCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.to_Member_TT");
    				refNoCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.refNo_TT");
    				traceNumberTT = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.rekon.tracenumber.internalname.tt");
    				
    				final PaymentCustomFieldValue traceNumberValueTT = customFieldHelper.getValue(traceNumberTT, customValues);
    				tracenum = traceNumberValueTT.getValue();
    				System.out.println("traceNumberValueTT :"+traceNumberValueTT);
    				System.out.println("traceNumberTT :"+traceNumberTT);
    				System.out.println("customValues :"+customValues);
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
        	}
            
			
			final PaymentCustomFieldValue TrxTypeCustomFieldValue = customFieldHelper.getValue(trxtypeCustomFieldName, customValues);
			final String trxtype = TrxTypeCustomFieldValue.getValue();
			
			final PaymentCustomFieldValue agentIdCustomFieldValue = customFieldHelper.getValue(fromagentCustomFieldName, customValues);
			final String agentId = agentIdCustomFieldValue.getValue();
			
			final PaymentCustomFieldValue memberFieldValue = customFieldHelper.getValue(toMemberCustomFieldName, customValues);
			final String toMember = memberFieldValue.getValue();
			
			final PaymentCustomFieldValue refNoFieldValue = customFieldHelper.getValue(refNoCustomFieldName, customValues);
			final String RefNo = refNoFieldValue.getValue();
			
            
            User user = null;
            try {
                user = elementServiceLocal.loadUser(agentId, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
                if (!(user instanceof MemberUser)) {
                    throw new Exception();
                }
            } catch (Exception e) {
              e.getMessage();
            }
        	
            Member member = (Member) user.getElement();

            final Collection<MemberCustomFieldValue> customValues2 =  member.getCustomValues();
        	String norekInternalName=null;
			try {
				norekInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("h2h.norek_customfield");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
            final MemberCustomFieldValue noRekCustom = customFieldHelper.getValue(norekInternalName, customValues2);
            final String noRekTujuan = noRekCustom.getValue();
            
            String branchCode = "";
            String storeCode = "";
            try{
            	if(transfer.getTo().getType().getId().equals(toAccountTypeId)){
            	//Setor Tunai
            		System.out.println("Masuk Setor Tunai");
            		System.out.println("traceNumber Setor Tuni"+tracenum);
            		if (tracenum.length()>=12) {
            			branchCode = tracenum.substring(9,12);
					}else{
						branchCode="";
					}
            		if (tracenum.length()>=16) {
						storeCode = tracenum.substring(13,16);
					}else{
						storeCode = "";
					}
            	
            		System.out.println("branchCode :"+branchCode);
            		System.err.println("storeCode :"+storeCode);
            		
            	}else{
            		System.out.println("masuk tarik tunai");
            		if (tracenum.length()>=12) {
            			branchCode = tracenum.substring(9,12);
					}else{
						branchCode="";
					}
            		if (tracenum.length()>=16) {
						storeCode = tracenum.substring(13,16);
					}else{
						storeCode = "";
					}
            	}
            }catch (Exception e){
            	logger.error("error when susbtring traceNumber, " + transfer.getTraceNumber());
            	logger.error(e.getMessage());
            }
            
        	remark3=trxtype + "/" + noRekTujuan.substring(noRekTujuan.length()-6) + "/" + toMember + "/" + RefNo.substring(RefNo.length()-7) + "/" + branchCode + "/" + storeCode;
        	
        	StringBuffer rekonKey = new StringBuffer();
        	rekonKey.append(" ");
        	rekonKey.append("|");
        	rekonKey.append(remark3);
        	rekonKey.append("|");
             
        	transfer.setTraceData(rekonKey.toString());
        	
            super.onTransferInserted(transfer);
        	
        }catch (final MaxTopupCCPerMonthExceededException e) {
        	logger.error(e);
        	throw e;  }
   }   
    
    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }
  
    }


