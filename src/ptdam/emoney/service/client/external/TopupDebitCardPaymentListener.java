/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import nl.strohalm.cyclos.entities.accounts.Account;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerMonthExceededException;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.utils.conversion.CoercionHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.accounts.AccountHistorySearchParameters;
import nl.strohalm.cyclos.webservices.utils.QueryHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.webservices.utils.SecurityUtil;

public class TopupDebitCardPaymentListener extends TransferListenerAdapter {
    @Autowired
    private SecurityUtil securityUtil;
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    private QueryHelper                    queryHelper;
    private PaymentServiceLocal            paymentServiceLocal;
    private AccountServiceLocal            accountService;
    
    private CustomFieldHelper customFieldHelper;
    private final Log               logger    = LogFactory.getLog(TopupDebitCardPaymentListener.class);

    
    @SuppressWarnings("unused")
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        try {
        	String [] listDebitTrxTypes = EmoneyConfiguration.getEmoneyProperties().getProperty("p8.debit").split(",");
        	
        	BigDecimal MonthlyTotalAmount = BigDecimal.ZERO;
        	final String maxLimitDebitPerMonthIntName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.debit.max.limit.per.month.internal.name");
            final String strMaxDebitLimitPerMonth = getCustomLabel(maxLimitDebitPerMonthIntName, transfer);
            BigDecimal maxDebitLimitPerMonth = (new BigDecimal(strMaxDebitLimitPerMonth)).setScale(2);
        	
        	for(int i = 0; i < listDebitTrxTypes.length; i++){
        		String strTopupCCTrfTypeId = EmoneyConfiguration.getEmoneyProperties().getProperty(listDebitTrxTypes[i]);
        		if(strTopupCCTrfTypeId.equals("") || strTopupCCTrfTypeId.length() == 0){
        			continue;
        		}
            	
        		Long topupCCTrfTypeId = Long.parseLong(strTopupCCTrfTypeId);
            	
            	String strEmoneyAccountTypeId = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.account.type.id");
            	Long emoneyAccountTypeId = Long.parseLong(strEmoneyAccountTypeId);
            	
            	MemberAccount memberAccount = (MemberAccount) transfer.getTo();
                Member member = customFieldHelper.loadMember(memberAccount.getMember().getId());
                AccountHistorySearchParameters params = new AccountHistorySearchParameters();
                params.setPrincipal(transfer.getTo().getOwnerName());
                params.setAccountTypeId(emoneyAccountTypeId);
                params.setPageSize(100);
                Calendar beginDate = Calendar.getInstance();
                beginDate.set(beginDate.get(Calendar.YEAR),beginDate.get(Calendar.MONTH),1,0,0,0);
                params.setBeginDate(beginDate);
                Calendar endDate = Calendar.getInstance();
                params.setEndDate(endDate);
                
                  // Get the query and account type
                final TransferQuery query = new TransferQuery();
                queryHelper.fill(params, query);
                AccountType accountType = CoercionHelper.coerce(AccountType.class, params.getAccountTypeId());
                query.setOwner(member);
                final Account account = accountService.getAccount(new AccountDTO(member, accountType), Account.Relationships.TYPE);
                query.setType(account.getType());
                if (params.getBeginDate() != null || params.getEndDate() != null) {
                    final Period period = new Period(params.getBeginDate(), params.getEndDate());
                    query.setPeriod(period);
                }
                
                TransferType trfType = new TransferType();
                trfType.setId(topupCCTrfTypeId);
                query.setTransferType(trfType);

                  final List<Transfer> transfers = paymentServiceLocal.search(query);
                  Iterator it = transfers.iterator();
                  
                  Calendar today = Calendar.getInstance();
                  
                  while(it.hasNext()){
                	  Transfer trf = (Transfer) it.next();
                	  Calendar processDate = trf.getProcessDate();

                	  if(isSameMonth(today,processDate)){
                		  MonthlyTotalAmount = MonthlyTotalAmount.add((BigDecimal) trf.getAmount());
                	  } 
                	  
                  }
                 
        	}
        	
        	 System.out.println("MonthlyTotalAmount" + MonthlyTotalAmount);
             
       	  	if((MonthlyTotalAmount.add(transfer.getAmount())).compareTo(maxDebitLimitPerMonth) > 0){
       	  		throw new MaxTopupCCPerMonthExceededException("MaxTopupCCPerMonthExceededException");
            }
       	  	
			final String vtmaskedcardInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.masked.card");
			final String vtapprovalcodeInternalName= EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topupcc.vt.approval.code");
			
			final Collection<PaymentCustomFieldValue> paymentCustomFields =  transfer.getCustomValues();
            
            final PaymentCustomFieldValue vtmaskedcardCustomField = customFieldHelper.getValue(vtmaskedcardInternalName, paymentCustomFields);
            String masked_card = vtmaskedcardCustomField.getValue();
            masked_card = noMaskCardNum(masked_card);
            
            final PaymentCustomFieldValue vtapprovalcodeCustomField = customFieldHelper.getValue(vtapprovalcodeInternalName, paymentCustomFields);
            String approval_code = vtapprovalcodeCustomField.getValue();
            
            String maxLengthAppCode = EmoneyConfiguration.getEmoneyProperties().getProperty("topupcc.maxLengthAppCode");

            if (approval_code.length() > Integer.parseInt(maxLengthAppCode)) {
              approval_code = approval_code.substring(0, Integer.parseInt(maxLengthAppCode));
            }
        	
       	  	final String remark = " |" +transfer.getType().getId().toString() + "/" + approval_code + "/" + transfer.getTo().getOwnerName() + "/" + masked_card + "|";
       	  	
       	  	transfer.setTraceData(remark);
            	  
              super.onTransferInserted(transfer);
        } catch (final MaxTopupCCPerMonthExceededException e) {
        	logger.error(e);
        	throw e;  
        } catch (final ExternalException e) {
        	logger.error(e);
        	throw e;
        } catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    private String getCustomLabel(final String internalName, final Transfer transfer) {
        final Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    public boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            return false;
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) 
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    
    
    public boolean isSameMonth(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            return false;
        return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
            && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }

    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }

    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountService = accountService;
    }
    
    public void setQueryHelper(final QueryHelper queryHelper) {
        this.queryHelper = queryHelper;
    }
    
    @Override
    public void onBeforeValidateBalance(final Transfer transfer) throws ExternalException {
    	System.out.println("onBeforeValidateBalance");
    }


    @Override
    public void onTransferProcessed(final Transfer transfer) {
    	System.out.println("onTransferProcessed");
    }
    
    private String noMaskCardNum(String value) {
        return value.substring(0, 6) + "-" + value.substring(value.length() - 4);
      }
    
}
