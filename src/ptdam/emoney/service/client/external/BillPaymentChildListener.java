/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import javax.xml.ws.WebServiceException;

import nl.strohalm.cyclos.entities.accounts.fees.transaction.TransactionFee;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.messages.Message;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.elements.MessageServiceLocal;
import nl.strohalm.cyclos.services.elements.SendMessageFromSystemDTO;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.binding.BeanBinder;
import nl.strohalm.cyclos.utils.binding.DataBinder;
import nl.strohalm.cyclos.utils.binding.PropertyBinder;
import nl.strohalm.cyclos.utils.binding.SimpleCollectionBinder;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.BillInfoList;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.client.UBPCMsgRequest;
import ptdam.emoney.webservice.client.UBPCMsgResponse;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

public class BillPaymentChildListener extends TransferListenerAdapter {
    @Autowired
    private SecurityUtil securityUtil;
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }
    
    private CustomFieldHelper customFieldHelper;
    private final Log               logger    = LogFactory.getLog(BillPaymentChildListener.class);
    private CoreWebServiceFactory coreWSFactory;
//    private ElementServiceLocal           elementService;
    
    private HashMap<String, Object> mapSmsMailing; 
    private SmsMailingServiceLocal smsMailingServiceLocal;
    private DataBinder<SmsMailing> dataBinder;
    private MessageServiceLocal       messageService;
    
    @SuppressWarnings("unused")
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }
    
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        // TODO Auto-generated method stub
    	String reservedField2 = "";

        Transfer transferParent = transfer.getParent();
        
        try {

            final UBPCMsgRequest request = new UBPCMsgRequest();
            
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.channelId");
            final String billFlagMap = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billFlagMap");
            final String billkey1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey1");
            final String billkey2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey2");
            final String billkey3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkey3");
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.glaccount");
            final String debitAccountType = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.customerAccountType");
            final String cardAcceptorTermId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.cardAcceptorTermId");
            final String traceNumberLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.traceNumberLength");
            final String track2DataLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.track2DataLength");
            final String languageCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.language_code.ind");
            final String currencyCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.currencyCode");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String transactionCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.transactionCode");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.tellerId");
            final String journalSequenceLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.journalSequenceLength");
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.extIdlength");
            final String reserved1Length = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.reserved1Length");
            final String valueDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.valuedate_format");
            final String companyCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.company_code_prefix");
            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.reqSecurityHeader"));
            final String paymentCustomFieldPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("pcf.sisf_prefix");
            final String validBillkeysPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.billkeys_prefix");
//            final String ubpAdvicePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.advice_prefix");
//            final boolean fieldSep = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.field_sep"));
//            final String labelValueSep = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.label_value_sep");
            final String skipChar = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.skip_label_char");
            final String emoneyRef = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.emoneyref");
            final String emoneyDate = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.emoneydate");
            final String emoneyDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.emoneydate_format");
            final String emoneyByr = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.emoneybyr");
            
            request.setBdyBillFlagMap(billFlagMap);

            
            // Get Bill Keys
//            final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
            final Collection<PaymentCustomFieldValue> customValues =  transferParent.getCustomValues();
            
            final String companyCode1Val = getCustomLabel(companyCodePrefix, transferParent); 

            if (companyCode1Val != null) {
                request.setBdyCompanyCode(Long.parseLong(companyCode1Val));
            } 
            else {
                logger.error("Company code is not setup correctly.");
                throw new ExternalException("CCINSC");
            }
            
            if (companyCode1Val.equals("30301")) {
                transfer.setAmount(BigDecimal.ZERO);
            }
            
            final PaymentCustomFieldValue billKey1CustomField = customFieldHelper.getValue(paymentCustomFieldPrefix + billkey1 + "_" + companyCode1Val, customValues);
            final String billKey1Val = billKey1CustomField.getValue();

            final String validBillkeys = getCustomLabel(validBillkeysPrefix, transferParent);

            final PaymentCustomFieldValue billKey2CustomField = customFieldHelper.getValue(billkey2, customValues);
            String billKey2Val = null;
            if (billKey2CustomField != null) {
                billKey2Val = billKey2CustomField.getValue();
            }
            
            final PaymentCustomFieldValue billKey3CustomField = customFieldHelper.getValue(billkey3, customValues);
            String billKey3Val = null;
            if (billKey3CustomField != null) {
                billKey3Val = billKey3CustomField.getValue();
            }
            request.setBdyBillKey1(billKey1Val);
            BigDecimal amount = transferParent.getAmount();
            amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
            
//            BigDecimal feeAmount = BigDecimal.ZERO;
            BigDecimal feeAmount = transfer.getAmount();
            
//            Collection<? extends TransactionFee> fees = transfer.getType().getTransactionFees();
//            if (fees != null) {
//                for (TransactionFee fee : fees) {
//                    if (fee.isEnabled()) {
//                        feeAmount = feeAmount.add(fee.getValue());
//                    }
//                }
//                amount = amount.add(feeAmount);
//            }
            
            amount = amount.add(feeAmount);

            if (validBillkeys.contains(billkey2)) {
                request.setBdyBillKey2(amount.longValue());
            } else {
                request.setBdyBillKey2(null);
            }
            request.setBdyCardAcceptorTermId(cardAcceptorTermId);
            request.setBdyChannelId(channelId);
                                    
            request.setBdyCreditAmount(transferParent.getAmount());
            request.setBdyCreditAmountCorporate("0");
            request.setBdyCreditCurrency(currencyCode);
            request.setBdyCustomerAccountType(debitAccountType);
            request.setBdyCustomerChargesAmount(BigDecimal.ZERO);
            request.setBdyDebitAccount(Long.parseLong(debitAccount));
            request.setBdyDebitAccountCorporate(Long.parseLong("0"));
            request.setBdyDebitAmount(transferParent.getAmount());
            request.setBdyDebitAmountCorporate(BigDecimal.ZERO);
            request.setBdyDebitCurrency(currencyCode);
            request.setBdyIbtBuyRate(BigDecimal.ONE);
            request.setBdyIbtSellRate(BigDecimal.ONE);
            request.setBdyLanguageCode(languageCode);
            request.setBdyCurrencyCode(currencyCode);
            
            final String reserved1 = RandomStringUtils.randomNumeric(Integer.parseInt(reserved1Length));
            request.setBdyReservedField1(reserved1);
            
            // Rekonsiliasi
            //final String rekonKey = transfer.getType().getId().toString() + transfer.getFrom().getOwnerName() + transfer.getTransactionNumber();
            reservedField2 = transferParent.getType().getId().toString() + "/" + transferParent.getFrom().getOwnerName() + "/" + transferParent.getTransactionNumber();
            request.setBdyReservedField2(reservedField2);
            
            final String traceNumber= RandomStringUtils.randomNumeric(Integer.parseInt(traceNumberLength));
            request.setBdyTraceNumber(Long.parseLong(traceNumber));
            
            final String track2Data = RandomStringUtils.randomNumeric(Integer.parseInt(track2DataLength));
            request.setBdyTrack2data(Long.parseLong(track2Data));

            request.setBdyTtBuyRate(BigDecimal.ONE);
            request.setBdyTtSellRate(BigDecimal.ONE);

            
            RequestHeader header = new RequestHeader();
            header.setHdrChannelId(request.getBdyChannelId());
            header.setHdrTellerId(Long.parseLong(tellerId));

            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
            header.setHdrExternalId(extId);
            
            header.setHdrTransactionCode(transactionCode);
            
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(Calendar.getInstance().getTime());
            header.setHdrTimestamp(sTimestamp);

            final SimpleDateFormat sdfValueDate = new SimpleDateFormat(valueDateFormat);
            final String valueDate = sdfValueDate.format(Calendar.getInstance().getTime());
            request.setBdyValueDate(valueDate);
            
            final String journalSequence = RandomStringUtils.randomNumeric(Integer.parseInt(journalSequenceLength));
            header.setHdrJournalSequence(Long.parseLong(journalSequence));
            
            if (reqSecHeader) {

                StringBuffer oriDigestKey = new StringBuffer();
                oriDigestKey.append(header.getHdrExternalId());
                oriDigestKey.append(header.getHdrChannelId());
    
                oriDigestKey.append(request.getBdyCompanyCode());
                oriDigestKey.append(request.getBdyChannelId());
                oriDigestKey.append(request.getBdyBillKey1().toString());
                oriDigestKey.append(request.getBdyDebitAccount());
                oriDigestKey.append(request.getBdyCreditAmount());
                oriDigestKey.append(valueDate);
                
                final String digestedKey = securityUtil.getSignature(oriDigestKey.toString());
                
                SecurityHeader securityHeader = new SecurityHeader();
                securityHeader.setHdrResponseToken(digestedKey);
                request.setSecurityHeader(securityHeader);

            }
            
            request.setHeader(header);                
            
            final UBPCMsgResponse response = coreWSFactory.getUBPChannelWebService().payment(request);
           
            String responseMsg = "";
            if (response != null) {
                if (response.getHeader() != null) {
                    switch (response.getHeader().getHdrResponseCode()){
                        case 1: 
                        	BillInfoList[] billInfo = response.getBdyReceiptInfo();
                            String text = "";
                            for (int i=0 ; i<billInfo.length; i++) {
                                String label = billInfo[i].getLabel();
                                String val = billInfo[i].getValue();
                                if (label.equals(skipChar)) {
                                    if (!val.equals(skipChar)) {
                                        
                                        if (val.equals(emoneyRef)) {
                                            String trxRefNo = transferParent.getTransactionNumber();
                                            int diff2 = trxRefNo.length() - 11;
                                            val = trxRefNo.substring(diff2, trxRefNo.length());
                                        }
                                        
                                        if (val.equals(emoneyDate)) {
                                            SimpleDateFormat sdfDate = new SimpleDateFormat(emoneyDateFormat);
                                            val = sdfDate.format(transferParent.getProcessDate().getTime());
                                        }
                                        
                                        if (val.contains(emoneyByr)) {
                                            val = val.replace(emoneyByr, "");
                                            NumberFormat nf = NumberFormat.getInstance();
                                            DecimalFormat df = (DecimalFormat) nf;
                                            
                                            df.applyPattern("#,###");
                                            val = df.format(new BigDecimal(val)).replace(",", ".");
                                        }
                                        
                                        text = text + val.trim() + " ";
                                    }
                                } else {

                                    if (val.contains(emoneyByr)) {
                                        val = val.replace(emoneyByr, "");
                                        NumberFormat nf = NumberFormat.getInstance();
                                        DecimalFormat df = (DecimalFormat) nf;
                                        
                                        df.applyPattern("#,###");
                                        val = df.format(new BigDecimal(val)).replace(",", ".");
                                    }

                                    text = text + label.trim() + " " + val.trim() + " ";
                                }
                            }

                            final StringBuffer rekonKey = new StringBuffer();
                            rekonKey.append(getUBPRemarks(request, response));
                            rekonKey.append("|");
                            rekonKey.append(reservedField2);
                            rekonKey.append("|");
                            rekonKey.append(" ");
                            logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
                            
                            transferParent.setTraceData(rekonKey.toString());
                            
                            transferParent.setDescription(text);
                            transfer.setParent(transferParent);
                            super.onTransferInserted(transfer);
                            break;
                        default:
                        	logger.error("response Code : " + response.getHeader().getHdrResponseCode());
                            responseMsg = response.getHeader().getHdrResponseMessage();
                            final String errorCode = response.getHeader().getHdrErrorNumber().trim();
                            
                            String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.payment.error_code." + companyCode1Val + "." + errorCode);
                            if (mappingErrorMsg == null) {
                                mappingErrorMsg = responseMsg;
                            }
                            logger.error("Host Error (" + errorCode + ") : " + mappingErrorMsg);
                            
                            //Update for childlistener PLN
                            if(errorCode == null || isHaveToDebet(errorCode)){
                            	logger.info("Error Code : " + errorCode + " - " + mappingErrorMsg);
                            	throw new Exception(errorCode);
                            }
                        	logger.info("Throw ExternalException");
                            throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                	logger.info("Throw Exception(RHIE)");
                    throw new Exception("RHIE");
                }
            } else {
                logger.error("Response is empty.");
            	logger.info("Throw Exception (RIE)");
                throw new Exception("RIE");
            }

        } catch (final ExternalException e) {
            logger.error(e);
            logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
            logger.info(e.getCause());
        	logger.info(e.getMessage());
        	logger.info("Throw Exception");
            throw e;
        } catch (final WebServiceException e) {
        	logger.error(e);
        	logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
        	logger.info(e.getCause());
        	logger.info(e.getMessage());
        	
            final StringBuffer rekonKey = new StringBuffer();
            rekonKey.append("");
            rekonKey.append("|");
            rekonKey.append(reservedField2);
            rekonKey.append("|");
            rekonKey.append(" ");

            transferParent.setTraceData(rekonKey.toString());
            transfer.setParent(transferParent);
            super.onTransferInserted(transfer);
        } catch (final SocketTimeoutException e) {
            logger.error(e);
            logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
            logger.info(e.getCause());
        	logger.info(e.getMessage());
        	

            final StringBuffer rekonKey = new StringBuffer();
            rekonKey.append("");
            rekonKey.append("|");
            rekonKey.append(reservedField2);
            rekonKey.append("|");
            rekonKey.append(" ");

            transferParent.setTraceData(rekonKey.toString());
            transfer.setParent(transferParent);
            super.onTransferInserted(transfer);
        }catch (SocketException e) {
            logger.error(e);
            logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
            logger.info(e.getCause());
        	logger.info(e.getMessage());

            StringBuffer rekonKey = new StringBuffer();
            rekonKey.append("");
            rekonKey.append("|");
            rekonKey.append(reservedField2);
            rekonKey.append("|");
            rekonKey.append(" ");

            transferParent.setTraceData(rekonKey.toString());
            transfer.setParent(transferParent);

            super.onTransferInserted(transfer);
          }
        catch (final Exception e) {
        	logger.error(e);
            logger.info(e.getCause());
            logger.info("From Member : " + transferParent.getFromOwner().toString() + "- transaction number : " + transferParent.getTransactionNumber().toString());
        	logger.info(e.getMessage());
            
            StringBuffer rekonKey = new StringBuffer();
            rekonKey.append("");
            rekonKey.append("|");
            rekonKey.append(reservedField2);
            rekonKey.append("|");
            rekonKey.append(" ");

            transferParent.setTraceData(rekonKey.toString());
            transfer.setParent(transferParent);

            super.onTransferInserted(transfer);
        } 
        
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    private String getCustomLabel(final String internalName, final Transfer transfer) {
        final Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }

    private Collection<PaymentCustomFieldValue> setCustomValue(final String internalName, Collection<PaymentCustomFieldValue> customValues, final String value) {
        
        for (PaymentCustomFieldValue customValue: customValues) {
            if (customValue.getField().getInternalName().equals(internalName)) {
                PaymentCustomFieldValue newValue = (PaymentCustomFieldValue) customValue.clone();
                customValues.remove(customValue);
                newValue.setStringValue(value);
                customValues.add(newValue);
                break;
            }
        }
    
        return customValues;
    }
    
    private String getUBPRemarks(final UBPCMsgRequest request, final UBPCMsgResponse response) {
        StringBuilder ubpRemarks = new StringBuilder();
        String result = "";
        
        ubpRemarks.append("UBP");
        ubpRemarks.append(response.getBdyMerchantType());
        ubpRemarks.append(request.getBdyCompanyCode());
        ubpRemarks.append(request.getBdyLanguageCode());
        ubpRemarks.append(request.getBdyBillFlagMap());
        ubpRemarks.append(request.getBdyBillKey1());
        
        if (ubpRemarks.toString().length() <= 40) {
            result = ubpRemarks.toString();
        } else {
            result = ubpRemarks.toString().substring(0, 40);
        }
        
        return result;
    }

    public void sendSMS(Member member, Transfer transfer){
        
        if (LoggedUser.isWebService()) {
        
            final SendMessageFromSystemDTO message = new SendMessageFromSystemDTO();
            message.setToMember(member);
            message.setType(Message.Type.EXTERNAL_PAYMENT);
            message.setEntity(transfer);
            message.setSubject("External");
            message.setBody(transfer.getDescription().trim());
            message.setSms(transfer.getDescription().trim());
    
            // Send the message
            messageService.sendFromSystem(message);

        } else {
        
            mapSmsMailing = new HashMap<String, Object>();
            mapSmsMailing.put("free", true);
            mapSmsMailing.put("text", transfer.getDescription().trim());
            mapSmsMailing.put("member", member);
            Collection<MemberGroup> groups = Collections.emptyList();
            mapSmsMailing.put("groups", groups);
            final SmsMailing smsMailing = getDataBinder().readFromString(mapSmsMailing);
            smsMailingServiceLocal.send(smsMailing);
        
        }
    }
    
    private DataBinder<SmsMailing> getDataBinder() {
        if (dataBinder == null) {
            final BeanBinder<SmsMailing> binder = BeanBinder.instance(SmsMailing.class);
            binder.registerBinder("free", PropertyBinder.instance(Boolean.TYPE, "free"));
            binder.registerBinder("text", PropertyBinder.instance(String.class, "text"));
            binder.registerBinder("member", PropertyBinder.instance(Member.class, "member"));
            binder.registerBinder("groups", SimpleCollectionBinder.instance(MemberGroup.class, "groups"));
            dataBinder = binder;
        }
        return dataBinder;
    }

    public void setSmsMailingServiceLocal(SmsMailingServiceLocal smsMailingServiceLocal) {
        this.smsMailingServiceLocal = smsMailingServiceLocal;
    }

    @Override
    public void onTransferProcessed(Transfer transfer) {
        // TODO Auto-generated method stub
    	Transfer transferParent = transfer.getParent();
        super.onTransferProcessed(transfer);
        Member member = null;
        
        try {
//            String companyCode1Val = "";
            boolean isCustomNotification = false;
            
            final Collection<PaymentCustomField> customFields = transferParent.getType().getCustomFields();
            
            for (PaymentCustomField customField: customFields) {
                if (customField.getInternalName().contains("custom_sms_notif_")) {
                    isCustomNotification = true;
                    break;
                }
            }

//            try {
//                final String companyCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.company_code_prefix");
//                companyCode1Val = getCustomLabel(companyCodePrefix, transfer);
//                isCustomNotification = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.customNotification." + companyCode1Val));
//            } catch (final Exception e) {
//                logger.error(e);
//            }
//
            if (isCustomNotification) {
                member = (Member) transferParent.getFromOwner();
                sendSMS(member, transferParent);
            }
        } catch (final Exception e) {
            logger.error("Send Sms=" + e);
        }

    }

    public void setMessageService(MessageServiceLocal messageService) {
        this.messageService = messageService;
    }

    public boolean isHaveToDebet(String errorNumberParam){
    	boolean result = false;
    	try {
			String listDebetErrorNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("list.error.number.biller.debet");
			String [] arrayDebetErrorNumbers = listDebetErrorNumber.split(",");
			for(String errorNumber : arrayDebetErrorNumbers){
				if(errorNumberParam.equals(errorNumber)){
					result = true;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
    	
    	
    	return result;
    }
    
    
}