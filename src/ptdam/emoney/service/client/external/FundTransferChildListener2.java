package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.payments.ChargebackResult;
import nl.strohalm.cyclos.webservices.payments.ChargebackStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

@SuppressWarnings("unused")
public class FundTransferChildListener2 extends TransferListenerAdapter {
    
//    @Autowired
    private SecurityUtil securityUtil;
    
    private final Log               logger    = LogFactory.getLog(FundTransferChildListener2.class);
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    
    private CustomFieldHelper customFieldHelper;
    private FundTransferWebService wsClient;
    private CoreWebServiceFactory coreWSFactory;
    private PaymentWebServiceImpl paymentWebServiceImpl;
    
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }
        

    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    public void setPaymentWebServiceImpl(PaymentWebServiceImpl paymentWebServiceImpl) {
		this.paymentWebServiceImpl = paymentWebServiceImpl;
	}

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        FundTransferMsgRequest request = new FundTransferMsgRequest();
        FundTransferMsgResponse response;
        Transfer TransferParent = transfer.getParent();
        
     // Rekonsiliasi
        String remark3 = "";
        String settleSeq = " ";
               
        try {
            
        	final Collection<PaymentCustomFieldValue> customValues =  TransferParent.getCustomValues();
        	
            final String norekInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("p62.custom_field_name");
            final PaymentCustomFieldValue noRekCustomField = customFieldHelper.getValue(norekInternalName, customValues);
            final String noRekTujuan = noRekCustomField.getValue();
            
            final String jSeqInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.journalSequence.internalName");
            final PaymentCustomFieldValue jSeqCustomField = customFieldHelper.getValue(jSeqInternalName, customValues);
//          final String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.journalSequence");
            Date now = new Date(); 
	  		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
	  		String dateNow = sdf.format(now);
	  		Random random = new Random();
	  		final String journalSeq = dateNow + random.nextInt(10); 
            
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.glaccount");
            final String defaultCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.defaultCurrencyCode");
            final String debitCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.debitCurrency");
            final String creditCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.creditCurrency");
            final String chargesCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.chargesCurrency");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.tellerId");
            final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.transactionCode");
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.channelId");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String valueDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.valuedate_format");
            final String settleSeqCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
            final String tranCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.trancode_prefix");
            
            final BigDecimal transferAmount = TransferParent.getAmount();
            
            
            final PaymentCustomFieldValue settleSeqCustomField = customFieldHelper.getValue(settleSeqCustomFieldName, customValues);
             
            if (settleSeqCustomField != null) {
                settleSeq = settleSeqCustomField.getValue();
            }
            
            //final String tranCodeCustom = getCustomLabel(tranCodePrefix, TransferParent);
            final String tranCodeCustom = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.transactionCode");
            
            final SimpleDateFormat sdfValueDate = new SimpleDateFormat(valueDateFormat);
            String sValueDate = sdfValueDate.format(transfer.getProcessDate().getTime());
            
            request.setBdyDebitAccount(new BigDecimal(debitAccount));
            request.setBdyDebitAmount(transferAmount);
            request.setBdyCreditAccount(Long.parseLong(noRekTujuan));
            request.setBdyCreditAmount(transferAmount);
            request.setBdyValueDate(sValueDate);
            request.setBdyChargesAmount(BigDecimal.ZERO);
            request.setBdyConvertedChargesAmount(BigDecimal.ZERO);

            request.setBdyDefaultCurrencyCode(defaultCurrency);
            request.setBdyDebitCurrency(debitCurrency);
            request.setBdyCreditCurrency(creditCurrency);
            request.setBdyChargesCurrency(chargesCurrency);
            
            request.setBdyIbtBuyRate(BigDecimal.ONE);
            request.setBdyIbtSellRate(BigDecimal.ONE);
            request.setBdyTtBuyRate(BigDecimal.ONE);
            request.setBdyTtSellRate(BigDecimal.ONE);
            
            
            
            String trxRefNo = TransferParent.getTransactionNumber();
            int diff2 = trxRefNo.length() - 7;
            
            if (settleSeq.trim().length() == 0) {
            	remark3 = TransferParent.getType().getId().toString() + "/" + TransferParent.getFrom().getOwnerName() + "/" + trxRefNo.substring(diff2, trxRefNo.length());
                final String remark1 = TransferParent.getFrom().getOwnerName() + "/" +trxRefNo;
                request.setBdyRemark1(remark1);
            } else {
                remark3 = TransferParent.getType().getId().toString() + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + settleSeq;
                request.setBdyRemark1(TransferParent.getDescription());
            }
            
            request.setBdyRemark3(remark3);
            
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.extIdlength");
            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
  
            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.reqSecurityHeader"));
          
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(TransferParent.getActualDate().getTime());
                        
            RequestHeader header = new RequestHeader();
            header.setHdrExternalId(extId);
            header.setHdrTellerId(Long.parseLong(tellerId));
        	header.setHdrJournalSequence(Long.parseLong(journalSeq));
            
            if (tranCodeCustom != null) {
                header.setHdrTransactionCode(tranCodeCustom);
            } else {
                header.setHdrTransactionCode(tranCode);
            }
            
            header.setHdrChannelId(channelId);
            header.setHdrTimestamp(sTimestamp);
                        
            if (reqSecHeader) {
                
                StringBuffer oriDigestKey = new StringBuffer();
                oriDigestKey.append(header.getHdrExternalId());
                oriDigestKey.append(header.getHdrChannelId());
                oriDigestKey.append(request.getBdyDebitAccount());
                oriDigestKey.append(request.getBdyCreditAccount());
                oriDigestKey.append(request.getBdyCreditAmount());
                oriDigestKey.append(sValueDate);
                
                final String digestedKey = securityUtil.getSignature(oriDigestKey.toString());
                
                SecurityHeader securityHeader = new SecurityHeader();
                securityHeader.setHdrResponseToken(digestedKey);
                securityHeader.setHdrChallengeType("1");
                securityHeader.setHdrAppliNo("1");
                securityHeader.setHdrChallenge1("1");
                securityHeader.setHdrCorporateId("1");
                securityHeader.setHdrCustomerAdditionalId("");
                
                request.setSecurityHeader(securityHeader);
            }
            
            TransferParent.setTraceData(extId);
            super.onTransferInserted(transfer);

            request.setHeader(header);
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
        try{
	        response = coreWSFactory.getFundTransferWebService().transfer(request);
	        
	        String responseMsg = "";
	        StringBuffer rekonKey = new StringBuffer();
	        
	        if (response != null) {
	            if (response.getResponseHeader() != null) {
	                switch (response.getResponseHeader().getHdrResponseCode()){
	                    case 1: 
	                    	logger.debug("responseCode : " + response.getResponseHeader().getHdrResponseCode());
	                    	rekonKey.append(" ");
	                        rekonKey.append("|");
	                        rekonKey.append(remark3);
	                        rekonKey.append("|");
	                        rekonKey.append(settleSeq);
	                        TransferParent.setTraceData(rekonKey.toString());
	                        TransferParent.setDescription(TransferParent.getDescription());
	                        super.onTransferInserted(transfer);
	                        break;
	                    default:
	                    	logger.debug("responseCode : " + response.getResponseHeader().getHdrResponseCode());
	                        responseMsg = response.getResponseHeader().getHdrResponseMessage();
	                        final String errorCode = response.getResponseHeader().getHdrErrorNumber().trim();
	                        logger.debug("hdrErrorNumber : " + errorCode);
	                        if(isHaveToDebet(errorCode)){// jika error code gagal tidak jelas
	                        	rekonKey.append(" ");
		                        rekonKey.append("|");
		                        rekonKey.append(remark3);
		                        rekonKey.append("|");
		                        rekonKey.append(settleSeq);
		                        TransferParent.setTraceData(rekonKey.toString()); 
	                        	TransferParent.setDescription("Ref:" + TransferParent.getTransactionNumber());
	                             super.onTransferInserted(transfer);
	                        }else{
	                        	String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + errorCode);
	                        	logger.error("Host Error (" + errorCode + ") : " + responseMsg);
	                            if (mappingErrorMsg == null) {
	                                mappingErrorMsg = responseMsg;
	                            }
	                            throw new ExternalException(mappingErrorMsg);
	                        }
	                }
	            } else {
	                logger.error("Response Header is empty.");
	                logger.info("Throw Exception(RHIE)");
                    throw new Exception("RHIE");
	            }
	        } else {
	            logger.error("Response is empty.");
	            logger.info("Throw Exception (RIE)");
                throw new Exception("RIE");
	        }
	        
        }
        catch (final ExternalException e) {
        	logger.error(e);
        	logger.info(e.getMessage());
        	throw e;
        }
        catch (final Exception e) {
        	logger.error(e);
                throw new ExternalException("GE");
        }
        
    }
    
    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    private String getCustomLabel(final String internalName, final Transfer transfer) {
    	Transfer transferParent = transfer.getParent();
        final Collection<PaymentCustomField> customFields = transferParent.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    public boolean isHaveToDebet(String errorNumberParam){
    	boolean result = false;
    	try {
			String listDebetErrorNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("list.error.number.biller.debet");
			String [] arrayDebetErrorNumbers = listDebetErrorNumber.split(",");
			for(String errorNumber : arrayDebetErrorNumbers){
				if(errorNumberParam.equals(errorNumber)){
					result = true;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
    	
    	
    	return result;
    }
}
