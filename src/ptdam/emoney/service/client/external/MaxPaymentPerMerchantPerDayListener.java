/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptdam.emoney.webservices.utils.SecurityUtil;

import nl.strohalm.cyclos.entities.accounts.Account;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.Period;
import nl.strohalm.cyclos.utils.conversion.CoercionHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.accounts.AccountHistorySearchParameters;
import nl.strohalm.cyclos.webservices.utils.QueryHelper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ptdam.emoney.EmoneyConfiguration;

import com.ptdam.emoney.webservices.utils.SecurityUtil;

public class MaxPaymentPerMerchantPerDayListener extends TransferListenerAdapter {
    @Autowired
    private SecurityUtil securityUtil;
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    private QueryHelper                    queryHelper;
    private PaymentServiceLocal            paymentServiceLocal;
    private AccountServiceLocal            accountService;
    private int i = 0; 
    
    private CustomFieldHelper customFieldHelper;
    private final Log               logger    = LogFactory.getLog(MaxPaymentPerMerchantPerDayListener.class);

    
    @SuppressWarnings("unused")
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        try {
        	String merchantPromos = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.appliedTo");
            String limitAmountPerDay = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.rule.type.MAX_AMOUNT_PER_DAY");
            String limitTrxPerDay = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.rule.type.MAX_TRX_PER_DAY");

        	int totalGroupMerchant = 0;
        	try{
        		totalGroupMerchant = Integer.parseInt(EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.appliedTo.total"));
        	}catch (Exception e){
        		logger.error("total group must be set !!");
        	}
        	
        	for (i=1; i<=totalGroupMerchant; i++){
	        	merchantPromos = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.appliedTo."+i);
	            limitAmountPerDay = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.rule.type.MAX_AMOUNT_PER_DAY."+i);
	            limitTrxPerDay = EmoneyConfiguration.getEmoneyProperties().getProperty("merchant.corporate.rule.type.MAX_TRX_PER_DAY."+i);
	
	            String strStartDate = EmoneyConfiguration.getEmoneyProperties().getProperty("effectivedates.start_date."+i);
	            String strEndDate = EmoneyConfiguration.getEmoneyProperties().getProperty("effectivedates.end_date."+i);
	            String strDays = EmoneyConfiguration.getEmoneyProperties().getProperty("effectivedates.days."+i);
	            String strExcludeDate = EmoneyConfiguration.getEmoneyProperties().getProperty("effectivedates.exclude.date."+i);
            Calendar transferDate = transfer.getProcessDate();

	            if ((isDateValidate(strStartDate, strEndDate, strDays, strExcludeDate, transferDate)) && (isMerchantPromo(merchantPromos, transfer.getTo().getOwnerName()))) {
        		String strEmoneyAccountTypeId = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.account.type.id");
	        	Long emoneyAccountTypeId = Long.parseLong(strEmoneyAccountTypeId);
	        	
	        	MemberAccount memberAccount = (MemberAccount) transfer.getFrom();
	            Member member = customFieldHelper.loadMember(memberAccount.getMember().getId());
	            
	            AccountHistorySearchParameters params = new AccountHistorySearchParameters();
	            params.setPrincipal(transfer.getFrom().getOwnerName());
	            params.setAccountTypeId(emoneyAccountTypeId);
	            params.setPageSize(100);
	            Calendar beginDate = Calendar.getInstance();
	            beginDate.set(beginDate.get(Calendar.YEAR),beginDate.get(Calendar.MONTH),beginDate.get(Calendar.DATE),0,0,0);
	            params.setBeginDate(beginDate);
	            Calendar endDate = Calendar.getInstance();
	            params.setEndDate(endDate);
	            
	              // Get the query and account type
	            final TransferQuery query = new TransferQuery();
	            queryHelper.fill(params, query);
	            AccountType accountType = CoercionHelper.coerce(AccountType.class, params.getAccountTypeId());
	            query.setOwner(member);
	            final Account account = accountService.getAccount(new AccountDTO(member, accountType), Account.Relationships.TYPE);
	            query.setType(account.getType());
	            if (params.getBeginDate() != null || params.getEndDate() != null) {
	                final Period period = new Period(params.getBeginDate(), params.getEndDate());
	                query.setPeriod(period);
	            }
	            
	            TransferType trfType = new TransferType();
	            Long bayarTokoTypeId = transfer.getType().getId();
	            
	            trfType.setId(bayarTokoTypeId);
	            query.setTransferType(trfType);
		            
		        
				final List<Transfer> transfers = paymentServiceLocal.search(query);
				Iterator it = transfers.iterator();
					  
				Calendar today = Calendar.getInstance();
					  
				BigDecimal dailylyTotalAmount = BigDecimal.ZERO;
				int dailyTotalTrx = 0;
					  
				while (it.hasNext()) {
			          Transfer trf = (Transfer)it.next();
			          Calendar processDate = trf.getProcessDate();
				          if ((isSameDay(today, processDate)) && (isMerchantPromo(merchantPromos, trf.getTo().getOwnerName()))) {
			            dailylyTotalAmount = dailylyTotalAmount.add(trf.getAmount());
			            dailyTotalTrx++;
			          }
			    }
		    	
				System.out.println("DailylyTotalAmount" + dailylyTotalAmount);
					 
				if ((dailylyTotalAmount.compareTo(new BigDecimal(limitAmountPerDay)) > 0) || (dailyTotalTrx > Integer.parseInt(limitTrxPerDay))) {
				          throw new ExternalException("MaxPaymentInMerchantPromoExceeded, " + transfer.getFrom().getOwnerName());
				    }
			    }
			}
        	
        	super.onTransferInserted(transfer);
        } catch (final ExternalException e) {
        	logger.error(e);
        	throw e;
        } catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
    }
    
    public boolean isDateValidate(String strStartDate, String strEndDate, String strDays, String strExcludeDate, Calendar transferDate){
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
    	
    	try {
			String dayOfWeek = transferDate.get(Calendar.DAY_OF_WEEK) + "";
			if(!strDays.contains(dayOfWeek)){
				return false;
			}
			
			String strToday = sdf2.format(transferDate.getTime());
			if(strExcludeDate.contains(strToday)){
				return false;
			}
			
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(sdf.parse(strStartDate));
			
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(sdf.parse(strEndDate));
			if(transferDate.compareTo(startDate) >= 0 && transferDate.compareTo(endDate) <= 0){
				return true;
			}else{
				return false;
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return false;
    	
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    private String getCustomLabel(final String internalName, final Transfer transfer) {
        final Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    public boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null)
            return false;
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA)
                && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) 
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    
    public boolean isMerchantPromo(String merchants, String merchant){
    	String [] arrayMerchant = merchants.split(",");
    	
    	for(String listMerchant : arrayMerchant){
    		if(listMerchant.equals(merchant)){
    			return true;
    		}
    	}
    	return false;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }

    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountService = accountService;
    }
    
    public void setQueryHelper(final QueryHelper queryHelper) {
        this.queryHelper = queryHelper;
    }
    
    
   
    
}

