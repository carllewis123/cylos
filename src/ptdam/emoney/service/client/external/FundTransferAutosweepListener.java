/*
 *** Ade Firmansyah***
 *
 *FundTransfer yang menggunakan remark = {trx-type}/{7 digit akhir emoney refno}/{no rekening tujuan}
 *SettleSeq dihapus karena tidak di panggil pada batchSettlement

 */
package ptdam.emoney.service.client.external;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import org.apache.catalina.ha.backend.CollectedInfo;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.ws.security.sts.provider.model.secext.EncodedString;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.transactions.CashOutWebService;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.conversion.CalendarConverter;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

@SuppressWarnings("unused")
public class FundTransferAutosweepListener extends TransferListenerAdapter {

	// @Autowiredn
	private SecurityUtil securityUtil;

	private final Log logger = LogFactory
			.getLog(FundTransferAutosweepListener.class);

	public void setSecurityUtil(SecurityUtil securityUtil) {
		this.securityUtil = securityUtil;
	}

	private CustomFieldHelper customFieldHelper;
	private FundTransferWebService wsClient;
	private CoreWebServiceFactory coreWSFactory;

	private class TransactionListnener implements TransactionCommitListener,
			TransactionRollbackListener {

		@Override
		public void onTransactionRollback() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTransactionCommit() {
			// TODO Auto-generated method stub

		}

	}

	public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
		this.coreWSFactory = coreWSFactory;
	}

	@Override
	public void onTransferInserted(Transfer transfer) throws ExternalException {
		// TODO Auto-generated method stub
		// super.onTransferInserted(transfer);
		FundTransferMsgRequest request = new FundTransferMsgRequest();
		FundTransferMsgResponse response;

		Transfer transferParent = transfer.getParent();

		try {

			final String norekInternalName = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.norektujuan.internalname");
			final Collection<PaymentCustomFieldValue> noRekCustomFields = transfer
					.getCustomValues();
			final PaymentCustomFieldValue noRekCustomField = customFieldHelper
					.getValue(norekInternalName, noRekCustomFields);
			final String noRekTujuan = noRekCustomField.getValue();
			final BigDecimal transferAmount = transfer.getAmount();

			final String debitAccount = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.glaccount");
			final String defaultCurrency = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.defaultCurrencyCode");
			final String debitCurrency = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.debitCurrency");
			final String creditCurrency = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.creditCurrency");
			final String chargesCurrency = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.chargesCurrency");
			final String tellerId = EmoneyConfiguration.getEmoneyProperties()
					.getProperty("core.fundtransfer.tellerId");
			// final String journalSeq =
			// EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.journalSequence");
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
			String dateNow = sdf.format(now);
			Random random = new Random();
			final String journalSeq = dateNow + random.nextInt(10);
			final String tranCode = EmoneyConfiguration.getEmoneyProperties()
					.getProperty("core.fundtransfer.transactionCode");
			final String channelId = EmoneyConfiguration.getEmoneyProperties()
					.getProperty("core.fundtransfer.channelId");
			final String timeStampFormat = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.ubpc.timestamp_format");
			final String valueDateFormat = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.ubpc.valuedate_format");
			final String tranCodePrefix = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.trancode_prefix");

			final Collection<PaymentCustomFieldValue> customValues = transferParent
					.getCustomValues();
			final String tranCodeCustom = getCustomLabel(tranCodePrefix,
					transfer);

			final SimpleDateFormat sdfValueDate = new SimpleDateFormat(
					valueDateFormat);
			String sValueDate = sdfValueDate.format(transfer.getProcessDate()
					.getTime());

			request.setBdyDebitAccount(new BigDecimal(debitAccount));
			request.setBdyDebitAmount(transferAmount);
			request.setBdyCreditAccount(Long.parseLong(noRekTujuan));
			request.setBdyCreditAmount(transferAmount);
			request.setBdyValueDate(sValueDate);
			request.setBdyChargesAmount(BigDecimal.ZERO);
			request.setBdyConvertedChargesAmount(BigDecimal.ZERO);

			request.setBdyDefaultCurrencyCode(defaultCurrency);
			request.setBdyDebitCurrency(debitCurrency);
			request.setBdyCreditCurrency(creditCurrency);
			request.setBdyChargesCurrency(chargesCurrency);

			request.setBdyIbtBuyRate(BigDecimal.ONE);
			request.setBdyIbtSellRate(BigDecimal.ONE);
			request.setBdyTtBuyRate(BigDecimal.ONE);
			request.setBdyTtSellRate(BigDecimal.ONE);

			// Rekonsiliasi
			String remark3 = "";

			String trxRefNo = transfer.getTransactionNumber();
			int diff2 = trxRefNo.length() - 7;
			final String remark1Template = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.remark1template");
			remark3 = transfer.getType().getId().toString() + "/"
					+ trxRefNo.substring(diff2, trxRefNo.length()) + "/"
					+ Long.parseLong(noRekTujuan);
			final String remark1 = remark1Template.replace("{0}", transfer
					.getFrom().getOwnerName());
			if (transferParent.getDescription() == null)
				request.setBdyRemark1(remark1);
			else {
				request.setBdyRemark1(transferParent.getDescription());
			}

			request.setBdyRemark3(remark3);

			final String extIdLength = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"core.fundtransfer.extIdlength");
			final String extId = RandomStringUtils.randomNumeric(Integer
					.parseInt(extIdLength));
			final boolean reqSecHeader = Boolean
					.parseBoolean(EmoneyConfiguration.getEmoneyProperties()
							.getProperty("core.fundtransfer.reqSecurityHeader"));

			final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(
					timeStampFormat);
			final String sTimestamp = sdfTimeStamp.format(transfer
					.getActualDate().getTime());

			RequestHeader header = new RequestHeader();
			header.setHdrExternalId(extId);
			header.setHdrTellerId(Long.parseLong(tellerId));
			header.setHdrJournalSequence(Long.parseLong(journalSeq));

			if (tranCodeCustom != null) {
				header.setHdrTransactionCode(tranCodeCustom);
			} else {
				header.setHdrTransactionCode(tranCode);
			}

			header.setHdrChannelId(channelId);
			header.setHdrTimestamp(sTimestamp);

			if (reqSecHeader) {

				StringBuffer oriDigestKey = new StringBuffer();
				oriDigestKey.append(header.getHdrExternalId());
				oriDigestKey.append(header.getHdrChannelId());
				oriDigestKey.append(request.getBdyDebitAccount());
				oriDigestKey.append(request.getBdyCreditAccount());
				oriDigestKey.append(request.getBdyCreditAmount());
				oriDigestKey.append(sValueDate);

				final String digestedKey = securityUtil
						.getSignature(oriDigestKey.toString());

				SecurityHeader securityHeader = new SecurityHeader();
				securityHeader.setHdrResponseToken(digestedKey);
				securityHeader.setHdrChallengeType("1");
				securityHeader.setHdrAppliNo("1");
				securityHeader.setHdrChallenge1("1");
				securityHeader.setHdrCorporateId("1");
				securityHeader.setHdrCustomerAdditionalId("");

				request.setSecurityHeader(securityHeader);
			}

			request.setHeader(header);

			response = coreWSFactory.getFundTransferWebService().transfer(
					request);

			String responseMsg = "";

			if (response != null) {
				if (response.getResponseHeader() != null) {
					switch (response.getResponseHeader().getHdrResponseCode()) {
					case 1:
						final StringBuffer rekonKey = new StringBuffer();
						rekonKey.append(" ");
						rekonKey.append("|");
						rekonKey.append(remark3);
						rekonKey.append("|");

						transfer.setTraceData(rekonKey.toString());
						if (transferParent.getDescription() != null) {
							transfer.setDescription(transferParent
									.getDescription());
						}
						super.onTransferInserted(transfer);
						break;
					default:
						responseMsg = response.getResponseHeader()
								.getHdrResponseMessage();
						final String errorCode = response.getResponseHeader()
								.getHdrErrorNumber().trim();
						String mappingErrorMsg = EmoneyConfiguration
								.getEmoneyProperties().getProperty(
										"core.fundtransfer.error_code."
												+ errorCode);
						logger.error("Host Error (" + errorCode + ") : "
								+ responseMsg);
						if (mappingErrorMsg == null) {
							mappingErrorMsg = responseMsg;
						}
						throw new ExternalException(errorCode);
					}
				} else {
					logger.error("Response Header is empty.");
					throw new ExternalException("RHIE");
				}
			} else {
				logger.error("Response is empty.");
				throw new ExternalException("RIE");
			}

		} catch (final ExternalException e) {
			throw e;
		} catch (final SocketTimeoutException e) {
			logger.error(e);
			throw new ExternalException("STE");
		} catch (final Exception e) {
			logger.error(e);
			throw new ExternalException("GE");
		}

	}

	public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
		this.customFieldHelper = customFieldHelper;
	}

	private String getCustomLabel(final String internalName,
			final Transfer transfer) {
		final Collection<PaymentCustomField> customFields = transfer.getType()
				.getCustomFields();

		for (PaymentCustomField customField : customFields) {
			if (customField.getInternalName().contains(internalName)) {
				return customField.getName();
			}
		}
		return null;
	}

	public boolean isHaveToDebet(String errorNumberParam) {
		boolean result = false;
		try {
			String listDebetErrorNumber = EmoneyConfiguration
					.getEmoneyProperties().getProperty(
							"list.error.number.biller.debet");
			String[] arrayDebetErrorNumbers = listDebetErrorNumber.split(",");
			for (String errorNumber : arrayDebetErrorNumbers) {
				if (errorNumberParam.equals(errorNumber)) {
					result = true;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		return result;
	}
}

