package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Collection;

import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.payments.ChargebackResult;
import nl.strohalm.cyclos.webservices.payments.ChargebackStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgRequestExtended;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

@SuppressWarnings("unused")
public class FundTransferListener2 extends TransferListenerAdapter {
    
//    @Autowired
    private SecurityUtil securityUtil;
    
    private final Log               logger    = LogFactory.getLog(FundTransferListener2.class);
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    
    private CustomFieldHelper customFieldHelper;
    private FundTransferWebService wsClient;
    private CoreWebServiceFactory coreWSFactory;
    private PaymentWebServiceImpl paymentWebServiceImpl;
    
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }
        

    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    public void setPaymentWebServiceImpl(PaymentWebServiceImpl paymentWebServiceImpl) {
		this.paymentWebServiceImpl = paymentWebServiceImpl;
	}

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        FundTransferMsgRequest request = new FundTransferMsgRequest();
        FundTransferMsgResponse response;
        
     // Rekonsiliasi
        String remark3 = "";
        String settleSeq = " ";
               
        try {
            
        	//Replace transfer.getDescription() contains .SUCCESS/.FAILED/.REVERSAL
        	String transferDescOld = transfer.getDescription();
        	if(transferDescOld.contains(".SUCCESS") || transferDescOld.contains(".FAILED") || transferDescOld.contains(".REVERSAL")) {
        		System.out.println("Desc Before : " + transferDescOld);
        		String transferDescNew = transferDescOld.replace(".SUCCESS", "").replace(".FAILED", "").replace(".REVERSAL", "");
        		transfer.setDescription(transferDescNew);
        		System.out.println("Desc After : " + transfer.getDescription());
        	}
        	
        	final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
        	
            final String norekInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
            final PaymentCustomFieldValue noRekCustomField = customFieldHelper.getValue(norekInternalName, customValues);
            final String noRekTujuan = noRekCustomField.getValue();
            
            final String jSeqInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.journalSequence.internalName");
            final PaymentCustomFieldValue jSeqCustomField = customFieldHelper.getValue(jSeqInternalName, customValues);
            String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.journalSequence");
            if (jSeqCustomField != null) {
            	journalSeq = jSeqCustomField.getValue();
            }
            
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.glaccount");
            final String defaultCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.defaultCurrencyCode");
            final String debitCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.debitCurrency");
            final String creditCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.creditCurrency");
            final String chargesCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.chargesCurrency");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.tellerId");
            final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.transactionCode");
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.channelId");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String valueDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.valuedate_format");
            final String settleSeqCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
            final String tranCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.trancode_prefix");
            
            final BigDecimal transferAmount = transfer.getAmount();
            
            
            final PaymentCustomFieldValue settleSeqCustomField = customFieldHelper.getValue(settleSeqCustomFieldName, customValues);
             
            if (settleSeqCustomField != null) {
                settleSeq = settleSeqCustomField.getValue();
            }
            
            final String tranCodeCustom = getCustomLabel(tranCodePrefix, transfer);
            
            final SimpleDateFormat sdfValueDate = new SimpleDateFormat(valueDateFormat);
            String sValueDate = sdfValueDate.format(transfer.getProcessDate().getTime());
            
            request.setBdyDebitAccount(new BigDecimal(debitAccount));
            request.setBdyDebitAmount(transferAmount);
            request.setBdyCreditAccount(Long.parseLong(noRekTujuan));
            request.setBdyCreditAmount(transferAmount);
            request.setBdyValueDate(sValueDate);
            request.setBdyChargesAmount(BigDecimal.ZERO);
            request.setBdyConvertedChargesAmount(BigDecimal.ZERO);

            request.setBdyDefaultCurrencyCode(defaultCurrency);
            request.setBdyDebitCurrency(debitCurrency);
            request.setBdyCreditCurrency(creditCurrency);
            request.setBdyChargesCurrency(chargesCurrency);
            
            request.setBdyIbtBuyRate(BigDecimal.ONE);
            request.setBdyIbtSellRate(BigDecimal.ONE);
            request.setBdyTtBuyRate(BigDecimal.ONE);
            request.setBdyTtSellRate(BigDecimal.ONE);
            
            
            
            String trxRefNo = transfer.getTransactionNumber();
            int diff2 = trxRefNo.length() - 8;
            
            if (settleSeq.trim().length() == 0) {
                remark3 = transfer.getType().getId().toString() + "/" + noRekTujuan + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + 
                		transfer.getFrom().getOwnerName();
                final String remark1 = transfer.getFrom().getOwnerName() + "/" +trxRefNo;
                request.setBdyRemark1(remark1);
                
            } else {
                remark3 = transfer.getType().getId().toString() + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + settleSeq;
                request.setBdyRemark1(transfer.getDescription());
            }
            
            request.setBdyRemark3(remark3);
            
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.extIdlength");
            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
  
            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.reqSecurityHeader"));
          
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(transfer.getActualDate().getTime());
                        
            RequestHeader header = new RequestHeader();
            header.setHdrExternalId(extId);
            header.setHdrTellerId(Long.parseLong(tellerId));
        	header.setHdrJournalSequence(Long.parseLong(journalSeq));
            
            if (tranCodeCustom != null) {
                header.setHdrTransactionCode(tranCodeCustom);
            } else {
                header.setHdrTransactionCode(tranCode);
            }
            
            header.setHdrChannelId(channelId);
            header.setHdrTimestamp(sTimestamp);
                        
            if (reqSecHeader) {
                
                StringBuffer oriDigestKey = new StringBuffer();
                oriDigestKey.append(header.getHdrExternalId());
                oriDigestKey.append(header.getHdrChannelId());
                oriDigestKey.append(request.getBdyDebitAccount());
                oriDigestKey.append(request.getBdyCreditAccount());
                oriDigestKey.append(request.getBdyCreditAmount());
                oriDigestKey.append(sValueDate);
                
                final String digestedKey = securityUtil.getSignature(oriDigestKey.toString());
                
                SecurityHeader securityHeader = new SecurityHeader();
                securityHeader.setHdrResponseToken(digestedKey);
                securityHeader.setHdrChallengeType("1");
                securityHeader.setHdrAppliNo("1");
                securityHeader.setHdrChallenge1("1");
                securityHeader.setHdrCorporateId("1");
                securityHeader.setHdrCustomerAdditionalId("");
                
                request.setSecurityHeader(securityHeader);
            }
            
            transfer.setTraceData(extId);
            super.onTransferInserted(transfer);

            request.setHeader(header);
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
        try{
	        response = coreWSFactory.getFundTransferWebService().transfer(request);
	        
	        String responseMsg = "";
	        StringBuffer rekonKey = new StringBuffer();
	        
	        if (response != null) {
	            if (response.getResponseHeader() != null) {
	                switch (response.getResponseHeader().getHdrResponseCode()){
	                    case 1: 
	                    	logger.debug("responseCode : " + response.getResponseHeader().getHdrResponseCode());
	                    	rekonKey.append(" ");
	                        rekonKey.append("|");
	                        rekonKey.append(remark3);
	                        rekonKey.append("|");
	                        rekonKey.append(settleSeq);
	                        transfer.setTraceData(rekonKey.toString());
	                        transfer.setDescription(transfer.getDescription() + ".SUCCESS");
	                        super.onTransferInserted(transfer);
	                        break;
	                    default:
	                    	logger.debug("responseCode : " + response.getResponseHeader().getHdrResponseCode());
	                        responseMsg = response.getResponseHeader().getHdrResponseMessage();
	                        final String errorCode = response.getResponseHeader().getHdrErrorNumber().trim();
	                        logger.debug("hdrErrorNumber : " + errorCode);
	                        if(isHaveToReversal(errorCode)){
	                        	 transfer.setDescription("Ref:" + transfer.getTransactionNumber() + ".REVERSAL");
	                             super.onTransferInserted(transfer);
	                        }else{
		                    	rekonKey.append(" ");
		                        rekonKey.append("|");
		                        rekonKey.append(remark3);
		                        rekonKey.append("|");
		                        rekonKey.append(settleSeq);
		                        transfer.setTraceData(rekonKey.toString());
		                        String errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + (errorCode!=null ? errorCode : ""));
	                        	transfer.setDescription("Ref:" + transfer.getTransactionNumber() + ".FAILED =" + errorMsg);
	                            super.onTransferInserted(transfer);
	                        }
	                }
	            } else {
	                logger.error("Response Header is empty.");
	            }
	        } else {
	            logger.error("Response is empty.");
	        }
        }catch (final Exception e) {
        	logger.error(e);
        	transfer.setDescription("Ref:" + transfer.getTransactionNumber() + ".REVERSAL");
            super.onTransferInserted(transfer);
        }
        
    }
    
    @Override
    public void onTransferProcessed(final Transfer transfer) {
    	String desc = transfer.getDescription();
    	if(desc.contains(".SUCCESS")){
    		super.onTransferInserted(transfer);
    	} else if(desc.contains(".FAILED")) {
    		ChargebackResult result = paymentWebServiceImpl.doCustomChargeback(transfer);
			if (result.getStatus().equals(ChargebackStatus.SUCCESS)){
				transfer.setDescription(transfer.getDescription().replace(".FAILED",".FAILED_CB"));
                super.onTransferInserted(transfer);
			}
    	}else if(desc.substring(desc.length()-9).equals(".REVERSAL")){

    		FundTransferMsgRequestExtended request = new FundTransferMsgRequestExtended();
            FundTransferMsgResponse response;
    		try {
            	final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
            	
                final String norekInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
                final PaymentCustomFieldValue noRekCustomField = customFieldHelper.getValue(norekInternalName, customValues);
                final String noRekTujuan = noRekCustomField.getValue();
                
                final String jSeqInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer2.journalSequence.internalName");
                final PaymentCustomFieldValue jSeqCustomField = customFieldHelper.getValue(jSeqInternalName, customValues);
                String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.journalSequence");
                if (jSeqCustomField != null) {
                	journalSeq = jSeqCustomField.getValue();
                }
                
                final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.glaccount");
                final String defaultCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.defaultCurrencyCode");
                final String debitCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.debitCurrency");
                final String creditCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.creditCurrency");
                final String chargesCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.chargesCurrency");
                final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.tellerId");
                final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.transactionCode");
                final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.channelId");
                final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
                final String valueDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.valuedate_format");
                final String settleSeqCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
                final String tranCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.trancode_prefix");
                
                final BigDecimal transferAmount = transfer.getAmount();
                
                
                final PaymentCustomFieldValue settleSeqCustomField = customFieldHelper.getValue(settleSeqCustomFieldName, customValues);
                String settleSeq = " "; 
                if (settleSeqCustomField != null) {
                    settleSeq = settleSeqCustomField.getValue();
                }
                
                final String tranCodeCustom = getCustomLabel(tranCodePrefix, transfer);
                
                final SimpleDateFormat sdfValueDate = new SimpleDateFormat(valueDateFormat);
                String sValueDate = sdfValueDate.format(transfer.getProcessDate().getTime());
                
                request.setBdyDebitAccount(new BigDecimal(debitAccount));
                request.setBdyDebitAmount(transferAmount);
                request.setBdyCreditAccount(Long.parseLong(noRekTujuan));
                request.setBdyCreditAmount(transferAmount);
                request.setBdyValueDate(sValueDate);
                request.setBdyChargesAmount(BigDecimal.ZERO);
                request.setBdyConvertedChargesAmount(BigDecimal.ZERO);

                request.setBdyDefaultCurrencyCode(defaultCurrency);
                request.setBdyDebitCurrency(debitCurrency);
                request.setBdyCreditCurrency(creditCurrency);
                request.setBdyChargesCurrency(chargesCurrency);
                
                request.setBdyIbtBuyRate(BigDecimal.ONE);
                request.setBdyIbtSellRate(BigDecimal.ONE);
                request.setBdyTtBuyRate(BigDecimal.ONE);
                request.setBdyTtSellRate(BigDecimal.ONE);
                
                // Rekonsiliasi
                String remark3 = "";
                
                String trxRefNo = transfer.getTransactionNumber();
                int diff2 = trxRefNo.length() - 8;
                
                if (settleSeq.trim().length() == 0) {
                    remark3 = transfer.getType().getId().toString() + "/" + noRekTujuan + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + 
                    		transfer.getFrom().getOwnerName();
                    final String remark1 = transfer.getFrom().getOwnerName() + "/" +trxRefNo;
                    request.setBdyRemark1(remark1);
                    
                } else {
                    remark3 = transfer.getType().getId().toString() + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + settleSeq;
                    request.setBdyRemark1(transfer.getDescription());
                }
                
                request.setBdyRemark3(remark3);
                
//                final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.extIdlength");
//                final String hdrExtId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
                
                String extId = transfer.getTraceData();
              
                final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
                final String sTimestamp = sdfTimeStamp.format(transfer.getActualDate().getTime());
                            
                RequestHeader header = new RequestHeader();
                header.setHdrExternalId(extId);
                header.setHdrTellerId(Long.parseLong(tellerId));
            	header.setHdrJournalSequence(Long.parseLong(journalSeq));
                
                if (tranCodeCustom != null) {
                    header.setHdrTransactionCode(tranCodeCustom);
                } else {
                    header.setHdrTransactionCode(tranCode);
                            }
                
                header.setHdrChannelId(channelId);
                header.setHdrTimestamp(sTimestamp);

                request.setHeader(header);

                request.setBdyOriginalExternalId(extId);
                StringBuffer rekonKey = new StringBuffer();
            	rekonKey.append(" ");
                rekonKey.append("|");
                rekonKey.append(remark3);
                rekonKey.append("|");
                rekonKey.append(settleSeq);
                transfer.setTraceData(rekonKey.toString());
                
                response = coreWSFactory.getReversalFundTransferWebService().reverse(request);
                
                String responseMsg = "";
                
                if (response != null) {
                    if (response.getResponseHeader() != null) {
                        switch (response.getResponseHeader().getHdrResponseCode()){
                            case 1: 
                            	transfer.setDescription(transfer.getDescription().replace(".REVERSAL", ".REVERSAL_OK"));
                                super.onTransferInserted(transfer);
                                
                    			ChargebackResult result = paymentWebServiceImpl.doCustomChargeback(transfer);
                    			if (result.getStatus().equals(ChargebackStatus.SUCCESS)){
                    				transfer.setDescription(transfer.getDescription().replace(".REVERSAL_OK",".REVERSAL_CB"));
                                    super.onTransferInserted(transfer);
                    			}
                                break;
                            default:
                                transfer.setDescription(transfer.getDescription().replace(".REVERSAL", ".REVERSAL_NOTOK"));
                                super.onTransferInserted(transfer);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
    	}
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    private String getCustomLabel(final String internalName, final Transfer transfer) {
        final Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    public boolean isHaveToReversal(String errorNumberParam){
    	boolean result = false;
    	try {
			String listReversalErrorNumbers = EmoneyConfiguration.getEmoneyProperties().getProperty("overbooking.soa.reversal.list.error.number");
			String [] arrayReversalErrorNumbers = listReversalErrorNumbers.split(",");
			for(String errorNumber : arrayReversalErrorNumbers){
				if(errorNumberParam.equals(errorNumber)){
					result = true;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
    	
    	
    	return result;
    }
}
