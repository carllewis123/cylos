/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;


import java.io.IOException;
import java.net.URLConnection;

import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

@SuppressWarnings("unused")
public class OneToMany2EmoneyListener extends TransferListenerAdapter {
    
    private CustomFieldHelper customFieldHelper;
    private FundTransferWebService wsClient;
    private CoreWebServiceFactory coreWSFactory;
    private URLConnection 			  m_client;
    
    private final Log               logger    = LogFactory.getLog(OneToMany2EmoneyListener.class);
    
    
    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }
        
    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        // TODO Auto-generated method stub
        FundTransferMsgRequest request = new FundTransferMsgRequest();
        FundTransferMsgResponse response;
        String errorMessage = "";
        String aliasName = "";
        try {
        	MemberAccount memberAccount = (MemberAccount) transfer.getTo();
            Member member = customFieldHelper.loadMember(memberAccount.getMember().getId());
        	
            if(groupCanReceivedTransfer(member.getGroup().getId().toString(), "onetomany.agent.group.can.receive.transfer")){
            	super.onTransferInserted(transfer);
            }else{
            	errorMessage = EmoneyConfiguration.getEmoneyProperties().getProperty("onetomany.error.message.group.can.receive.transfer");
            	logger.info(errorMessage);
            	throw new ExternalException(errorMessage);
            }
            
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
//        catch (final SocketTimeoutException e) {
//            logger.error(e);
//            throw new ExternalException("STE");
//        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
    }
    
    public boolean groupCanReceivedTransfer(String groupId, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(groupId)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(groupId)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
}
