package ptdam.emoney.service.client.external;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.transactions.exceptions.MaxTopupCCPerMonthExceededException;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;

public class PaymentTransferListenerFirst extends TransferListenerAdapter{

    private final Log               logger    = LogFactory.getLog(PaymentTransferListenerFirst.class);
    
    @SuppressWarnings("unused")
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
    	String remark3="";
    	
        try {
        	remark3=transfer.getDescription();
        	
        	
        	StringBuffer rekonKey = new StringBuffer();
        	rekonKey.append(" ");
        	rekonKey.append("|");
        	rekonKey.append(remark3);
        	rekonKey.append("|");
             
        	transfer.setTraceData(rekonKey.toString());
        	
            super.onTransferInserted(transfer);
        	
        }catch (final MaxTopupCCPerMonthExceededException e) {
        	logger.error(e);
        	throw e;  }
   }   
    
  
    }


