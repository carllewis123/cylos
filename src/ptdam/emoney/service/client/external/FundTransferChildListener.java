/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.PrivateKey;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.settings.SettingsServiceLocal;

import javax.xml.ws.WebServiceException;

import org.apache.catalina.ha.backend.CollectedInfo;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.ws.security.sts.provider.model.secext.EncodedString;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.AccountDetail;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.transactions.CashOutWebService;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.Account;
import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.conversion.CalendarConverter;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

@SuppressWarnings("unused")
public class FundTransferChildListener extends TransferListenerAdapter {
    
//    @Autowiredn
    private SecurityUtil securityUtil;
    
    private final Log               logger    = LogFactory.getLog(FundTransferChildListener.class);
    
    public void setSecurityUtil(SecurityUtil securityUtil) {
        this.securityUtil = securityUtil;
    }

    public void setSettingsServiceLocal(final SettingsServiceLocal settingsService) {
        this.settingsService = settingsService;
      }
    
    private CustomFieldHelper customFieldHelper;
    private FundTransferWebService wsClient;
    private CoreWebServiceFactory coreWSFactory;
    private SettingsServiceLocal settingsService;
    private AccountServiceLocal               accountService;
    
    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }
        

    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        // TODO Auto-generated method stub
        //super.onTransferInserted(transfer);
    	Transfer transferParent = transfer.getParent();
  
        System.out.println("transferParent : " + transferParent);
        System.out.println("transfer : " + transfer);
        
        try {
                
        	FundTransferMsgRequest request = new FundTransferMsgRequest();
        	
            final String norekInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");

//            final Collection<PaymentCustomFieldValue> noRekCustomFields =  transfer.getCustomValues();
            final Collection<PaymentCustomFieldValue> noRekCustomFields =  transfer.getParent().getCustomValues();
            final PaymentCustomFieldValue noRekCustomField = customFieldHelper.getValue(norekInternalName, noRekCustomFields);
            final String noRekTujuan = noRekCustomField.getValue();
//            final BigDecimal transferAmount = transfer.getAmount();
            final BigDecimal transferAmount = transfer.getParent().getAmount();
            
            final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.glaccount");
            final String defaultCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.defaultCurrencyCode");
            final String debitCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.debitCurrency"); 
            final String creditCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.creditCurrency");
            final String chargesCurrency = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.chargesCurrency");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.tellerId");
//            final String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.journalSequence");
            Date now = new Date(); 
    		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
    		String dateNow = sdf.format(now);
    		Random random = new Random();
            final String journalSeq = dateNow + random.nextInt(10); 
            final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.transactionCode");
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.channelId");
            final String timeStampFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.timestamp_format");
            final String valueDateFormat = EmoneyConfiguration.getEmoneyProperties().getProperty("core.ubpc.valuedate_format");
            final String settleSeqCustomFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
            final String tranCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.trancode_prefix");
            
            final String trftypeid = EmoneyConfiguration.getEmoneyProperties().getProperty("ussd.transferID."+transferParent.getType().getId());
            
            final String customFromInternalName = EmoneyConfiguration.getEmoneyProperties().getProperty(trftypeid+".custom_field_name2");
            this.logger.info("(FundTransferChildListener) customFromInternalName : " + customFromInternalName);

            
            final PaymentCustomFieldValue settleSeqCustomField = customFieldHelper.getValue(settleSeqCustomFieldName, noRekCustomFields);
            Collection customFromCustomFields = transfer.getParent().getCustomValues();
            PaymentCustomFieldValue customFromCustomField = (PaymentCustomFieldValue)this.customFieldHelper.getValue(customFromInternalName, noRekCustomFields);
            String customFrom = customFromCustomField.getValue();
            this.logger.info("(FundTransferChildListener) custom from : " + customFrom);
            String settleSeq = " "; 
            
            if (settleSeqCustomField != null) {
                settleSeq = settleSeqCustomField.getValue();
            }
            
            final String tranCodeCustom = getCustomLabel(tranCodePrefix, transferParent);
            
            final SimpleDateFormat sdfValueDate = new SimpleDateFormat(valueDateFormat);
            String sValueDate = sdfValueDate.format(transfer.getParent().getProcessDate().getTime());
            
            request.setBdyDebitAccount(new BigDecimal(debitAccount));
            request.setBdyDebitAmount(transferAmount);
            request.setBdyCreditAccount(Long.parseLong(noRekTujuan));
            request.setBdyCreditAmount(transferAmount);
            request.setBdyValueDate(sValueDate);
            request.setBdyChargesAmount(BigDecimal.ZERO);
            request.setBdyConvertedChargesAmount(BigDecimal.ZERO);

            request.setBdyDefaultCurrencyCode(defaultCurrency);
            request.setBdyDebitCurrency(debitCurrency);
            request.setBdyCreditCurrency(creditCurrency);
            request.setBdyChargesCurrency(chargesCurrency);
            
            request.setBdyIbtBuyRate(BigDecimal.ONE);
            request.setBdyIbtSellRate(BigDecimal.ONE);
            request.setBdyTtBuyRate(BigDecimal.ONE);
            request.setBdyTtSellRate(BigDecimal.ONE);
//          request.setBdyRemark1(transfer.getDescription());
            
            // Rekonsiliasi
            String remark3 = "";
            
            String trxRefNo = transfer.getParent().getTransactionNumber();
            int diff2 = trxRefNo.length() - 13;
            
            if (settleSeq.trim().length() == 0) {
//                remark3 = transfer.getType().getId().toString() + "/" + transfer.getFrom().getOwnerName() + "/" + trxRefNo.substring(diff2, trxRefNo.length());
                final String remark1Template = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.remark1template");
                remark3 = transfer.getParent().getType().getId().toString() + "/" + noRekTujuan + "/" + trxRefNo.substring(diff2, trxRefNo.length());
                final String remark1 = remark1Template.replace("{0}", transfer.getParent().getFrom().getOwnerName());
                request.setBdyRemark1(remark1);
                
            } else {
                remark3 = transfer.getParent().getType().getId().toString() + "/" + trxRefNo.substring(diff2, trxRefNo.length()) + "/" + settleSeq;
                request.setBdyRemark1(transfer.getParent().getDescription());
            }
            
            request.setBdyRemark3(remark3);
            
            final String extIdLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.extIdlength");
            final String extId = RandomStringUtils.randomNumeric(Integer.parseInt(extIdLength));
            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.reqSecurityHeader"));
          
            final SimpleDateFormat sdfTimeStamp = new SimpleDateFormat(timeStampFormat);
            final String sTimestamp = sdfTimeStamp.format(transfer.getParent().getActualDate().getTime());
                        
            RequestHeader header = new RequestHeader();
            header.setHdrExternalId(extId);
            header.setHdrTellerId(Long.parseLong(tellerId));
            header.setHdrJournalSequence(Long.parseLong(journalSeq));
            
            if (tranCodeCustom != null) {
                header.setHdrTransactionCode(tranCodeCustom);
            } else {
                header.setHdrTransactionCode(tranCode);
            }
            
            header.setHdrChannelId(channelId);
            header.setHdrTimestamp(sTimestamp);
                        
            if (reqSecHeader) {
                
                StringBuffer oriDigestKey = new StringBuffer();
                oriDigestKey.append(header.getHdrExternalId());
                oriDigestKey.append(header.getHdrChannelId());
                oriDigestKey.append(request.getBdyDebitAccount());
                oriDigestKey.append(request.getBdyCreditAccount());
                oriDigestKey.append(request.getBdyCreditAmount());
                oriDigestKey.append(sValueDate);
                
                final String digestedKey = securityUtil.getSignature(oriDigestKey.toString());
                
                SecurityHeader securityHeader = new SecurityHeader();
                securityHeader.setHdrResponseToken(digestedKey);
                securityHeader.setHdrChallengeType("1");
                securityHeader.setHdrAppliNo("1");
                securityHeader.setHdrChallenge1("1");
                securityHeader.setHdrCorporateId("1");
                securityHeader.setHdrCustomerAdditionalId("");
                
                request.setSecurityHeader(securityHeader);
            }

            request.setHeader(header);
            System.out.println("AKAN MASUK KE REQUEST : " + request);
            
            FundTransferMsgResponse response = coreWSFactory.getFundTransferWebService().transfer(request);
            
            String responseMsg = "";
            String errorCode = "";
            System.out.println("response : FundTransferChildListener.java " + response);
            
            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()){
                        case 1: 
                            final StringBuffer rekonKey = new StringBuffer();
                            rekonKey.append(" ");
                            rekonKey.append("|");
                            rekonKey.append(remark3);
                            rekonKey.append("|");
                            rekonKey.append(settleSeq);
                            
                            logger.info("RESPONSE CODE 1");
                            
                            transfer.getParent().setTraceData(rekonKey.toString());
                            super.onTransferInserted(transfer);
                            
                            if (checkTransferTypeCode(transferParent.getType().getId().toString(), "need.notif.invoke.fromMember")) {
                            	            
                                String DescInternalName = 
                                        EmoneyConfiguration.getEmoneyProperties().getProperty("b1.descInternalName");
                                      this.logger.info("DescInternalName" + DescInternalName);

                                      final Collection<PaymentCustomFieldValue> noTujuanCustomFields =  transfer.getCustomValues();

                                      final Collection<PaymentCustomField> customFields = transfer
                          					.getType().getLinkedCustomFields();

                                      String getdesc = null;
                                      for (PaymentCustomField customField : customFields) {
                                        if ((!customField.getInternalName().contains(DescInternalName)) || 
                                          (!customField.isEnabled())) continue;
                                        customField.getDescription();
                                        getdesc = customField.getDescription();
                                        this.logger.info("desc:" + getdesc);
                                      }
                                      this.logger.info("desc SMS :" + getdesc);

                                      this.logger.info("transferParent.getAmount() :" + transferParent.getAmount());
               
                                      String formattedAmount = "Rp. " + transferAmount;
                                      MemberAccount From = (MemberAccount)transferParent.getFrom();
                                      final LocalSettings localSettings = settingsService.getLocalSettings();

                                      final AccountStatus fromStatus = accountService.getCurrentStatus(new AccountDTO(From));
                                      final BigDecimal availableBalance = localSettings.round(fromStatus.getAvailableBalance());
                                      
                                      DecimalFormat formatted = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                                      DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

                                      formatRp.setCurrencySymbol("Rp. ");
                                      formatRp.setMonetaryDecimalSeparator(',');
                                      formatRp.setGroupingSeparator('.');

                                      formatted.setDecimalFormatSymbols(formatRp);
                                      
                                      String formattedBalance = formatted.format(availableBalance);
                                      System.out.println("getChildren :" + From.getMember().getName());
                                      String name = From.getMember().getName();
                                      if (name.length() > 15) {
                                        name = name.substring(0, 14);
                                      }
                                      System.out.println(trxRefNo);
                                      System.out.println(transferAmount);
                                      getdesc = getdesc
                                        .replace("#amount#", formattedAmount)
                                        .replace("#name#", From.getMember().getName())
                                        .replace("#balance#", formattedBalance)
                                        .replace("#transaction_number#", trxRefNo);
                                      this.logger.info("processed desc:" + getdesc);
                                      SendSMS(customFrom, getdesc);
                               
							}
                
                            break;
                        default:
                            logger.error("response Code : " + response.getResponseHeader().getHdrResponseCode());
                            responseMsg = response.getResponseHeader().getHdrResponseMessage();
                            errorCode = response.getResponseHeader().getHdrErrorNumber().trim();
                            
                            String mappingErrorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + errorCode);
                            if (mappingErrorMsg == null) {
                                mappingErrorMsg = responseMsg;
                            }
                            logger.error("Host Error (" + errorCode + ") : " + mappingErrorMsg);
                            
                            if(errorCode == null || isHaveToDebet(errorCode)){
                            	logger.info("Error Code : " + errorCode + " - " + mappingErrorMsg);
                            	throw new Exception(errorCode);
                            }
                        	logger.info("Throw ExternalException");
                        	logger.info("errorCode : " + errorCode);
                            logger.info("mappingErrorMsg : " + mappingErrorMsg);
                            errorCode = responseMsg;
                            logger.info("errorCode setelah disamakan dengan mappingErrorMessage : " + errorCode);
                            throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                	logger.info("Throw Exception(RHIE)");
                    throw new Exception("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("STE");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
    }

    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    private String getCustomLabel(final String internalName, final Transfer transfer) {
        final Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountService = accountService;
    }
    
    public boolean isHaveToDebet(String errorNumberParam){
    	boolean result = false;
    	try {
			String listDebetErrorNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("list.error.number.biller.debet.deposit.settlement");
			String [] arrayDebetErrorNumbers = listDebetErrorNumber.split(",");
			for(String errorNumber : arrayDebetErrorNumbers){
				if(errorNumberParam.equals(errorNumber)){
					result = true;
				}
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
    	
    	
    	return result;
    }
    
    public boolean checkTransferTypeCode(String transferTypeCode, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(transferTypeCode)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(transferTypeCode)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
    private void SendSMS(String phone, String message) throws IOException {
        String url = EmoneyConfiguration.getEmoneyProperties().getProperty("url.sms");
        String replaceNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("phone.prefix.replace");
        try
        {
          phone = phone.replaceFirst(phone.substring(0, 1), replaceNumber);
          url = url.replace("{0}", phone);
          url = url.replace("{1}", URLEncoder.encode(message, "UTF-8"));

          URL obj = new URL(url);
          HttpURLConnection conn = (HttpURLConnection)obj.openConnection();

          conn.setRequestMethod("GET");
          int responseCode = conn.getResponseCode();
          System.out.println("\nSending 'GET' request to URL : " + url);
          System.out.println("Response Code : " + responseCode);

          BufferedReader in = new BufferedReader(
            new InputStreamReader(conn.getInputStream()));

          StringBuffer response = new StringBuffer();
          String inputLine;
          while ((inputLine = in.readLine()) != null)
          {
            response.append(inputLine);
          }
          in.close();

          System.out.println(response.toString());
        }
        catch (MalformedURLException e) {
          this.logger.error(e);
        }
        catch (IOException e) {
          this.logger.error(e);
        }
      }
    }