/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.service.client.external;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import antlr.StringUtils;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.StringUtil;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.TelcoBit126;
import ptdam.emoney.webservice.client.TelcoDSPMWHeader;
import ptdam.emoney.webservice.client.TelcoDSPSocketHeader;
import ptdam.emoney.webservice.client.TelcoPaymentWebService;
import ptdam.emoney.webservice.client.TelcoRequestMessage;
import ptdam.emoney.webservice.client.TelcoResponseMessage;

import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldPossibleValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;

import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unused")
public class TelcoPaymentListener extends TransferListenerAdapter {
    private final Log               logger    = LogFactory.getLog(TelcoPaymentListener.class);
    private CustomFieldHelper customFieldHelper;
    private CoreWebServiceFactory coreWSFactory;
    
    
    public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }

    private class TransactionListnener implements TransactionCommitListener, TransactionRollbackListener {

        @Override
        public void onTransactionRollback() {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onTransactionCommit() {
            // TODO Auto-generated method stub
            
        }
        
    }

    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }

    @Override
    public void onTransferInserted(Transfer transfer) throws ExternalException {
        // TODO Auto-generated method stub
    	TelcoRequestMessage request = new TelcoRequestMessage();;
    	Collection<PaymentCustomField> customFields = transfer.getType().getCustomFields();
    	String noPonselTujuan="";
        try {
            
            // PAN
            final String originMember = transfer.getFrom().getOwnerName();
            request.setBit_02_2(originMember);

            // Processing Code
            final String telcoProcPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.processing_code_prefix");
             
            final String processingCode = getCustomLabel(telcoProcPrefix, customFields);
            request.setBit_03(processingCode);
 
            final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
                        
            // Flagging Amount
            BigDecimal amount = transfer.getAmount();
            amount = amount.multiply(new BigDecimal(100));
            amount = amount.setScale(0, BigDecimal.ROUND_FLOOR);
            request.setBit_04(amount.toString());

            // System Trace Number
            final String traceNumber= RandomStringUtils.randomNumeric(5);
            request.setBit_11("0" + traceNumber);
            
            // System Time
            Calendar now = Calendar.getInstance();
            final SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
            final String nowTime = sdfTime.format(now.getTime());
            request.setBit_12(nowTime);
            
            // System Date
            final SimpleDateFormat sdfDate = new SimpleDateFormat("ddMM");
            final String nowDate = sdfDate.format(now.getTime());
            request.setBit_13(nowDate);
            
            // Terminal ID
            final String terminalCounterDigits = EmoneyConfiguration.getTerminalIDs().getProperty("core.telco.terminal.counter.digits");
            final String terminalIdCounter = RandomStringUtils.randomNumeric(Integer.parseInt(terminalCounterDigits)); 
            final String terminalId = EmoneyConfiguration.getTerminalIDs().getProperty("core.telco.terminal." + terminalIdCounter);
            request.setBit_41(terminalId);
            
            // Currency Code
            final String currencyCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.currencyCode");
            request.setBit_49(currencyCode);
            
            // PIN Block - not used
            request.setBit_52("");
            
            // Bit 61
            request.setBit_61_1("");
            // Debit Account Number
            //final String debitAccount = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.glaccount");
            request.setBit_61_2(EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.glaccount"));

            // Rekonsiliasi
            final String debitAccount = originMember;
            
            String bit_61_2_nohp = "";
            int diff = 13 - debitAccount.length();
            
            if (diff > 0) {
                bit_61_2_nohp = debitAccount + StringUtil.repeatString(" ", diff);
            } else {
                bit_61_2_nohp = debitAccount.substring(0, 13);
            }
            
            String trxRefNo = transfer.getTransactionNumber();
            int diff2 = trxRefNo.length() - 7;
            
            String bit_61_2_refno = trxRefNo.substring(diff2, trxRefNo.length());
            
//            request.setBit_61_2(bit_61_2_nohp + bit_61_2_refno);
            
            // No Ponsel Tujuan
            //final Collection<PaymentCustomFieldValue> customValues =  transfer.getCustomValues();
            final String noTujuanPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.notujuan_prefix");
            final PaymentCustomFieldValue noPonselTujuanCustomFieldValue = customFieldHelper.getValue(noTujuanPrefix + processingCode, customValues);
            noPonselTujuan = noPonselTujuanCustomFieldValue.getValue();
            String noPonselTujuanExt = "";
            request.setBit_61_3(noPonselTujuan);
            //request.setBit_61_3(bit_61_2_nohp + bit_61_2_refno);
            
            // Area Code (first 4 digits of No Ponsel Tujuan)
            String areaCode = "";
            if (noPonselTujuan != null && noPonselTujuan.length() > 0) {
                if (noPonselTujuan.length() >= 4) {
                    areaCode = noPonselTujuan.substring(0, 4);
                } else {
                    areaCode = noPonselTujuan;
                }
            }
            request.setBit_61_4(areaCode);
            
            
            // Phone Number Prefix
            final String telcoPhoneNoLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.phone_no_length");
            final String telcoPhoneCustomPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.phone_no_custom_prefix");
            final String telcoPhonePrefixDefaultLength = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.phone_prefix_default_length");
            
            int iTelcoPhoneNoLength = 0;
            if (telcoPhoneNoLength != null) {
                try {
                    iTelcoPhoneNoLength = Integer.parseInt(getCustomLabel(telcoPhoneNoLength, customFields));
                } catch (final NumberFormatException e) {
                    logger.info(e);
                }
            }
            
            int iTelcoPhonePrefixLength = 4;
            if (telcoPhonePrefixDefaultLength != null) {
                try {
                    iTelcoPhonePrefixLength = Integer.parseInt(telcoPhonePrefixDefaultLength);
                } catch (final NumberFormatException e) {
                    logger.info(e);
                }
            }
            
            if (iTelcoPhoneNoLength > 0) {
                
                if (noPonselTujuan.length() < iTelcoPhoneNoLength) {
                    int paddingLength = iTelcoPhoneNoLength - noPonselTujuan.length();
                    StringBuffer sb = new StringBuffer();
                    sb.append(noPonselTujuan.substring(0, iTelcoPhonePrefixLength));
                    sb.append(StringUtil.repeatString("0", paddingLength));
                    sb.append(noPonselTujuan.substring(iTelcoPhonePrefixLength));
                    noPonselTujuanExt = sb.toString();
                    
                } else {
                    noPonselTujuanExt = noPonselTujuan.substring(0, iTelcoPhoneNoLength);
                }
                
            } else {
                noPonselTujuanExt = noPonselTujuan;
            }
            
            // To Account Number
            request.setBit_61_5("");
            
            // RRNO
            request.setBit_61_6("");
            
            // Biller Code
            final String billerCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.biller_code_prefix");
            final String billerCode = getCustomLabel(billerCodePrefix, customFields);
            request.setBit_61_7(billerCode);
            
            // Debiting Amount
            request.setBit_61_8(request.getBit_04());
            
            // Crediting Amount
            request.setBit_61_9(request.getBit_04());
            
            // Commission Amount
            request.setBit_61_10("");
            
            
            // Bit 126
            request.setBit_126_1("");
            
            TelcoBit126 telcoBit126 = new TelcoBit126();
            
            // Token ID
            final String tokenID = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.tokenID");
            telcoBit126.setBit_126_1(tokenID);
            
            // Util Code
            final String utilCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.util_code_prefix");
            final String utilCode = getCustomLabel(utilCodePrefix, customFields);
            telcoBit126.setBit_126_2(utilCode);
            
            // Bill Phone Num
            telcoBit126.setBit_126_3(noPonselTujuanExt);
            
            // Prov Code
            final String provCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.prov_code");
            telcoBit126.setBit_126_4_1(provCode);
            
            // Package Code (for excelcom only)
            final String packageCodePrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.package_code_prefix");
            final String packageCode = getCustomLabel(packageCodePrefix, customFields);
            
            if (packageCode != null) {
                telcoBit126.setBit_126_4_2(packageCode);
            } else {
                telcoBit126.setBit_126_4_2("");
            }
            
            // Filler
            telcoBit126.setBit_126_4_3("");
            
            // Bill Cust Num
            telcoBit126.setBit_126_5("");
            
            // Bill Cust Nam
            telcoBit126.setBit_126_6("");
            
            // NPWP
            telcoBit126.setBit_126_7("");
            
            // Kandatel Num
            final String kandatelNum = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.kandatelnum");
            telcoBit126.setBit_126_8(kandatelNum);
            
            // Bill Status Code
            telcoBit126.setBit_126_9("");
            
            // Bill Ref Num 1, 2, 3
            telcoBit126.setBit_126_10("");
            telcoBit126.setBit_126_11("");
            telcoBit126.setBit_126_12("");
            
            // Bill Amount 1, 2, 3
            telcoBit126.setBit_126_13(amount.toString());
            telcoBit126.setBit_126_14("");
            telcoBit126.setBit_126_15("");
            
            // Bit 126.16
            telcoBit126.setBit_126_16_1("");
            telcoBit126.setBit_126_16_2("");
            telcoBit126.setBit_126_16_3("");
            telcoBit126.setBit_126_16_4("");
            telcoBit126.setBit_126_16_5("");
            telcoBit126.setBit_126_16_6("");
            telcoBit126.setBit_126_16_7("");
            telcoBit126.setBit_126_16_8("");
            telcoBit126.setBit_126_16_9("");
            
            request.setBit_126_2(telcoBit126);
            
            final TelcoResponseMessage response = coreWSFactory.getTelcoPaymentWebService().payment(request);
            final String successCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.success");

            String responseMsg = "";
            
            if (response != null) {
                if (response.getBit_39() != null) {
                    if (response.getBit_39().equals(successCode)) {
                        TelcoDSPMWHeader mwHeader = response.getTelcoMiddlewareHeader();
                        String text = "";
                        String serialVoucher = "";
                        String npwp = "";
                        
                        final String npwpPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.npwp_prefix");
                        
                        npwp = getCustomLabel(npwpPrefix, customFields);
                        
                        if (mwHeader != null) {
                            serialVoucher = noPonselTujuan + ". SN:" + mwHeader.getVersionNo();
                            text = serialVoucher;
                        }
                        
                        if (npwp != null) {
                            text = text  + "\r\n" + npwp;
                        }
                        
                        transfer.setDescription(text);
                        
                        final StringBuffer rekonKey = new StringBuffer();
                        rekonKey.append(" ");
                        rekonKey.append("|");
                        rekonKey.append(mwHeader.getVersionNo() + "/" + request.getBit_41() + "/" + request.getBit_11());
                        rekonKey.append("|");
                        rekonKey.append(" ");
                        
                        transfer.setTraceData(rekonKey.toString());
                        
                        super.onTransferInserted(transfer);
                    } else if(response.getBit_39().equals("RR")){
	                    String text = "";
	                    String serialVoucher = "";
	                    String npwp = "";
	                    try{
	        	            final String npwpPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.npwp_prefix");
	        	            
	        	            npwp = getCustomLabel(npwpPrefix, customFields);
	        	            
	        	                serialVoucher = noPonselTujuan + ". SN:";
	        	                text = serialVoucher;
	        	            
	        	            if (npwp != null) {
	        	                text = text  + "\r\n" + npwp;
	        	            }
	        	            
	        	            transfer.setDescription(text);
	        	            
	        	            final StringBuffer rekonKey = new StringBuffer();
	        	            rekonKey.append(" ");
	        	            rekonKey.append("|");
	        	            rekonKey.append("");
	        	            rekonKey.append("|");
	        	            rekonKey.append(" ");
	        	            
	        	            transfer.setTraceData(rekonKey.toString());
	        	            
	        	            super.onTransferInserted(transfer);
	                    }catch(Exception ex){
	                    	logger.error(ex.getMessage());
	                    	logger.error("response RR, exception");
	                    }
                    }else {
                        final String errorCode = response.getBit_39().trim();
                        responseMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.error_code." + errorCode);
                        logger.error("Host Error (" + errorCode + ") : " + responseMsg);
                        throw new ExternalException(errorCode);
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("RHIE");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("RIE");
            }
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            String text = "";
            String serialVoucher = "";
            String npwp = "";
            try{
	            final String npwpPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("core.telco.npwp_prefix");
	            
	            npwp = getCustomLabel(npwpPrefix, customFields);
	            
	                serialVoucher = noPonselTujuan + ". SN:";
	                text = serialVoucher;
	            
	            if (npwp != null) {
	                text = text  + "\r\n" + npwp;
	            }
	            
	            transfer.setDescription(text);
	            
	            final StringBuffer rekonKey = new StringBuffer();
	            rekonKey.append(" ");
	            rekonKey.append("|");
	            rekonKey.append("");
	            rekonKey.append("|");
	            rekonKey.append(" ");
	            
	            transfer.setTraceData(rekonKey.toString());
	            
	            super.onTransferInserted(transfer);
            }catch(Exception ex){
            	logger.error(e.getMessage());
            	logger.error("socket exception. exception when try to insert to transfer");
            }
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("GE");
        } 
        
    }
    
    private String getCustomLabel(final String internalName, final Collection<PaymentCustomField> customFields) {
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getName();
            }
        }
        return null;
    }
    
   
    private Collection<CustomFieldPossibleValue> getCustomPossibleValue(final String internalName, final Collection<PaymentCustomField> customFields) {
        
        for (PaymentCustomField customField: customFields) {
            if (customField.getInternalName().contains(internalName)) {
                return customField.getPossibleValues();
            }
        }
        return null;
    }
    
}
