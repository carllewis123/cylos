package ptdam.emoney.service.client.external;

/*
 This file is part of Cyclos <http://project.cyclos.org>

 Cyclos is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Cyclos is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import org.apache.catalina.ha.backend.CollectedInfo;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.ws.security.sts.provider.model.secext.EncodedString;
import org.springframework.beans.factory.annotation.Autowired;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;
import com.ptdam.emoney.webservices.utils.SecurityUtil;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.FundTransferMsgRequest;
import ptdam.emoney.webservice.client.FundTransferMsgResponse;
import ptdam.emoney.webservice.client.FundTransferWebService;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.transactions.CashOutWebService;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.accounts.MemberAccount;
import nl.strohalm.cyclos.entities.accounts.transactions.Payment;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferListenerAdapter;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomField;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.customization.PaymentCustomFieldServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.conversion.CalendarConverter;
import nl.strohalm.cyclos.utils.transaction.TransactionCommitListener;
import nl.strohalm.cyclos.utils.transaction.TransactionRollbackListener;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;
import nl.strohalm.cyclos.webservices.payments.ChargebackResult;
import nl.strohalm.cyclos.webservices.payments.ChargebackStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;

@SuppressWarnings("unused")
public class SendSmsListener extends TransferListenerAdapter {

	// @Autowired
	private SecurityUtil securityUtil;

	private final Log logger = LogFactory.getLog(SendSmsListener.class);

	public void setSecurityUtil(SecurityUtil securityUtil) {
		this.securityUtil = securityUtil;
	}

	private CustomFieldHelper customFieldHelper;
	private FundTransferWebService wsClient;
	private CoreWebServiceFactory coreWSFactory;
	private PaymentWebServiceImpl paymentWebServiceImpl;

	private class TransactionListnener implements TransactionCommitListener,
			TransactionRollbackListener {

		@Override
		public void onTransactionRollback() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTransactionCommit() {
			// TODO Auto-generated method stub

		}

	}

	public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
		this.coreWSFactory = coreWSFactory;
	}

	@Override
	public void onTransferInserted(Transfer transfer) throws ExternalException {
		FundTransferMsgRequest request = new FundTransferMsgRequest();
		FundTransferMsgResponse response;
		
		Transfer TransferParent = transfer.getParent();

		
		try {

			final BigDecimal transferAmount = TransferParent.getAmount();
			logger.info("amount" + transferAmount);
			String trxRefNo = transfer.getTransactionNumber();

			final String noTujuanInternalName = EmoneyConfiguration
					.getEmoneyProperties().getProperty("p62.custom_from");
			logger.info("norekfieldinternalname" + noTujuanInternalName);
			final String DescInternalName = EmoneyConfiguration
					.getEmoneyProperties().getProperty("p62.descInternalName");
			logger.info("DescInternalName" + DescInternalName);

			final Collection<PaymentCustomFieldValue> noRekCustomFields = transfer
					.getCustomValues();
			logger.info("getnoRekCustoFields");
			final PaymentCustomFieldValue noRekCustomField = customFieldHelper
					.getValue(noTujuanInternalName, noRekCustomFields);
			final String noTujuan = noRekCustomField.getValue();
			logger.info("noTujuan:"+noTujuan);
			final Collection<PaymentCustomField> customFields = transfer
					.getType().getLinkedCustomFields();

			String getdesc = null;
			for (PaymentCustomField customField : customFields) {
				if (customField.getInternalName().contains(DescInternalName)
						&& customField.isEnabled()) {
					customField.getDescription();
					getdesc = customField.getDescription();
					logger.info("desc:"+getdesc);
				}
			}

			MemberAccount From = (MemberAccount) TransferParent.getFrom();
			// a.getMember().getName();

			System.out.println("getChildren :" + From.getMember().getName());
			String name = From.getMember().getName();
			if(name.length()>15){
				name = name.substring(0, 14);
			}
			System.out.println(trxRefNo);
			System.out.println(transferAmount);
			getdesc = getdesc
					.replace("#amount#", transferAmount.toPlainString())
					.replace("#name#", From.getMember().getName())
					.replace("#transaction_number#", trxRefNo);
			logger.info("processed desc:"+getdesc);

			this.SendSMS(noTujuan, getdesc);

		} catch (final ExternalException e) {
			throw e;
		} catch (final Exception e) {
			logger.error(e);
			throw new ExternalException("GE");
		}

	}

	public void setCustomFieldHelper(CustomFieldHelper customFieldHelper) {
		this.customFieldHelper = customFieldHelper;
	}

	private String getCustomLabel(final String internalName,
			final Transfer transfer) {
		final Collection<PaymentCustomField> customFields = transfer.getType()
				.getCustomFields();

		for (PaymentCustomField customField : customFields) {
			if (customField.getInternalName().contains(internalName)) {
				return customField.getName();
			}
		}
		return null;
	}

	private void SendSMS(String phone, String message) throws IOException {
		String url = EmoneyConfiguration.getEmoneyProperties().getProperty("url.sms");
		URL obj;
		try {
			url = url.replace("{0}", phone);
			url = url.replace("{1}", URLEncoder.encode(message, "UTF-8"));

			obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

			conn.setRequestMethod("GET");
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
	}

}