/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class TelcoDSPMWHeader implements Serializable {

    private static final long serialVersionUID = 7024369976817789923L;

    private String headerLength;
    private String messageLength;
    private String versionNo;
    private String headerFormatID;
    private String dataFormatID;
    private String sourceID;
    private String destinationID;
    private String routingNo;
    private String messageStatus;
    private String bankIDNo;
    private String Node;
    private String exchangeID;
    private String scenarioNo;
    private String tranCode;
    private String retrieveRefNo;
    private String acquirerRefNo;
    private String transmissionNo;
    private String noOfRecordsToLoad;
    private String noOfErrorToLoad;
    private String userID;
    private String terminalID;
    private String supervisorID;
    private String moreRecordsIndicator;
    private String cutOffIndicator;
    private String userData;
    public String getHeaderLength() {
        return headerLength;
    }
    public void setHeaderLength(String headerLength) {
        this.headerLength = headerLength;
    }
    public String getMessageLength() {
        return messageLength;
    }
    public void setMessageLength(String messageLength) {
        this.messageLength = messageLength;
    }

    public String getVersionNo() {
        return versionNo;
    }
    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }
    public String getHeaderFormatID() {
        return headerFormatID;
    }
    public void setHeaderFormatID(String headerFormatID) {
        this.headerFormatID = headerFormatID;
    }
    public String getDataFormatID() {
        return dataFormatID;
    }
    public void setDataFormatID(String dataFormatID) {
        this.dataFormatID = dataFormatID;
    }
    public String getSourceID() {
        return sourceID;
    }
    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }
    public String getDestinationID() {
        return destinationID;
    }
    public void setDestinationID(String destinationID) {
        this.destinationID = destinationID;
    }
    public String getRoutingNo() {
        return routingNo;
    }
    public void setRoutingNo(String routingNo) {
        this.routingNo = routingNo;
    }
    public String getMessageStatus() {
        return messageStatus;
    }
    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }
    public String getBankIDNo() {
        return bankIDNo;
    }
    public void setBankIDNo(String bankIDNo) {
        this.bankIDNo = bankIDNo;
    }
    public String getNode() {
        return Node;
    }
    public void setNode(String node) {
        Node = node;
    }
    public String getExchangeID() {
        return exchangeID;
    }
    public void setExchangeID(String exchangeID) {
        this.exchangeID = exchangeID;
    }
    public String getScenarioNo() {
        return scenarioNo;
    }
    public void setScenarioNo(String scenarioNo) {
        this.scenarioNo = scenarioNo;
    }
    public String getTranCode() {
        return tranCode;
    }
    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }
    public String getRetrieveRefNo() {
        return retrieveRefNo;
    }
    public void setRetrieveRefNo(String retrieveRefNo) {
        this.retrieveRefNo = retrieveRefNo;
    }
    public String getAcquirerRefNo() {
        return acquirerRefNo;
    }
    public void setAcquirerRefNo(String acquirerRefNo) {
        this.acquirerRefNo = acquirerRefNo;
    }
    public String getTransmissionNo() {
        return transmissionNo;
    }
    public void setTransmissionNo(String transmissionNo) {
        this.transmissionNo = transmissionNo;
    }
    public String getNoOfRecordsToLoad() {
        return noOfRecordsToLoad;
    }
    public void setNoOfRecordsToLoad(String noOfRecordsToLoad) {
        this.noOfRecordsToLoad = noOfRecordsToLoad;
    }
    public String getNoOfErrorToLoad() {
        return noOfErrorToLoad;
    }
    public void setNoOfErrorToLoad(String noOfErrorToLoad) {
        this.noOfErrorToLoad = noOfErrorToLoad;
    }
    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }
    public String getTerminalID() {
        return terminalID;
    }
    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }
    public String getSupervisorID() {
        return supervisorID;
    }
    public void setSupervisorID(String supervisorID) {
        this.supervisorID = supervisorID;
    }
    public String getMoreRecordsIndicator() {
        return moreRecordsIndicator;
    }
    public void setMoreRecordsIndicator(String moreRecordsIndicator) {
        this.moreRecordsIndicator = moreRecordsIndicator;
    }
    public String getCutOffIndicator() {
        return cutOffIndicator;
    }
    public void setCutOffIndicator(String cutOffIndicator) {
        this.cutOffIndicator = cutOffIndicator;
    }
    public String getUserData() {
        return userData;
    }
    public void setUserData(String userData) {
        this.userData = userData;
    }
    
}
