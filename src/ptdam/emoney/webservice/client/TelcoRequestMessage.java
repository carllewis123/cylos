/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class TelcoRequestMessage implements Serializable {

    private static final long serialVersionUID = -7952224687077153409L;

    private TelcoDSPSocketHeader telcoSocketHeader;
    private TelcoDSPMWHeader telcoMiddlewareHeader;
    private String messageType;
    private String primaryBitmap;
    private String secondaryBitmap;
    
    // Field Length of PAN
    private String bit_03_1;
    
    // PAN Number
    private String bit_02_2;
    
    // Processing Code
    private String bit_03;
    
    // Flagging Amount
    private String bit_04;
    
    // System Trace Number
    private String bit_11;
    
    // Time
    private String bit_12;
    
    // Date
    private String bit_13;
    
    // Terminal ID
    private String bit_41;
    
    // Currency Code
    private String bit_49;
    
    // PIN Block
    private String bit_52;
    
    // Field Length
    private String bit_61_1;
    
    // From account number
    private String bit_61_2;
    
    // MSISDN Number
    private String bit_61_3;
    
    // Area Code
    private String bit_61_4;
    
    // To account number
    private String bit_61_5;
    
    // RRNO
    private String bit_61_6;
    
    // Biller Code
    private String bit_61_7;
    
    public String getBit_126_1() {
        return bit_126_1;
    }

    public void setBit_126_1(String bit_126_1) {
        this.bit_126_1 = bit_126_1;
    }

    public TelcoBit126 getBit_126_2() {
        return bit_126_2;
    }

    public void setBit_126_2(TelcoBit126 bit_126_2) {
        this.bit_126_2 = bit_126_2;
    }

    // Debiting Amount
    private String bit_61_8;
    
    // Crediting Amount
    private String bit_61_9;
    
    // Commission Amount
    private String bit_61_10;

    // Field Length
    private String bit_126_1;
    private TelcoBit126 bit_126_2;

    public String getBit_02_2() {
        return bit_02_2;
    }

    public void setBit_02_2(String bit_02_2) {
        this.bit_02_2 = bit_02_2;
    }

    public String getBit_03() {
        return bit_03;
    }

    public void setBit_03(String bit_03) {
        this.bit_03 = bit_03;
    }

    public String getBit_04() {
        return bit_04;
    }

    public void setBit_04(String bit_04) {
        this.bit_04 = bit_04;
    }

    public String getBit_11() {
        return bit_11;
    }

    public void setBit_11(String bit_11) {
        this.bit_11 = bit_11;
    }

    public String getBit_12() {
        return bit_12;
    }

    public void setBit_12(String bit_12) {
        this.bit_12 = bit_12;
    }

    public String getBit_13() {
        return bit_13;
    }

    public void setBit_13(String bit_13) {
        this.bit_13 = bit_13;
    }

    public String getBit_41() {
        return bit_41;
    }

    public void setBit_41(String bit_41) {
        this.bit_41 = bit_41;
    }

    public String getBit_49() {
        return bit_49;
    }

    public void setBit_49(String bit_49) {
        this.bit_49 = bit_49;
    }

    public String getBit_52() {
        return bit_52;
    }

    public void setBit_52(String bit_52) {
        this.bit_52 = bit_52;
    }

    public String getBit_61_2() {
        return bit_61_2;
    }

    public void setBit_61_2(String bit_61_2) {
        this.bit_61_2 = bit_61_2;
    }

    public String getBit_61_3() {
        return bit_61_3;
    }

    public void setBit_61_3(String bit_61_3) {
        this.bit_61_3 = bit_61_3;
    }

    public String getBit_61_4() {
        return bit_61_4;
    }

    public void setBit_61_4(String bit_61_4) {
        this.bit_61_4 = bit_61_4;
    }

    public String getBit_61_5() {
        return bit_61_5;
    }

    public void setBit_61_5(String bit_61_5) {
        this.bit_61_5 = bit_61_5;
    }

    public String getBit_61_6() {
        return bit_61_6;
    }

    public void setBit_61_6(String bit_61_6) {
        this.bit_61_6 = bit_61_6;
    }

    public String getBit_61_7() {
        return bit_61_7;
    }

    public void setBit_61_7(String bit_61_7) {
        this.bit_61_7 = bit_61_7;
    }

    public String getBit_61_8() {
        return bit_61_8;
    }

    public void setBit_61_8(String bit_61_8) {
        this.bit_61_8 = bit_61_8;
    }

    public String getBit_61_9() {
        return bit_61_9;
    }

    public void setBit_61_9(String bit_61_9) {
        this.bit_61_9 = bit_61_9;
    }

    public String getBit_61_10() {
        return bit_61_10;
    }

    public void setBit_61_10(String bit_61_10) {
        this.bit_61_10 = bit_61_10;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getPrimaryBitmap() {
        return primaryBitmap;
    }

    public void setPrimaryBitmap(String primaryBitmap) {
        this.primaryBitmap = primaryBitmap;
    }

    public String getSecondaryBitmap() {
        return secondaryBitmap;
    }

    public void setSecondaryBitmap(String secondaryBitmap) {
        this.secondaryBitmap = secondaryBitmap;
    }

    public String getBit_03_1() {
        return bit_03_1;
    }

    public void setBit_03_1(String bit_03_1) {
        this.bit_03_1 = bit_03_1;
    }

    public String getBit_61_1() {
        return bit_61_1;
    }

    public void setBit_61_1(String bit_61_1) {
        this.bit_61_1 = bit_61_1;
    }

    public TelcoDSPSocketHeader getTelcoSocketHeader() {
        return telcoSocketHeader;
    }

    public void setTelcoSocketHeader(TelcoDSPSocketHeader telcoSocketHeader) {
        this.telcoSocketHeader = telcoSocketHeader;
    }

    public TelcoDSPMWHeader getTelcoMiddlewareHeader() {
        return telcoMiddlewareHeader;
    }

    public void setTelcoMiddlewareHeader(TelcoDSPMWHeader telcoMiddlewareHeader) {
        this.telcoMiddlewareHeader = telcoMiddlewareHeader;
    }
    
}
