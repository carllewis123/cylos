/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AccountDetail implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;
    
    private static final long serialVersionUID = 5592720338648421916L;
    private String branchNumber;
    private String CIFNumber;
    private String accountType;
    private String accountNumber;
    private String shortName;
    private String accountRelationship;
    private String productCode;
    private String currencyCode;
    private BigDecimal balance;
    private BigDecimal implementLimitAmount;
    private BigDecimal approvalLimitAmount;
    private String facilityCode;
    private String additionalName1;
    private BigDecimal interestRate;
    private long CDTerm;
    private String CDTermCode;
    private Date dateIssued;
    private long status;
    private Date maturityDate;
    private long receiptSerialNo;
    private String serviceProgrammeId;
    private long agreementStatus;
    private long accountCategory;
    /**
     * Getter for property branchNumber
     */
    public String getBranchNumber() {
        return branchNumber;
    }
    /**
     * Getter for property cIFNumber
     */
    public String getCIFNumber() {
        return CIFNumber;
    }
    /**
     * Getter for property accountType
     */
    public String getAccountType() {
        return accountType;
    }
    /**
     * Getter for property accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }
    /**
     * Getter for property shortName
     */
    public String getShortName() {
        return shortName;
    }
    /**
     * Getter for property accountRelationship
     */
    public String getAccountRelationship() {
        return accountRelationship;
    }
    /**
     * Getter for property productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * Getter for property currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }
    /**
     * Getter for property balance
     */
    public BigDecimal getBalance() {
        return balance;
    }
    /**
     * Getter for property implementLimitAmount
     */
    public BigDecimal getImplementLimitAmount() {
        return implementLimitAmount;
    }
    /**
     * Getter for property approvalLimitAmount
     */
    public BigDecimal getApprovalLimitAmount() {
        return approvalLimitAmount;
    }
    /**
     * Getter for property facilityCode
     */
    public String getFacilityCode() {
        return facilityCode;
    }
    /**
     * Getter for property additionalName1
     */
    public String getAdditionalName1() {
        return additionalName1;
    }
    /**
     * Getter for property interestRate
     */
    public BigDecimal getInterestRate() {
        return interestRate;
    }
    /**
     * Getter for property cDTerm
     */
    public long getCDTerm() {
        return CDTerm;
    }
    /**
     * Getter for property cDTermCode
     */
    public String getCDTermCode() {
        return CDTermCode;
    }
    /**
     * Getter for property dateIssued
     */
    public Date getDateIssued() {
        return dateIssued;
    }
    /**
     * Getter for property status
     */
    public long getStatus() {
        return status;
    }
    /**
     * Getter for property maturityDate
     */
    public Date getMaturityDate() {
        return maturityDate;
    }
    /**
     * Getter for property receiptSerialNo
     */
    public long getReceiptSerialNo() {
        return receiptSerialNo;
    }
    /**
     * Getter for property gserviceProgrammeId
     */
    public String getServiceProgrammeId() {
        return serviceProgrammeId;
    }
    /**
     * Getter for property agreementStatus
     */
    public long getAgreementStatus() {
        return agreementStatus;
    }
    /**
     * Getter for property accountCategory
     */
    public long getAccountCategory() {
        return accountCategory;
    }
    /**
     * Setter for property branchNumber
     */
    public void setBranchNumber(String branchNumber) {
        this.branchNumber = branchNumber;
    }
    /**
     * Setter for property cIFNumber
     */
    public void setCIFNumber(String cIFNumber) {
        CIFNumber = cIFNumber;
    }
    /**
     * Setter for property accountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    /**
     * Setter for property accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    /**
     * Setter for property shortName
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    /**
     * Setter for property accountRelationship
     */
    public void setAccountRelationship(String accountRelationship) {
        this.accountRelationship = accountRelationship;
    }
    /**
     * Setter for property productCode
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * Setter for property currencyCode
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    /**
     * Setter for property balance
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    /**
     * Setter for property implementLimitAmount
     */
    public void setImplementLimitAmount(BigDecimal implementLimitAmount) {
        this.implementLimitAmount = implementLimitAmount;
    }
    /**
     * Setter for property approvalLimitAmount
     */
    public void setApprovalLimitAmount(BigDecimal approvalLimitAmount) {
        this.approvalLimitAmount = approvalLimitAmount;
    }
    /**
     * Setter for property facilityCode
     */
    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }
    /**
     * Setter for property additionalName1
     */
    public void setAdditionalName1(String additionalName1) {
        this.additionalName1 = additionalName1;
    }
    /**
     * Setter for property interestRate
     */
    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }
    /**
     * Setter for property cDTerm
     */
    public void setCDTerm(long cDTerm) {
        CDTerm = cDTerm;
    }
    /**
     * Setter for property cDTermCode
     */
    public void setCDTermCode(String cDTermCode) {
        CDTermCode = cDTermCode;
    }
    /**
     * Setter for property dateIssued
     */
    public void setDateIssued(Date dateIssued) {
        this.dateIssued = dateIssued;
    }
    /**
     * Setter for property status
     */
    public void setStatus(long status) {
        this.status = status;
    }
    /**
     * Setter for property maturityDate
     */
    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }
    /**
     * Setter for property receiptSerialNo
     */
    public void setReceiptSerialNo(long receiptSerialNo) {
        this.receiptSerialNo = receiptSerialNo;
    }
    /**
     * Setter for property gserviceProgrammeId
     */
    public void setServiceProgrammeId(String serviceProgrammeId) {
        this.serviceProgrammeId = serviceProgrammeId;
    }
    /**
     * Setter for property agreementStatus
     */
    public void setAgreementStatus(long agreementStatus) {
        this.agreementStatus = agreementStatus;
    }
    /**
     * Setter for property accountCategory
     */
    public void setAccountCategory(long accountCategory) {
        this.accountCategory = accountCategory;
    }
    
    
    
}
