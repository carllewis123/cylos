package ptdam.emoney.webservice.client;

import java.io.Serializable;

/**
 * @author Siscoes
 */
public class CIFInfo implements Serializable {
	
	private static final long serialVersionUID = 7052779487627615952L;
	
	private String cifNo;
	private String idNo;
	private String cifName1;
	private String cifName2;
	private String cifAddress1;
	private String cifAddress2;
	private String cifAddress3;
	private String cifAddress4;
	private String contactNumber;
	private String residentCode;
	public String getCifNo() {
		return cifNo;
	}
	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getCifName1() {
		return cifName1;
	}
	public void setCifName1(String cifName1) {
		this.cifName1 = cifName1;
	}
	public String getCifName2() {
		return cifName2;
	}
	public void setCifName2(String cifName2) {
		this.cifName2 = cifName2;
	}
	public String getCifAddress1() {
		return cifAddress1;
	}
	public void setCifAddress1(String cifAddress1) {
		this.cifAddress1 = cifAddress1;
	}
	public String getCifAddress2() {
		return cifAddress2;
	}
	public void setCifAddress2(String cifAddress2) {
		this.cifAddress2 = cifAddress2;
	}
	public String getCifAddress3() {
		return cifAddress3;
	}
	public void setCifAddress3(String cifAddress3) {
		this.cifAddress3 = cifAddress3;
	}
	public String getCifAddress4() {
		return cifAddress4;
	}
	public void setCifAddress4(String cifAddress4) {
		this.cifAddress4 = cifAddress4;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getResidentCode() {
		return residentCode;
	}
	public void setResidentCode(String residentCode) {
		this.residentCode = residentCode;
	}
	
	
	
}
