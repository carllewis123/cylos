/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import ptdam.emoney.webservice.member.Status;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class FundTransferMsgResponse implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -849598284211335011L;
    private ResponseHeaderExtended responseHeader;
    private SecurityHeader securityHeader;

    /**
     * Getter for property responseHeader
     */
    public ResponseHeaderExtended getResponseHeader() {
        return responseHeader;
    }

    /**
     * Setter for property responseHeader
     */
    public void setResponseHeader(ResponseHeaderExtended responseHeader) {
        this.responseHeader = responseHeader;
    }

    /**
     * Getter for property securityHeader
     */
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }

    /**
     * Setter for property securityHeader
     */
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    
}
