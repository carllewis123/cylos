/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class DspHeader implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 6716676940071275612L;
    private String destinationId;
    private String messageStatus;
    private long bankIdNumber;
    private long node;
    private String retrievalReferenceNumber;
    private String acquirerReferenceNumber;
    private long transmissionNumber;
    private long numberOfRecordsToBeLoaded;
    private long numberOfErrorsToBeLoaded;
    private String userId;
    private String supervisorId;
    private String moreRecorIndicator;
    private String cutOffIndicator;
    private String userData;
    /**
     * Getter for property destinationId
     */
    public String getDestinationId() {
        return destinationId;
    }
    /**
     * Getter for property messageStatus
     */
    public String getMessageStatus() {
        return messageStatus;
    }
    /**
     * Getter for property bankIdNumber
     */
    public long getBankIdNumber() {
        return bankIdNumber;
    }
    /**
     * Getter for property node
     */
    public long getNode() {
        return node;
    }
    /**
     * Getter for property retrievalReferenceNumber
     */
    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }
    /**
     * Getter for property acquirerReferenceNumber
     */
    public String getAcquirerReferenceNumber() {
        return acquirerReferenceNumber;
    }
    /**
     * Getter for property transmissionNumber
     */
    public long getTransmissionNumber() {
        return transmissionNumber;
    }
    /**
     * Getter for property numberOfRecordsToBeLoaded
     */
    public long getNumberOfRecordsToBeLoaded() {
        return numberOfRecordsToBeLoaded;
    }
    /**
     * Getter for property numberOfErrorsToBeLoaded
     */
    public long getNumberOfErrorsToBeLoaded() {
        return numberOfErrorsToBeLoaded;
    }
    /**
     * Getter for property userId
     */
    public String getUserId() {
        return userId;
    }
    /**
     * Getter for property supervisorId
     */
    public String getSupervisorId() {
        return supervisorId;
    }
    /**
     * Getter for property moreRecorIndicator
     */
    public String getMoreRecorIndicator() {
        return moreRecorIndicator;
    }
    /**
     * Getter for property cutOffIndicator
     */
    public String getCutOffIndicator() {
        return cutOffIndicator;
    }
    /**
     * Getter for property userData
     */
    public String getUserData() {
        return userData;
    }
    /**
     * Setter for property destinationId
     */
    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }
    /**
     * Setter for property messageStatus
     */
    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }
    /**
     * Setter for property bankIdNumber
     */
    public void setBankIdNumber(long bankIdNumber) {
        this.bankIdNumber = bankIdNumber;
    }
    /**
     * Setter for property node
     */
    public void setNode(long node) {
        this.node = node;
    }
    /**
     * Setter for property retrievalReferenceNumber
     */
    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }
    /**
     * Setter for property acquirerReferenceNumber
     */
    public void setAcquirerReferenceNumber(String acquirerReferenceNumber) {
        this.acquirerReferenceNumber = acquirerReferenceNumber;
    }
    /**
     * Setter for property transmissionNumber
     */
    public void setTransmissionNumber(long transmissionNumber) {
        this.transmissionNumber = transmissionNumber;
    }
    /**
     * Setter for property numberOfRecordsToBeLoaded
     */
    public void setNumberOfRecordsToBeLoaded(long numberOfRecordsToBeLoaded) {
        this.numberOfRecordsToBeLoaded = numberOfRecordsToBeLoaded;
    }
    /**
     * Setter for property numberOfErrorsToBeLoaded
     */
    public void setNumberOfErrorsToBeLoaded(long numberOfErrorsToBeLoaded) {
        this.numberOfErrorsToBeLoaded = numberOfErrorsToBeLoaded;
    }
    /**
     * Setter for property userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    /**
     * Setter for property supervisorId
     */
    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }
    /**
     * Setter for property moreRecorIndicator
     */
    public void setMoreRecorIndicator(String moreRecorIndicator) {
        this.moreRecorIndicator = moreRecorIndicator;
    }
    /**
     * Setter for property cutOffIndicator
     */
    public void setCutOffIndicator(String cutOffIndicator) {
        this.cutOffIndicator = cutOffIndicator;
    }
    /**
     * Setter for property userData
     */
    public void setUserData(String userData) {
        this.userData = userData;
    }
    
    

    }
