/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class DataHeaderMBase implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -5274532270630612788L;
    private long referenceNumber;
    private long rebidNumber;
    private String endOfGroupIndicator;
    private long blockMessageNumber;
    private long bankNumber;
    private long branchNumber;
    private String reviewSupervisorID;
    private String transmitSupervisorID;
    private String hostSupervisorId;
    private String actionCode;
    private String transactionMode;
    private long noOfRecordsToRetrieve;
    private String moreRecordsIndicator;
    private String searchMethod;
    private String dateInFromClientDdmmyyyy;
    private String timeInFromClientHhmmss;
    private long accountNo;
    private String accountType;
    private long CIFno;
    /**
     * Getter for property referenceNumber
     */
    public long getReferenceNumber() {
        return referenceNumber;
    }
    /**
     * Getter for property rebidNumber
     */
    public long getRebidNumber() {
        return rebidNumber;
    }
    /**
     * Getter for property endOfGroupIndicator
     */
    public String getEndOfGroupIndicator() {
        return endOfGroupIndicator;
    }
    /**
     * Getter for property blockMessageNumber
     */
    public long getBlockMessageNumber() {
        return blockMessageNumber;
    }
    /**
     * Getter for property bankNumber
     */
    public long getBankNumber() {
        return bankNumber;
    }
    /**
     * Getter for property branchNumber
     */
    public long getBranchNumber() {
        return branchNumber;
    }
    /**
     * Getter for property reviewSupervisorID
     */
    public String getReviewSupervisorID() {
        return reviewSupervisorID;
    }
    /**
     * Getter for property transmitSupervisorID
     */
    public String getTransmitSupervisorID() {
        return transmitSupervisorID;
    }
    /**
     * Getter for property hostSupervisorId
     */
    public String getHostSupervisorId() {
        return hostSupervisorId;
    }
    /**
     * Getter for property actionCode
     */
    public String getActionCode() {
        return actionCode;
    }
    /**
     * Getter for property transactionMode
     */
    public String getTransactionMode() {
        return transactionMode;
    }
    /**
     * Getter for property noOfRecordsToRetrieve
     */
    public long getNoOfRecordsToRetrieve() {
        return noOfRecordsToRetrieve;
    }
    /**
     * Getter for property moreRecordsIndicator
     */
    public String getMoreRecordsIndicator() {
        return moreRecordsIndicator;
    }
    /**
     * Getter for property searchMethod
     */
    public String getSearchMethod() {
        return searchMethod;
    }
    /**
     * Getter for property dateInFromClientDdmmyyyy
     */
    public String getDateInFromClientDdmmyyyy() {
        return dateInFromClientDdmmyyyy;
    }
    /**
     * Getter for property timeInFromClientHhmmss
     */
    public String getTimeInFromClientHhmmss() {
        return timeInFromClientHhmmss;
    }
    /**
     * Getter for property accountNo
     */
    public long getAccountNo() {
        return accountNo;
    }
    /**
     * Getter for property accountType
     */
    public String getAccountType() {
        return accountType;
    }
    /**
     * Getter for property cIFno
     */
    public long getCIFno() {
        return CIFno;
    }
    /**
     * Setter for property referenceNumber
     */
    public void setReferenceNumber(long referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    /**
     * Setter for property rebidNumber
     */
    public void setRebidNumber(long rebidNumber) {
        this.rebidNumber = rebidNumber;
    }
    /**
     * Setter for property endOfGroupIndicator
     */
    public void setEndOfGroupIndicator(String endOfGroupIndicator) {
        this.endOfGroupIndicator = endOfGroupIndicator;
    }
    /**
     * Setter for property blockMessageNumber
     */
    public void setBlockMessageNumber(long blockMessageNumber) {
        this.blockMessageNumber = blockMessageNumber;
    }
    /**
     * Setter for property bankNumber
     */
    public void setBankNumber(long bankNumber) {
        this.bankNumber = bankNumber;
    }
    /**
     * Setter for property branchNumber
     */
    public void setBranchNumber(long branchNumber) {
        this.branchNumber = branchNumber;
    }
    /**
     * Setter for property reviewSupervisorID
     */
    public void setReviewSupervisorID(String reviewSupervisorID) {
        this.reviewSupervisorID = reviewSupervisorID;
    }
    /**
     * Setter for property transmitSupervisorID
     */
    public void setTransmitSupervisorID(String transmitSupervisorID) {
        this.transmitSupervisorID = transmitSupervisorID;
    }
    /**
     * Setter for property hostSupervisorId
     */
    public void setHostSupervisorId(String hostSupervisorId) {
        this.hostSupervisorId = hostSupervisorId;
    }
    /**
     * Setter for property actionCode
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
    /**
     * Setter for property transactionMode
     */
    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }
    /**
     * Setter for property noOfRecordsToRetrieve
     */
    public void setNoOfRecordsToRetrieve(long noOfRecordsToRetrieve) {
        this.noOfRecordsToRetrieve = noOfRecordsToRetrieve;
    }
    /**
     * Setter for property moreRecordsIndicator
     */
    public void setMoreRecordsIndicator(String moreRecordsIndicator) {
        this.moreRecordsIndicator = moreRecordsIndicator;
    }
    /**
     * Setter for property searchMethod
     */
    public void setSearchMethod(String searchMethod) {
        this.searchMethod = searchMethod;
    }
    /**
     * Setter for property dateInFromClientDdmmyyyy
     */
    public void setDateInFromClientDdmmyyyy(String dateInFromClientDdmmyyyy) {
        this.dateInFromClientDdmmyyyy = dateInFromClientDdmmyyyy;
    }
    /**
     * Setter for property timeInFromClientHhmmss
     */
    public void setTimeInFromClientHhmmss(String timeInFromClientHhmmss) {
        this.timeInFromClientHhmmss = timeInFromClientHhmmss;
    }
    /**
     * Setter for property accountNo
     */
    public void setAccountNo(long accountNo) {
        this.accountNo = accountNo;
    }
    /**
     * Setter for property accountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    /**
     * Setter for property cIFno
     */
    public void setCIFno(long cIFno) {
        CIFno = cIFno;
    }
    
}
