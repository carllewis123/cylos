/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ArrayOfAccounts implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 6169044811126013888L;
    private AccountDetail[] arrayOfAccounts;

    public AccountDetail[] getArrayOfAccounts() {
        return arrayOfAccounts;
    }

    public void setArrayOfAccounts(AccountDetail[] arrayOfAccounts) {
        this.arrayOfAccounts = arrayOfAccounts;
    }
    
}
