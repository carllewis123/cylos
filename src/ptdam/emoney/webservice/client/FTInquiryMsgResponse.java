package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Siscoes
 */
public class FTInquiryMsgResponse implements Serializable {
	private static final long serialVersionUID = -4323295855487937079L;

	private ResponseHeaderExtended soaHeader;
    private DspHeader dspHeader;
    private DataHeaderAbcs dataHeader;
    
    private String bdyResponseCode;
    private String bdyResponseHeader;
    private String bdyAccount;
    private String bdyAccountType;
    private String bdyBranchNumber;
    private String bdyShortName;
    private String bdyAccountStatus;
    private String bdyCurrencyType;
    private String bdyProductCode;
    private CIFInfo bdyCifInfo;
    private String bdyPassbookOrStatement;
    private BigDecimal bdyAvailableBalance;
    private BigDecimal bdyLedgerBalance;
    private String bdyLastActiveDate;
    private BigDecimal bdyOriginalBalance;
    private String bdyTellerStatus;
	
    public ResponseHeaderExtended getSoaHeader() {
		return soaHeader;
	}
	public void setSoaHeader(ResponseHeaderExtended soaHeader) {
		this.soaHeader = soaHeader;
	}
	public DspHeader getDspHeader() {
		return dspHeader;
	}
	public void setDspHeader(DspHeader dspHeader) {
		this.dspHeader = dspHeader;
	}
	public DataHeaderAbcs getDataHeader() {
		return dataHeader;
	}
	public void setDataHeader(DataHeaderAbcs dataHeader) {
		this.dataHeader = dataHeader;
	}
	public String getBdyResponseCode() {
		return bdyResponseCode;
	}
	public void setBdyResponseCode(String bdyResponseCode) {
		this.bdyResponseCode = bdyResponseCode;
	}
	public String getBdyResponseHeader() {
		return bdyResponseHeader;
	}
	public void setBdyResponseHeader(String bdyResponseHeader) {
		this.bdyResponseHeader = bdyResponseHeader;
	}
	public String getBdyAccount() {
		return bdyAccount;
	}
	public void setBdyAccount(String bdyAccount) {
		this.bdyAccount = bdyAccount;
	}
	public String getBdyAccountType() {
		return bdyAccountType;
	}
	public void setBdyAccountType(String bdyAccountType) {
		this.bdyAccountType = bdyAccountType;
	}
	public String getBdyBranchNumber() {
		return bdyBranchNumber;
	}
	public void setBdyBranchNumber(String bdyBranchNumber) {
		this.bdyBranchNumber = bdyBranchNumber;
	}
	public String getBdyShortName() {
		return bdyShortName;
	}
	public void setBdyShortName(String bdyShortName) {
		this.bdyShortName = bdyShortName;
	}
	public String getBdyAccountStatus() {
		return bdyAccountStatus;
	}
	public void setBdyAccountStatus(String bdyAccountStatus) {
		this.bdyAccountStatus = bdyAccountStatus;
	}
	public String getBdyCurrencyType() {
		return bdyCurrencyType;
	}
	public void setBdyCurrencyType(String bdyCurrencyType) {
		this.bdyCurrencyType = bdyCurrencyType;
	}
	public String getBdyProductCode() {
		return bdyProductCode;
	}
	public void setBdyProductCode(String bdyProductCode) {
		this.bdyProductCode = bdyProductCode;
	}
	public CIFInfo getBdyCifInfo() {
		return bdyCifInfo;
	}
	public void setBdyCifInfo(CIFInfo bdyCifInfo) {
		this.bdyCifInfo = bdyCifInfo;
	}
	public String getBdyPassbookOrStatement() {
		return bdyPassbookOrStatement;
	}
	public void setBdyPassbookOrStatement(String bdyPassbookOrStatement) {
		this.bdyPassbookOrStatement = bdyPassbookOrStatement;
	}
	public BigDecimal getBdyAvailableBalance() {
		return bdyAvailableBalance;
	}
	public void setBdyAvailableBalance(BigDecimal bdyAvailableBalance) {
		this.bdyAvailableBalance = bdyAvailableBalance;
	}
	public BigDecimal getBdyLedgerBalance() {
		return bdyLedgerBalance;
	}
	public void setBdyLedgerBalance(BigDecimal bdyLedgerBalance) {
		this.bdyLedgerBalance = bdyLedgerBalance;
	}
	public String getBdyLastActiveDate() {
		return bdyLastActiveDate;
	}
	public void setBdyLastActiveDate(String bdyLastActiveDate) {
		this.bdyLastActiveDate = bdyLastActiveDate;
	}
	public BigDecimal getBdyOriginalBalance() {
		return bdyOriginalBalance;
	}
	public void setBdyOriginalBalance(BigDecimal bdyOriginalBalance) {
		this.bdyOriginalBalance = bdyOriginalBalance;
	}
	public String getBdyTellerStatus() {
		return bdyTellerStatus;
	}
	public void setBdyTellerStatus(String bdyTellerStatus) {
		this.bdyTellerStatus = bdyTellerStatus;
	}

    
}
