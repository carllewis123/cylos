/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class AcctInfoInquiryRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -8956027832202386240L;
    private RequestHeader header;
    private SecurityHeader securityHeader;
    private DspHeader dspHeader;
    private DataHeaderMBase dataHeader;
    
    private String bdyCustomerNumber;
    private String bdyAccountType;
    private String bdyAccountNumber;
    private String bdyShortName;
    private String bdyAccountRelationship;
    /**
     * Getter for property header
     */
    public RequestHeader getHeader() {
        return header;
    }
    /**
     * Getter for property securityHeader
     */
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    /**
     * Getter for property bdyCustomerNumber
     */
    public String getBdyCustomerNumber() {
        return bdyCustomerNumber;
    }
    /**
     * Getter for property bdyAccountType
     */
    public String getBdyAccountType() {
        return bdyAccountType;
    }
    /**
     * Getter for property bdyAccountNumber
     */
    public String getBdyAccountNumber() {
        return bdyAccountNumber;
    }
    /**
     * Getter for property bdyShortName
     */
    public String getBdyShortName() {
        return bdyShortName;
    }
    /**
     * Getter for property bdyAccountRelationship
     */
    public String getBdyAccountRelationship() {
        return bdyAccountRelationship;
    }
    /**
     * Setter for property header
     */
    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    /**
     * Setter for property securityHeader
     */
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    /**
     * Setter for property bdyCustomerNumber
     */
    public void setBdyCustomerNumber(String bdyCustomerNumber) {
        this.bdyCustomerNumber = bdyCustomerNumber;
    }
    /**
     * Setter for property bdyAccountType
     */
    public void setBdyAccountType(String bdyAccountType) {
        this.bdyAccountType = bdyAccountType;
    }
    /**
     * Setter for property bdyAccountNumber
     */
    public void setBdyAccountNumber(String bdyAccountNumber) {
        this.bdyAccountNumber = bdyAccountNumber;
    }
    /**
     * Setter for property bdyShortName
     */
    public void setBdyShortName(String bdyShortName) {
        this.bdyShortName = bdyShortName;
    }
    /**
     * Setter for property bdyAccountRelationship
     */
    public void setBdyAccountRelationship(String bdyAccountRelationship) {
        this.bdyAccountRelationship = bdyAccountRelationship;
    }
    /**
     * Getter for property dspHeader
     */
    public DspHeader getDspHeader() {
        return dspHeader;
    }
    /**
     * Getter for property dataHeader
     */
    public DataHeaderMBase getDataHeader() {
        return dataHeader;
    }
    /**
     * Setter for property dspHeader
     */
    public void setDspHeader(DspHeader dspHeader) {
        this.dspHeader = dspHeader;
    }
    /**
     * Setter for property dataHeader
     */
    public void setDataHeader(DataHeaderMBase dataHeader) {
        this.dataHeader = dataHeader;
    }
    
    
    
}
