/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import ptdam.emoney.webservice.member.Status;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class UBPCInqMsgResponse implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 2470867903668198523L;

    private ResponseHeaderExtended responseHeader;
    
    private String bdyCurrencyCode;
    private Long bdyCIFNo;
    private Long bdyCompanyAccountNumber;
    private BigDecimal bdyCustomerChargeAmount;
    private BigDecimal bdyCompanyChargeAmount;
    private String bdyCustomerName;
    private Long bdyCustomerCode;
    private String bdyReserved1;
    private String bdyReserved2;
    
    private BillInfoList[] bdyBillInfos;
    private BillItemList[] bdyBillItems;
    
    private String bdyRemarkHint;
    private Long bdyMerchantType;
    private Long bdyPaymentType;
    private BigDecimal bdyChargeAccountNumber;
    
    /**
     * Getter for property responseHeader
     */
    public ResponseHeaderExtended getResponseHeader() {
        return responseHeader;
    }

    /**
     * Setter for property responseHeader
     */
    public void setResponseHeader(ResponseHeaderExtended responseHeader) {
        this.responseHeader = responseHeader;
    }

    public String getBdyCurrencyCode() {
        return bdyCurrencyCode;
    }

    public void setBdyCurrencyCode(String bdyCurrencyCode) {
        this.bdyCurrencyCode = bdyCurrencyCode;
    }

    public Long getBdyCIFNo() {
        return bdyCIFNo;
    }

    public void setBdyCIFNo(Long bdyCIFNo) {
        this.bdyCIFNo = bdyCIFNo;
    }

    public Long getBdyCompanyAccountNumber() {
        return bdyCompanyAccountNumber;
    }

    public void setBdyCompanyAccountNumber(Long bdyCompanyAccountNumber) {
        this.bdyCompanyAccountNumber = bdyCompanyAccountNumber;
    }

    public BigDecimal getBdyCustomerChargeAmount() {
        return bdyCustomerChargeAmount;
    }

    public void setBdyCustomerChargeAmount(BigDecimal bdyCustomerChargeAmount) {
        this.bdyCustomerChargeAmount = bdyCustomerChargeAmount;
    }

    public BigDecimal getBdyCompanyChargeAmount() {
        return bdyCompanyChargeAmount;
    }

    public void setBdyCompanyChargeAmount(BigDecimal bdyCompanyChargeAmount) {
        this.bdyCompanyChargeAmount = bdyCompanyChargeAmount;
    }

    public String getBdyCustomerName() {
        return bdyCustomerName;
    }

    public void setBdyCustomerName(String bdyCustomerName) {
        this.bdyCustomerName = bdyCustomerName;
    }

    public Long getBdyCustomerCode() {
        return bdyCustomerCode;
    }

    public void setBdyCustomerCode(Long bdyCustomerCode) {
        this.bdyCustomerCode = bdyCustomerCode;
    }

    public String getBdyReserved1() {
        return bdyReserved1;
    }

    public void setBdyReserved1(String bdyReserved1) {
        this.bdyReserved1 = bdyReserved1;
    }

    public String getBdyReserved2() {
        return bdyReserved2;
    }

    public void setBdyReserved2(String bdyReserved2) {
        this.bdyReserved2 = bdyReserved2;
    }

    public String getBdyRemarkHint() {
        return bdyRemarkHint;
    }

    public void setBdyRemarkHint(String bdyRemarkHint) {
        this.bdyRemarkHint = bdyRemarkHint;
    }

    public Long getBdyMerchantType() {
        return bdyMerchantType;
    }

    public void setBdyMerchantType(Long bdyMerchantType) {
        this.bdyMerchantType = bdyMerchantType;
    }

    public Long getBdyPaymentType() {
        return bdyPaymentType;
    }

    public void setBdyPaymentType(Long bdyPaymentType) {
        this.bdyPaymentType = bdyPaymentType;
    }

    public BigDecimal getBdyChargeAccountNumber() {
        return bdyChargeAccountNumber;
    }

    public void setBdyChargeAccountNumber(BigDecimal bdyChargeAccountNumber) {
        this.bdyChargeAccountNumber = bdyChargeAccountNumber;
    }

    public BillInfoList[] getBdyBillInfos() {
        return bdyBillInfos;
    }

    public void setBdyBillInfos(BillInfoList[] bdyBillInfos) {
        this.bdyBillInfos = bdyBillInfos;
    }

    public BillItemList[] getBdyBillItems() {
        return bdyBillItems;
    }

    public void setBdyBillItems(BillItemList[] bdyBillItems) {
        this.bdyBillItems = bdyBillItems;
    }    
    
}
