/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class RequestHeader implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 1939084733041655465L;
    private String hdrExternalId;
    private long hdrTellerId;
    private long hdrJournalSequence;
    private String hdrTransactionCode;
    private String hdrChannelId;
    private String hdrTimestamp;
    
    /**
     * Getter for property hdrExternalId
     */
    public String getHdrExternalId() {
        return hdrExternalId;
    }
    /**
     * Getter for property hdrTellerId
     */
    public long getHdrTellerId() {
        return hdrTellerId;
    }
    /**
     * Getter for property hdrJournalSequence
     */
    public long getHdrJournalSequence() {
        return hdrJournalSequence;
    }
    /**
     * Getter for property hdrTransactionCode
     */
    public String getHdrTransactionCode() {
        return hdrTransactionCode;
    }
    /**
     * Getter for property hdrChannelId
     */
    public String getHdrChannelId() {
        return hdrChannelId;
    }
    /**
     * Getter for property hdrTimestamp
     */
    public String getHdrTimestamp() {
        return hdrTimestamp;
    }
    /**
     * Setter for property hdrExternalId
     */
    public void setHdrExternalId(String hdrExternalId) {
        this.hdrExternalId = hdrExternalId;
    }
    /**
     * Setter for property hdrTellerId
     */
    public void setHdrTellerId(long hdrTellerId) {
        this.hdrTellerId = hdrTellerId;
    }
    /**
     * Setter for property hdrJournalSequence
     */
    public void setHdrJournalSequence(long hdrJournalSequence) {
        this.hdrJournalSequence = hdrJournalSequence;
    }
    /**
     * Setter for property hdrTransactionCode
     */
    public void setHdrTransactionCode(String hdrTransactionCode) {
        this.hdrTransactionCode = hdrTransactionCode;
    }
    /**
     * Setter for property hdrChannelId
     */
    public void setHdrChannelId(String hdrChannelId) {
        this.hdrChannelId = hdrChannelId;
    }
    /**
     * Setter for property hdrTimestamp
     */
    public void setHdrTimestamp(String hdrTimestamp) {
        this.hdrTimestamp = hdrTimestamp;
    }
 
    
}
