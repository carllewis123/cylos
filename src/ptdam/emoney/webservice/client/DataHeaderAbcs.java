package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author Siscoes
 */
public class DataHeaderAbcs implements Serializable {
	private static final long serialVersionUID = 7438108843920921096L;
	
	private String routerQueue;
	private String deviceNameWorkstation;
	private long messageNumber;
	private String communicationQueue;
	private long finalSocketAddress;
	private Calendar entryTime;
	private String uniqueTransactionSynchronisationId;
	
	public String getRouterQueue() {
		return routerQueue;
	}
	public void setRouterQueue(String routerQueue) {
		this.routerQueue = routerQueue;
	}
	public String getDeviceNameWorkstation() {
		return deviceNameWorkstation;
	}
	public void setDeviceNameWorkstation(String deviceNameWorkstation) {
		this.deviceNameWorkstation = deviceNameWorkstation;
	}
	public long getMessageNumber() {
		return messageNumber;
	}
	public void setMessageNumber(long messageNumber) {
		this.messageNumber = messageNumber;
	}
	public String getCommunicationQueue() {
		return communicationQueue;
	}
	public void setCommunicationQueue(String communicationQueue) {
		this.communicationQueue = communicationQueue;
	}
	public long getFinalSocketAddress() {
		return finalSocketAddress;
	}
	public void setFinalSocketAddress(long finalSocketAddress) {
		this.finalSocketAddress = finalSocketAddress;
	}
	public Calendar getEntryTime() {
		return entryTime;
	}
	public void setEntryTime(Calendar entryTime) {
		this.entryTime = entryTime;
	}
	public String getUniqueTransactionSynchronisationId() {
		return uniqueTransactionSynchronisationId;
	}
	public void setUniqueTransactionSynchronisationId(
			String uniqueTransactionSynchronisationId) {
		this.uniqueTransactionSynchronisationId = uniqueTransactionSynchronisationId;
	}
	    
}
