/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class TelcoResponseMessage implements Serializable {

    private static final long serialVersionUID = 5494365677418268367L;

    private TelcoDSPSocketHeader telcoSocketHeader;
    private TelcoDSPMWHeader telcoMiddlewareHeader;
    private String messageType;
    private String primaryBitmap;
    private String secondaryBitmap;

    // PAN Number
    private String bit_02_1;
    private String bit_02_2;
    
    // Processing Code
    private String bit_03;
    
    // Flagging Amount
    private String bit_04;
    
    // System Trace Number
    private String bit_11;
    
    // Time
    private String  bit_12;
    
    // Date 
    private String bit_13;
    
    // Response Code
    private String bit_39;
    
    // Terminal ID
    private String bit_41;
    
    // Currency Code
    private String bit_49;
    
    // Length
    private String bit_61_1;
    
    // From account number
    private String bit_61_2;
    
    // MSISDN number
    private String bit_61_3;
    
    // Area Code
    private String bit_61_4;
    
    // To number account
    private String bit_61_5;
    
    // RRNO
    private String bit_61_6;
    
    // Account Branch
    private String bit_61_7;
    
    // Biller Code
    private String bit_61_8;
    
    public TelcoDSPSocketHeader getTelcoSocketHeader() {
        return telcoSocketHeader;
    }


    public void setTelcoSocketHeader(TelcoDSPSocketHeader telcoSocketHeader) {
        this.telcoSocketHeader = telcoSocketHeader;
    }


    public TelcoDSPMWHeader getTelcoMiddlewareHeader() {
        return telcoMiddlewareHeader;
    }


    public void setTelcoMiddlewareHeader(TelcoDSPMWHeader telcoMiddlewareHeader) {
        this.telcoMiddlewareHeader = telcoMiddlewareHeader;
    }


    // Debiting amount
    private String bit_61_9;
    
    // Crediting amount
    private String bit_61_10;
    
    // Commission amount
    private String bit_61_11;
    
    // Account3 Customer admin charges
    private String bit_61_12;
    
    // Amount3 Bank admin charges
    private String bit_61_13;
    
    // Account Branch
    private String bit_61_14;
    
    // Account4 3rd party admin charges
    private String bit_61_15;
    
    // Amount4 3rd party bank charges
    private String bit_61_16;
    
    // Account Branch
    private String bit_61_17;
    
    
    // Field length
    private String bit_62_1;
    
    // Amount 1
    private String bit_62_2;
    
    // Negative / Positive Balance
    private String bit_62_3;
    
    
    // Field Length
    private String bit_126_1;
    
    private TelcoBit126 bit_126_2;


    public String getMessageType() {
        return messageType;
    }


    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }


    public String getPrimaryBitmap() {
        return primaryBitmap;
    }


    public void setPrimaryBitmap(String primaryBitmap) {
        this.primaryBitmap = primaryBitmap;
    }


    public String getSecondaryBitmap() {
        return secondaryBitmap;
    }


    public void setSecondaryBitmap(String secondaryBitmap) {
        this.secondaryBitmap = secondaryBitmap;
    }


    public String getBit_02_1() {
        return bit_02_1;
    }


    public void setBit_02_1(String bit_02_1) {
        this.bit_02_1 = bit_02_1;
    }


    public String getBit_02_2() {
        return bit_02_2;
    }


    public void setBit_02_2(String bit_02_2) {
        this.bit_02_2 = bit_02_2;
    }


    public String getBit_03() {
        return bit_03;
    }


    public void setBit_03(String bit_03) {
        this.bit_03 = bit_03;
    }


    public String getBit_04() {
        return bit_04;
    }


    public void setBit_04(String bit_04) {
        this.bit_04 = bit_04;
    }


    public String getBit_11() {
        return bit_11;
    }


    public void setBit_11(String bit_11) {
        this.bit_11 = bit_11;
    }


    public String getBit_12() {
        return bit_12;
    }


    public void setBit_12(String bit_12) {
        this.bit_12 = bit_12;
    }


    public String getBit_13() {
        return bit_13;
    }


    public void setBit_13(String bit_13) {
        this.bit_13 = bit_13;
    }


    public String getBit_39() {
        return bit_39;
    }


    public void setBit_39(String bit_39) {
        this.bit_39 = bit_39;
    }


    public String getBit_41() {
        return bit_41;
    }


    public void setBit_41(String bit_41) {
        this.bit_41 = bit_41;
    }


    public String getBit_49() {
        return bit_49;
    }


    public void setBit_49(String bit_49) {
        this.bit_49 = bit_49;
    }


    public String getBit_61_1() {
        return bit_61_1;
    }


    public void setBit_61_1(String bit_61_1) {
        this.bit_61_1 = bit_61_1;
    }


    public String getBit_61_2() {
        return bit_61_2;
    }


    public void setBit_61_2(String bit_61_2) {
        this.bit_61_2 = bit_61_2;
    }


    public String getBit_61_3() {
        return bit_61_3;
    }


    public void setBit_61_3(String bit_61_3) {
        this.bit_61_3 = bit_61_3;
    }


    public String getBit_61_4() {
        return bit_61_4;
    }


    public void setBit_61_4(String bit_61_4) {
        this.bit_61_4 = bit_61_4;
    }


    public String getBit_61_5() {
        return bit_61_5;
    }


    public void setBit_61_5(String bit_61_5) {
        this.bit_61_5 = bit_61_5;
    }


    public String getBit_61_6() {
        return bit_61_6;
    }


    public void setBit_61_6(String bit_61_6) {
        this.bit_61_6 = bit_61_6;
    }


    public String getBit_61_7() {
        return bit_61_7;
    }


    public void setBit_61_7(String bit_61_7) {
        this.bit_61_7 = bit_61_7;
    }


    public String getBit_61_8() {
        return bit_61_8;
    }


    public void setBit_61_8(String bit_61_8) {
        this.bit_61_8 = bit_61_8;
    }


    public String getBit_61_9() {
        return bit_61_9;
    }


    public void setBit_61_9(String bit_61_9) {
        this.bit_61_9 = bit_61_9;
    }


    public String getBit_61_10() {
        return bit_61_10;
    }


    public void setBit_61_10(String bit_61_10) {
        this.bit_61_10 = bit_61_10;
    }


    public String getBit_61_11() {
        return bit_61_11;
    }


    public void setBit_61_11(String bit_61_11) {
        this.bit_61_11 = bit_61_11;
    }


    public String getBit_61_12() {
        return bit_61_12;
    }


    public void setBit_61_12(String bit_61_12) {
        this.bit_61_12 = bit_61_12;
    }


    public String getBit_61_13() {
        return bit_61_13;
    }


    public void setBit_61_13(String bit_61_13) {
        this.bit_61_13 = bit_61_13;
    }


    public String getBit_61_14() {
        return bit_61_14;
    }


    public void setBit_61_14(String bit_61_14) {
        this.bit_61_14 = bit_61_14;
    }


    public String getBit_61_15() {
        return bit_61_15;
    }


    public void setBit_61_15(String bit_61_15) {
        this.bit_61_15 = bit_61_15;
    }


    public String getBit_61_16() {
        return bit_61_16;
    }


    public void setBit_61_16(String bit_61_16) {
        this.bit_61_16 = bit_61_16;
    }


    public String getBit_61_17() {
        return bit_61_17;
    }


    public void setBit_61_17(String bit_61_17) {
        this.bit_61_17 = bit_61_17;
    }


    public String getBit_62_1() {
        return bit_62_1;
    }


    public void setBit_62_1(String bit_62_1) {
        this.bit_62_1 = bit_62_1;
    }


    public String getBit_62_2() {
        return bit_62_2;
    }


    public void setBit_62_2(String bit_62_2) {
        this.bit_62_2 = bit_62_2;
    }


    public String getBit_62_3() {
        return bit_62_3;
    }


    public void setBit_62_3(String bit_62_3) {
        this.bit_62_3 = bit_62_3;
    }


    public String getBit_126_1() {
        return bit_126_1;
    }


    public void setBit_126_1(String bit_126_1) {
        this.bit_126_1 = bit_126_1;
    }


    public TelcoBit126 getBit_126_2() {
        return bit_126_2;
    }


    public void setBit_126_2(TelcoBit126 bit_126_2) {
        this.bit_126_2 = bit_126_2;
    }
    
}
