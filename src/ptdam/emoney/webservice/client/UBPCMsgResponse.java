/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class UBPCMsgResponse implements Serializable  {

    private static final long serialVersionUID = 2501657777072974176L;
    private ResponseHeaderExtended header;
    
    private String bdyEbsRemark;
    private BillInfoList[] bdyReceiptInfo;
    private Long bdyMerchantType;
    private String bdyReserved1;
    private String bdyReserved2;
    public ResponseHeaderExtended getHeader() {
        return header;
    }
    public void setHeader(ResponseHeaderExtended header) {
        this.header = header;
    }
    public String getBdyEbsRemark() {
        return bdyEbsRemark;
    }
    public void setBdyEbsRemark(String bdyEbsRemark) {
        this.bdyEbsRemark = bdyEbsRemark;
    }
    public Long getBdyMerchantType() {
        return bdyMerchantType;
    }
    public void setBdyMerchantType(Long bdyMerchantType) {
        this.bdyMerchantType = bdyMerchantType;
    }
    public String getBdyReserved1() {
        return bdyReserved1;
    }
    public void setBdyReserved1(String bdyReserved1) {
        this.bdyReserved1 = bdyReserved1;
    }
    public String getBdyReserved2() {
        return bdyReserved2;
    }
    public void setBdyReserved2(String bdyReserved2) {
        this.bdyReserved2 = bdyReserved2;
    }
    public BillInfoList[] getBdyReceiptInfo() {
        return bdyReceiptInfo;
    }
    public void setBdyReceiptInfo(BillInfoList[] bdyReceiptInfo) {
        this.bdyReceiptInfo = bdyReceiptInfo;
    }
    
}
