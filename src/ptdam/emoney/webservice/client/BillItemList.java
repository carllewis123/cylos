/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;

public class BillItemList implements Serializable {

    private static final long serialVersionUID = -7480950753541939464L;
    private String billCode;
    private String billName;
    private BigDecimal billAmount;
    private BigDecimal billCustomerChargeAmount;
    private BigDecimal billCompanyChargeAmount;
    private String billPaidFlag;
    private String billCurrency;
    public String getBillCode() {
        return billCode;
    }
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }
    public String getBillName() {
        return billName;
    }
    public void setBillName(String billName) {
        this.billName = billName;
    }
    public BigDecimal getBillAmount() {
        return billAmount;
    }
    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }
    public BigDecimal getBillCustomerChargeAmount() {
        return billCustomerChargeAmount;
    }
    public void setBillCustomerChargeAmount(BigDecimal billCustomerChargeAmount) {
        this.billCustomerChargeAmount = billCustomerChargeAmount;
    }
    public BigDecimal getBillCompanyChargeAmount() {
        return billCompanyChargeAmount;
    }
    public void setBillCompanyChargeAmount(BigDecimal billCompanyChargeAmount) {
        this.billCompanyChargeAmount = billCompanyChargeAmount;
    }
    public String getBillPaidFlag() {
        return billPaidFlag;
    }
    public void setBillPaidFlag(String billPaidFlag) {
        this.billPaidFlag = billPaidFlag;
    }
    public String getBillCurrency() {
        return billCurrency;
    }
    public void setBillCurrency(String billCurrency) {
        this.billCurrency = billCurrency;
    }
    
}
