/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ptdam.emoney.webservice.member.Status;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class CustInfoInquiryResponse implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;
    
    private static final long serialVersionUID = -7936615938148422023L;
    private ResponseHeader responseHeader;
    private SecurityHeader securityHeader;
    
    private long bdyBankNumber;
    private long bdyBranchNumber;
    private String bdyTitleBeforeName;
    private String bdyCIFName1;
    private String bdyCIFName2;
    private String bydTitleAfterName;
    private Date bydDayOfBirth;
    private String bdyBirthIncorporationPlace;
    private String bdyCustomerTypeCode;
    private String bydIDTypeCode;
    private String bdyIDNumber;
    private String bdyAddress1;
    private String bdyAddress2;
    private String bdyAddress3;
    private String bdyAddress4;
    private String bdyPostalCode;
    private String bdyMotherMaidenName;
    private String bdyRemarkLine1;
    private String bdyRemarkLine2;
    private String bdyRemarkLine3;
    private String bdyRemarkLine4;
    /**
     * Getter for property responseHeader
     */
    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }
    /**
     * Getter for property bdyBankNumber
     */
    public long getBdyBankNumber() {
        return bdyBankNumber;
    }
    /**
     * Getter for property bdyBranchNumber
     */
    public long getBdyBranchNumber() {
        return bdyBranchNumber;
    }
    /**
     * Getter for property bdyTitleBeforeName
     */
    public String getBdyTitleBeforeName() {
        return bdyTitleBeforeName;
    }
    /**
     * Getter for property bdyCIFName1
     */
    public String getBdyCIFName1() {
        return bdyCIFName1;
    }
    /**
     * Getter for property bdyCIFName2
     */
    public String getBdyCIFName2() {
        return bdyCIFName2;
    }
    /**
     * Getter for property bydTitleAfterName
     */
    public String getBydTitleAfterName() {
        return bydTitleAfterName;
    }
    /**
     * Getter for property bydDayOfBirth
     */
    public Date getBydDayOfBirth() {
        return bydDayOfBirth;
    }
    /**
     * Getter for property bdyBirthIncorporationPlace
     */
    public String getBdyBirthIncorporationPlace() {
        return bdyBirthIncorporationPlace;
    }
    /**
     * Getter for property bdyCustomerTypeCode
     */
    public String getBdyCustomerTypeCode() {
        return bdyCustomerTypeCode;
    }
    /**
     * Getter for property bydIDTypeCode
     */
    public String getBydIDTypeCode() {
        return bydIDTypeCode;
    }
    /**
     * Getter for property bdyIDNumber
     */
    public String getBdyIDNumber() {
        return bdyIDNumber;
    }
    /**
     * Getter for property bdyAddress1
     */
    public String getBdyAddress1() {
        return bdyAddress1;
    }
    /**
     * Getter for property bdyAddress2
     */
    public String getBdyAddress2() {
        return bdyAddress2;
    }
    /**
     * Getter for property bdyAddress3
     */
    public String getBdyAddress3() {
        return bdyAddress3;
    }
    /**
     * Getter for property bdyAddress4
     */
    public String getBdyAddress4() {
        return bdyAddress4;
    }
    /**
     * Getter for property bdyPostalCode
     */
    public String getBdyPostalCode() {
        return bdyPostalCode;
    }
    /**
     * Getter for property bdyMotherMaidenName
     */
    public String getBdyMotherMaidenName() {
        return bdyMotherMaidenName;
    }
    /**
     * Getter for property bdyRemarkLine1
     */
    public String getBdyRemarkLine1() {
        return bdyRemarkLine1;
    }
    /**
     * Getter for property bdyRemarkLine2
     */
    public String getBdyRemarkLine2() {
        return bdyRemarkLine2;
    }
    /**
     * Getter for property bdyRemarkLine3
     */
    public String getBdyRemarkLine3() {
        return bdyRemarkLine3;
    }
    /**
     * Getter for property bdyRemarkLine4
     */
    public String getBdyRemarkLine4() {
        return bdyRemarkLine4;
    }
    /**
     * Setter for property responseHeader
     */
    public void setResponseHeader(ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }
    /**
     * Setter for property bdyBankNumber
     */
    public void setBdyBankNumber(long bdyBankNumber) {
        this.bdyBankNumber = bdyBankNumber;
    }
    /**
     * Setter for property bdyBranchNumber
     */
    public void setBdyBranchNumber(long bdyBranchNumber) {
        this.bdyBranchNumber = bdyBranchNumber;
    }
    /**
     * Setter for property bdyTitleBeforeName
     */
    public void setBdyTitleBeforeName(String bdyTitleBeforeName) {
        this.bdyTitleBeforeName = bdyTitleBeforeName;
    }
    /**
     * Setter for property bdyCIFName1
     */
    public void setBdyCIFName1(String bdyCIFName1) {
        this.bdyCIFName1 = bdyCIFName1;
    }
    /**
     * Setter for property bdyCIFName2
     */
    public void setBdyCIFName2(String bdyCIFName2) {
        this.bdyCIFName2 = bdyCIFName2;
    }
    /**
     * Setter for property bydTitleAfterName
     */
    public void setBydTitleAfterName(String bydTitleAfterName) {
        this.bydTitleAfterName = bydTitleAfterName;
    }
    /**
     * Setter for property bydDayOfBirth
     */
    public void setBydDayOfBirth(Date bydDayOfBirth) {
        this.bydDayOfBirth = bydDayOfBirth;
    }
    /**
     * Setter for property bdyBirthIncorporationPlace
     */
    public void setBdyBirthIncorporationPlace(String bdyBirthIncorporationPlace) {
        this.bdyBirthIncorporationPlace = bdyBirthIncorporationPlace;
    }
    /**
     * Setter for property bdyCustomerTypeCode
     */
    public void setBdyCustomerTypeCode(String bdyCustomerTypeCode) {
        this.bdyCustomerTypeCode = bdyCustomerTypeCode;
    }
    /**
     * Setter for property bydIDTypeCode
     */
    public void setBydIDTypeCode(String bydIDTypeCode) {
        this.bydIDTypeCode = bydIDTypeCode;
    }
    /**
     * Setter for property bdyIDNumber
     */
    public void setBdyIDNumber(String bdyIDNumber) {
        this.bdyIDNumber = bdyIDNumber;
    }
    /**
     * Setter for property bdyAddress1
     */
    public void setBdyAddress1(String bdyAddress1) {
        this.bdyAddress1 = bdyAddress1;
    }
    /**
     * Setter for property bdyAddress2
     */
    public void setBdyAddress2(String bdyAddress2) {
        this.bdyAddress2 = bdyAddress2;
    }
    /**
     * Setter for property bdyAddress3
     */
    public void setBdyAddress3(String bdyAddress3) {
        this.bdyAddress3 = bdyAddress3;
    }
    /**
     * Setter for property bdyAddress4
     */
    public void setBdyAddress4(String bdyAddress4) {
        this.bdyAddress4 = bdyAddress4;
    }
    /**
     * Setter for property bdyPostalCode
     */
    public void setBdyPostalCode(String bdyPostalCode) {
        this.bdyPostalCode = bdyPostalCode;
    }
    /**
     * Setter for property bdyMotherMaidenName
     */
    public void setBdyMotherMaidenName(String bdyMotherMaidenName) {
        this.bdyMotherMaidenName = bdyMotherMaidenName;
    }
    /**
     * Setter for property bdyRemarkLine1
     */
    public void setBdyRemarkLine1(String bdyRemarkLine1) {
        this.bdyRemarkLine1 = bdyRemarkLine1;
    }
    /**
     * Setter for property bdyRemarkLine2
     */
    public void setBdyRemarkLine2(String bdyRemarkLine2) {
        this.bdyRemarkLine2 = bdyRemarkLine2;
    }
    /**
     * Setter for property bdyRemarkLine3
     */
    public void setBdyRemarkLine3(String bdyRemarkLine3) {
        this.bdyRemarkLine3 = bdyRemarkLine3;
    }
    /**
     * Setter for property bdyRemarkLine4
     */
    public void setBdyRemarkLine4(String bdyRemarkLine4) {
        this.bdyRemarkLine4 = bdyRemarkLine4;
    }
    /**
     * Getter for property securityHeader
     */
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    /**
     * Setter for property securityHeader
     */
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
  
    
    
}
