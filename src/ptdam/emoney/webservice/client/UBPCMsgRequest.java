/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UBPCMsgRequest implements Serializable {

    private static final long serialVersionUID = -5754449688608083726L;
    private RequestHeader header;
    private SecurityHeader securityHeader;
    
    private Long bdyCompanyCode;
    private String bdyChannelId;
    private long bdyTraceNumber;
    private String bdyCardAcceptorTermId;
    private String bdyCustomerAccountType;
    private long bdyTrack2data;
    private String bdyLanguageCode;
    private String bdyCurrencyCode;
    private String bdyBillKey1;
    private Long bdyBillKey2;
    private Long bdyBillKey3;

    private String bdyBillFlagMap;
    private Long bdyDebitAccount;
    private BigDecimal bdyDebitAmount;
    
    private BigDecimal bdyIbtBuyRate;
    private BigDecimal bdyIbtSellRate;
    private BigDecimal bdyTtBuyRate;
    private BigDecimal bdyTtSellRate;
    private Long bdyDebitAccountCorporate;
    private BigDecimal bdyDebitAmountCorporate;
    
    private BigDecimal bdyCreditAmount;
    private String bdyValueDate;
    private BigDecimal bdyCustomerChargesAmount;
    
    private String bdyCreditAmountCorporate;
    private String bdyDebitCurrency;
    private String bdyCreditCurrency;
    
    
    private String bdyReservedField1;
    private String bdyReservedField2;
    public RequestHeader getHeader() {
        return header;
    }
    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    public Long getBdyCompanyCode() {
        return bdyCompanyCode;
    }
    public void setBdyCompanyCode(Long bdyCompanyCode) {
        this.bdyCompanyCode = bdyCompanyCode;
    }
    public String getBdyChannelId() {
        return bdyChannelId;
    }
    public void setBdyChannelId(String bdyChannelId) {
        this.bdyChannelId = bdyChannelId;
    }
    public long getBdyTraceNumber() {
        return bdyTraceNumber;
    }
    public void setBdyTraceNumber(long bdyTraceNumber) {
        this.bdyTraceNumber = bdyTraceNumber;
    }
    public String getBdyCardAcceptorTermId() {
        return bdyCardAcceptorTermId;
    }
    public void setBdyCardAcceptorTermId(String bdyCardAcceptorTermId) {
        this.bdyCardAcceptorTermId = bdyCardAcceptorTermId;
    }
    public String getBdyCustomerAccountType() {
        return bdyCustomerAccountType;
    }
    public void setBdyCustomerAccountType(String bdyCustomerAccountType) {
        this.bdyCustomerAccountType = bdyCustomerAccountType;
    }
    public long getBdyTrack2data() {
        return bdyTrack2data;
    }
    public void setBdyTrack2data(long bdyTrack2data) {
        this.bdyTrack2data = bdyTrack2data;
    }
    public String getBdyLanguageCode() {
        return bdyLanguageCode;
    }
    public void setBdyLanguageCode(String bdyLanguageCode) {
        this.bdyLanguageCode = bdyLanguageCode;
    }
    public String getBdyCurrencyCode() {
        return bdyCurrencyCode;
    }
    public void setBdyCurrencyCode(String bdyCurrencyCode) {
        this.bdyCurrencyCode = bdyCurrencyCode;
    }
    public String getBdyBillKey1() {
        return bdyBillKey1;
    }
    public void setBdyBillKey1(String bdyBillKey1) {
        this.bdyBillKey1 = bdyBillKey1;
    }
    public Long getBdyBillKey2() {
        return bdyBillKey2;
    }
    public void setBdyBillKey2(Long bdyBillKey2) {
        this.bdyBillKey2 = bdyBillKey2;
    }
    public Long getBdyBillKey3() {
        return bdyBillKey3;
    }
    public void setBdyBillKey3(Long bdyBillKey3) {
        this.bdyBillKey3 = bdyBillKey3;
    }
    public String getBdyBillFlagMap() {
        return bdyBillFlagMap;
    }
    public void setBdyBillFlagMap(String bdyBillFlagMap) {
        this.bdyBillFlagMap = bdyBillFlagMap;
    }
    public Long getBdyDebitAccount() {
        return bdyDebitAccount;
    }
    public void setBdyDebitAccount(Long bdyDebitAccount) {
        this.bdyDebitAccount = bdyDebitAccount;
    }
    public BigDecimal getBdyDebitAmount() {
        return bdyDebitAmount;
    }
    public void setBdyDebitAmount(BigDecimal bdyDebitAmount) {
        this.bdyDebitAmount = bdyDebitAmount;
    }
    public BigDecimal getBdyIbtBuyRate() {
        return bdyIbtBuyRate;
    }
    public void setBdyIbtBuyRate(BigDecimal bdyIbtBuyRate) {
        this.bdyIbtBuyRate = bdyIbtBuyRate;
    }
    public BigDecimal getBdyIbtSellRate() {
        return bdyIbtSellRate;
    }
    public void setBdyIbtSellRate(BigDecimal bdyIbtSellRate) {
        this.bdyIbtSellRate = bdyIbtSellRate;
    }
    public BigDecimal getBdyTtBuyRate() {
        return bdyTtBuyRate;
    }
    public void setBdyTtBuyRate(BigDecimal bdyTtBuyRate) {
        this.bdyTtBuyRate = bdyTtBuyRate;
    }
    public BigDecimal getBdyTtSellRate() {
        return bdyTtSellRate;
    }
    public void setBdyTtSellRate(BigDecimal bdyTtSellRate) {
        this.bdyTtSellRate = bdyTtSellRate;
    }
    public Long getBdyDebitAccountCorporate() {
        return bdyDebitAccountCorporate;
    }
    public void setBdyDebitAccountCorporate(Long bdyDebitAccountCorporate) {
        this.bdyDebitAccountCorporate = bdyDebitAccountCorporate;
    }
    public BigDecimal getBdyDebitAmountCorporate() {
        return bdyDebitAmountCorporate;
    }
    public void setBdyDebitAmountCorporate(BigDecimal bdyDebitAmountCorporate) {
        this.bdyDebitAmountCorporate = bdyDebitAmountCorporate;
    }
    public BigDecimal getBdyCreditAmount() {
        return bdyCreditAmount;
    }
    public void setBdyCreditAmount(BigDecimal bdyCreditAmount) {
        this.bdyCreditAmount = bdyCreditAmount;
    }
    public String getBdyValueDate() {
        return bdyValueDate;
    }
    public void setBdyValueDate(String bdyValueDate) {
        this.bdyValueDate = bdyValueDate;
    }
    public BigDecimal getBdyCustomerChargesAmount() {
        return bdyCustomerChargesAmount;
    }
    public void setBdyCustomerChargesAmount(BigDecimal bdyCustomerChargesAmount) {
        this.bdyCustomerChargesAmount = bdyCustomerChargesAmount;
    }
    public String getBdyCreditAmountCorporate() {
        return bdyCreditAmountCorporate;
    }
    public void setBdyCreditAmountCorporate(String bdyCreditAmountCorporate) {
        this.bdyCreditAmountCorporate = bdyCreditAmountCorporate;
    }
    public String getBdyDebitCurrency() {
        return bdyDebitCurrency;
    }
    public void setBdyDebitCurrency(String bdyDebitCurrency) {
        this.bdyDebitCurrency = bdyDebitCurrency;
    }
    public String getBdyCreditCurrency() {
        return bdyCreditCurrency;
    }
    public void setBdyCreditCurrency(String bdyCreditCurrency) {
        this.bdyCreditCurrency = bdyCreditCurrency;
    }
    public String getBdyReservedField1() {
        return bdyReservedField1;
    }
    public void setBdyReservedField1(String bdyReservedField1) {
        this.bdyReservedField1 = bdyReservedField1;
    }
    public String getBdyReservedField2() {
        return bdyReservedField2;
    }
    public void setBdyReservedField2(String bdyReservedField2) {
        this.bdyReservedField2 = bdyReservedField2;
    }
    
}
