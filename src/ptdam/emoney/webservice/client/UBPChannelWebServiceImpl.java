/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import nl.strohalm.cyclos.access.Permission;
import nl.strohalm.cyclos.dao.groups.GroupDAO;
import nl.strohalm.cyclos.entities.Entity;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.GroupQuery;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.ElementQuery;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.members.PendingMember;
import nl.strohalm.cyclos.entities.members.RegisteredMember;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.accounts.CreditLimitDTO;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupFilterService;
import nl.strohalm.cyclos.services.groups.GroupFilterServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupService;
import nl.strohalm.cyclos.services.groups.GroupServiceImpl;
import nl.strohalm.cyclos.setup.Permissions;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.webservices.PrincipalParameters;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.model.GroupVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.GroupHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;

import org.apache.commons.lang.StringUtils;

/**
 * Web service implementation
 * @author luis
 */
@WebService(name = "corebillpayment", serviceName = "corebillpayment")
public class UBPChannelWebServiceImpl implements UBPChannelWebService {

    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private MemberHelper                  memberHelper;
    private GroupHelper                   groupHelper;
    private ChannelHelper                 channelHelper;
    private CustomFieldHelper             customFieldHelper;
    private AccountServiceLocal           accountServiceLocal;
    private FetchServiceLocal       fetchService;
    
    
//    private static final String     RESPONSE_ACTIVATION_SUCCESS = "00";
//    private static final String     RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE = "01";
//    private static final String     RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED = "02";
//    private static final String     RESPONSE_ACCOUNT_NOT_FOUND = "03";
//    private static final String     RESPONSE_SYSTEM_ERROR = "99";
//    
//    private static final String UNREGISTERED = "UNREGISTERED";
//    private static final String REGISTERED = "REGISTERED";
//    
//    private static final String CUSTOM_FIELD_CIF = "cif";
//    
//    private static final String NEW_ACCOUNT = "Nasabah Baru";
//    private static final String REMARKS = "Aktivasi Emoney Online";
   
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    @Override
    public UBPCInqMsgResponse inquiry(final UBPCInqMsgRequest request) {
        // TODO Auto-generated method stub
        UBPCInqMsgResponse response = new UBPCInqMsgResponse();
        return response;
    }

    @Override
    public UBPCMsgResponse payment(final UBPCMsgRequest request) {
        // TODO Auto-generated method stub
        UBPCMsgResponse response = new UBPCMsgResponse();
        return response;
    }

    public void setGroupHelper(final GroupHelper groupHelper) {
        this.groupHelper = groupHelper;
    }
    
    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountServiceLocal = accountService;
    }

    public void setFetchServiceLocal(final FetchServiceLocal fetchService) {
        this.fetchService = fetchService;
    }
}