/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class TelcoDSPSocketHeader implements Serializable {

    private static final long serialVersionUID = -4381300012760805098L;

    private String socketMessageLength;
    private String headerType;
    private String deviceName;
    private String socketNumber;
    private String portNo;
    private String filler;
    
  
    public String getSocketMessageLength() {
        return socketMessageLength;
    }
    public void setSocketMessageLength(String socketMessageLength) {
        this.socketMessageLength = socketMessageLength;
    }
    public String getHeaderType() {
        return headerType;
    }
    public void setHeaderType(String headerType) {
        this.headerType = headerType;
    }
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public String getSocketNumber() {
        return socketNumber;
    }
    public void setSocketNumber(String socketNumber) {
        this.socketNumber = socketNumber;
    }
    public String getPortNo() {
        return portNo;
    }
    public void setPortNo(String portNo) {
        this.portNo = portNo;
    }
    public String getFiller() {
        return filler;
    }
    public void setFiller(String filler) {
        this.filler = filler;
    }
    
}
