/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;

public class TelcoBit126 implements Serializable {

    private static final long serialVersionUID = -2815079124727741318L;
    
    // Token ID
    private String bit_126_1;
    
    // Util Code
    private String bit_126_2;
    
    // Bill Phone Number
    private String bit_126_3;
    
    // Bill Voucher Num
    // Prov Code
    private String bit_126_4_1;
    // Package Code
    private String bit_126_4_2;
    // Filler
    private String bit_126_4_3;
    
    // Bill Cust Num
    private String bit_126_5;
    
    // Bill Cust Name
    private String bit_126_6;
    
    // NPWP
    private String bit_126_7;
    
    // Kandatel Num
    private String bit_126_8;
    
    // Bill Status Code
    private String bit_126_9;
    
    // Bill Ref Nos
    private String bit_126_10;
    private String bit_126_11;
    private String bit_126_12;
    
    // Bill Amounts
    private String bit_126_13;
    private String bit_126_14;
    private String bit_126_15;
    
    // Only for Indosat
    // Cust Id
    private String bit_126_16_1;
    // User Id
    private String bit_126_16_2;
    // Invoice No
    private String bit_126_16_3;
    // Add 1
    private String bit_126_16_4;
    // Add 2
    private String bit_126_16_5;
    // City
    private String bit_126_16_6;
    // Post Code
    private String bit_126_16_7;
    // Area Code
    private String bit_126_16_8;
    // Filler
    private String bit_126_16_9;
    public String getBit_126_1() {
        return bit_126_1;
    }
    public void setBit_126_1(String bit_126_1) {
        this.bit_126_1 = bit_126_1;
    }
    public String getBit_126_2() {
        return bit_126_2;
    }
    public void setBit_126_2(String bit_126_2) {
        this.bit_126_2 = bit_126_2;
    }
    public String getBit_126_3() {
        return bit_126_3;
    }
    public void setBit_126_3(String bit_126_3) {
        this.bit_126_3 = bit_126_3;
    }
    public String getBit_126_4_1() {
        return bit_126_4_1;
    }
    public void setBit_126_4_1(String bit_126_4_1) {
        this.bit_126_4_1 = bit_126_4_1;
    }
    public String getBit_126_4_2() {
        return bit_126_4_2;
    }
    public void setBit_126_4_2(String bit_126_4_2) {
        this.bit_126_4_2 = bit_126_4_2;
    }
    public String getBit_126_4_3() {
        return bit_126_4_3;
    }
    public void setBit_126_4_3(String bit_126_4_3) {
        this.bit_126_4_3 = bit_126_4_3;
    }
    public String getBit_126_5() {
        return bit_126_5;
    }
    public void setBit_126_5(String bit_126_5) {
        this.bit_126_5 = bit_126_5;
    }
    public String getBit_126_6() {
        return bit_126_6;
    }
    public void setBit_126_6(String bit_126_6) {
        this.bit_126_6 = bit_126_6;
    }
    public String getBit_126_7() {
        return bit_126_7;
    }
    public void setBit_126_7(String bit_126_7) {
        this.bit_126_7 = bit_126_7;
    }
    public String getBit_126_8() {
        return bit_126_8;
    }
    public void setBit_126_8(String bit_126_8) {
        this.bit_126_8 = bit_126_8;
    }
    public String getBit_126_9() {
        return bit_126_9;
    }
    public void setBit_126_9(String bit_126_9) {
        this.bit_126_9 = bit_126_9;
    }
    public String getBit_126_10() {
        return bit_126_10;
    }
    public void setBit_126_10(String bit_126_10) {
        this.bit_126_10 = bit_126_10;
    }
    public String getBit_126_11() {
        return bit_126_11;
    }
    public void setBit_126_11(String bit_126_11) {
        this.bit_126_11 = bit_126_11;
    }
    public String getBit_126_12() {
        return bit_126_12;
    }
    public void setBit_126_12(String bit_126_12) {
        this.bit_126_12 = bit_126_12;
    }
    public String getBit_126_13() {
        return bit_126_13;
    }
    public void setBit_126_13(String bit_126_13) {
        this.bit_126_13 = bit_126_13;
    }
    public String getBit_126_14() {
        return bit_126_14;
    }
    public void setBit_126_14(String bit_126_14) {
        this.bit_126_14 = bit_126_14;
    }
    public String getBit_126_15() {
        return bit_126_15;
    }
    public void setBit_126_15(String bit_126_15) {
        this.bit_126_15 = bit_126_15;
    }
    public String getBit_126_16_1() {
        return bit_126_16_1;
    }
    public void setBit_126_16_1(String bit_126_16_1) {
        this.bit_126_16_1 = bit_126_16_1;
    }
    public String getBit_126_16_2() {
        return bit_126_16_2;
    }
    public void setBit_126_16_2(String bit_126_16_2) {
        this.bit_126_16_2 = bit_126_16_2;
    }
    public String getBit_126_16_3() {
        return bit_126_16_3;
    }
    public void setBit_126_16_3(String bit_126_16_3) {
        this.bit_126_16_3 = bit_126_16_3;
    }
    public String getBit_126_16_4() {
        return bit_126_16_4;
    }
    public void setBit_126_16_4(String bit_126_16_4) {
        this.bit_126_16_4 = bit_126_16_4;
    }
    public String getBit_126_16_5() {
        return bit_126_16_5;
    }
    public void setBit_126_16_5(String bit_126_16_5) {
        this.bit_126_16_5 = bit_126_16_5;
    }
    public String getBit_126_16_6() {
        return bit_126_16_6;
    }
    public void setBit_126_16_6(String bit_126_16_6) {
        this.bit_126_16_6 = bit_126_16_6;
    }
    public String getBit_126_16_7() {
        return bit_126_16_7;
    }
    public void setBit_126_16_7(String bit_126_16_7) {
        this.bit_126_16_7 = bit_126_16_7;
    }
    public String getBit_126_16_8() {
        return bit_126_16_8;
    }
    public void setBit_126_16_8(String bit_126_16_8) {
        this.bit_126_16_8 = bit_126_16_8;
    }
    public String getBit_126_16_9() {
        return bit_126_16_9;
    }
    public void setBit_126_16_9(String bit_126_16_9) {
        this.bit_126_16_9 = bit_126_16_9;
    }
    
}
