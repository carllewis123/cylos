/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class CustInfoInquiryRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -8834912679124203227L;
    private RequestHeader header;
    private SecurityHeader securityHeader;
    private String bdyCIFNumber;
    /**
     * Getter for property header
     */
    public RequestHeader getHeader() {
        return header;
    }
    /**
     * Getter for property bdyCIFNumber
     */
    public String getBdyCIFNumber() {
        return bdyCIFNumber;
    }
    /**
     * Setter for property header
     */
    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    /**
     * Setter for property bdyCIFNumber
     */
    public void setBdyCIFNumber(String bdyCIFNumber) {
        this.bdyCIFNumber = bdyCIFNumber;
    }
    /**
     * Getter for property securityHeader
     */
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    /**
     * Setter for property securityHeader
     */
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    
}
