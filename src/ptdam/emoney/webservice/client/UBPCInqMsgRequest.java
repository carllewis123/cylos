/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class UBPCInqMsgRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -6025979170886904002L;
    private RequestHeader header;
    private SecurityHeader securityHeader;

    private long bdyCompanyCode;
    private String bdyChannelId;
    private long bdyCustomerAccountNumber;
    private long bdyTraceNumber;
    private String bdyCardAcceptorTermId;
    private String bdyCustomerAccountType;
    private long bdyTrack2data;
    private long bdyLanguageCode;
    private String bdyCurrencyCode;
    private String bdyBillKey1;
    private Long bdyBillKey2;
    private Long bdyBillKey3;
    private String bdyReservedField1;
    private String bdyReservedField2;
    public RequestHeader getHeader() {
        return header;
    }
    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    public long getBdyCompanyCode() {
        return bdyCompanyCode;
    }
    public void setBdyCompanyCode(long bdyCompanyCode) {
        this.bdyCompanyCode = bdyCompanyCode;
    }
    public String getBdyChannelId() {
        return bdyChannelId;
    }
    public void setBdyChannelId(String bdyChannelId) {
        this.bdyChannelId = bdyChannelId;
    }
    public long getBdyCustomerAccountNumber() {
        return bdyCustomerAccountNumber;
    }
    public void setBdyCustomerAccountNumber(long bdyCustomerAccountNumber) {
        this.bdyCustomerAccountNumber = bdyCustomerAccountNumber;
    }
    public long getBdyTraceNumber() {
        return bdyTraceNumber;
    }
    public void setBdyTraceNumber(long bdyTraceNumber) {
        this.bdyTraceNumber = bdyTraceNumber;
    }
    public String getBdyCardAcceptorTermId() {
        return bdyCardAcceptorTermId;
    }
    public void setBdyCardAcceptorTermId(String bdyCardAcceptorTermId) {
        this.bdyCardAcceptorTermId = bdyCardAcceptorTermId;
    }
    public String getBdyCustomerAccountType() {
        return bdyCustomerAccountType;
    }
    public void setBdyCustomerAccountType(String bdyCustomerAccountType) {
        this.bdyCustomerAccountType = bdyCustomerAccountType;
    }
    public long getBdyTrack2data() {
        return bdyTrack2data;
    }
    public void setBdyTrack2data(long bdyTrack2data) {
        this.bdyTrack2data = bdyTrack2data;
    }
    public long getBdyLanguageCode() {
        return bdyLanguageCode;
    }
    public void setBdyLanguageCode(long bdyLanguageCode) {
        this.bdyLanguageCode = bdyLanguageCode;
    }
    public String getBdyCurrencyCode() {
        return bdyCurrencyCode;
    }
    public void setBdyCurrencyCode(String bdyCurrencyCode) {
        this.bdyCurrencyCode = bdyCurrencyCode;
    }
    public String getBdyBillKey1() {
        return bdyBillKey1;
    }
    public void setBdyBillKey1(String bdyBillKey1) {
        this.bdyBillKey1 = bdyBillKey1;
    }
    public Long getBdyBillKey2() {
        return bdyBillKey2;
    }
    public void setBdyBillKey2(Long bdyBillKey2) {
        this.bdyBillKey2 = bdyBillKey2;
    }
    public Long getBdyBillKey3() {
        return bdyBillKey3;
    }
    public void setBdyBillKey3(Long bdyBillKey3) {
        this.bdyBillKey3 = bdyBillKey3;
    }
    public String getBdyReservedField1() {
        return bdyReservedField1;
    }
    public void setBdyReservedField1(String bdyReservedField1) {
        this.bdyReservedField1 = bdyReservedField1;
    }
    public String getBdyReservedField2() {
        return bdyReservedField2;
    }
    public void setBdyReservedField2(String bdyReservedField2) {
        this.bdyReservedField2 = bdyReservedField2;
    }
    
}
