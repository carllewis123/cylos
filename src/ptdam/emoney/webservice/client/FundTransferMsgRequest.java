/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class FundTransferMsgRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -337425553561884820L;
    private RequestHeader header;
    private SecurityHeader securityHeader;
  
    private BigDecimal bdyDebitAccount;
    private BigDecimal bdyDebitAmount;
    private BigDecimal bdyIbtBuyRate;
    private BigDecimal bdyIbtSellRate;
    private BigDecimal bdyTtBuyRate;
    private BigDecimal bdyTtSellRate;
    private long bdyCreditAccount;
    private BigDecimal bdyCreditAmount;
    private String bdyValueDate;
    private BigDecimal bdyChargesAmount;
    private BigDecimal bdyConvertedChargesAmount;
    private String bdyDefaultCurrencyCode;
    private String bdyRemark1;
    private String bdyRemark2;
    private String bdyRemark3;
    private String bdyRemark4;
    private String bdyDebitCurrency;
    private String bdyCreditCurrency;
    private String bdyChargesCurrency;
    /**
     * Getter for property header
     */
    public RequestHeader getHeader() {
        return header;
    }
    /**
     * Getter for property securityHeader
     */
    public SecurityHeader getSecurityHeader() {
        return securityHeader;
    }
    /**
     * Getter for property bdyDebitAccount
     */
    public BigDecimal getBdyDebitAccount() {
        return bdyDebitAccount;
    }
    /**
     * Getter for property bdyDebitAmount
     */
    public BigDecimal getBdyDebitAmount() {
        return bdyDebitAmount;
    }
    /**
     * Getter for property bdyIbtBuyRate
     */
    public BigDecimal getBdyIbtBuyRate() {
        return bdyIbtBuyRate;
    }
    /**
     * Getter for property bdyIbtSellRate
     */
    public BigDecimal getBdyIbtSellRate() {
        return bdyIbtSellRate;
    }
    /**
     * Getter for property bdyTtBuyRate
     */
    public BigDecimal getBdyTtBuyRate() {
        return bdyTtBuyRate;
    }
    /**
     * Getter for property bdyTtSellRate
     */
    public BigDecimal getBdyTtSellRate() {
        return bdyTtSellRate;
    }
    /**
     * Getter for property bdyCreditAccount
     */
    public long getBdyCreditAccount() {
        return bdyCreditAccount;
    }
    /**
     * Getter for property bdyCreditAmount
     */
    public BigDecimal getBdyCreditAmount() {
        return bdyCreditAmount;
    }
    /**
     * Getter for property bdyValueDate
     */
    public String getBdyValueDate() {
        return bdyValueDate;
    }
    /**
     * Getter for property bdyChargesAmount
     */
    public BigDecimal getBdyChargesAmount() {
        return bdyChargesAmount;
    }
    /**
     * Getter for property bdyConvertedChargesAmount
     */
    public BigDecimal getBdyConvertedChargesAmount() {
        return bdyConvertedChargesAmount;
    }
    /**
     * Getter for property bdyDefaultCurrencyCode
     */
    public String getBdyDefaultCurrencyCode() {
        return bdyDefaultCurrencyCode;
    }
    /**
     * Getter for property bdyRemark1
     */
    public String getBdyRemark1() {
        return bdyRemark1;
    }
    /**
     * Getter for property bdyRemark2
     */
    public String getBdyRemark2() {
        return bdyRemark2;
    }
    /**
     * Getter for property bdyRemark3
     */
    public String getBdyRemark3() {
        return bdyRemark3;
    }
    /**
     * Getter for property bdyRemark4
     */
    public String getBdyRemark4() {
        return bdyRemark4;
    }
    /**
     * Getter for property bdyDebitCurrency
     */
    public String getBdyDebitCurrency() {
        return bdyDebitCurrency;
    }
    /**
     * Getter for property bdyCreditCurrency
     */
    public String getBdyCreditCurrency() {
        return bdyCreditCurrency;
    }
    /**
     * Getter for property bdyChargesCurrency
     */
    public String getBdyChargesCurrency() {
        return bdyChargesCurrency;
    }
    /**
     * Setter for property header
     */
    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    /**
     * Setter for property securityHeader
     */
    public void setSecurityHeader(SecurityHeader securityHeader) {
        this.securityHeader = securityHeader;
    }
    /**
     * Setter for property bdyDebitAccount
     */
    public void setBdyDebitAccount(BigDecimal bdyDebitAccount) {
        this.bdyDebitAccount = bdyDebitAccount;
    }
    /**
     * Setter for property bdyDebitAmount
     */
    public void setBdyDebitAmount(BigDecimal bdyDebitAmount) {
        this.bdyDebitAmount = bdyDebitAmount;
    }
    /**
     * Setter for property bdyIbtBuyRate
     */
    public void setBdyIbtBuyRate(BigDecimal bdyIbtBuyRate) {
        this.bdyIbtBuyRate = bdyIbtBuyRate;
    }
    /**
     * Setter for property bdyIbtSellRate
     */
    public void setBdyIbtSellRate(BigDecimal bdyIbtSellRate) {
        this.bdyIbtSellRate = bdyIbtSellRate;
    }
    /**
     * Setter for property bdyTtBuyRate
     */
    public void setBdyTtBuyRate(BigDecimal bdyTtBuyRate) {
        this.bdyTtBuyRate = bdyTtBuyRate;
    }
    /**
     * Setter for property bdyTtSellRate
     */
    public void setBdyTtSellRate(BigDecimal bdyTtSellRate) {
        this.bdyTtSellRate = bdyTtSellRate;
    }
    /**
     * Setter for property bdyCreditAccount
     */
    public void setBdyCreditAccount(long bdyCreditAccount) {
        this.bdyCreditAccount = bdyCreditAccount;
    }
    /**
     * Setter for property bdyCreditAmount
     */
    public void setBdyCreditAmount(BigDecimal bdyCreditAmount) {
        this.bdyCreditAmount = bdyCreditAmount;
    }
    /**
     * Setter for property bdyValueDate
     */
    public void setBdyValueDate(String bdyValueDate) {
        this.bdyValueDate = bdyValueDate;
    }
    /**
     * Setter for property bdyChargesAmount
     */
    public void setBdyChargesAmount(BigDecimal bdyChargesAmount) {
        this.bdyChargesAmount = bdyChargesAmount;
    }
    /**
     * Setter for property bdyConvertedChargesAmount
     */
    public void setBdyConvertedChargesAmount(BigDecimal bdyConvertedChargesAmount) {
        this.bdyConvertedChargesAmount = bdyConvertedChargesAmount;
    }
    /**
     * Setter for property bdyDefaultCurrencyCode
     */
    public void setBdyDefaultCurrencyCode(String bdyDefaultCurrencyCode) {
        this.bdyDefaultCurrencyCode = bdyDefaultCurrencyCode;
    }
    /**
     * Setter for property bdyRemark1
     */
    public void setBdyRemark1(String bdyRemark1) {
        this.bdyRemark1 = bdyRemark1;
    }
    /**
     * Setter for property bdyRemark2
     */
    public void setBdyRemark2(String bdyRemark2) {
        this.bdyRemark2 = bdyRemark2;
    }
    /**
     * Setter for property bdyRemark3
     */
    public void setBdyRemark3(String bdyRemark3) {
        this.bdyRemark3 = bdyRemark3;
    }
    /**
     * Setter for property bdyRemark4
     */
    public void setBdyRemark4(String bdyRemark4) {
        this.bdyRemark4 = bdyRemark4;
    }
    /**
     * Setter for property bdyDebitCurrency
     */
    public void setBdyDebitCurrency(String bdyDebitCurrency) {
        this.bdyDebitCurrency = bdyDebitCurrency;
    }
    /**
     * Setter for property bdyCreditCurrency
     */
    public void setBdyCreditCurrency(String bdyCreditCurrency) {
        this.bdyCreditCurrency = bdyCreditCurrency;
    }
    /**
     * Setter for property bdyChargesCurrency
     */
    public void setBdyChargesCurrency(String bdyChargesCurrency) {
        this.bdyChargesCurrency = bdyChargesCurrency;
    }
    
}
