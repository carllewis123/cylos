/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.client;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class SecurityHeader implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -7126824694856465017L;
    private String hdrChallengeType;
    private String hdrCorporateId;
    private String hdrCustomerAdditionalId;
    private String hdrAppliNo;
    private String hdrChallenge1;
    private String hdrChallenge2;
    private String hdrChallenge3;
    private String hdrChallenge4;
    private String hdrResponseToken;
    /**
     * Getter for property hdrChallengeType
     */
    public String getHdrChallengeType() {
        return hdrChallengeType;
    }
    /**
     * Getter for property hdrCorporateId
     */
    public String getHdrCorporateId() {
        return hdrCorporateId;
    }
    /**
     * Getter for property hdrCustomerAdditionalId
     */
    public String getHdrCustomerAdditionalId() {
        return hdrCustomerAdditionalId;
    }
    /**
     * Getter for property hdrAppliNo
     */
    public String getHdrAppliNo() {
        return hdrAppliNo;
    }
    /**
     * Getter for property hdrChallenge1
     */
    public String getHdrChallenge1() {
        return hdrChallenge1;
    }
    /**
     * Getter for property hdrChallenge2
     */
    public String getHdrChallenge2() {
        return hdrChallenge2;
    }
    /**
     * Getter for property hdrChallenge3
     */
    public String getHdrChallenge3() {
        return hdrChallenge3;
    }
    /**
     * Getter for property hdrChallenge4
     */
    public String getHdrChallenge4() {
        return hdrChallenge4;
    }
    /**
     * Getter for property hdrResponseToken
     */
    public String getHdrResponseToken() {
        return hdrResponseToken;
    }
    /**
     * Setter for property hdrChallengeType
     */
    public void setHdrChallengeType(String hdrChallengeType) {
        this.hdrChallengeType = hdrChallengeType;
    }
    /**
     * Setter for property hdrCorporateId
     */
    public void setHdrCorporateId(String hdrCorporateId) {
        this.hdrCorporateId = hdrCorporateId;
    }
    /**
     * Setter for property hdrCustomerAdditionalId
     */
    public void setHdrCustomerAdditionalId(String hdrCustomerAdditionalId) {
        this.hdrCustomerAdditionalId = hdrCustomerAdditionalId;
    }
    /**
     * Setter for property hdrAppliNo
     */
    public void setHdrAppliNo(String hdrAppliNo) {
        this.hdrAppliNo = hdrAppliNo;
    }
    /**
     * Setter for property hdrChallenge1
     */
    public void setHdrChallenge1(String hdrChallenge1) {
        this.hdrChallenge1 = hdrChallenge1;
    }
    /**
     * Setter for property hdrChallenge2
     */
    public void setHdrChallenge2(String hdrChallenge2) {
        this.hdrChallenge2 = hdrChallenge2;
    }
    /**
     * Setter for property hdrChallenge3
     */
    public void setHdrChallenge3(String hdrChallenge3) {
        this.hdrChallenge3 = hdrChallenge3;
    }
    /**
     * Setter for property hdrChallenge4
     */
    public void setHdrChallenge4(String hdrChallenge4) {
        this.hdrChallenge4 = hdrChallenge4;
    }
    /**
     * Setter for property hdrResponseToken
     */
    public void setHdrResponseToken(String hdrResponseToken) {
        this.hdrResponseToken = hdrResponseToken;
    }
    
}
