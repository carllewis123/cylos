/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;

import ptdam.emoney.webservice.client.ResponseHeaderExtended;
import ptdam.emoney.webservice.client.SecurityHeader;
import ptdam.emoney.webservice.member.Status;

/**
 * Parameters used to update members
 * 
 * @author ptdam
 */
public class SettlementPaymentResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1001635585938101323L;
	private ResponseHeaderExtended responseHeader;
    private SecurityHeader securityHeader;
	private Status status;

    /**
     * Getter for property status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Setter for property status
     */
    public void setStatus(Status status) {
        this.status = status;
    }
    
    public SecurityHeader getSecurityHeader() {
		return securityHeader;
	}

	public void setSecurityHeader(SecurityHeader securityHeader) {
		this.securityHeader = securityHeader;
	}

	public ResponseHeaderExtended getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeaderExtended responseHeader) {
		this.responseHeader = responseHeader;
	}


}
