/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class TopupInquiryRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 7620355201957480608L;
    private String emoneyAccountNo;
    private String channelId;
    private String companyCode;
    private String topupAmount;

    /**
     * Getter for property channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * Getter for property companyCode
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Setter for property channelId
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * Setter for property companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }

    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }

    public String getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(String topupAmount) {
        this.topupAmount = topupAmount;
    }
    
}
