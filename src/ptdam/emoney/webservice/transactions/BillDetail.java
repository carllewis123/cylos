/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class BillDetail implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 1395118468838865370L;

    private java.lang.String billCode;

    private java.lang.String billName;

    private java.lang.String billShortName;

    private java.lang.String billAmount;

    private java.lang.String reference1;

    private java.lang.String reference2;

    private java.lang.String reference3;

    public BillDetail() {
    }

    public BillDetail(
           java.lang.String billCode,
           java.lang.String billName,
           java.lang.String billShortName,
           java.lang.String billAmount,
           java.lang.String reference1,
           java.lang.String reference2,
           java.lang.String reference3) {
           this.billCode = billCode;
           this.billName = billName;
           this.billShortName = billShortName;
           this.billAmount = billAmount;
           this.reference1 = reference1;
           this.reference2 = reference2;
           this.reference3 = reference3;
    }

    /**
     * Getter for property billCode
     */
    public java.lang.String getBillCode() {
        return billCode;
    }

    /**
     * Getter for property billName
     */
    public java.lang.String getBillName() {
        return billName;
    }

    /**
     * Getter for property billShortName
     */
    public java.lang.String getBillShortName() {
        return billShortName;
    }

    /**
     * Getter for property billAmount
     */
    public java.lang.String getBillAmount() {
        return billAmount;
    }

    /**
     * Getter for property reference1
     */
    public java.lang.String getReference1() {
        return reference1;
    }

    /**
     * Getter for property reference2
     */
    public java.lang.String getReference2() {
        return reference2;
    }

    /**
     * Getter for property reference3
     */
    public java.lang.String getReference3() {
        return reference3;
    }

    /**
     * Setter for property billCode
     */
    public void setBillCode(java.lang.String billCode) {
        this.billCode = billCode;
    }

    /**
     * Setter for property billName
     */
    public void setBillName(java.lang.String billName) {
        this.billName = billName;
    }

    /**
     * Setter for property billShortName
     */
    public void setBillShortName(java.lang.String billShortName) {
        this.billShortName = billShortName;
    }

    /**
     * Setter for property billAmount
     */
    public void setBillAmount(java.lang.String billAmount) {
        this.billAmount = billAmount;
    }

    /**
     * Setter for property reference1
     */
    public void setReference1(java.lang.String reference1) {
        this.reference1 = reference1;
    }

    /**
     * Setter for property reference2
     */
    public void setReference2(java.lang.String reference2) {
        this.reference2 = reference2;
    }

    /**
     * Setter for property reference3
     */
    public void setReference3(java.lang.String reference3) {
        this.reference3 = reference3;
    }
    
    
}
