/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class TopupReversalRequestExtended implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = -4151858321567538624L;
    private String emoneyAccountNo;
    private String topupAmount;
    private String transactionId;
    private String channelId;
    private String companyCode;
    private String reversalReason;
    
    private String language;
    private String trxDateTime;
    private String transmissionDateTime;
    private String reference1;
    private String reference2;
    private String reference3;
    private String terminalID;
    private String currency;
    
    private String origTrxDateTime;
    private String origTransmissionDateTime;
    
    /**
     * Getter for property reversalReason
     */
    public String getReversalReason() {
        return reversalReason;
    }
    /**
     * Setter for property reversalReason
     */
    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }
    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }
    /**
     * Getter for property topupAmount
     */
    public String getTopupAmount() {
        return topupAmount;
    }
    /**
     * Getter for property transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    /**
     * Getter for property channelId
     */
    public String getChannelId() {
        return channelId;
    }
    /**
     * Getter for property companyCode
     */
    public String getCompanyCode() {
        return companyCode;
    }
    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }
    /**
     * Setter for property topupAmount
     */
    public void setTopupAmount(String topupAmount) {
        this.topupAmount = topupAmount;
    }
    /**
     * Setter for property transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    /**
     * Setter for property channelId
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    /**
     * Setter for property companyCode
     */
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
    public String getLanguage() {
        return language;
    }
    public void setLanguage(String language) {
        this.language = language;
    }
    public String getTrxDateTime() {
        return trxDateTime;
    }
    public void setTrxDateTime(String trxDateTime) {
        this.trxDateTime = trxDateTime;
    }
    public String getTransmissionDateTime() {
        return transmissionDateTime;
    }
    public void setTransmissionDateTime(String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }
    public String getReference1() {
        return reference1;
    }
    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }
    public String getReference2() {
        return reference2;
    }
    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }
    public String getReference3() {
        return reference3;
    }
    public void setReference3(String reference3) {
        this.reference3 = reference3;
    }
    public String getTerminalID() {
        return terminalID;
    }
    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getOrigTrxDateTime() {
        return origTrxDateTime;
    }
    public void setOrigTrxDateTime(String origTrxDateTime) {
        this.origTrxDateTime = origTrxDateTime;
    }
    public String getOrigTransmissionDateTime() {
        return origTransmissionDateTime;
    }
    public void setOrigTransmissionDateTime(String origTransmissionDateTime) {
        this.origTransmissionDateTime = origTransmissionDateTime;
    }
    
}
