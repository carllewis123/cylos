/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class CashOutMsgReversalRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 4286134755341169830L;
    private String emoneyAccountNo;
    private String terminalId;
    private String retrievalReferenceNo;
    private String localTransactionDate;
    private String localTransactionTime;
    private String reversalReason;
    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }
    /**
     * Getter for property terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }
    /**
     * Getter for property retrievalReferenceNo
     */
    public String getRetrievalReferenceNo() {
        return retrievalReferenceNo;
    }
    /**
     * Getter for property localTransactionDate
     */
    public String getLocalTransactionDate() {
        return localTransactionDate;
    }
    /**
     * Getter for property localTransactionTime
     */
    public String getLocalTransactionTime() {
        return localTransactionTime;
    }
    /**
     * Getter for property reversalReason
     */
    public String getReversalReason() {
        return reversalReason;
    }
    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }
    /**
     * Setter for property terminalId
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
    /**
     * Setter for property retrievalReferenceNo
     */
    public void setRetrievalReferenceNo(String retrievalReferenceNo) {
        this.retrievalReferenceNo = retrievalReferenceNo;
    }
    /**
     * Setter for property localTransactionDate
     */
    public void setLocalTransactionDate(String localTransactionDate) {
        this.localTransactionDate = localTransactionDate;
    }
    /**
     * Setter for property localTransactionTime
     */
    public void setLocalTransactionTime(String localTransactionTime) {
        this.localTransactionTime = localTransactionTime;
    }
    /**
     * Setter for property reversalReason
     */
    public void setReversalReason(String reversalReason) {
        this.reversalReason = reversalReason;
    }
 
    
    
}
