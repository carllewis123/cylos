/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.jws.WebService;

import nl.strohalm.cyclos.entities.access.Channel;
import nl.strohalm.cyclos.entities.accounts.AccountOwner;
import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.entities.accounts.LockedAccountsOnPayments;
import nl.strohalm.cyclos.entities.accounts.SystemAccountOwner;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferType;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.services.ServiceClient;
import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.exceptions.PermissionDeniedException;
import nl.strohalm.cyclos.services.access.AccessServiceLocal;
import nl.strohalm.cyclos.services.access.exceptions.BlockedCredentialsException;
import nl.strohalm.cyclos.services.access.exceptions.InvalidCredentialsException;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.application.ApplicationServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementService;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.transactions.DoPaymentDTO;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.utils.validation.ValidationException;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.model.AccountHistoryTransferVO;
import nl.strohalm.cyclos.webservices.model.AccountStatusVO;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.payments.PaymentParameters;
import nl.strohalm.cyclos.webservices.payments.PaymentResult;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.payments.PaymentStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.PaymentHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ptdam.emoney.EmoneyConfiguration;

/**
 * Web service implementation
 * @author ptdam
 */
@WebService(name = "coresettlement", serviceName = "coresettlement")
public class SettlementWebServiceImpl implements SettlementWebService {
    private ElementServiceLocal           elementServiceLocal;
    private ChannelHelper                 channelHelper;
    private AccountHelper                 accountHelper;
    private PaymentHelper                     paymentHelper;
    private WebServiceHelper                  webServiceHelper;
    private PaymentServiceLocal               paymentServiceLocal;
    private ApplicationServiceLocal           applicationServiceLocal;
    private AccessServiceLocal                accessServiceLocal;
    private AccountServiceLocal           accountServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private ElementService    elementService;
    private final Log               logger    = LogFactory.getLog(SettlementWebServiceImpl.class);
    
//    private String     RESPONSE_REVERSE_SUCCESS = "00";
 
    public SettlementWebServiceImpl() {
        
    }
  
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }
    
    public void setPaymentHelper(final PaymentHelper paymentHelper) {
        this.paymentHelper = paymentHelper;
    }
    
    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    public void setApplicationServiceLocal(final ApplicationServiceLocal applicationServiceLocal) {
        this.applicationServiceLocal = applicationServiceLocal;
    }

    public void setAccessServiceLocal(AccessServiceLocal accessServiceLocal) {
        this.accessServiceLocal = accessServiceLocal;
    }
    
    public final void setElementService(final ElementService elementService) {
        this.elementService = elementService;
    }
    
	@Override
	 public PaymentResult_Extended feeSettlement(final SettlementPaymentRequest input) {
	        final PaymentWebServiceImpl payment = new PaymentWebServiceImpl();
	        final PaymentParameters paymentParams = new PaymentParameters();
	        PaymentResult_Extended result = new PaymentResult_Extended();
	        String errorMsg = "";
	        try {
	            paymentParams.setFromSystem(true);
                paymentParams.setToSystem(true);
	            paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
	            
	            if (input.getDesc() != null) {
	                paymentParams.setDescription(input.getDesc());
	            }
	            
	            try {
	                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("fee_settlement.transferID." + input.getTrxtype().toLowerCase())));
	            } catch (final IOException e) {
	                logger.error(e);
	            }
	            
	            String customFieldName = "";
	            String customFieldName2 = "";
	            String customValue = "";
	            String customValue2 = "";
	            try {
	                customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
	                customValue = input.getField2() != null ? input.getField2() : "";;
	                customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
	                customValue2 = input.getField1() != null ? input.getField1() : "";
	                
	            } catch (final IOException e) {
	                logger.error(e);
	            }
	            
	            if (customFieldName != null && !customFieldName.isEmpty() && customFieldName2 != null && !customFieldName2.isEmpty()) {
	            
	                FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
	                List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
	                
	                FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
	                ArrayList addCustomValues = new ArrayList(customValues);
	                addCustomValues.add(customFieldValue2);
	                paymentParams.setCustomValues(addCustomValues);
	            
	            }
	            
	            payment.setWebServiceHelper(webServiceHelper);
	            payment.setPaymentHelper(paymentHelper);
	            payment.setPaymentServiceLocal(paymentServiceLocal);
	            payment.setAccountHelper(accountHelper);
	            payment.setApplicationServiceLocal(applicationServiceLocal);
	            payment.setElementServiceLocal(elementServiceLocal);
	            payment.setChannelHelper(channelHelper);
	            payment.setAccessServiceLocal(accessServiceLocal);
	            
	            result = payment.doPaymentExtended(paymentParams);
		        if(result != null && result.getField1() != null && !result.getField1().equals("")){
		        	errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
		        	if(errorMsg == null){
		        		errorMsg = result.getField1();
		        	}
		        }
	            result.setField1(errorMsg);
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
	        return result;
	    }
	
	@Override
	 public PaymentResult_Extended merchantSettlementToSystem(final SettlementPaymentRequest input) {
	        final PaymentParameters paymentParams = new PaymentParameters();
	        PaymentResult_Extended result = new PaymentResult_Extended();
	        String errorMsg = "";
	        try {
	        	paymentParams.setToSystem(true);
	            paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
	            paymentParams.setTraceNumber(input.getField1() + input.getField2());
	            
	            if (input.getDesc() != null) {
	                paymentParams.setDescription(input.getDesc());
	            }
	            
	            String customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
	            String customValue = input.getField2() != null ? input.getField2() : "";
		        if (customFieldName != null && !customFieldName.isEmpty()) {
		            FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
		            List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
		            paymentParams.setCustomValues(customValues);
		        
		        }
	            
                paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("merchant_settlement.transfertypeid.tosystem")));
                long memberId = Long.parseLong(input.getField1());
                Member member = getMember(memberId);
                if(member!=null){
     		       paymentParams.setFromMember(member.getUser().getUsername());
     	    	}
                result = doPayment(paymentParams);
                if(!result.getStatus().equals(PaymentStatus.PROCESSED)){
                	errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("paymentstatus." + result.getStatus().toString());
                	if(errorMsg == null){
		        		errorMsg = result.getStatus().toString();
		        	}
                }
                result.setField1(errorMsg);
	        } catch (final IOException e) {
                logger.error(e);
	        } catch (final ExternalException e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        } catch (final Exception e) {
	            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);
	        }
	        
	        return result;
	    }
	
	@Override
	public PaymentResult_Extended merchantSettlementToMember(final SettlementPaymentRequest input) {
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        String customFieldName = "";
        String customFieldName2 = "";
        String customValue = "";
        String customValue2 = "";
        String errorMsg = "";
        try {
	        paymentParams.setFromSystem(true);
	        paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
	        if (input.getDesc() != null) {
	            paymentParams.setDescription(input.getDesc());
	        }
	        	paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("merchant_settlement.transfertypeid.tomember")));
	        	customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
	            String customFieldRekening = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.rekening");
	            long memberId = Long.parseLong(input.getField1());
	            Member member = getMember(memberId);
	            if(member!=null){
	 		       Collection<MemberCustomFieldValue> memberCustomFields = member.getCustomValues();
	 		       Iterator itMember = memberCustomFields.iterator();
	 		       paymentParams.setToMember(member.getUser().getUsername());
	 		       while(itMember.hasNext()){
	 			       	MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember.next();
	 			       	if(mCustomField.getField().getInternalName().equals(customFieldRekening)){
	 			       		customValue = mCustomField.getValue();
	 			       		break;
	 			       	}
	 		       }
	 	    	}
	            
	            customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
	            customValue2 = input.getField2() != null ? input.getField2() : "";
	        
	        
	        if (customFieldName != null && !customFieldName.isEmpty() && customFieldName2 != null && !customFieldName2.isEmpty()) {
	        
	            FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
	            List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
	            
	            FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
	            ArrayList addCustomValues = new ArrayList(customValues);
	            addCustomValues.add(customFieldValue2);
	            paymentParams.setCustomValues(addCustomValues);
	        
	        }
	        
	        result = doPayment(paymentParams);
	        if(result != null && result.getField1() != null && !result.getField1().equals("")){
	        	errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
	        	if(errorMsg == null){
	        		errorMsg = result.getField1();
	        	}
	        }
            result.setField1(errorMsg);
            
        } catch (final IOException e) {
        	result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);  
	    } catch (final ExternalException e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    } catch (final Exception e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    }
        return result;
	}
	
	@Override
	public PaymentResult_Extended merchantReverseSettlement(final SettlementPaymentRequest input) {
		PaymentResult result = new PaymentResult();
		PaymentStatus status = null;
		String errorCode = "";
		try {
            Transfer transfer = paymentServiceLocal.loadTransferForReverse(input.getField1() + input.getField2());

            if (transfer != null) {
                Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                status = PaymentStatus.PROCESSED;
            }else{
            	status = PaymentStatus.INVALID_PARAMETERS;
            }
        } catch (Exception e) {
        	status = PaymentStatus.UNKNOWN_ERROR;
            logger.error(e);
            errorCode = e.getMessage();
        }
        finally {
        	result.setStatus(status);
        }
		
		PaymentResult_Extended resultExt = new PaymentResult_Extended(result.getStatus(), result.getTransfer(), result.getFromAccountStatus(), result.getToAccountStatus());
        resultExt.setField1(errorCode);
        return resultExt;
		
	}
	
	@Override
	public PaymentResult_Extended agentSettlementToSystem(final SettlementPaymentRequest input) {
        final PaymentParameters paymentParams = new PaymentParameters();
        PaymentResult_Extended result = new PaymentResult_Extended();
        String errorMsg = "";
        try {
        	paymentParams.setToSystem(true);
            paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
            paymentParams.setTraceNumber(input.getField1() + input.getField2());
            
            if (input.getDesc() != null) {
                paymentParams.setDescription(input.getDesc());
            }
            
            String customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
            String customValue = input.getField2() != null ? input.getField2() : "";
	        if (customFieldName != null && !customFieldName.isEmpty()) {
	            FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
	            List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
	            paymentParams.setCustomValues(customValues);
	        
	        }
            
            paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("agent_settlement.transfertypeid.tosystem")));
                long memberId = Long.parseLong(input.getField1());
                Member member = getMember(memberId);
                if(member!=null){
     		       paymentParams.setFromMember(member.getUser().getUsername());
     	    	}
            result = doPayment(paymentParams);
            if(!result.getStatus().equals(PaymentStatus.PROCESSED)){
            	errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("paymentstatus." + result.getStatus().toString());
            	if(errorMsg == null){
	        		errorMsg = result.getStatus().toString();
	        	}
            }
            result.setField1(errorMsg);
            } catch (final IOException e) {
                logger.error(e);
        } catch (final ExternalException e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        } catch (final Exception e) {
            result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);
        }
        
        return result;
    }
	
	@Override
	public PaymentResult_Extended agentSettlementToMember(final SettlementPaymentRequest input) {
	       final PaymentParameters paymentParams = new PaymentParameters();
	        PaymentResult_Extended result = new PaymentResult_Extended();
	       String customFieldName = "";
	       String customFieldName2 = "";
	       String customValue = "";
	       String customValue2 = "";
	        String errorMsg = "";
	       try {
		        paymentParams.setFromSystem(true);
		        paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
		        if (input.getDesc() != null) {
		            paymentParams.setDescription(input.getDesc());
		        }
		        	paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("agent_settlement.transfertypeid.tomember")));
		        	customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.norektujuan.internalname");
		            String customFieldRekening = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.rekening");
		            long memberId = Long.parseLong(input.getField1());
		            Member member = getMember(memberId);
		            if(member!=null){
		 		       Collection<MemberCustomFieldValue> memberCustomFields = member.getCustomValues();
		 		       Iterator itMember = memberCustomFields.iterator();
		 		       paymentParams.setToMember(member.getUser().getUsername());
		 		       while(itMember.hasNext()){
		 			       	MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember.next();
		 			       	if(mCustomField.getField().getInternalName().equals(customFieldRekening)){
		 			       		customValue = mCustomField.getValue();
		 			       		break;
		 			       	}
		 		       }
		 	    	}
		            
		            customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("settlement.customfield.sequence");
		            customValue2 = input.getField2() != null ? input.getField2() : "";
		            
		        
		        if (customFieldName != null && !customFieldName.isEmpty() && customFieldName2 != null && !customFieldName2.isEmpty()) {
		        
		            FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
		            List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
		            
		            FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
		            ArrayList addCustomValues = new ArrayList(customValues);
		            addCustomValues.add(customFieldValue2);
		            paymentParams.setCustomValues(addCustomValues);
		        
		        }
		        
		        result = doPayment(paymentParams);
		        if(result != null && result.getField1() != null && !result.getField1().equals("")){
		        	errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
		        	if(errorMsg == null){
		        		errorMsg = result.getField1();
		        	}
		        }
	            result.setField1(errorMsg);
		        
	        } catch (final IOException e) {
	        	result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	            logger.error(e);  
		    } catch (final ExternalException e) {
		        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
		        logger.error(e);
		    } catch (final Exception e) {
		        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
		        logger.error(e);
		    }
	        return result;
		}
	
	public MemberCustomField getCustomField(final String internalName, final Collection<MemberCustomField> customFields) {
    	
    	for (MemberCustomField field : customFields) {
    		if (field.getInternalName().equals(internalName)) {
    			return field;
    		}
    	}
    	
    	return null;
    }
	
	
	public Member getMember(long memberId) {
        Member member = null;

        // Load the member to send to, if any
        if (memberId > 0L) {
            // Ensure a member
            final Element element = elementService.load(memberId, Element.Relationships.USER);
            if (!(element instanceof Member)) {
                throw new ValidationException();
            }
            member = (Member) element;
        }

        return member;
    }
	
	private PaymentResult_Extended doPayment(final PaymentParameters params) {
        AccountHistoryTransferVO transferVO = null;
        Transfer transfer = null;
        PaymentStatus status = null;
        AccountStatusVO fromMemberStatus = null;
        AccountStatusVO toMemberStatus = null;
        String errorCode = "";

        try {
            final PrepareParametersResult result = prepareParameters(params);
            status = result.getStatus();

            if (status == null) {
                // Status null means no "pre-payment" errors (like validation, pin, channel...)
                // Perform the payment
                final DoPaymentDTO dto = paymentHelper.toExternalPaymentDTO(params, result.getFrom(), result.getTo());

                // Validate the transfer type
                if (!validateTransferType(dto)) {
                    status = PaymentStatus.INVALID_PARAMETERS;
                    webServiceHelper.trace(status + ". Reason: The service client doesn't have permission to the specified transfer type: " + dto.getTransferType());
                } else {
                    transfer = (Transfer) paymentServiceLocal.doPayment(dto);
                    status = paymentHelper.toStatus(transfer);
                    transferVO = accountHelper.toVO(WebServiceContext.getMember(), transfer, null, result.getFromRequiredFields(), result.getToRequiredFields());
                    final AccountStatusVO[] statuses = getAccountStatusesForPayment(params, transfer);
                    fromMemberStatus = statuses[0];
                    toMemberStatus = statuses[1];
                }
            }
        } catch (final Exception e) {
            webServiceHelper.error(e);
            if (applicationServiceLocal.getLockedAccountsOnPayments() == LockedAccountsOnPayments.NONE) {
                // The payment is committed on the same transaction so it will be rolled back by this exception, then status is given by this
                // exception.
                status = paymentHelper.toStatus(e);
            } else if (status == null) {
                /*
                 * The payment is committed on a new transaction. So this exception won't roll back the payment. The status sholuldn't be modified by
                 * this exception unless there isn't a status, in which case return this exception.
                 */
                status = paymentHelper.toStatus(e);
            }
            errorCode = e.getMessage();
        }

        if (!status.isSuccessful()) {
            webServiceHelper.error("Payment error status: " + status);
        }

        PaymentResult_Extended result = new PaymentResult_Extended(status, transferVO, fromMemberStatus, toMemberStatus);

        result.setField1(errorCode);

        return result;
    }
	
	private static class PrepareParametersResult {
        private final PaymentStatus                 status;
        private final AccountOwner                  from;
        private final AccountOwner                  to;
        private final Collection<MemberCustomField> fromRequiredFields;
        private final Collection<MemberCustomField> toRequiredFields;

        public PrepareParametersResult(final PaymentStatus status, final AccountOwner from, final AccountOwner to, final Collection<MemberCustomField> fromRequiredFields, final Collection<MemberCustomField> toRequiredFields) {
            this.status = status;
            this.from = from;
            this.to = to;
            this.fromRequiredFields = fromRequiredFields;
            this.toRequiredFields = toRequiredFields;
        }

        public AccountOwner getFrom() {
            return from;
        }

        public Collection<MemberCustomField> getFromRequiredFields() {
            return fromRequiredFields;
        }

        public PaymentStatus getStatus() {
            return status;
        }

        public AccountOwner getTo() {
            return to;
        }

        public Collection<MemberCustomField> getToRequiredFields() {
            return toRequiredFields;
        }
    }

    private PrepareParametersResult prepareParameters(final PaymentParameters params) {

        final Member restricted = WebServiceContext.getMember();
        final boolean fromSystem = params.getFromSystem();
        final boolean toSystem = params.getToSystem();
        PaymentStatus status = null;
        Member fromMember = null;
        Member toMember = null;
        // Load the from member
        if (!fromSystem) {
            try {
                fromMember = paymentHelper.resolveFromMember(params);
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.FROM_NOT_FOUND;
            }
        }
        // Load the to member
        if (status == null && !toSystem) {
            try {
                toMember = paymentHelper.resolveToMember(params);
                
                String dormantGroup="";
            	try {
					dormantGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("dormant.group.name");
				} catch (IOException e) {
					dormantGroup = "DORMANT";
					e.printStackTrace();
				}
            	
            	if(toMember.getMemberGroup()!= null && toMember.getMemberGroup().getName().equalsIgnoreCase(dormantGroup)){
            		status = PaymentStatus.TO_MEMBER_IS_DORMANT;
            	}
                
            } catch (final EntityNotFoundException e) {
                webServiceHelper.error(e);
                status = PaymentStatus.TO_NOT_FOUND;
            }
        }

        if (status == null) {
            if (restricted == null) {
                // Ensure has the do payment permission
                if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                    throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                }
                // Check the channel immediately, as needed by SMS controller
                if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                    status = PaymentStatus.INVALID_CHANNEL;
                }
            } else {
                // Enforce the restricted to member parameters
                if (fromSystem) {
                    // Restricted to member can't perform payment from system
                    status = PaymentStatus.FROM_NOT_FOUND;
                } else {
                    if (fromMember == null) {
                        fromMember = restricted;
                    } else if (toMember == null && !toSystem) {
                        toMember = restricted;
                    }
                }
                if (status == null) {
                    // Check make / receive payment permissions
                    if (fromMember.equals(restricted)) {
                        if (!WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                        }
                    } else {
                        if (!WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                            throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                        }
                    }
                    // Ensure that either from or to member is the restricted one
                    if (!fromMember.equals(restricted) && !toMember.equals(restricted)) {
                        status = PaymentStatus.INVALID_PARAMETERS;
                        webServiceHelper.trace(status + ". Reason: Neither the origin nor the destination members are equal to the restricted: " + restricted);
                    }
                }
                if (status == null) {
                    // Enforce the permissions
                    if (restricted.equals(fromMember) && !WebServiceContext.hasPermission(ServiceOperation.DO_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.DO_PAYMENT);
                    } else if (restricted.equals(toMember) && !WebServiceContext.hasPermission(ServiceOperation.RECEIVE_PAYMENT)) {
                        throw new PermissionDeniedException("The service client doesn't have the following permission: " + ServiceOperation.RECEIVE_PAYMENT);
                    }
                }
            }
        }

        // Ensure both from and to member are present
        if (status == null) {
            if (fromMember == null && !fromSystem) {
                status = PaymentStatus.FROM_NOT_FOUND;
            } else if (toMember == null && !toSystem) {
                status = PaymentStatus.TO_NOT_FOUND;
            } else if (fromMember != null && toMember != null) {
                // Ensure the to member is visible by the from member
                final Collection<MemberGroup> visibleGroups = fromMember.getMemberGroup().getCanViewProfileOfGroups();
                if (CollectionUtils.isEmpty(visibleGroups) || !visibleGroups.contains(toMember.getGroup())) {
                    status = PaymentStatus.TO_NOT_FOUND;
                }
            }
        }

        // Ensure required CF are present ONLY for unrestricted client
        Collection<MemberCustomField> fromMemberfields = null, toMemberfields = null;
        if (status == null) {
            boolean hasFromRequired = CollectionUtils.isNotEmpty(params.getFromMemberFieldsToReturn());
            boolean hasToRequired = CollectionUtils.isNotEmpty(params.getToMemberFieldsToReturn());
            if (restricted != null && (hasFromRequired || hasToRequired) || hasFromRequired && fromSystem || hasToRequired && toSystem) {
                webServiceHelper.trace(restricted != null ? "Restricted web service clients are not allowed to require member custom field values" : "Can't require custom field values for a system payment");
                status = PaymentStatus.INVALID_PARAMETERS;
            }
            if (status == null && hasFromRequired) {
                fromMemberfields = getMemberCustomFields(fromMember, params.getFromMemberFieldsToReturn());
                status = fromMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }

            if (status == null && hasToRequired) {
                toMemberfields = getMemberCustomFields(toMember, params.getToMemberFieldsToReturn());
                status = toMemberfields == null ? PaymentStatus.INVALID_PARAMETERS : null;
            }
        }

        if (status == null) {
            // Check the channel
            if (fromMember != null && !accessServiceLocal.isChannelEnabledForMember(channelHelper.restricted(), fromMember)) {
                status = PaymentStatus.INVALID_CHANNEL;
            }
        }
        if (status == null) {
            // Check the credentials
            boolean checkCredentials;
            if (restricted != null) {
                checkCredentials = !fromMember.equals(restricted);
            } else {
                checkCredentials = !fromSystem && WebServiceContext.getClient().isCredentialsRequired();
            }
            if (checkCredentials) {
                try {
                    checkCredentials(fromMember, WebServiceContext.getChannel(), params.getCredentials());
                } catch (final InvalidCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.INVALID_CREDENTIALS;
                } catch (final BlockedCredentialsException e) {
                    webServiceHelper.error(e);
                    status = PaymentStatus.BLOCKED_CREDENTIALS;
                }
            }
        }

        // No error
        final AccountOwner fromOwner = fromSystem ? SystemAccountOwner.instance() : fromMember;
        final AccountOwner toOwner = toSystem ? SystemAccountOwner.instance() : toMember;
        return new PrepareParametersResult(status, fromOwner, toOwner, fromMemberfields, toMemberfields);
    }
    
    private boolean validateTransferType(final DoPaymentDTO dto) {
        final Collection<TransferType> possibleTypes = paymentHelper.listPossibleTypes(dto);
        return possibleTypes != null && possibleTypes.contains(dto.getTransferType());
    }

    private AccountStatusVO[] getAccountStatusesForPayment(final PaymentParameters params, final Transfer transfer) {
        AccountStatus fromMemberStatus = null;
        AccountStatus toMemberStatus = null;
        if (WebServiceContext.getClient().getPermissions().contains(ServiceOperation.ACCOUNT_DETAILS) && params.getReturnStatus()) {
            if (WebServiceContext.getMember() == null) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            } else if (WebServiceContext.getMember().equals(paymentHelper.resolveFromMember(params))) {
                fromMemberStatus = transfer.isFromSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getFrom()));
            } else {
                toMemberStatus = transfer.isToSystem() ? null : accountServiceLocal.getCurrentStatus(new AccountDTO(transfer.getTo()));
            }
        }
        return new AccountStatusVO[] {
                accountHelper.toVO(fromMemberStatus),
                accountHelper.toVO(toMemberStatus) };
    }

    private Collection<MemberCustomField> getMemberCustomFields(final Member member, final List<String> fieldInternalNames) {
        Collection<MemberCustomField> fields = new HashSet<MemberCustomField>();

        for (final String internalName : fieldInternalNames) {
            MemberCustomFieldValue mcfv = (MemberCustomFieldValue) CollectionUtils.find(member.getCustomValues(), new Predicate() {
                @Override
                public boolean evaluate(final Object object) {
                    MemberCustomFieldValue mcfv = (MemberCustomFieldValue) object;
                    return mcfv.getField().getInternalName().equals(internalName);
                }
            });
            if (mcfv == null) {
                webServiceHelper.trace(String.format("Required field '%1$s' was not found for member %2$s", internalName, member));
                return null;
            } else {
                fields.add(memberCustomFieldServiceLocal.load(mcfv.getField().getId()));
            }
        }

        return fields;
    }

    private void checkCredentials(Member member, final Channel channel, final String credentials) {
        if (member == null) {
            return;
        }
        final ServiceClient client = WebServiceContext.getClient();
        final Member restrictedMember = client.getMember();
        if (restrictedMember == null) {
            // Non-restricted clients use the flag credentials required
            if (!client.isCredentialsRequired()) {
                // No credentials should be checked
                throw new InvalidCredentialsException();
            }
        } else {
            // Restricted clients don't need check if is the same member
            if (restrictedMember.equals(member)) {
                throw new InvalidCredentialsException();
            }
        }
        if (StringUtils.isEmpty(credentials)) {
            throw new InvalidCredentialsException();
        }
        member = elementServiceLocal.load(member.getId(), Element.Relationships.USER);
        accessServiceLocal.checkCredentials(channel, member.getMemberUser(), credentials, WebServiceContext.getRequest().getRemoteAddr(), WebServiceContext.getMember());
    }
    
    @Override
    public PaymentResult_Extended member_payroll(final SettlementPaymentRequest input) {
    	final PaymentParameters paymentParams = new PaymentParameters();
    	PaymentResult_Extended result = new PaymentResult_Extended();
        String customFieldName = "";
       	String customFieldName2 = "";
       	String customValue = "";
       	String customValue2 = "";
        String errorMsg = "";
        try {
			paymentParams.setFromSystem(true);
			paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
			if (input.getDesc() != null) {
			    paymentParams.setDescription(input.getDesc());
			}
			
			if (input.getField3() != null) {
				paymentParams.setToMember(input.getField3());
			}
				paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("member.payroll.transfertypeid")));
			    
				customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.transaction.id.internal.name");
			    customValue = input.getField1() != null ? input.getField1() : "";
			    
				customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.settle.sequence.internal.name");
			    customValue2 = input.getField2() != null ? input.getField2() : "";
			    
			if (customFieldName != null && !customFieldName.isEmpty() 
					&& customFieldName2 != null && !customFieldName2.isEmpty()){
			
			    FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
			    	List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
			    FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
			    ArrayList addCustomValues = new ArrayList(customValues);
			    addCustomValues.add(customFieldValue2);
			    paymentParams.setCustomValues(addCustomValues);
			
			}
			
			result = doPayment(paymentParams);
			if(result != null && result.getField1() != null && !result.getField1().equals("")){
				errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
				if(errorMsg == null){
					errorMsg = result.getField1();
				}
			}
			result.setField1(errorMsg);

        } catch (final IOException e) {
        	result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);  
	    } catch (final ExternalException e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    } catch (final Exception e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    }
        return result;
	}
    
    @Override
    public PaymentResult_Extended fee_member_payroll(final SettlementPaymentRequest input) {
    	final PaymentParameters paymentParams = new PaymentParameters();
    	PaymentResult_Extended result = new PaymentResult_Extended();
        String customFieldName = "";
       	String customFieldName2 = "";
       	String customValue = "";
       	String customValue2 = "";
        String errorMsg = "";
        try {
			paymentParams.setFromSystem(true);
			paymentParams.setToSystem(true);
			
			paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
			if (input.getDesc() != null) {
			    paymentParams.setDescription(input.getDesc());
			}
				paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("fee.member.payroll.transfertypeid")));
			    
				customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.transaction.id.internal.name");
			    customValue = input.getField1() != null ? input.getField1() : "";
			    
				customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.settle.sequence.internal.name");
			    customValue2 = input.getField2() != null ? input.getField2() : "";
			    
			    
			
			if (customFieldName != null && !customFieldName.isEmpty() && customFieldName2 != null && !customFieldName2.isEmpty()) {
			
			    FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
			    List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
			    
			    FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
			    ArrayList addCustomValues = new ArrayList(customValues);
			    addCustomValues.add(customFieldValue2);
			    paymentParams.setCustomValues(addCustomValues);
			
			}
			
			result = doPayment(paymentParams);
			if(result != null && result.getField1() != null && !result.getField1().equals("")){
				errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
				if(errorMsg == null){
					errorMsg = result.getField1();
				}
			}
			result.setField1(errorMsg);

        } catch (final IOException e) {
        	result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);  
	    } catch (final ExternalException e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    } catch (final Exception e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    }
        return result;
	}
    
    @Override
    public PaymentResult_Extended reversal_member_payroll(final SettlementPaymentRequest input) {
    	final PaymentParameters paymentParams = new PaymentParameters();
    	PaymentResult_Extended result = new PaymentResult_Extended();
        String customFieldName = "";
       	String customFieldName2 = "";
       	String customFieldName3 = "";
       	String customValue = "";
       	String customValue2 = "";
       	String customValue3 = "";
        String errorMsg = "";
        try {
			paymentParams.setFromSystem(true);
			paymentParams.setAmount(new BigDecimal(input.getAmount()).setScale(2, RoundingMode.HALF_UP));
			if (input.getDesc() != null) {
			    paymentParams.setDescription(input.getDesc());
			}
			
			if (input.getField3() != null) {
				paymentParams.setToMember(input.getField3());
			}
				paymentParams.setTransferTypeId(Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("reversal.member.payroll.transfertypeid")));
			    
				customFieldName = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.transaction.id.internal.name");
			    customValue = input.getField1() != null ? input.getField1() : "";
			    
				customFieldName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("payroll.settle.sequence.internal.name");
			    customValue2 = input.getField2() != null ? input.getField2() : "";
			
			if (customFieldName != null && !customFieldName.isEmpty() 
					&& customFieldName2 != null && !customFieldName2.isEmpty()){
			    FieldValueVO customFieldValue = new FieldValueVO(customFieldName, customValue);
			    	List<FieldValueVO> customValues = Arrays.asList(customFieldValue);
			    FieldValueVO customFieldValue2 = new FieldValueVO(customFieldName2, customValue2);
			    ArrayList addCustomValues = new ArrayList(customValues);
			    addCustomValues.add(customFieldValue2);
			    paymentParams.setCustomValues(addCustomValues);
			}
			
			result = doPayment(paymentParams);
			if(result != null && result.getField1() != null && !result.getField1().equals("")){
				errorMsg = EmoneyConfiguration.getEmoneyProperties().getProperty("core.fundtransfer.error_code." + result.getField1());
				if(errorMsg == null){
					errorMsg = result.getField1();
				}
			}
			result.setField1(errorMsg);

        } catch (final IOException e) {
        	result.setStatus(PaymentStatus.UNKNOWN_ERROR);
            logger.error(e);  
	    } catch (final ExternalException e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    } catch (final Exception e) {
	        result.setStatus(PaymentStatus.UNKNOWN_ERROR);
	        logger.error(e);
	    }
        return result;
	}
}