/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import ptdam.emoney.webservice.member.ActivationRequest;
import ptdam.emoney.webservice.member.ActivationResponse;
import ptdam.emoney.webservice.member.Status;

import nl.strohalm.cyclos.entities.services.ServiceOperation;
import nl.strohalm.cyclos.webservices.Permission;
import nl.strohalm.cyclos.webservices.PrincipalParameters;
import nl.strohalm.cyclos.webservices.model.GroupVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;

/**
 * Web service interface for emoneyaccountactivation
 * @author ptdam
 */
@WebService
public interface TopupWebService {

    @Permission(ServiceOperation.MANAGE_MEMBERS)
    @WebMethod
    @WebResult(name = "echoTestResult")
    String echoTest(@WebParam(name = "tx") String tx);
 
    @Permission(ServiceOperation.MANAGE_MEMBERS)
    @WebMethod
    @WebResult(name = "inquiryResult")
    TopupInquiryResponse inquiry(@WebParam(name = "request") TopupInquiryRequest request);
    
    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT})
    @WebMethod
    @WebResult(name = "paymentResult")
    TopupPaymentResponse payment(@WebParam(name = "request") TopupPaymentRequest request);
 
    @Permission(ServiceOperation.MANAGE_MEMBERS)
    @WebMethod
    @WebResult(name = "registerResult")
    Status registerIfNotExist(@WebParam(name = "request") TopupPaymentRequest request);

    @Permission({ ServiceOperation.CHARGEBACK})
    @WebMethod
    @WebResult(name = "reversalResult")
    TopupReversalResponse reverse(@WebParam(name = "request") TopupReversalRequest request);

    @Permission({ ServiceOperation.DO_PAYMENT, ServiceOperation.RECEIVE_PAYMENT})
    @WebMethod
    @WebResult(name = "paymentResult")
    TopupPaymentResponse payment_extended(@WebParam(name = "request") TopupPaymentRequestExtended request);

    @Permission({ ServiceOperation.CHARGEBACK})
    @WebMethod
    @WebResult(name = "reversalResult")
    TopupReversalResponse reverse_extended(@WebParam(name = "request") TopupReversalRequestExtended request);

    
}
