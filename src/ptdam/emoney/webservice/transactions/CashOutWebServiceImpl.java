/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.jws.WebService;

import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.application.ApplicationServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.webservices.external.ExternalWebServiceHelper;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.GroupHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.PaymentHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;
import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.member.Status;
//import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl.PrepareParametersResult;
//import ptdam.emoney.webservice.client.CashOutExternalMsgRequest;
//import ptdam.emoney.webservice.client.CashOutExternalMsgResponse;
//import ptdam.emoney.webservice.client.CashOutExternalWebService;

/**
 * Web service implementation
 * @author luis
 */
@WebService(name = "emoneywithdraw", serviceName = "emoneywithdraw")
public class CashOutWebServiceImpl implements CashOutWebService {

    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private MemberHelper                  memberHelper;
    private GroupHelper                   groupHelper;
    private ChannelHelper                 channelHelper;
    private CustomFieldHelper             customFieldHelper;
    private GroupServiceLocal             groupServiceLocal;
    private AccountServiceLocal           accountServiceLocal;
    private AccountHelper                 accountHelper;
   
    private PaymentHelper                     paymentHelper;
    private WebServiceHelper                  webServiceHelper;
    private PaymentServiceLocal               paymentServiceLocal;
    private ApplicationServiceLocal           applicationServiceLocal;
    private ExternalWebServiceHelper          externalWebServiceHelper;
    
    private CashOutWebService  cashOutWebService;
    
    private String     RESPONSE_WITHDRAW_SUCCESS = "00";
    private String     RESPONSE_INVALID_OTP = "76";
    private String     RESPONSE_INVALID_DENOM = "13";
    private String     RESPONSE_SYSTEM_ERROR = "99";
    private String     RESPONSE_INSUFFICIENT_BALANCE = "56";
    private String     RESPONSE_MAX_DENOM_EXCEEDED = "77";
    private String     RESPONSE_MAX_MONTH_AMOUNT = "88";
    private String     RESPONSE_MAX_DAILY_AMOUNT = "33";
        
    private String     RESPONSE_REVERSE_SUCCESS = "00";
    
    public CashOutWebServiceImpl() {
        try {
            RESPONSE_WITHDRAW_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.success");
 
            RESPONSE_INVALID_OTP = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.invalid_otp");
            RESPONSE_INVALID_DENOM = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.invalid_denom");
            RESPONSE_WITHDRAW_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.success");
            RESPONSE_SYSTEM_ERROR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.syserror");
            RESPONSE_INSUFFICIENT_BALANCE = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.insufficient_balance");
        
            RESPONSE_REVERSE_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.reverse");

            RESPONSE_MAX_DENOM_EXCEEDED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.max_denom_exceeded");
            RESPONSE_MAX_MONTH_AMOUNT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.max_trxamount_per_month_exceeded");
            RESPONSE_MAX_DAILY_AMOUNT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cashout.max_trxamount_per_day_exceeded");
                   
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    

    @Override
    public String echoTest(final String tx) {
        return tx;
    }

    @Override
    public CashOutMsgResponse withDraw(final CashOutMsgRequest request) {
        Status status = new Status();
        CashOutMsgResponse response = new CashOutMsgResponse();
        
        try {
                
            response = getCashOutWebService().withDraw(request);

            switch (response.getStatus().getErrorCode()) {
                case "PROCESSED":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_WITHDRAW_SUCCESS);
                    status.setStatusDescription("Berhasil");
                    
                    BigDecimal withdraw = new BigDecimal(response.getWithdrawAmount());
                    withdraw = withdraw.multiply(new BigDecimal("100"));
                    withdraw = withdraw.setScale(0, RoundingMode.FLOOR);
                    
                    String sWithdraw = "000000000000";
                    
                    if (withdraw.toString().length() <= 12) {
                        sWithdraw = sWithdraw.substring(0, (12 - withdraw.toString().length())) + withdraw.toString();
                    } else {
                        sWithdraw = withdraw.toString().substring(0,12); // need to modify later
                    }
                    response.setWithdrawAmount(sWithdraw);
                    break;
                case "NOT_ENOUGH_CREDITS":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INSUFFICIENT_BALANCE);
                    status.setStatusDescription("Saldo tidak cukup");
                    break;
                case "INVALID_DENOM":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INVALID_DENOM);
                    status.setStatusDescription("Invalid Denom");
                    break;
                case "INVALID_OTP":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INVALID_OTP);
                    status.setStatusDescription("Invalid Moku No. and/or OTP");
                    break;
                case "MAX_DENOM_EXCEEDED":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_MAX_DENOM_EXCEEDED);
                    status.setStatusDescription("Maximum Denom Exceeded");
                    break;
                case "MAX_MONTH_AMOUNT_EXCEEDED":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_MAX_MONTH_AMOUNT);
                    status.setStatusDescription("Maximum Transaction Amount per Month exceeded");
                    break;
                case "MAX_DAILY_AMOUNT_EXCEEDED":
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_MAX_DAILY_AMOUNT);
                    status.setStatusDescription("Maximum Transaction Amount per day exceeded");
                    break;
                default:
                    status.setIsError(true);
                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                    status.setStatusDescription("Unknown Error");
                    break;
            }

            response.setStatus(status);

        } 
        catch (Exception e) {
            e.printStackTrace();
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
            response.setStatus(status);
        }
        
        return response;

    }
        
    public CashOutMsgReversalResponse reverse(final CashOutMsgReversalRequest request) {
        CashOutMsgReversalResponse response = new CashOutMsgReversalResponse();
        Status status = new Status();

        try {

            response = getCashOutWebService().reverse(request);
            
            switch (response.getStatus().getErrorCode()) {
                case "SUCCESS":
                    status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                    status.setIsError(true);
                    status.setStatusDescription("Transaction was successfully reversed.");
                    break;
                case "TRANSFER_ALREADY_CHARGEDBACK":
                    status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                    status.setIsError(true);
                    status.setStatusDescription("Transaction was already reversed.");
                    break;
                case "NOT_PERFORMED":
                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                    status.setIsError(true);
                    status.setStatusDescription("Reversal failed.");
                    break;
            }
            
        } catch (Exception e) {
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            response.setStatus(status);
        }
        
        return response;
    }
    
    private CashOutWebService getCashOutWebService() {
        final String url;

        if (cashOutWebService == null) {
            try {
                  url = EmoneyConfiguration.getEmoneyProperties().getProperty("external.endpointurl.cashout");
                  cashOutWebService = ExternalWebServiceHelper.proxyFor(CashOutWebService.class, url);
            } catch (IOException e1) {
                  // TODO Auto-generated catch block
                  e1.printStackTrace();
            }
        }
        return cashOutWebService;
    }
    
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setGroupHelper(final GroupHelper groupHelper) {
        this.groupHelper = groupHelper;
    }
    
    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
    
    public void setGroupServiceLocal(final GroupServiceLocal groupService) {
        this.groupServiceLocal = groupService;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountServiceLocal = accountService;
    }
    
    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }
    
    public void setPaymentHelper(final PaymentHelper paymentHelper) {
        this.paymentHelper = paymentHelper;
    }
    
    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    public void setApplicationServiceLocal(final ApplicationServiceLocal applicationServiceLocal) {
        this.applicationServiceLocal = applicationServiceLocal;
    }

    public void setMemberHelper(final MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }
    
    public void setExternalWebServiceHelper(final ExternalWebServiceHelper externalWebServiceHelper) {
        this.externalWebServiceHelper = externalWebServiceHelper;
    }
}