/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebService;

import nl.strohalm.cyclos.dao.accounts.transactions.PaymentTransferDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.accounts.AccountStatus;
import nl.strohalm.cyclos.entities.accounts.MemberGroupAccountSettings;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransfer;
import nl.strohalm.cyclos.entities.accounts.transactions.PaymentTransferHistories;
import nl.strohalm.cyclos.entities.accounts.transactions.Transfer;
import nl.strohalm.cyclos.entities.accounts.transactions.TransferQuery;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.PaymentCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.GroupQuery;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.services.accounts.AccountDTO;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.application.ApplicationServiceLocal;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.groups.GroupServiceLocal;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentServiceLocal;
import nl.strohalm.cyclos.services.transactions.PaymentTransferService;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.HashHandler;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.webservices.accounts.AccountHistorySearchParameters;
import nl.strohalm.cyclos.webservices.members.MemberWebService;
import nl.strohalm.cyclos.webservices.members.MemberWebServiceImpl;
import nl.strohalm.cyclos.webservices.members.RegisterMemberParameters;
import nl.strohalm.cyclos.webservices.model.FieldValueVO;
import nl.strohalm.cyclos.webservices.model.MemberVO;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.payments.PaymentParameters;
import nl.strohalm.cyclos.webservices.payments.PaymentParametersExtended;
import nl.strohalm.cyclos.webservices.payments.PaymentResult;
import nl.strohalm.cyclos.webservices.payments.PaymentResult_Extended;
import nl.strohalm.cyclos.webservices.payments.PaymentStatus;
import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl;
import nl.strohalm.cyclos.webservices.utils.AccountHelper;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import nl.strohalm.cyclos.webservices.utils.GroupHelper;
import nl.strohalm.cyclos.webservices.utils.MemberHelper;
import nl.strohalm.cyclos.webservices.utils.PaymentHelper;
import nl.strohalm.cyclos.webservices.utils.WebServiceHelper;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferProcessingRequest;
import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferRequest;
import com.ptdam.emoney.webservices.paymenttransfer.PaymentTransferResponse;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.member.Status;
//import nl.strohalm.cyclos.webservices.payments.PaymentWebServiceImpl.PrepareParametersResult;

/**
 * Web service implementation
 * @author luis
 */
@WebService(name = "emoneytopup", serviceName = "emoneytopup")
public class TopupWebServiceImpl implements TopupWebService {

    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private MemberHelper                  memberHelper;
    private GroupHelper                   groupHelper;
    private ChannelHelper                 channelHelper;
    private PaymentTransferDAO                paymentTransferDao;
    private CustomFieldHelper             customFieldHelper;
    private GroupServiceLocal             groupServiceLocal;
    private AccountServiceLocal           accountServiceLocal;
    private AccountHelper                 accountHelper;
    private PaymentTransferService paymentTransferService;
    private PaymentHelper                     paymentHelper;
    private WebServiceHelper                  webServiceHelper;
    private PaymentServiceLocal               paymentServiceLocal;
    private ApplicationServiceLocal           applicationServiceLocal;
    
    private final Log               logger    = LogFactory.getLog(TopupWebServiceImpl.class);
    
    private String	   RESPONSE_INQUIRY_MPT_SUCCESS = "0000";
    private String     RESPONSE_INQUIRY_SUCCESS = "00";
    private String     RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE = "E1";
    private String     RESPONSE_SYSTEM_ERROR = "99";
    
    private String     RESPONSE_PAYMENT_SUCCESS = "00";
    private String     RESPONSE_PAYMENT_EXCEED_LIMIT = "E2";
    
    private String     RESPONSE_REVERSE_SUCCESS = "00";
    
    private String     UNREGISTERED = "UNREGISTERED";
    
    private String     CUSTOM_FIELD_MOBILE_PHONE = "mobilePhone";
    
    private String     NEW_ACCOUNT = "Nasabah Baru";
    
    private String     BILL_CODE_MOKU = "00";
    private String     BILL_NAME = "Topup Moku";
    private String     BILL_SHORT_NAME = "Topup Moku";

    private String     MANDIRI_COMPANY_CODE = "00000";  
    private String     INIT_PWD_PREFIX = "hidden_initpwd";
    private String     DEVICE_PAIR = "hidden_devicepairingflag";
    private String	   CIF_NAME	= "nama_cif";
    private String     VALID_ECASH_NUMBERS = "";
    
    private String     MPT_RESPONSE_CODE_PT_NOT_FOUND = "E6";
    private String     MPT_RESPONSE_EXPIRED_DATE = "B8";
    private String	   MPT_RESPONSE_INVALID_AMOUNT="02";
    private String	   MPT_RESPONSE_BILL_ALREADY_PAID="B8";
    
    private HashHandler hashHandler;
    private String[]   validEcashNumbers;
    
    private SmsMailingServiceLocal         smsMailingServiceLocal;

    private String     MVA_PREFIX = "";
    private int        MVA_EMONEYNO_MIN_LENGTH = 7;

    public TopupWebServiceImpl() {
        try {
            RESPONSE_INQUIRY_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.topup.success");
            RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.topup.acctnotactive");
            RESPONSE_SYSTEM_ERROR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.syserror");

            RESPONSE_PAYMENT_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.topup.success");
            RESPONSE_PAYMENT_EXCEED_LIMIT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.topup.topupexceedlimit");

            RESPONSE_REVERSE_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.topup.reverse");
            
            UNREGISTERED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.group.unregistered");
            
            CUSTOM_FIELD_MOBILE_PHONE = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.mobilephone_custom_field");
            
            NEW_ACCOUNT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.new_account");

            BILL_CODE_MOKU = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.bill_code");
            BILL_NAME = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.bill_name");
            BILL_SHORT_NAME = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.bill_short_name");
            
            MANDIRI_COMPANY_CODE = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.company_code");
            INIT_PWD_PREFIX = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.init_prefix");
            DEVICE_PAIR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.devicepair");

            VALID_ECASH_NUMBERS = EmoneyConfiguration.getEmoneyProperties().getProperty("valid.ecash_no.whitelist");
            MPT_RESPONSE_CODE_PT_NOT_FOUND =EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.response.code.ptnot.found");
            MPT_RESPONSE_EXPIRED_DATE = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.response.expired.date");
            MPT_RESPONSE_INVALID_AMOUNT = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.response.invalid.amount");
            MPT_RESPONSE_BILL_ALREADY_PAID = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.response.allready.paid");
        	CIF_NAME = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.nama_cif_custom_fieldname");
            if (VALID_ECASH_NUMBERS != null) {
                validEcashNumbers = VALID_ECASH_NUMBERS.split("[,]");
            }
            
            MVA_PREFIX = EmoneyConfiguration.getEmoneyProperties().getProperty("topup.emoney.mva_prefix");
            MVA_EMONEYNO_MIN_LENGTH = Integer.parseInt(EmoneyConfiguration.getEmoneyProperties().getProperty("topup.emoney.mva_no_min_length"));
            
        } catch (IOException e) {
            logger.error(e);
        }
        
    }
    
    @Override
    public String echoTest(final String tx) {
        return tx;
    }

    @Override
    public TopupInquiryResponse inquiry(final TopupInquiryRequest request) {
        final String emoneyAccountNo = request.getEmoneyAccountNo();
        final Status status = new Status();
        final TopupInquiryResponse response = new TopupInquiryResponse();
        Boolean isNewAccount = true;
        
        BigDecimal upperLimit = BigDecimal.ZERO;
        BigDecimal accountBalance = BigDecimal.ZERO;
        BigDecimal maxTopup = BigDecimal.ZERO;
        BigDecimal topupAmount = new BigDecimal(request.getTopupAmount());
        
        Member member = null;
        String mptprefix;
        String mptprefix2;
        String mptprefix3;
        String memberName = "";
        try {
        	memberName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.new_account");;
        	mptprefix = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer");
        	mptprefix2 = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer2");
        	mptprefix3 = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer3");
        	
        	System.out.println("EmoneyAccountNo Inquiry :"+emoneyAccountNo);
        	if ((emoneyAccountNo.substring(0, 3).contains(mptprefix)) || (emoneyAccountNo.substring(0, 1).equals(mptprefix2))) {
			isNewAccount = false;	
			 String ptNumberUnique;
			 String ptNumber;
			if(emoneyAccountNo.substring(0, 1).equals(mptprefix2)){
				ptNumberUnique = mptprefix3+emoneyAccountNo+topupAmount;
				ptNumber = mptprefix3+emoneyAccountNo;
			}else{
				ptNumberUnique = emoneyAccountNo+topupAmount;
				ptNumber = emoneyAccountNo;
			}
			
			 final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
			PaymentTransfer paymentParam=null;

        try {
				 paymentParam = paymentTransferService.load(ptNumberUnique, null);
				 if (paymentParam != null) {
            
					 try {
						 member = elementServiceLocal.loadByPrincipal(principalType, paymentParam.getMid(), FETCH);						
					} catch (Exception e) {
						logger.info(e);
					}
					 
					 if (member != null) {
						
						 if (paymentParam.getEcashRefNo()==null) {
							
						
					
					 Calendar expired_date = paymentParam.getExpiredDate();
					 Calendar timeNow = Calendar.getInstance();
			
					 if(timeNow.before(expired_date)){
				//	 int lengthTopupAmount = request.getTopupAmount().length();
						 int lengthTopupAmount = topupAmount.toString().length();
		             String AmountUnique = paymentParam.getAmountUnique().toString();
		           //  if (request.getTopupAmount().equals(AmountUnique.substring(0, lengthTopupAmount))) {
		             System.out.println("topupAmount.toString() :" +topupAmount.toString());
		             System.out.println("AmountUnique :"+AmountUnique);
		             System.out.println("AmountUnique.substring :"+AmountUnique.substring(0, lengthTopupAmount));
		             if (topupAmount.toString().equals(AmountUnique.substring(0, lengthTopupAmount))) {
            response.setEmoneyAccountNo(emoneyAccountNo);
        				 response.setEmoneyAccountName(member.getName()+"/"+paymentParam.getDescription());
           
            BillDetail billDetail = new BillDetail();
            billDetail.setBillCode(BILL_CODE_MOKU);
            if (BILL_NAME.length() >= 20) {
                billDetail.setBillName(BILL_NAME.substring(0, 20));
            } else {
                billDetail.setBillName(BILL_NAME.substring(0, BILL_NAME.length()));
            }
            
            if (BILL_SHORT_NAME.length() >= 10) {
                billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, 10));
            } else {
                billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, BILL_SHORT_NAME.length()));
            }
            billDetail.setBillAmount(request.getTopupAmount());
            billDetail.setReference1("");
            billDetail.setReference2("");
            billDetail.setReference3("");
            ArrayOfBillDetails arrayOfbills = new ArrayOfBillDetails();
            arrayOfbills.setArrayOfAccounts(new BillDetail[] {billDetail});
            response.setBillDetails(arrayOfbills);
            status.setIsError(false);
                         status.setErrorCode(RESPONSE_INQUIRY_SUCCESS);
                         status.setStatusDescription("Success");
            
            }else{
            	status.setIsError(false);
                status.setErrorCode(MPT_RESPONSE_INVALID_AMOUNT);
                status.setStatusDescription("INVALID AMOUNT");
                setBillDetails(request.getTopupAmount(), response);
            }
            
					 }else{
						 status.setIsError(false);
			                status.setErrorCode(MPT_RESPONSE_EXPIRED_DATE);
			                status.setStatusDescription("Expired Date");
			                response.setStatus(status);
			                setBillDetails(request.getTopupAmount(), response);
					 }
            
						 }else{
							status.setIsError(true);
							status.setErrorCode(MPT_RESPONSE_BILL_ALREADY_PAID);
							status.setStatusDescription("Transaksi sudah dilakukan");
							setBillDetails(request.getTopupAmount(), response);
							
						 }
						 
						 }else{
						 status.setIsError(true);
						 status.setErrorCode("RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE");
						 status.setErrorCode("INVALID_MERCHANT_ID");
						 setBillDetails(request.getTopupAmount(), response);
					 }
					 }else{
						    
			                paymentParam = paymentTransferService.load(ptNumber, null);
			                
			                if (paymentParam==null) {
			                	status.setIsError(false);
			                   	status.setErrorCode(MPT_RESPONSE_CODE_PT_NOT_FOUND);
			                   	status.setStatusDescription("INVALID PTNO");
			                   	
			                    setBillDetails(request.getTopupAmount(), response);
							}else{
				            	status.setIsError(false);
				                status.setErrorCode(MPT_RESPONSE_INVALID_AMOUNT);
				                status.setStatusDescription("INVALID AMOUNT");
				                
				                setBillDetails(request.getTopupAmount(), response);
							}
				 }
			 } catch (Exception e) {
				 status.setIsError(true);
				 status.setErrorCode("INVALID_PARAMETER");
			 }
            
        	}else{ 
            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);

            if (isMVAAccount(emoneyAccountNo)) {
            	//Modification for issue top up 8 Juni 2014
                member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo.substring(2), FETCH);
            } else {
                member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);
            }

            if (member != null) {
            	
            	String cifName="";
            	
            	if (checkTransferTypeCode(member.getGroup().getId().toString(), "topup.whitelist_name.bygroupid")) {
            		Collection<MemberCustomFieldValue> memberCustomFields = member
                            .getCustomValues();
                    Iterator<MemberCustomFieldValue> itMember = memberCustomFields.iterator();
                    // paymentParams.setToMember(member.getUser().getUsername());
                    while (itMember.hasNext()) {
                        MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember
                                .next();
                        if (mCustomField.getField().getInternalName()
                                .equals(CIF_NAME)) {
                            cifName = mCustomField.getValue();
                            break;
                        }
                    }
                    if (cifName.length()>10) {
                    	cifName=cifName.substring(0,10);
					}
                    String name=member.getName();
                    if (member.getName().length()>10) {
						name=member.getName().substring(0,10);
					}
                    memberName = cifName+"/"+name;
				}else{
					memberName = member.getName();
				}
            	
                isNewAccount = false;

                final MemberGroup memberGroup = member.getMemberGroup();
                
                if (memberGroup.isActive()) {
                    
                    AccountHistorySearchParameters params = new AccountHistorySearchParameters();
                    
                    if (isMVAAccount(emoneyAccountNo)) {
                    	//Modification for issue top up 8 Juni 2014
                        params.setPrincipal(emoneyAccountNo.substring(2));
                    } else {
                        params.setPrincipal(emoneyAccountNo);
                    }

                    final TransferQuery query = accountHelper.toQuery(params, member);
                    final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                    
                    accountBalance = accountStatus.getAvailableBalance();
                    upperLimit = accountStatus.getUpperCreditLimit();
                    
                    if (accountBalance.compareTo(BigDecimal.ZERO) > 0) {

                        if (upperLimit.compareTo(accountBalance) >= 0) {
                            maxTopup = upperLimit.subtract(accountBalance);
                        } else {
                            maxTopup = BigDecimal.ZERO;
                        }
                        
                    } else {
                        maxTopup = upperLimit;
                    }
                
                    response.setEmoneyAccountNo(emoneyAccountNo);
                    response.setEmoneyAccountName(memberName);
                    
                    BillDetail billDetail = new BillDetail();
                    billDetail.setBillCode(BILL_CODE_MOKU);
                    if (BILL_NAME.length() >= 20) {
                        billDetail.setBillName(BILL_NAME.substring(0, 20));
                    } else {
                        billDetail.setBillName(BILL_NAME.substring(0, BILL_NAME.length()));
                    }
                    
                    if (BILL_SHORT_NAME.length() >= 10) {
                        billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, 10));
                    } else {
                        billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, BILL_SHORT_NAME.length()));
                    }
                    billDetail.setBillAmount(request.getTopupAmount());
                    billDetail.setReference1("");
                    billDetail.setReference2("");
                    billDetail.setReference3("");

                    ArrayOfBillDetails arrayOfbills = new ArrayOfBillDetails();
                    arrayOfbills.setArrayOfAccounts(new BillDetail[] {billDetail});
                    response.setBillDetails(arrayOfbills);
                    
                    if (maxTopup.compareTo(topupAmount) >= 0) {

                        status.setIsError(false);
                        status.setErrorCode(RESPONSE_INQUIRY_SUCCESS);
                        status.setStatusDescription("Success");
                        
                    } else {
                        status.setIsError(true);
                        status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                        status.setStatusDescription("Exceed maximum topup of (" + maxTopup.toPlainString() + ")");
                        
                    }
                    
                    String dormantGroup="";
                	try {
    					dormantGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("dormant.group.name");
    				} catch (IOException e) {
    					dormantGroup = "DORMANT";
    					e.printStackTrace();
    				}
                	
                	if(memberGroup!= null && memberGroup.getName().equalsIgnoreCase(dormantGroup)){
                    	status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                        status.setStatusDescription("Account is dormant");
                    }
                    
                    
                } else {

                    response.setEmoneyAccountNo(emoneyAccountNo);
                    response.setEmoneyAccountName(memberName);

                    BillDetail billDetail = new BillDetail();
                    billDetail.setBillCode(BILL_CODE_MOKU);
                    if (BILL_NAME.length() >= 20) {
                        billDetail.setBillName(BILL_NAME.substring(0, 20));
                    } else {
                        billDetail.setBillName(BILL_NAME.substring(0, BILL_NAME.length()));
                    }
                    
                    if (BILL_SHORT_NAME.length() >= 10) {
                        billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, 10));
                    } else {
                        billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, BILL_SHORT_NAME.length()));
                    }
                    billDetail.setBillAmount(request.getTopupAmount());
                    billDetail.setReference1("");
                    billDetail.setReference2("");
                    billDetail.setReference3("");

                    ArrayOfBillDetails arrayOfbills = new ArrayOfBillDetails();
                    arrayOfbills.setArrayOfAccounts(new BillDetail[] {billDetail});
                    response.setBillDetails(arrayOfbills);
                    
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                    status.setStatusDescription("Account Not Active");
                }
            }
        }       
        } 
        catch (EntityNotFoundException e) {
            if (!isNewAccount) {
                logger.error(e);
                status.setIsError(true);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription(e.getMessage());
            }
        }
        catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            if (!isNewAccount) {
                response.setStatus(status);
            }
        }

        if (isNewAccount) {
            
            if (!validateEcashNumber(emoneyAccountNo, true)) {
                status.setIsError(false);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription("Inquiry: Invalid e-cash number.");
                response.setStatus(status);
                return response;
            }
            
            try {
                
                GroupQuery query = new GroupQuery();
                query.setNatures(Group.Nature.MEMBER);
                
                List<? extends Group> possibleNewGroups = groupServiceLocal.search(query);

                for (int i = 0; i < possibleNewGroups.size(); i++)
                {
                    Group group = possibleNewGroups.get(i);
                    String newGroupName = group.getName();
                    if (newGroupName.equals(UNREGISTERED))
                    {
                        MemberGroup memberGroup = (MemberGroup) group;
                        if (memberGroup.getAccountSettings().size() > 0) {
                            MemberGroupAccountSettings groupSettings = memberGroup.getAccountSettings().iterator().next();
                            upperLimit = groupSettings.getDefaultUpperCreditLimit();
                        } else {
                            upperLimit = BigDecimal.ZERO;
                        }
                        break;
                    }
                }
                
                if (member!=null) {
                	String cifName="";
                	
                	if (checkTransferTypeCode(member.getGroup().getId().toString(), "topup.whitelist_name.bygroupid")) {
                		Collection<MemberCustomFieldValue> memberCustomFields = member
                                .getCustomValues();
                        Iterator<MemberCustomFieldValue> itMember = memberCustomFields.iterator();
                        // paymentParams.setToMember(member.getUser().getUsername());
                        while (itMember.hasNext()) {
                            MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember
                                    .next();
                            if (mCustomField.getField().getInternalName()
                                    .equals(CIF_NAME)) {
                                cifName = mCustomField.getValue();
                                break;
                            }
                        }
                        
                        if (cifName.length()>10) {
                        	cifName=cifName.substring(0,10);
    					}
                        String name=member.getName();
                        if (member.getName().length()>10) {
    						name=member.getName().substring(0,10);
    					}
                        memberName = cifName+"/"+name;
    				}else{
    					memberName = member.getName();
    				}
				}

                
                response.setEmoneyAccountNo(emoneyAccountNo);
                response.setEmoneyAccountName(memberName);
                
                BillDetail billDetail = new BillDetail();
                billDetail.setBillCode(BILL_CODE_MOKU);
                
                if (BILL_NAME.length() >= 20) {
                    billDetail.setBillName(BILL_NAME.substring(0, 20));
                } else {
                    billDetail.setBillName(BILL_NAME.substring(0, BILL_NAME.length()));
                }
                
                if (BILL_SHORT_NAME.length() >= 10) {
                    billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, 10));
                } else {
                    billDetail.setBillShortName(BILL_SHORT_NAME.substring(0, BILL_SHORT_NAME.length()));
                }              
                
                billDetail.setBillAmount(request.getTopupAmount());
                billDetail.setReference1("");
                billDetail.setReference2("");
                billDetail.setReference3("");
                

                ArrayOfBillDetails arrayOfbills = new ArrayOfBillDetails();
                arrayOfbills.setArrayOfAccounts(new BillDetail[] {billDetail});
                response.setBillDetails(arrayOfbills);

                if (upperLimit.compareTo(topupAmount) >= 0) {
                
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INQUIRY_SUCCESS);
                    status.setStatusDescription("Success");
                
                } else {
                    status.setIsError(true);
                    status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                    status.setStatusDescription("Exceed maximum topup of (" + upperLimit.toPlainString() + ")");
                }
                
            } 
            catch (Exception e) {
                logger.error(e);
                status.setIsError(true);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription(e.getMessage());
            }
            finally {
                response.setStatus(status);
            }
        }
        
        return response;
    }

    @Override
    public TopupPaymentResponse payment(final TopupPaymentRequest request) {
        final String emoneyAccountNo = request.getEmoneyAccountNo();
        Status status = new Status();
        final TopupPaymentResponse response = new TopupPaymentResponse();
        
        BigDecimal upperLimit = BigDecimal.ZERO;
        BigDecimal accountBalance = BigDecimal.ZERO;
        BigDecimal maxTopup = BigDecimal.ZERO;
        BigDecimal paymentAmount = BigDecimal.ZERO;
        
        String traceId = request.getTransactionId();
        String sPaymentAmount = request.getTopupAmount();
        paymentAmount = new BigDecimal(sPaymentAmount);
        String companyCode = request.getCompanyCode();
        String channelId = request.getChannelId();
        
        Member member;

        try {

            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);

            if (member != null) {
                final MemberGroup memberGroup = member.getMemberGroup();
                
                if (memberGroup.isActive()) {
                    
                    AccountHistorySearchParameters params = new AccountHistorySearchParameters();
                    params.setPrincipal(emoneyAccountNo);
    
                    final TransferQuery query = accountHelper.toQuery(params, member);
                    final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                    
                    accountBalance = accountStatus.getAvailableBalance();
                    upperLimit = accountStatus.getUpperCreditLimit();
                    
                    if (accountBalance.compareTo(BigDecimal.ZERO) > 0) {
    
                        if (upperLimit.compareTo(accountBalance) >= 0) {
                            maxTopup = upperLimit.subtract(accountBalance);
                        } else {
                            maxTopup = BigDecimal.ZERO;
                        }
                        
                    } else {
                        maxTopup = upperLimit;
                    }
                    
                    response.setEmoneyAccountNo(emoneyAccountNo);
                    response.setEmoneyAccountName(member.getName());
                    
                    if (paymentAmount.compareTo(BigDecimal.ZERO) > 0) {
                        
                        if (maxTopup.compareTo(paymentAmount) >= 0) {
    
                            PaymentWebServiceImpl topup = new PaymentWebServiceImpl();

                            PaymentParameters topupParams = new PaymentParameters();
                            topupParams.setFromSystem(true);
                            topupParams.setToMember(emoneyAccountNo);
                            topupParams.setAmount(paymentAmount);
                            
                            Long trxTypeMandiri = 0L;
                            Long trxTypeBersama = 0L;

                            try {
                                trxTypeMandiri = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.transaction_type." + channelId));
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                            
                            if (companyCode.equals(MANDIRI_COMPANY_CODE)) {
                                topupParams.setTransferTypeId(trxTypeMandiri);
                            } else {
                                topupParams.setTransferTypeId(trxTypeBersama);
                            }
                            topupParams.setTraceNumber(traceId);
                            
                            topup.setWebServiceHelper(webServiceHelper);
                            topup.setPaymentHelper(paymentHelper);
                            topup.setPaymentServiceLocal(paymentServiceLocal);
                            topup.setAccountHelper(accountHelper);
                            topup.setApplicationServiceLocal(applicationServiceLocal);
                            topup.setElementServiceLocal(elementServiceLocal);
                            
                            PaymentResult result = topup.doPayment(topupParams);
                       
                            switch (result.getStatus()) {
                                case PROCESSED:
                                    status.setIsError(false);
                                    status.setErrorCode(RESPONSE_PAYMENT_SUCCESS);
                                    status.setStatusDescription("Success");
                                    if (result.getTransfer().getTransactionNumber() != null) {
                                        response.setTopupAmount(result.getTransfer().getTransactionNumber());
                                    }
                                    else {
                                        response.setTopupAmount("");
                                    }
                                    break;
                                case RECEIVER_UPPER_CREDIT_LIMIT_REACHED:
                                    status.setIsError(false);
                                    status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                                    status.setStatusDescription("Account Exceed Limit.");
                                    break;
                                default:
                                    status.setIsError(true);
                                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                                    status.setStatusDescription("Payment Error" + result.getStatus());
                                    break;
                            }
                            
                            
                            if (status.getErrorCode().equals(RESPONSE_PAYMENT_SUCCESS)){
                                maxTopup = maxTopup.subtract(paymentAmount);
                            }
                            
                        } else {
                            
                            status.setIsError(true);
                            status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                            status.setStatusDescription("Exceed maximum topup of (" + maxTopup.toPlainString() + ")");
                        }
                        
                    } else {
                        status.setIsError(true);
                        status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                        status.setStatusDescription("Payment amount is zero");
                    }
                    
                    String dormantGroup="";
                	try {
    					dormantGroup = EmoneyConfiguration.getEmoneyProperties().getProperty("dormant.group.name");
    				} catch (IOException e) {
    					dormantGroup = "DORMANT";
    					e.printStackTrace();
    				}
                	
                	if(memberGroup!= null && memberGroup.getName().equalsIgnoreCase(dormantGroup)){
                    	status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                        status.setStatusDescription("Account is dormant");
                    }
                    
                } else {
        
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                    status.setStatusDescription("Account Not Active");
                }
            } else {
                status.setIsError(true);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription("Account not found");
            }
            
            response.setEmoneyAccountNo(emoneyAccountNo);
            response.setEmoneyAccountName(member.getName());

        } 
        catch (EntityNotFoundException e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            response.setStatus(status);
        }
        
        
        // Add reverse in case the trx is created but response is not success
        if (!status.getErrorCode().equals(RESPONSE_PAYMENT_SUCCESS)) {
            
            try {
                Transfer transfer = paymentServiceLocal.loadTransferForReverse(traceId);

                if (transfer != null) {
                    Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                }
                
            } catch (Exception e) {
                logger.error(e);
            }
            
        }
        
        return response;

    }
    
    public Status registerIfNotExist(final TopupPaymentRequest request) {
        Status status = new Status();
        final String emoneyAccountNo = request.getEmoneyAccountNo();
        Boolean isNewAccount = false;
        Member member = null;

        String mptprefix;
        String mptprefix2;

        try {
        	mptprefix = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer");
        	mptprefix2 = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer2");
            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
    
            //Penambahan Logic untuk transaksi MPT
            if ((emoneyAccountNo.substring(0, 3).contains(mptprefix)) || (emoneyAccountNo.substring(0, 1).equals(mptprefix2))) {
            	status.setIsError(false);
                status.setErrorCode(this.RESPONSE_INQUIRY_SUCCESS);
                status.setStatusDescription("Is Merchant Payment Transfer");
			}else{
            
            try {
                if (isMVAAccount(emoneyAccountNo)) {
                	//Modification for issue top up 8 Juni 2014
                    member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo.substring(2), FETCH);
                } else {
                    member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);
                }
            } 
            catch (EntityNotFoundException e) {
                isNewAccount = true;
            }
            
            if (isNewAccount) {
    
                if (!validateEcashNumber(emoneyAccountNo, true)) {
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                    status.setStatusDescription("RegIfNotExist: Invalid e-cash number.");
                    return status;
                }
                
                MemberWebServiceImpl newAccountService = new MemberWebServiceImpl();
                newAccountService.setElementServiceLocal(elementServiceLocal);
                newAccountService.setMemberHelper(memberHelper);
                
                String init = RandomStringUtils.randomNumeric(6);
//                init = hashHandler.newHash(HashHandler.SHA_256, hashHandler.newHash(HashHandler.MD5, init).toLowerCase());
                                
                RegisterMemberParameters registrationParams = new RegisterMemberParameters();
                RegistrationFieldValueVO customField = new RegistrationFieldValueVO(CUSTOM_FIELD_MOBILE_PHONE, emoneyAccountNo , false);
                RegistrationFieldValueVO customField1 = new RegistrationFieldValueVO(INIT_PWD_PREFIX, init, false);
                RegistrationFieldValueVO customField2 = new RegistrationFieldValueVO(DEVICE_PAIR, "false", false);
                RegistrationFieldValueVO customField3 = new RegistrationFieldValueVO(CIF_NAME, NEW_ACCOUNT, false);
                List<RegistrationFieldValueVO> fieldValueVOs = Arrays.asList(customField, customField1, customField2, customField3);
                
                if (isMVAAccount(emoneyAccountNo)) {
                	//Modification for issue top up 8 Juni 2014
                    registrationParams.setUsername(emoneyAccountNo.substring(2));
                } else {
                    registrationParams.setUsername(emoneyAccountNo);
                }
                
                registrationParams.setName(NEW_ACCOUNT);
                registrationParams.setLoginPassword(init);
                registrationParams.setFields(fieldValueVOs);
                
                Long groupId = new Long(0);
                final GroupQuery query = new GroupQuery();
                query.setNatures(Group.Nature.MEMBER);
                
                final List<? extends Group> possibleNewGroups = groupServiceLocal.search(query);

                if (possibleNewGroups != null) {
                    for (int i = 0; i < possibleNewGroups.size(); i++)
                    {
                        final Group group = possibleNewGroups.get(i);
                        if (group.getName().equals(UNREGISTERED))
                        {
                            groupId = group.getId();
                            break;
                        }
                    }
                }
                
                if (groupId > 0) {
                    registrationParams.setGroupId(groupId);
                }
                
                newAccountService.registerMember(registrationParams);
                
                if (isMVAAccount(emoneyAccountNo)) {
                	//Modification for issue top up 8 Juni 2014
                    member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo.substring(2), FETCH);
                } else {
                    member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);
                }
    
            }
    
            if (member != null) {
                if (isNewAccount) {
                    
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INQUIRY_SUCCESS);
                    status.setStatusDescription("Account is created");
                    
                    // Send notification to member how to use e-cash
                    sendSMS(member);

                } else {
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_INQUIRY_SUCCESS);
                    status.setStatusDescription("Existing account");
                }
            } else {
                status.setIsError(true);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription("Account not found");
            }
        
			}
        } catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }

        return status;
    }
    
    public TopupReversalResponse reverse(final TopupReversalRequest request) {
        TopupReversalResponse response = new TopupReversalResponse();
        Status status = new Status();
        String traceId = request.getTransactionId();
        
        try {
            Transfer transfer = paymentServiceLocal.loadTransferForReverse(traceId);

            if (transfer != null) {
                Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                status.setIsError(false);
                status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                status.setStatusDescription("Topup is successfully reversed");
            } else {
                status.setIsError(false);
                status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                status.setStatusDescription("Topup transaction not found");
            }
            
        } catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            response.setStatus(status);
        }
        
        return response;
    }

    @Override
    public TopupPaymentResponse payment_extended(final TopupPaymentRequestExtended request) {
        String emoneyAccountNo = request.getEmoneyAccountNo();
        Status status = new Status();
        final TopupPaymentResponse response = new TopupPaymentResponse();
        
        BigDecimal upperLimit = BigDecimal.ZERO;
        BigDecimal accountBalance = BigDecimal.ZERO;
        BigDecimal maxTopup = BigDecimal.ZERO;
        BigDecimal paymentAmount = BigDecimal.ZERO;
        
        String transactionId = request.getTransactionId();
        String sPaymentAmount = request.getTopupAmount();
        paymentAmount = new BigDecimal(sPaymentAmount);
        String companyCode = request.getCompanyCode();
        String channelId = request.getChannelId();

        String language = request.getLanguage();
        String trxDateTime = request.getTrxDateTime();
        String transmissionDateTime = request.getTransmissionDateTime();
        String reference1 = request.getReference1();
        String reference2 = request.getReference2();
        String reference3 = request.getReference3();
        String terminalID = request.getTerminalID();
        String currency = request.getCurrency();
                
        String merchantType = "";
        String billPaidFlag = "";
        String mptPrefix = "";
        String mptPrefix2 = "";
        String mptPrefix3 = "";
        String MVAAccountMpt="";
        String memberName="";
        
        try {
        	memberName = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.topup.new_account");;
            merchantType = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.merchant_type");
            billPaidFlag = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.billFlagMap");
            mptPrefix = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer");
            mptPrefix2 = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer2");
            mptPrefix3 = EmoneyConfiguration.getEmoneyProperties().getProperty("prefix.merchant.payment.transfer3");
            MVAAccountMpt=EmoneyConfiguration.getEmoneyProperties().getProperty("mvaaccount.merchant.payment.transfer");
        } catch (final IOException e) {
            logger.error(e);
        }
        
        // Rekonsiliasi u/ Topup
        
        StringBuilder ubpRemarksBuilder = new StringBuilder();
        ubpRemarksBuilder.append("UBP");
        ubpRemarksBuilder.append(merchantType);
        ubpRemarksBuilder.append(companyCode);
        ubpRemarksBuilder.append(language);
        ubpRemarksBuilder.append(billPaidFlag);
        ubpRemarksBuilder.append(emoneyAccountNo);
        
        String ubpRemarks = "";
        
        if (ubpRemarksBuilder.toString().length() <= 40) {
            ubpRemarks = ubpRemarksBuilder.toString();
        } else {
            ubpRemarks = ubpRemarksBuilder.toString().substring(0, 40);
        }
                
        if (terminalID == null) {
            terminalID = "";
        }
        
        StringBuilder traceIdBuilder = new StringBuilder();
        traceIdBuilder.append(terminalID);
        traceIdBuilder.append(trxDateTime);
        traceIdBuilder.append(transmissionDateTime);
        traceIdBuilder.append(transactionId);

        final String traceId = traceIdBuilder.toString();
        
        StringBuilder rekonKey = new StringBuilder();
        rekonKey.append(ubpRemarks);
        rekonKey.append("|");
        rekonKey.append(reference2);
        rekonKey.append("|");
        rekonKey.append(" ");
        
        Member member = null;
        PaymentWebServiceImpl topup = new PaymentWebServiceImpl();
        PaymentParametersExtended topupParams = new PaymentParametersExtended();

    	System.out.println("EmoneyAccountNo Payment :"+emoneyAccountNo);
        if (emoneyAccountNo.substring(0, 3).equals(MVAAccountMpt)) {
			emoneyAccountNo = emoneyAccountNo.substring(2);
			PaymentTransfer paymentParam;
		}
        if ((emoneyAccountNo.substring(0, 3).contains(mptPrefix)) || (emoneyAccountNo.substring(0, 1).equals(mptPrefix2))) {
        	String ptnumber = null;
        	String orderid = null;
        	String merchantid = null;
        	String urlcallback = null;
        	String ptnumberUnique = null;
        	String paymentTicket = null;
        	String originFrom = null;
        	Long trxTypeMpt = 0L;
        	String[] stringAmount = null;
            
        	final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
        	
        	if (emoneyAccountNo.substring(0, 1).equals(mptPrefix2)) {
        		if (sPaymentAmount.contains(".")) {
        	          stringAmount = sPaymentAmount.split("\\.");
        	          ptnumberUnique = mptPrefix3 + emoneyAccountNo + stringAmount[0];
        	        } else {
        	          ptnumberUnique = mptPrefix3 + emoneyAccountNo + paymentAmount;
        	        }

        	      }
        	      else if (sPaymentAmount.contains(".")) {
        	        stringAmount = sPaymentAmount.split("\\.");
        	        ptnumberUnique = emoneyAccountNo + stringAmount[0];
        	      } else {
        	        ptnumberUnique = emoneyAccountNo + paymentAmount;
        	      }
        	
            PaymentTransfer paymentParam = paymentTransferService.load(ptnumberUnique, null);
            
            if (paymentParam != null) {
            	try {
            		member = elementServiceLocal.loadByPrincipal(principalType, paymentParam.getMid(), FETCH);	
				} catch (Exception e) {
					logger.error(e);
				}
            	
            	if (member != null) {
					
				if (paymentParam.getEcashRefNo() == null) {
					
				
            	
            	int lengthTopupAmount = request.getTopupAmount().length();
            	String AmountUnique = paymentParam.getAmountUnique().toString();
                if (request.getTopupAmount().equals(AmountUnique.substring(0, lengthTopupAmount))) {
                    topupParams.setFromSystem(true);
                    topupParams.setToSystem(true);
                    		 
					try {
						if (reference3 != null && reference3.trim().length() > 0) {
							trxTypeMpt = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.transfertypeid."+reference3));
						}else{
							trxTypeMpt = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.transfertypeid"));
						}
						
		  	    ptnumber = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname."+trxTypeMpt);
	                    orderid = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname2."+trxTypeMpt);
	                    merchantid = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname3."+trxTypeMpt);
	                    urlcallback = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname4."+trxTypeMpt);
	                    paymentTicket = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname5."+trxTypeMpt);
						originFrom = EmoneyConfiguration.getEmoneyProperties().getProperty("mpt.customfieldname6."+trxTypeMpt);	    	
	                    
							} catch (IOException e) {
							logger.error(e);
						}
							 
                    topupParams.setTransferTypeId(trxTypeMpt);
                    topupParams.setTraceNumber(traceId);
                    topupParams.setTraceData(rekonKey.toString());
                    topupParams.setAmount(paymentAmount);
                    if (reference2!=null) {
                    	topupParams.setDescription(reference2);	
					}
                            
                    if (ptnumber != null && orderid != null && merchantid != null) {
						FieldValueVO customFieldValue = new FieldValueVO(ptnumber, paymentParam.getPtNumber());
						List<FieldValueVO> customValues =Arrays.asList(customFieldValue);
						FieldValueVO customFieldValue2 = new FieldValueVO(orderid, paymentParam.getOrderId());
						FieldValueVO customFieldValue3 = new FieldValueVO(merchantid, paymentParam.getMid());
						FieldValueVO customFieldValue4 = new FieldValueVO(urlcallback, paymentParam.getUrlCallback());
						FieldValueVO customFieldValue5 = new FieldValueVO(paymentTicket, paymentParam.getPaymentTicket());
						FieldValueVO customFieldValue6 = new FieldValueVO(originFrom, paymentParam.getMid());
						ArrayList addCustomValues = new ArrayList(customValues);
						addCustomValues.add(customFieldValue2);
						addCustomValues.add(customFieldValue3);
						addCustomValues.add(customFieldValue4);
						addCustomValues.add(customFieldValue5);
						addCustomValues.add(customFieldValue6);
						topupParams.setCustomValues(addCustomValues);
					}
                            
                    topup.setWebServiceHelper(webServiceHelper);
                    topup.setPaymentHelper(paymentHelper);
                    topup.setPaymentServiceLocal(paymentServiceLocal);
                    topup.setAccountHelper(accountHelper);
                    topup.setApplicationServiceLocal(applicationServiceLocal);
                    topup.setElementServiceLocal(elementServiceLocal);
                             
                    PaymentResult_Extended result = topup.doPaymentExternal(topupParams);
                        
                    switch (result.getStatus()) {
                       case PROCESSED:
                    	   Long transferid = result.getTransfer().getId();
                    	   System.out.println("transferId :"+transferid);
                    	   paymentParam.setEcashRefNo(transferid.toString()); 
                           paymentTransferDao.update(paymentParam);
                           System.out.println("paymentparam :"+paymentParam);
                           response.setEmoneyAccountNo(emoneyAccountNo);
                    	   status.setIsError(false);
                           status.setErrorCode(RESPONSE_PAYMENT_SUCCESS);
                           status.setStatusDescription("PROCESSED");
                           response.setEmoneyAccountName(member.getName()+"/"+paymentParam.getDescription());

                           if (result.getTransfer().getTransactionNumber() != null) {
                               response.setTopupAmount(result.getTransfer().getTransactionNumber());
                           }
                           else {
                               response.setTopupAmount("");
                           }
                           
                            break;
                      case RECEIVER_UPPER_CREDIT_LIMIT_REACHED:
                           status.setIsError(false);
                           status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                           status.setStatusDescription("Account Exceed Limit.");
                          
                           break;
                              default:
                                 status.setIsError(true);
                                 status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                                 status.setStatusDescription("Payment Error" + result.getStatus());
                                 break;
                             }
             
                     } else {
                         
                    	 status.setIsError(false);
			                status.setErrorCode(MPT_RESPONSE_INVALID_AMOUNT);
			                status.setStatusDescription("INVALID AMOUNT");
			              }
				}else{
					status.setIsError(false);
					status.setErrorCode(MPT_RESPONSE_BILL_ALREADY_PAID);
					status.setStatusDescription("Transaksi Sudah dilakukan");
				} 
				}else{
            	status.setIsError(false);
            	status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                status.setStatusDescription("INVALID_MERCHANT_ID");

            }
            	
            }else {
               status.setIsError(false);
           	status.setErrorCode(MPT_RESPONSE_CODE_PT_NOT_FOUND);
               status.setStatusDescription("INVALID PTNO");
               
            }
            response.setStatus(status);
             
            
         // Add reverse in case the trx is created but response is not success
            if (!status.getErrorCode().equals(RESPONSE_PAYMENT_SUCCESS)) {
                
                try {
                    Transfer transfer = paymentServiceLocal.loadTransferForReverse(traceId);

                    if (transfer != null) {
                        Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                    }
                    
                } catch (Exception e) {
                    logger.error(e);
                }
            }
        
        }else{

        try {
            
            if (!validateEcashNumber(emoneyAccountNo, false)) {
                status.setIsError(false);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription("Payment: Invalid e-cash number.");
                response.setStatus(status);
                return response;
            }

            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            
            if (isMVAAccount(emoneyAccountNo)) {
            	//Modification for issue top up 8 Juni 2014
                member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo.substring(2), FETCH);
            } else {
                member = elementServiceLocal.loadByPrincipal(principalType, emoneyAccountNo, FETCH);
            }

            if (member != null) {
            	
            	String cifName="";
            	if (checkTransferTypeCode(member.getGroup().getId().toString(), "topup.whitelist_name.bygroupid")) {
            		Collection<MemberCustomFieldValue> memberCustomFields = member
                            .getCustomValues();
                    Iterator<MemberCustomFieldValue> itMember = memberCustomFields.iterator();
                    // paymentParams.setToMember(member.getUser().getUsername());
                    while (itMember.hasNext()) {
                        MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) itMember
                                .next();
                        if (mCustomField.getField().getInternalName()
                                .equals(CIF_NAME)) {
                            cifName = mCustomField.getValue();
                            break;
                        }
                    }
                    
                    if (cifName.length()>10) {
                    	cifName=cifName.substring(0,10);
					}
                    String name=member.getName();
                    if (member.getName().length()>10) {
						name=member.getName().substring(0,10);
					}
                    memberName = cifName+"/"+name;
				}else{
					memberName = member.getName();
				}
                final MemberGroup memberGroup = member.getMemberGroup();
                
                if (memberGroup.isActive()) {
                    
                    AccountHistorySearchParameters params = new AccountHistorySearchParameters();
                    
                    if (isMVAAccount(emoneyAccountNo)) {
                    	//Modification for issue top up 8 Juni 2014
                        params.setPrincipal(emoneyAccountNo.substring(2));
                    } else {
                        params.setPrincipal(emoneyAccountNo);
                    }
    
                    final TransferQuery query = accountHelper.toQuery(params, member);
                    final AccountStatus accountStatus = accountServiceLocal.getCurrentStatus(new AccountDTO(member, query.getType()));
                    
                    accountBalance = accountStatus.getAvailableBalance();
                    upperLimit = accountStatus.getUpperCreditLimit();
                    
                    Long accountType = accountStatus.getAccount().getType().getId();
                    
                    if (upperLimit == null) {
                        upperLimit = BigDecimal.ZERO;
                    }
                    
                    if (accountBalance.compareTo(BigDecimal.ZERO) > 0) {
    
                        if (upperLimit.compareTo(accountBalance) >= 0) {
                            maxTopup = upperLimit.subtract(accountBalance);
                        } else {
                            maxTopup = BigDecimal.ZERO;
                        }
                        
                    } else {
                        maxTopup = upperLimit;
                    }
                    
                    response.setEmoneyAccountNo(emoneyAccountNo);
                    response.setEmoneyAccountName(memberName);
                    
                    if (paymentAmount.compareTo(BigDecimal.ZERO) > 0) {
                        
                        if (maxTopup.compareTo(paymentAmount) >= 0) {
                            topupParams.setFromSystem(true);
                            
                            if (isMVAAccount(emoneyAccountNo)) {
                            	//Modification for issue top up 8 Juni 2014
                                topupParams.setToMember(emoneyAccountNo.substring(2));
                            } else {
                                topupParams.setToMember(emoneyAccountNo);
                            }
                            
                            
                            topupParams.setAmount(paymentAmount);

                            Long trxTypeMandiri = 0L;
//                            Long trxTypeMVA = 0L;
                            
                            try {
                                // Perubahan terkait dengan penambahan informasi untuk bisa membedakan asal dari mana sumber dana pengisian. Topup dilakukan di atm mandiri  (i.e.: BM = Bank Mandiri, ATMB = ATM Bersama)
                                String sourceOfFund = "";
                                
                                if (reference3 != null && reference3.trim().length() > 0) {
                                    sourceOfFund = "." + reference3.trim();
                                }

                                trxTypeMandiri = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.transaction_type." + channelId + "." + accountType + sourceOfFund));
//                                trxTypeMVA = Long.parseLong(EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.transaction_type." + channelId + "." + accountType + "." + companyCode + sourceOfFund));

                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                            
//                            if (companyCode.equals(MANDIRI_COMPANY_CODE)) {
                                topupParams.setTransferTypeId(trxTypeMandiri);
//                            } else {
//                                topupParams.setTransferTypeId(trxTypeMVA);
//                            }
                            
                            topupParams.setTraceNumber(traceId);
                            topupParams.setTraceData(rekonKey.toString());
                            
                            topup.setWebServiceHelper(webServiceHelper);
                            topup.setPaymentHelper(paymentHelper);
                            topup.setPaymentServiceLocal(paymentServiceLocal);
                            topup.setAccountHelper(accountHelper);
                            topup.setApplicationServiceLocal(applicationServiceLocal);
                            topup.setElementServiceLocal(elementServiceLocal);
                            
                            PaymentResult result = topup.doPayment(topupParams);
                       
                            switch (result.getStatus()) {
                                case PROCESSED:
                                    status.setIsError(false);
                                    status.setErrorCode(RESPONSE_PAYMENT_SUCCESS);
                                    status.setStatusDescription("Success");
                                    if (result.getTransfer().getTransactionNumber() != null) {
                                        response.setTopupAmount(result.getTransfer().getTransactionNumber());
                                    }
                                    else {
                                        response.setTopupAmount("");
                                    }
                                    break;
                                case RECEIVER_UPPER_CREDIT_LIMIT_REACHED:
                                    status.setIsError(true);
                                    status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                                    status.setStatusDescription("Account Exceed Limit.");
                                    break;
                                case MAX_DAILY_AMOUNT_RECEIVER_EXCEEDED:
                                    status.setIsError(true);
                                    status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                                    status.setStatusDescription("Account Exceed Limit.");
                                    break;
                                case MAX_MONTH_AMOUNT_RECEIVER_EXCEEDED:
                                    status.setIsError(true);
                                    status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                                    status.setStatusDescription("Account Exceed Limit.");
                                    break;
                                default:
                                    status.setIsError(true);
                                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                                    status.setStatusDescription("Payment Error" + result.getStatus());
                                    break;
                            }
                            
                            
                            if (status.getErrorCode().equals(RESPONSE_PAYMENT_SUCCESS)){
                                maxTopup = maxTopup.subtract(paymentAmount);
                            }
                            
                        } else {
                            
                            status.setIsError(true);
                            status.setErrorCode(RESPONSE_PAYMENT_EXCEED_LIMIT);
                            status.setStatusDescription("Exceed maximum topup of (" + maxTopup.toPlainString() + ")");
                        }
                        
                    } else {
                        status.setIsError(true);
                        status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                        status.setStatusDescription("Payment amount is zero");
                    }
                    
                } else {
        
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                    status.setStatusDescription("Account Not Active");
                }
            } else {
                status.setIsError(true);
                status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                status.setStatusDescription("Account not found");
            }
            
            response.setEmoneyAccountNo(emoneyAccountNo);
            response.setEmoneyAccountName(memberName);

        } 
        catch (EntityNotFoundException e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            response.setStatus(status);
        }
        
        // Add reverse in case the trx is created but response is not success
        if (!status.getErrorCode().equals(RESPONSE_PAYMENT_SUCCESS)) {
            
            try {
                Transfer transfer = paymentServiceLocal.loadTransferForReverse(traceId);

                if (transfer != null) {
                    Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                }
                
            } catch (Exception e) {
                logger.error(e);
            }
            
        }
		}  
        
        return response;

    }

    @SuppressWarnings("unused")
    public TopupReversalResponse reverse_extended(final TopupReversalRequestExtended request) {
        TopupReversalResponse response = new TopupReversalResponse();
        Status status = new Status();
        String language = request.getLanguage();
        String oriTrxDateTime = request.getOrigTrxDateTime();
        String oriTransmissionDateTime = request.getOrigTransmissionDateTime();
        String reference1 = request.getReference1();
        String reference2 = request.getReference2();
        String reference3 = request.getReference3();
        String terminalID = request.getTerminalID();
        String currency = request.getCurrency();
        String transactionId = request.getTransactionId();

        logger.info("oriTrxDateTime : " + oriTrxDateTime);
        logger.info("oriTransmissionDateTime : " + oriTransmissionDateTime);
        logger.info("transactionId : " + transactionId);
        
        if (terminalID == null) {
            terminalID = "";
        }
        
        StringBuilder traceIdBuilder = new StringBuilder();
        traceIdBuilder.append(terminalID);
        traceIdBuilder.append(oriTrxDateTime);
        traceIdBuilder.append(oriTransmissionDateTime);
        traceIdBuilder.append(transactionId);

        final String traceId = traceIdBuilder.toString();
        String ptNumber = "";
        boolean isMTP = false;
        
        try {
            Transfer transfer = paymentServiceLocal.loadTransferForReverse(traceId);
            
            if (transfer != null) {
                Transfer reversetransfer = paymentServiceLocal.chargeback(transfer);
                status.setIsError(false);
                status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                status.setStatusDescription("Topup is successfully reversed");
                
                // Update ecash ref no in payment transfer 
                // Copy original payment record to payment transfer history
                try {
                    Collection<PaymentCustomFieldValue> transferCustomFields = transfer.getCustomValues();
                    final Iterator<PaymentCustomFieldValue> itTransfer = transferCustomFields.iterator();
                    while (itTransfer.hasNext()) {
                        PaymentCustomFieldValue pvcfv = itTransfer.next();

                        if(pvcfv.getField().getInternalName().startsWith("ptnumber_mpt")){
                            ptNumber = pvcfv.getValue();
                            isMTP = true;
                            break;
                        }
                    }
                    
                    if (isMTP) {
                        final String ptNumberUnique = ptNumber + transfer.getAmount().setScale(0).toString();
                        
                        PaymentTransferProcessingRequest paymentTransferProcessingRequest = new PaymentTransferProcessingRequest();
                        paymentTransferProcessingRequest.setPtNumberUnique(ptNumberUnique);
                        paymentTransferProcessingRequest.setStatus("REVERSAL");
                        paymentTransferProcessingRequest.setStatusDesc("Transaksi gagal, silahkan melakukan pembayaran ulang.");
                        
                        paymentTransferService.move(paymentTransferProcessingRequest);
                    }

                } catch (final Exception ex) {
                    logger.error(ex);
                }
                
            } else {
                status.setIsError(false);
                status.setErrorCode(RESPONSE_REVERSE_SUCCESS);
                status.setStatusDescription("Topup transaction not found");
            }
            
        } catch (Exception e) {
            logger.error(e);
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        }
        finally {
            response.setStatus(status);
        }
        
        return response;
    }
    
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setGroupHelper(final GroupHelper groupHelper) {
        this.groupHelper = groupHelper;
    }
    
	public void setPaymentTransferService(
			PaymentTransferService paymentTransferService) {
		this.paymentTransferService = paymentTransferService;
	}
    
    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
    
    public void setGroupServiceLocal(final GroupServiceLocal groupService) {
        this.groupServiceLocal = groupService;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountServiceLocal = accountService;
    }
    
    public void setAccountHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }
    
    public void setPaymentHelper(final PaymentHelper paymentHelper) {
        this.paymentHelper = paymentHelper;
    }
    
    public void setWebServiceHelper(final WebServiceHelper webServiceHelper) {
        this.webServiceHelper = webServiceHelper;
    }
    
    public void setPaymentServiceLocal(final PaymentServiceLocal paymentService) {
        paymentServiceLocal = paymentService;
    }
    
    public void setApplicationServiceLocal(final ApplicationServiceLocal applicationServiceLocal) {
        this.applicationServiceLocal = applicationServiceLocal;
    }

    public void setMemberHelper(final MemberHelper memberHelper) {
        this.memberHelper = memberHelper;
    }
    
    public void setHashHandler(final HashHandler hashHandler) {
        this.hashHandler = hashHandler;
    }
    
    private boolean validateEcashNumber(final String ecashNumber, boolean excludeAgent) {
        String regex;
        boolean isValid = false;
        String ecashNumberEx = "";
        
        if (isMVAAccount(ecashNumber)) {
        	//Modification for issue top up 8 Juni 2014
            ecashNumberEx = ecashNumber.substring(2);
        } else {
            ecashNumberEx = ecashNumber;
        }
        
        if (validEcashNumbers.length > 0) {
        
            for (int i=0; i<validEcashNumbers.length; i++) {
                
                regex = "";
            
                // rule to prevent auto create
                if (!(validEcashNumbers[i].equals("agen") && excludeAgent)) {

                    try {
                        regex = EmoneyConfiguration.getEmoneyProperties().getProperty(validEcashNumbers[i].toString() + ".regex");
                    } catch (IOException e) {
                        logger.error(e);
                    }
                
                    if (regex.length() > 0) {
                        final Pattern validateEcashPattern = Pattern.compile(regex);
                        final Matcher validateEcashMatcher = validateEcashPattern.matcher(ecashNumberEx);
                        isValid = validateEcashMatcher.find();
                    }

                }
                
                
                if (isValid) {
                    break;
                }

            }

        }
        
        return isValid;
    }

    public void setSmsMailingServiceLocal(SmsMailingServiceLocal smsMailingServiceLocal) {
        this.smsMailingServiceLocal = smsMailingServiceLocal;
    }

    public void sendSMS(Member member){
        
        try {
           final SmsMailing sms = new SmsMailing();
           final String smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("topup.auto-create");
           sms.setFree(true);
           sms.setMember(member);
           sms.setText(smsTemplate);
           LoggedUser.init(member.getUser());
           smsMailingServiceLocal.send(sms);
           LoggedUser.cleanup();
       } catch (Exception e) {
           logger.error(e);
       }

    }
    
    /*
     * MVA Transaction Validation
     */
    private boolean isMVAAccount(final String emoneyAccountNo) {
        return (emoneyAccountNo != null && emoneyAccountNo.startsWith(MVA_PREFIX) && emoneyAccountNo.length() >= MVA_EMONEYNO_MIN_LENGTH);
    }
    
    public void setPaymentTransferDao(PaymentTransferDAO paymentTransferDao) {
		this.paymentTransferDao = paymentTransferDao;
	}
    
    public void setBillDetails(String getAmount, TopupInquiryResponse response){
    	BillDetail billDetail = new BillDetail();
        billDetail.setBillCode(BILL_CODE_MOKU);
        billDetail.setBillName("");
        billDetail.setBillShortName("");
        billDetail.setBillAmount(getAmount);
        billDetail.setReference1("");
        billDetail.setReference2("");
        billDetail.setReference3("");
        ArrayOfBillDetails arrayOfbills = new ArrayOfBillDetails();
        arrayOfbills.setArrayOfAccounts(new BillDetail[] {billDetail});
        response.setBillDetails(arrayOfbills);
    }
    
    public boolean checkTransferTypeCode(String transferTypeCode, String prop){
		String temp = "";
		try {
			temp = EmoneyConfiguration.getEmoneyProperties().getProperty(prop);
			while(true){
				if(temp.indexOf(",")!= -1){
					if(temp.substring(0, temp.indexOf(",")).equals(transferTypeCode)){
						return true;
					}
				}else{
					if(temp.substring(0, temp.length()).equals(transferTypeCode)){
						return true;
					}
					break;
				}
				temp = temp.substring(temp.indexOf(",")+1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
		
	}
    
}
