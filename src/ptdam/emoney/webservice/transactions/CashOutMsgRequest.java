/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class CashOutMsgRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;
    
    private static final long serialVersionUID = 6547983351025920904L;
    private String emoneyAccountNo;
    private String Denom;
    private String OTP;
    private String transactionId;
    private String currency;
    private String transactionDateTime;
    private String captureDate;
    private String locationCountry;
    private String locationState;
    private String locationCity;
    private String terminalId;
    private String retrievalReferenceNo;
    private String localTransactionDate;
    private String localTransactionTime;
    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }
    /**
     * Getter for property denom
     */
    public String getDenom() {
        return Denom;
    }
    /**
     * Getter for property oTP
     */
    public String getOTP() {
        return OTP;
    }
    /**
     * Getter for property transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }
    /**
     * Getter for property currency
     */
    public String getCurrency() {
        return currency;
    }
    /**
     * Getter for property transactionDateTime
     */
    public String getTransactionDateTime() {
        return transactionDateTime;
    }
    /**
     * Getter for property captureDate
     */
    public String getCaptureDate() {
        return captureDate;
    }
    /**
     * Getter for property locationCountry
     */
    public String getLocationCountry() {
        return locationCountry;
    }
    /**
     * Getter for property locationState
     */
    public String getLocationState() {
        return locationState;
    }
    /**
     * Getter for property locationCity
     */
    public String getLocationCity() {
        return locationCity;
    }
    /**
     * Getter for property terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }
    /**
     * Getter for property retrievalReferenceNo
     */
    public String getRetrievalReferenceNo() {
        return retrievalReferenceNo;
    }
    /**
     * Getter for property localTransactionDate
     */
    public String getLocalTransactionDate() {
        return localTransactionDate;
    }
    /**
     * Getter for property localTransactionTime
     */
    public String getLocalTransactionTime() {
        return localTransactionTime;
    }
    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }
    /**
     * Setter for property denom
     */
    public void setDenom(String denom) {
        Denom = denom;
    }
    /**
     * Setter for property oTP
     */
    public void setOTP(String oTP) {
        OTP = oTP;
    }
    /**
     * Setter for property transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
    /**
     * Setter for property currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    /**
     * Setter for property transactionDateTime
     */
    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }
    /**
     * Setter for property captureDate
     */
    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }
    /**
     * Setter for property locationCountry
     */
    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }
    /**
     * Setter for property locationState
     */
    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }
    /**
     * Setter for property locationCity
     */
    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }
    /**
     * Setter for property terminalId
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }
    /**
     * Setter for property retrievalReferenceNo
     */
    public void setRetrievalReferenceNo(String retrievalReferenceNo) {
        this.retrievalReferenceNo = retrievalReferenceNo;
    }
    /**
     * Setter for property localTransactionDate
     */
    public void setLocalTransactionDate(String localTransactionDate) {
        this.localTransactionDate = localTransactionDate;
    }
    /**
     * Setter for property localTransactionTime
     */
    public void setLocalTransactionTime(String localTransactionTime) {
        this.localTransactionTime = localTransactionTime;
    }
     
}
