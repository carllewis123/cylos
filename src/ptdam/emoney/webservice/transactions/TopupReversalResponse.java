/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.util.List;

import ptdam.emoney.webservice.member.Status;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class TopupReversalResponse implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;
    
    private static final long serialVersionUID = 3150941032719545046L;
    private String emoneyAccountNo;
    private String emoneyAccountName;
    private String topupAmount;
    private String emoneyTransactionId;
    private String accountMaxTopup;
    /**
     * Getter for property emoneyAccountName
     */
    public String getEmoneyAccountName() {
        return emoneyAccountName;
    }
    /**
     * Getter for property accountMaxTopup
     */
    public String getAccountMaxTopup() {
        return accountMaxTopup;
    }
    /**
     * Setter for property emoneyAccountName
     */
    public void setEmoneyAccountName(String emoneyAccountName) {
        this.emoneyAccountName = emoneyAccountName;
    }
    /**
     * Setter for property accountMaxTopup
     */
    public void setAccountMaxTopup(String accountMaxTopup) {
        this.accountMaxTopup = accountMaxTopup;
    }
    private Status status;
    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }
    /**
     * Getter for property topupAmount
     */
    public String getTopupAmount() {
        return topupAmount;
    }
    /**
     * Getter for property emoneyTransactionId
     */
    public String getEmoneyTransactionId() {
        return emoneyTransactionId;
    }
    /**
     * Getter for property status
     */
    public Status getStatus() {
        return status;
    }
    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }
    /**
     * Setter for property topupAmount
     */
    public void setTopupAmount(String topupAmount) {
        this.topupAmount = topupAmount;
    }
    /**
     * Setter for property emoneyTransactionId
     */
    public void setEmoneyTransactionId(String emoneyTransactionId) {
        this.emoneyTransactionId = emoneyTransactionId;
    }
    /**
     * Setter for property status
     */
    public void setStatus(Status status) {
        this.status = status;
    }

}
