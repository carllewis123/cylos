/*
    This file is part of Cyclos <http://project.cyclos.org>

    Cyclos is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Cyclos. If not, see <http://www.gnu.org/licenses/>.

 */
package ptdam.emoney.webservice.transactions;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ArrayOfBillDetails implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 6596335305658056553L;
    private BillDetail[] arrayOfBillDetail;

    public BillDetail[] getArrayOfAccounts() {
        return arrayOfBillDetail;
    }

    public void setArrayOfAccounts(BillDetail[] arrayOfBillDetail) {
        this.arrayOfBillDetail = arrayOfBillDetail;
    }
    
}
