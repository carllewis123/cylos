package ptdam.emoney.webservice.transactions;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.payments.FundTransferPaymentForm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.AccountDetail;
import ptdam.emoney.webservice.client.AcctInfoInquiryRequest;
import ptdam.emoney.webservice.client.AcctInfoInquiryResponse;
import ptdam.emoney.webservice.client.ArrayOfAccounts;
import ptdam.emoney.webservice.client.DataHeaderMBase;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

public class AjaxRekeningNumber extends Action {
	private final Log logger = LogFactory.getLog(AjaxRekeningNumber.class);
	private CoreWebServiceFactory coreWSFactory;
	
	 @Inject
	    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
	        this.coreWSFactory = coreWSFactory;
	    }

	public ActionForward execute(ActionMapping mapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse res) throws Exception {
		HttpSession session = request.getSession();
		FundTransferPaymentForm form = (FundTransferPaymentForm) actionForm;
		
		if(session != null && session.getAttribute("cifNumber")  != null){
			String cifNumber = (String) session.getAttribute("cifNumber");
			loadListRekening(cifNumber, form, session);
		}
		return mapping.findForward("success");
	}
	
	 public void loadListRekening (String cifNumber, FundTransferPaymentForm form, HttpSession session) throws IOException{	    	
	    	if(cifNumber != null && !cifNumber.equals("")){
	    		ArrayOfAccounts arrAcct = new ArrayOfAccounts();
			   AcctInfoInquiryResponse resp = getCIFAccountInquiry(cifNumber,null,null);
			   if(resp !=null && resp.getBdyAccountList() != null && resp.getBdyAccountList().getArrayOfAccounts() != null){
				   arrAcct = resp.getBdyAccountList();
				   int noOfRecords = 0;
					try {
						noOfRecords = Long.valueOf(EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.noofrecordstoretrieve")).intValue();
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			   while(noOfRecords > 1 && resp.getDataHeader() != null && resp.getDataHeader().getMoreRecordsIndicator() !=null && resp.getDataHeader().getMoreRecordsIndicator().equalsIgnoreCase("Y")){
					   AccountDetail[] acctDetails =  arrAcct.getArrayOfAccounts();
					   String accountNumber = resp.getBdyAccountList().getArrayOfAccounts()[noOfRecords-1].getAccountNumber();
					   String accountType = resp.getBdyAccountList().getArrayOfAccounts()[noOfRecords-1].getAccountType();
					   resp = null;	
					   resp = getCIFAccountInquiry(cifNumber,accountNumber,accountType);
					   if(resp !=null && resp.getBdyAccountList() !=null  && resp.getBdyAccountList().getArrayOfAccounts() != null){
						   for(AccountDetail acctDet : resp.getBdyAccountList().getArrayOfAccounts()){
							   if(!acctDet.getAccountNumber().equals(accountNumber)){
								   acctDetails = append(acctDetails,acctDet);
							   }
						   }
						   arrAcct.setArrayOfAccounts(acctDetails);
					   }
				   }
			   }
			
			   AccountDetail[] acctDetails =  arrAcct.getArrayOfAccounts();
			   ArrayList listRekeningNo = new ArrayList();
			   for(AccountDetail accDet : acctDetails ){
				   if(check(accDet.getAccountType())){
					   listRekeningNo.add(new LabelValueBean(accDet.getAccountNumber(), accDet.getAccountNumber()));
				   }
			   }
			   session.setAttribute("listRekeningNo", listRekeningNo);
			   form.setListRekeningNo(listRekeningNo);
		   }
	    }
	    
	 public AcctInfoInquiryResponse getCIFAccountInquiry(String customerNumber, String accountNumber, String accountType) {
	    	AcctInfoInquiryRequest request = new AcctInfoInquiryRequest();
	    	AcctInfoInquiryResponse response = null;
	        
	    	SimpleDateFormat sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0Z'");
	        try {
	            final String extId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.extId");
	            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.tellerId");
	            final String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.journalSequence");
	            final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.transactionCode");
	            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.channelId");
	            final long noOfRecords = Long.valueOf(EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.noofrecordstoretrieve")).longValue();
	            
	            request.setBdyCustomerNumber(customerNumber);
	            if(accountNumber!=null && accountType!=null){
	            	request.setBdyAccountNumber(accountNumber);
	            	request.setBdyAccountType(accountType);
	            }
	            
	            Calendar actualDate = Calendar.getInstance();
	            String sActualDate = sdfTimestamp.format(actualDate.getTime());
	            
	            RequestHeader header = new RequestHeader();
	            header.setHdrExternalId(extId);
	            header.setHdrTellerId(Long.parseLong(tellerId));
	            header.setHdrJournalSequence(Long.parseLong(journalSeq));
	            header.setHdrTransactionCode(tranCode);
	            header.setHdrChannelId(channelId);
	            header.setHdrTimestamp(sActualDate);
	            
	            DataHeaderMBase dataHeader = new DataHeaderMBase();
	            dataHeader.setNoOfRecordsToRetrieve(noOfRecords);
	            request.setDataHeader(dataHeader);
	                        
	            SecurityHeader securityHeader = new SecurityHeader();
	            securityHeader.setHdrAppliNo("1");
	            securityHeader.setHdrChallenge1("1");
	            securityHeader.setHdrChallenge2("1");
	            securityHeader.setHdrChallenge3("1");
	            securityHeader.setHdrChallenge4("1");
	            securityHeader.setHdrChallengeType("1");
	            securityHeader.setHdrCorporateId("1");
	            securityHeader.setHdrCustomerAdditionalId("1");
	            securityHeader.setHdrResponseToken("1");
	            request.setSecurityHeader(securityHeader);
	            request.setHeader(header);
	            
	            response = coreWSFactory.getAccountInquiryWebService().inquiry(request);
	            String responseMsg = "";
	            if (response != null) {
	                if (response.getResponseHeader() != null) {
	                    switch (response.getResponseHeader().getHdrResponseCode()){
	                        case 1: 
	                        	logger.info("sukses get CIF account.");
	                        	//return response;
	                        	break;
	                        default:
	                            responseMsg = response.getResponseHeader().getHdrResponseMessage();
	                            logger.error(responseMsg);
                         throw new Exception("System Error (Host Error) : " + responseMsg);
	                    }
	                } else {
	                    logger.error("Response Header is empty.");
                 throw new Exception("System Error (Response Header is empty).");
	                }
	            } else {
	                logger.error("Response is empty.");
             throw new Exception("System Error (Response is empty).");
	            }
	            
	        } 
	        catch (final SocketTimeoutException e) {
	        	logger.error(e);
	        }
	        catch (final Exception e) {
	        	logger.error(e);
	        }
     
     return response;
	    }
	    
	    static <T> T[] append(T[] arr, T element) {
		    final int N = arr.length;
		    arr = Arrays.copyOf(arr, N + 1);
		    arr[N] = element;
		    return arr;
		}
	        
	    public boolean check(String a){
			String temp = "";
			try {
				temp = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifacctinquiry.accounttype");
				while(true){
					if(temp.indexOf(",")!= -1){
						if(temp.substring(0, temp.indexOf(",")).equals(a)){
							return true;
						}
					}else{
						if(temp.substring(0, temp.length()).equals(a)){
							return true;
						}
						break;
					}
					temp = temp.substring(temp.indexOf(",")+1);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
			
		}

}