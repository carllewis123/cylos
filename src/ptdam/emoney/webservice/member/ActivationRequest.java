/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.member;

import java.io.Serializable;
import java.util.List;

import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;

/**
 * Parameters used to update members
 * 
 * @author luis
 */
public class ActivationRequest implements Serializable {

//    private static final long              serialVersionUID = -5508819586207313001L;

    private static final long serialVersionUID = 9222322189006974444L;
    private String                         language;
    private String                         trxDateTime;
    private String                         transmissionDateTime;
    private String                         channelID;
    private String                         emoneyAccountNo;
    private String                         nama;
    private String                         noKtp;
    private String                         alamat;
    private String                         alamatEmail;
    private String                         tanggalLahir;
    private String                         pekerjaan;
    private String                         namaIbuKandung;
    private String                         tempatLahir;
    private String                         cifNo;
    
    public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getAlamatEmail() {
		return alamatEmail;
	}
	public void setAlamatEmail(String alamatEmail) {
		this.alamatEmail = alamatEmail;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getPekerjaan() {
		return pekerjaan;
	}
	public void setPekerjaan(String pekerjaan) {
		this.pekerjaan = pekerjaan;
	}
	public String getNamaIbuKandung() {
		return namaIbuKandung;
	}
	public void setNamaIbuKandung(String namaIbuKandung) {
		this.namaIbuKandung = namaIbuKandung;
	}
	public String getTempatLahir() {
		return tempatLahir;
	}
	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}
	
    /**
     * Getter for property language
     */
    public String getLanguage() {
        return language;
    }
    /**
     * Getter for property trxDateTime
     */
    public String getTrxDateTime() {
        return trxDateTime;
    }
    /**
     * Getter for property transmissionDateTime
     */
    public String getTransmissionDateTime() {
        return transmissionDateTime;
    }
    /**
     * Getter for property channelID
     */
    public String getChannelID() {
        return channelID;
    }
    /**
     * Getter for property emoneyAccountNo
     */
    public String getEmoneyAccountNo() {
        return emoneyAccountNo;
    }
    /**
     * Getter for property cifNo
     */
    public String getCifNo() {
        return cifNo;
    }
    /**
     * Setter for property language
     */
    public void setLanguage(String language) {
        this.language = language;
    }
    /**
     * Setter for property trxDateTime
     */
    public void setTrxDateTime(String trxDateTime) {
        this.trxDateTime = trxDateTime;
    }
    /**
     * Setter for property transmissionDateTime
     */
    public void setTransmissionDateTime(String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }
    /**
     * Setter for property channelID
     */
    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }
    /**
     * Setter for property emoneyAccountNo
     */
    public void setEmoneyAccountNo(String emoneyAccountNo) {
        this.emoneyAccountNo = emoneyAccountNo;
    }
    /**
     * Setter for property cifNo
     */
    public void setCifNo(String cifNo) {
        this.cifNo = cifNo;
    }

}
