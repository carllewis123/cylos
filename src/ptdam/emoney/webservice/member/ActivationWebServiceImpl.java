/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.webservice.member;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jws.WebService;

import nl.strohalm.cyclos.dao.customizations.CustomFieldValueDAO;
import nl.strohalm.cyclos.entities.Relationship;
import nl.strohalm.cyclos.entities.access.MemberUser;
import nl.strohalm.cyclos.entities.access.PrincipalType;
import nl.strohalm.cyclos.entities.access.User;
import nl.strohalm.cyclos.entities.accounts.AccountType;
import nl.strohalm.cyclos.entities.customization.fields.CustomFieldValue;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.exceptions.EntityNotFoundException;
import nl.strohalm.cyclos.entities.groups.Group;
import nl.strohalm.cyclos.entities.groups.MemberGroup;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.sms.SmsMailing;
import nl.strohalm.cyclos.services.accounts.AccountServiceLocal;
import nl.strohalm.cyclos.services.accounts.CreditLimitDTO;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldServiceLocal;
import nl.strohalm.cyclos.services.elements.ElementServiceLocal;
import nl.strohalm.cyclos.services.fetch.FetchServiceLocal;
import nl.strohalm.cyclos.services.sms.SmsMailingServiceLocal;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.RelationshipHelper;
import nl.strohalm.cyclos.utils.access.LoggedUser;
import nl.strohalm.cyclos.utils.validation.ValidationError;
import nl.strohalm.cyclos.utils.validation.ValidationException;
import nl.strohalm.cyclos.webservices.WebServiceContext;
import nl.strohalm.cyclos.webservices.model.RegistrationFieldValueVO;
import nl.strohalm.cyclos.webservices.utils.ChannelHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DuplicateKeyException;

import ptdam.emoney.EmoneyConfiguration;

/**
 * Web service implementation
 * @author luis
 */
@WebService(name = "emoneyactivation", serviceName = "emoneyactivation")
public class ActivationWebServiceImpl implements ActivationWebService {

    private static final Relationship[]   FETCH = { Element.Relationships.USER, Element.Relationships.GROUP, Member.Relationships.IMAGES, Member.Relationships.CUSTOM_VALUES };
    private ElementServiceLocal           elementServiceLocal;
    private MemberCustomFieldServiceLocal memberCustomFieldServiceLocal;
    private ChannelHelper                 channelHelper;
    private CustomFieldHelper             customFieldHelper;
    private AccountServiceLocal           accountServiceLocal;
    private FetchServiceLocal       fetchService;
    
    private String      RESPONSE_ACTIVATION_SUCCESS = "00";
    private String      RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE = "76";
    private String      RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED = "12";
    private String      RESPONSE_ACCOUNT_NOT_FOUND = "87";
    private String      RESPONSE_SYSTEM_ERROR = "99";
    private String		CIF_MAX_LIMIT = "98";
    
    private String      UNREGISTERED = "UNREGISTERED";
    private String      REGISTERED = "REGISTERED";
    
    private String      CUSTOM_FIELD_CIF = "cif";
    private String      CUSTOM_FIELD_NAMA_CIF = "nama_cif";
    
    private String      NEW_ACCOUNT = "Nasabah Belum Terdaftar";
    private String      REMARKS = "Aktivasi Emoney Online";
    
    private String      NO_KTP = "ktp";
    private String      ALAMAT = "address1";
    private String      TANGGAL_LAHIR = "dateofbirth";
    private String      PEKERJAAN = "pekerjaan";
    private String      NAMA_IBU_KANDUNG = "nama_ibu_kandung";
    private String      TEMPAT_LAHIR = "tempat_lahir";
    private String      RESPONSE_REQUEST_REQUIRED  = "E99";
    
//    private MessageServiceLocal       messageServiceLocal;
    private SmsMailingServiceLocal         smsMailingServiceLocal;
    private final Log                      logger                    = LogFactory.getLog(ActivationWebServiceImpl.class);

    protected CustomFieldValueDAO         customFieldValueDao;
    CustomFieldValue fieldValue;

    public final void setCustomFieldValueDao(final CustomFieldValueDAO customFieldValueDao) {
        this.customFieldValueDao = customFieldValueDao;
    }
    
    public ActivationWebServiceImpl() {
        try {
            RESPONSE_ACTIVATION_SUCCESS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.activation.success");
            RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.activation.acctnotactive");
            RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.activation.alreadyregister");
            RESPONSE_ACCOUNT_NOT_FOUND = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.activation.acctnotfound");
            RESPONSE_SYSTEM_ERROR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.syserror");
            RESPONSE_REQUEST_REQUIRED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.request.required");
            CIF_MAX_LIMIT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney2host.response_code.cifmaxlimit");
            UNREGISTERED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.group.unregistered");
            REGISTERED = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.group.registered");
            CUSTOM_FIELD_CIF = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.cif_custom_fieldname");
            CUSTOM_FIELD_NAMA_CIF = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.nama_cif_custom_fieldname");
            NEW_ACCOUNT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.new_account");
            REMARKS = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.remarks");
            NO_KTP = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.noktp_custom_fieldname");
            ALAMAT = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.alamat_custom_fieldname");
            TANGGAL_LAHIR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.tanggallahir_custom_fieldname");
            PEKERJAAN = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.pekerjaan_custom_fieldname");
            NAMA_IBU_KANDUNG = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.namaibukandung_custom_fieldname");
            TEMPAT_LAHIR = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.tempatlahir_custom_fieldname");
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public ActivationResponse inquiry(final ActivationRequest request) {
        User user;
        String username;
        final Status status = new Status();
        final ActivationResponse response = new ActivationResponse();        

        username = request.getEmoneyAccountNo();
        
        if (StringUtils.isEmpty(username)) {
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription("Invalid Parameters");         
            response.setStatus(status);
            return response;
        }
        
        try {
            
            user = elementServiceLocal.loadUser(username, RelationshipHelper.nested(User.Relationships.ELEMENT, Element.Relationships.GROUP), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.CUSTOM_VALUES), RelationshipHelper.nested(User.Relationships.ELEMENT, Member.Relationships.IMAGES));
            if (!(user instanceof MemberUser)) {
                throw new Exception();
            }
            
            final Member member = (Member) user.getElement();
            final MemberGroup oldGroup = member.getMemberGroup();
            final String groupName = oldGroup.getName();

            response.setEmoneyAccountName(member.getName());
            response.setEmoneyAccountNo(username);

            if (!groupName.equals(UNREGISTERED)){
            
                if (groupName.equals(REGISTERED)){
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED);
                    status.setStatusDescription("Account has already registered");
                } else {

                    if (!oldGroup.isActive()){
                        status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                        status.setStatusDescription("Account Not Active");
                    } else {
                        status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED);
                        status.setStatusDescription("Account has already registered");
                    }
                }

            } else {                
                
                status.setIsError(false);
                status.setErrorCode(RESPONSE_ACTIVATION_SUCCESS);
                status.setStatusDescription("Success");
            }
            
        } catch (EntityNotFoundException e) {
            response.setEmoneyAccountNo(username);
            response.setEmoneyAccountName(NEW_ACCOUNT);
            status.setIsError(false);
            status.setErrorCode(RESPONSE_ACCOUNT_NOT_FOUND);
            status.setStatusDescription("Account not found");
        } catch (Exception e) {
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getMessage());
        } finally {
            response.setStatus(status);
        }
        return response;
    }

    @Override
    public ActivationResponse activate(final ActivationRequest request) {
        String emoneyaccountno;
        Member member;
        final Status status = new Status();
        final ActivationResponse response = new ActivationResponse();        

        emoneyaccountno = request.getEmoneyAccountNo();
        String cifNo = request.getCifNo();
        
        if (StringUtils.isEmpty(emoneyaccountno) || StringUtils.isEmpty(cifNo)) {
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription("Invalid Parameters");            
            response.setStatus(status);
            return response;
        }
        
        try {
            
            final PrincipalType principalType = channelHelper.resolvePrincipalType(null);
            member = elementServiceLocal.loadByPrincipal(principalType, emoneyaccountno, FETCH);
            
            member = (Member) member.clone();
            
            final MemberGroup oldGroup = member.getMemberGroup();
            final String groupName = oldGroup.getName();

            response.setEmoneyAccountName(member.getName());
            response.setEmoneyAccountNo(emoneyaccountno);

            if (!groupName.equals(UNREGISTERED)){
            
                if (groupName.equals(REGISTERED)){
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED);
                    status.setStatusDescription("Account has already registered");
                } else {
                    if (!oldGroup.isActive()){
                        status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_STATUS_NOT_ACTIVE);
                        status.setStatusDescription("Account Not Active");
                    } else {
                        status.setIsError(false);
                        status.setErrorCode(RESPONSE_ACCOUNT_HAS_ALREADY_REGISTERED);
                        status.setStatusDescription("Account has already registered");
                    }
                }
            } else {
                
            	//menghapus angka 0 di depan
            	int lengthCifNo = cifNo.length();
            	for (int i = 0; i < lengthCifNo; i++) {
					if (!cifNo.startsWith("0")) {
						
						break;
					}
					cifNo = cifNo.substring(1);
				}
            	
                RegistrationFieldValueVO customField = new RegistrationFieldValueVO(CUSTOM_FIELD_CIF, cifNo , true);
                List<RegistrationFieldValueVO> fieldValueVOs = Arrays.asList(customField);
                ArrayList<RegistrationFieldValueVO> addCustomValues = new ArrayList<RegistrationFieldValueVO>(fieldValueVOs);
         	   member.setEmail(request.getAlamatEmail());
               
               
                if (request.getNama()!=null && !request.getNama().isEmpty()&& 
               		request.getAlamat()!=null && !request.getAlamat().isEmpty() &&
               		request.getTanggalLahir()!=null && !request.getTanggalLahir().isEmpty()&&
               		request.getTempatLahir()!=null && !request.getTempatLahir().isEmpty()) {
            	   
            	   RegistrationFieldValueVO customFieldAlamat = new RegistrationFieldValueVO(ALAMAT, request.getAlamat() != null? request.getAlamat() : "" , false);
            	   addCustomValues.add(customFieldAlamat);
            	   
            	   RegistrationFieldValueVO customFieldDob = new RegistrationFieldValueVO(TANGGAL_LAHIR, request.getTanggalLahir() != null? request.getTanggalLahir() : "" , false);
            	   addCustomValues.add(customFieldDob);
            	   
                   RegistrationFieldValueVO customFieldkTP = new RegistrationFieldValueVO(NO_KTP, request.getNoKtp() != null? request.getNoKtp() : "" , false);
                   addCustomValues.add(customFieldkTP);
            	   
            	   RegistrationFieldValueVO customFieldPekerjaan = new RegistrationFieldValueVO(PEKERJAAN, request.getPekerjaan() != null? request.getPekerjaan() : "" , false);
            	   addCustomValues.add(customFieldPekerjaan);
            	   
            	   RegistrationFieldValueVO customFieldNamaIbu = new RegistrationFieldValueVO(NAMA_IBU_KANDUNG, request.getNamaIbuKandung() != null? request.getNamaIbuKandung() : "" , false);
            	   addCustomValues.add(customFieldNamaIbu);
            	   
            	   RegistrationFieldValueVO customFieldLahir = new RegistrationFieldValueVO(TEMPAT_LAHIR, request.getTempatLahir() != null? request.getTempatLahir() : "" , false);
            	   addCustomValues.add(customFieldLahir);

            	   RegistrationFieldValueVO customFieldNamaCif = new RegistrationFieldValueVO(CUSTOM_FIELD_NAMA_CIF, request.getNama() != null? request.getNama() : "" , false);
            	   addCustomValues.add(customFieldNamaCif);

                
                }else{
            	   status.setIsError(false);
            	   status.setErrorCode(RESPONSE_REQUEST_REQUIRED);
            	   status.setStatusDescription("parameter can not be empty");
            	   response.setStatus(status);
            	   return response;
               }
               
            	   
                List<MemberCustomField> allowedFields = customFieldHelper.onlyForGroup(memberCustomFieldServiceLocal.list(), member.getMemberGroup());
               Collection<MemberCustomFieldValue> newFieldValues = customFieldHelper.mergeFieldValues(member, addCustomValues, allowedFields);
               
                member.setCustomValues(newFieldValues);

                Member result = elementServiceLocal.changeMemberProfileByWebService(WebServiceContext.getClient(), member);
                
                if (result != null) {

                    MemberGroup newGroup = new MemberGroup();
                    
                    List<? extends Group> possibleNewGroups = elementServiceLocal.getPossibleNewGroups(member);

                    for (int i = 0; i < possibleNewGroups.size(); i++)
                    {
                        String newGroupName = possibleNewGroups.get(i).getName();
                        if (newGroupName.equals(REGISTERED))
                        {
                            newGroup = (MemberGroup) possibleNewGroups.get(i);  
                            CreditLimitDTO limits = accountServiceLocal.getCreditLimits(member);
                            Map<? extends AccountType, BigDecimal> upperLimitPerType = limits.getUpperLimitPerType();
                            Map<? extends AccountType, BigDecimal> cashInDailyLimitPerType = limits.getCashInPerDayLimitPerType();
                            Map<? extends AccountType, BigDecimal> cashInMonthlyLimitPerType = limits.getCashInPerMonthLimitPerType();
                            Map<? extends AccountType, BigDecimal> cashOutDailyLimitPerType = limits.getCashOutPerDayLimitPerType();
                            Map<? extends AccountType, BigDecimal> cashOutMonthlyLimitPerType = limits.getTrxPerMonthLimitPerType();

                            
                            final Map<AccountType, BigDecimal> newUpperLimitPerType = new HashMap<AccountType, BigDecimal>();
                            final Map<AccountType, BigDecimal> newCashInDailyLimitPerType = new HashMap<AccountType, BigDecimal>();
                            final Map<AccountType, BigDecimal> newCashInMonthlyLimitPerType = new HashMap<AccountType, BigDecimal>();
                            final Map<AccountType, BigDecimal> newCashOutDailyLimitPerType = new HashMap<AccountType, BigDecimal>();
                            final Map<AccountType, BigDecimal> newCashOutMonthlyLimitPerType = new HashMap<AccountType, BigDecimal>();
                            
                            if (upperLimitPerType != null && cashInDailyLimitPerType != null && cashInMonthlyLimitPerType != null & cashOutDailyLimitPerType != null & cashOutMonthlyLimitPerType != null) {
                                for (AccountType accountType : upperLimitPerType.keySet()) {
                                    final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultUpperCreditLimit();
                                    accountType = fetchService.fetch(accountType);
                                    newUpperLimitPerType.put(accountType, limit);
                                }
                                
                                for (AccountType accountType : cashInDailyLimitPerType.keySet()) {
                                    final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashInPerDayLimit();
                                    accountType = fetchService.fetch(accountType);
                                    newCashInDailyLimitPerType.put(accountType, limit);
                                }
                                
                                for (AccountType accountType : cashInMonthlyLimitPerType.keySet()) {
                                    final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashInPerMonthLimit();
                                    accountType = fetchService.fetch(accountType);
                                    newCashInMonthlyLimitPerType.put(accountType, limit);
                                }
                                
                                for (AccountType accountType : cashOutDailyLimitPerType.keySet()) {
                                    final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultCashOutPerDayLimit();
                                    accountType = fetchService.fetch(accountType);
                                    newCashOutDailyLimitPerType.put(accountType, limit);
                                }
                                
                                for (AccountType accountType : cashOutMonthlyLimitPerType.keySet()) {
                                    final BigDecimal limit = newGroup.getAccountSettings().iterator().next().getDefaultTrxPerMonthLimit();
                                    accountType = fetchService.fetch(accountType);
                                    newCashOutMonthlyLimitPerType.put(accountType, limit);
                                }
                                
                            }
                            
                          
                            limits.setUpperLimitPerType(newUpperLimitPerType);
                            limits.setCashInPerDayLimitPerType(newCashInDailyLimitPerType);
                            limits.setCashInPerMonthLimitPerType(newCashInMonthlyLimitPerType);
                            limits.setCashOutPerDayLimitPerType(newCashOutDailyLimitPerType);
                            limits.setTrxPerMonthLimitPerType(newCashOutMonthlyLimitPerType);
                            
                            accountServiceLocal.setCreditLimitExtended(member, limits);
                            break;
                        }
                    }

                    elementServiceLocal.changeGroupExtended(member, newGroup, REMARKS);
                    
                    status.setIsError(false);
                    status.setErrorCode(RESPONSE_ACTIVATION_SUCCESS);
                    status.setStatusDescription("Success");
                    
                    sendSMS(member);

                } else {
                    
                    status.setIsError(true);
                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                    status.setStatusDescription("Activation Failed");
                    
                }
                
            }
            
        } catch (EntityNotFoundException e) {
            status.setIsError(false);
            status.setErrorCode(RESPONSE_ACCOUNT_NOT_FOUND);
            status.setStatusDescription("Account not found");
        } catch (DuplicateKeyException e) {
        	status.setIsError(false);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription("Customer have already two accounts");
		}
        catch (ValidationException e) {
        	String key = "error.validation";
        	final Entry<String, Collection<ValidationError>> entry = e.getErrorsByProperty().entrySet().iterator().next();
        	final Collection<ValidationError> errors = entry.getValue();
            if (!errors.isEmpty()) {
                // We must show the validation error in a friendly way
                final ValidationError error = errors.iterator().next();
                key = error.getKey();
                
                if(key.equals("errors.maxduplicate")) {
                	status.setIsError(true);
                    status.setErrorCode(CIF_MAX_LIMIT);
                    status.setStatusDescription("CIF "+ error.getArguments().get(0) +" dengan value melebihi batas "+ error.getArguments().get(1) +" record");
                } else {
                	status.setIsError(true);
                    status.setErrorCode(RESPONSE_SYSTEM_ERROR);
                    status.setStatusDescription(e.getLocalizedMessage());
                }
            }
        	
        }
        catch (Exception e) {
            status.setIsError(true);
            status.setErrorCode(RESPONSE_SYSTEM_ERROR);
            status.setStatusDescription(e.getLocalizedMessage());
        } finally {
            response.setStatus(status);
        }
        return response;
    }
    
    public void setElementServiceLocal(final ElementServiceLocal elementService) {
        elementServiceLocal = elementService;
    }

    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
    
    public void setMemberCustomFieldServiceLocal(final MemberCustomFieldServiceLocal memberCustomFieldService) {
        memberCustomFieldServiceLocal = memberCustomFieldService;
    }
    
    public void setChannelHelper(final ChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }
    
    public void setAccountServiceLocal(final AccountServiceLocal accountService) {
        this.accountServiceLocal = accountService;
    }

    public void setFetchServiceLocal(final FetchServiceLocal fetchService) {
        this.fetchService = fetchService;
    }
    
    public void sendSMS(Member member){
        
             try {
                final SmsMailing sms = new SmsMailing();
                final String smsTemplate = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.sms_template");
                sms.setFree(true);
                sms.setMember(member);
                sms.setText(smsTemplate);
                LoggedUser.init(member.getUser());
                smsMailingServiceLocal.send(sms);
                LoggedUser.cleanup();
            } catch (Exception e) {
                logger.error(e);
            }

    }

    public void setSmsMailingServiceLocal(SmsMailingServiceLocal smsMailingServiceLocal) {
        this.smsMailingServiceLocal = smsMailingServiceLocal;
    }


}