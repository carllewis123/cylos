package ptdam.emoney.services.channel;

import java.io.IOException;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.dao.channel.ChannelSequencesDAO;


public class ChannelSequencesServiceImpl implements ChannelSequencesServiceLocal{
	private String[] terminals;
	private long ctr;
	
	private int total;
	
	private ChannelSequencesDAO channelSequencesDao;
	
	public void setChannelSequencesDao(final ChannelSequencesDAO channelSequencesDao) {
        this.channelSequencesDao = channelSequencesDao;
    }
	
	public ChannelSequencesServiceImpl(){ 
		try{
			total = Integer.parseInt(EmoneyConfiguration.getTerminalIDs().getProperty("core.telco.terminal.counter.total"));
			
			terminals = new String[total];
			for(int i=0 ; i < total ; i++){
				terminals[i] = EmoneyConfiguration.getTerminalIDs().getProperty("core.telco.terminal." + (i+1));
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getTerminal(){
		ctr = channelSequencesDao.getCurrentSequence();
    	System.out.println(ctr);

		return terminals[(int) (ctr % total)];
	}
}
