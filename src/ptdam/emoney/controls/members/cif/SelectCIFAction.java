/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.controls.members.cif;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.strohalm.cyclos.annotations.Inject;
import nl.strohalm.cyclos.controls.ActionContext;
import nl.strohalm.cyclos.controls.BaseAction;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomField;
import nl.strohalm.cyclos.entities.customization.fields.MemberCustomFieldValue;
import nl.strohalm.cyclos.entities.members.Element;
import nl.strohalm.cyclos.entities.members.Member;
import nl.strohalm.cyclos.entities.settings.LocalSettings;
import nl.strohalm.cyclos.exceptions.ExternalException;
import nl.strohalm.cyclos.services.customization.MemberCustomFieldService;
import nl.strohalm.cyclos.utils.CustomFieldHelper;
import nl.strohalm.cyclos.utils.validation.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;

import ptdam.emoney.EmoneyConfiguration;
import ptdam.emoney.webservice.client.CustInfoInquiryRequest;
import ptdam.emoney.webservice.client.CustInfoInquiryResponse;
import ptdam.emoney.webservice.client.RequestHeader;
import ptdam.emoney.webservice.client.SecurityHeader;

import com.ptdam.emoney.webservices.CoreWebServiceFactory;

/**
 * Action used to get inquiry CIF
 */
public class SelectCIFAction extends BaseAction {

	private MemberCustomFieldService    memberCustomFieldService;
	private CustomFieldHelper           customFieldHelper;
	private final Log logger = LogFactory.getLog(SelectCIFAction.class);
	private CoreWebServiceFactory coreWSFactory;
	
	@Inject
    public void setMemberCustomFieldService(final MemberCustomFieldService memberCustomFieldService) {
        this.memberCustomFieldService = memberCustomFieldService;
    }
	
	@Inject
    public void setCustomFieldHelper(final CustomFieldHelper customFieldHelper) {
        this.customFieldHelper = customFieldHelper;
    }
	
    @Inject
    public void setCoreWSFactory(CoreWebServiceFactory coreWSFactory) {
        this.coreWSFactory = coreWSFactory;
    }
	  
    @Override
    protected ActionForward executeAction(final ActionContext context) throws Exception {
    	
        final HttpServletRequest request = context.getRequest();
        final SelectCIFForm form = (SelectCIFForm)context.getForm();
        final Member member = resolveToMember(context);
        final String cifHiddenPref = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.hidden");
        final List<MemberCustomField> allFields = memberCustomFieldService.list();
        Collection<MemberCustomFieldValue> memberCustomFields = member.getCustomValues();
        Collection<MemberCustomFieldValue> customFieldValues  = new ArrayList<MemberCustomFieldValue>(allFields.size());
        Iterator it = memberCustomFields.iterator();
        String cifNumber = "";
        List<MemberCustomField> customFields;
        customFields = customFieldHelper.onlyForGroup(allFields, member.getMemberGroup());
        String cif = EmoneyConfiguration.getEmoneyProperties().getProperty("emoney.activation.cif_custom_fieldname");
        while(it.hasNext()){
        	MemberCustomFieldValue mCustomField = (MemberCustomFieldValue) it.next();
        	MemberCustomField tempMemberCustomField = getCustomField(mCustomField.getField().getInternalName(), customFields);
            if (tempMemberCustomField != null) {
              customFieldValues.add(mCustomField);
            }
        	if(mCustomField.getField().getInternalName().equals(cif)){
        		cifNumber = mCustomField.getValue();
        	}
        }

        CustInfoInquiryResponse resp = getCIFInquiry(cifNumber);
        
        String bankNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.bankNumber");
        if(resp !=null){
        	MemberCustomField bankNumberCustomField = getCustomField(cifHiddenPref+bankNumber, customFields);
        	if(bankNumberCustomField != null){
	            MemberCustomFieldValue bankNumberCustomValue = new MemberCustomFieldValue();
	            bankNumberCustomValue.setField(bankNumberCustomField);
	            bankNumberCustomValue.setValue(new Long(resp.getBdyBranchNumber()).toString());
	            customFieldValues.add(bankNumberCustomValue);
        	}
        	String branchNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.branchNumber");
	        MemberCustomField branchNumberCustomField = getCustomField(cifHiddenPref+branchNumber, customFields);
	        if(branchNumberCustomField != null){
	            MemberCustomFieldValue branchCustomValue = new MemberCustomFieldValue();
	            branchCustomValue.setField(branchNumberCustomField);
	            branchCustomValue.setValue(new Long(resp.getBdyBranchNumber()).toString());
	            customFieldValues.add(branchCustomValue);
        	}
	        String titleBeforeName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.titleBeforeName");
            MemberCustomField titleBeforeNameCustomField = getCustomField(cifHiddenPref + titleBeforeName, customFields);
            if(titleBeforeNameCustomField != null){
	            MemberCustomFieldValue titleBeforeNameCustomValue = new MemberCustomFieldValue();
	            titleBeforeNameCustomValue.setField(titleBeforeNameCustomField);
	            titleBeforeNameCustomValue.setValue(resp.getBdyTitleBeforeName());
	            customFieldValues.add(titleBeforeNameCustomValue);
            }
            String cifName1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.cifName1");
            MemberCustomField cifName1CustomField = getCustomField(cifHiddenPref + cifName1, customFields);
            if(cifName1CustomField != null){
	            MemberCustomFieldValue cifName1CustomValue = new MemberCustomFieldValue();
	            cifName1CustomValue.setField(cifName1CustomField);
	            cifName1CustomValue.setValue(resp.getBdyCIFName1());
	            customFieldValues.add(cifName1CustomValue);
            }
            String cifName2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.cifName2");
            MemberCustomField cifName2CustomField = getCustomField(cifHiddenPref + cifName2, customFields);
            if(cifName2CustomField != null){
	            MemberCustomFieldValue cifName2CustomValue = new MemberCustomFieldValue();
	            cifName2CustomValue.setField(cifName2CustomField);
	            cifName2CustomValue.setValue(resp.getBdyCIFName2());
	            customFieldValues.add(cifName2CustomValue);
            }
            String titleAfterName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.titleAfterName");
            MemberCustomField titleAfterNameCustomField = getCustomField(cifHiddenPref + titleAfterName, customFields);
            if(titleAfterNameCustomField != null){
	            MemberCustomFieldValue titleAfterNameCustomValue = new MemberCustomFieldValue();
	            titleAfterNameCustomValue.setField(titleAfterNameCustomField);
	            titleAfterNameCustomValue.setValue(resp.getBydTitleAfterName());
	            customFieldValues.add(titleAfterNameCustomValue);
            }
            String dayOfBirth = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.dayOfBirth");
            MemberCustomField dayOfBirthCustomField = getCustomField(cifHiddenPref + dayOfBirth, customFields);
            SimpleDateFormat sdf = null;
            if(dayOfBirthCustomField != null){
            	final LocalSettings settings = settingsService.getLocalSettings();
                switch(settings.getDatePattern()){
    	            case YYYY_MM_DD_SLASH : 
    	            	sdf = new SimpleDateFormat("yyyy/MM/dd");
    	            	break;
    	            case YYYY_MM_DD_PERIOD : 
    	            	sdf = new SimpleDateFormat("yyyy.MM.dd");
    	            	break;
    	            case YYYY_MM_DD_DASH : 
    	            	sdf = new SimpleDateFormat("yyyy-MM-dd");
    	            	break;
    	            case MM_DD_YYYY_SLASH : 
    	            	sdf = new SimpleDateFormat("MM/dd/yyyy");
    	            	break;
    	            case MM_DD_YYYY_PERIOD : 
    	            	sdf = new SimpleDateFormat("MM.dd.yyyy");
    	            	break;
    	            case MM_DD_YYYY_DASH : 
    	            	sdf = new SimpleDateFormat("MM-dd-yyyy");
    	            	break;
    	            case DD_MM_YYYY_SLASH : 
    	            	sdf = new SimpleDateFormat("dd/MM/yyyy");
    	            	break;
    	            case DD_MM_YYYY_PERIOD : 
    	            	sdf = new SimpleDateFormat("dd.MM.yyyy"); 
    	            	break;
    	            case DD_MM_YYYY_DASH : 
    	            	sdf = new SimpleDateFormat("dd-MM-yyyy");
    	            	break;
    	            default :
    	            	sdf = new SimpleDateFormat("dd/MM/yyyy");
    	            	break;
                }
	            MemberCustomFieldValue dayOfBirthCustomValue = new MemberCustomFieldValue();
	            dayOfBirthCustomValue.setField(dayOfBirthCustomField);
	            dayOfBirthCustomValue.setValue(sdf.format(resp.getBydDayOfBirth()));
	            customFieldValues.add(dayOfBirthCustomValue);
            }
            String birthIncorporationPlace = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.birthIncorporationPlace");
            MemberCustomField birthIncorporationPlaceCustomField = getCustomField(cifHiddenPref + birthIncorporationPlace, customFields);
            if(birthIncorporationPlaceCustomField != null){
	            MemberCustomFieldValue birthIncorporationPlaceCustomValue = new MemberCustomFieldValue();
	            birthIncorporationPlaceCustomValue.setField(birthIncorporationPlaceCustomField);
	            birthIncorporationPlaceCustomValue.setValue(resp.getBdyBirthIncorporationPlace());
	            customFieldValues.add(birthIncorporationPlaceCustomValue);
            }
            String customerTypeCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.customerTypeCode");
            MemberCustomField customerTypeCodeCustomField = getCustomField(cifHiddenPref + customerTypeCode, customFields);
            if(customerTypeCodeCustomField != null){
	            MemberCustomFieldValue customerTypeCodeCustomValue = new MemberCustomFieldValue();
	            customerTypeCodeCustomValue.setField(customerTypeCodeCustomField);
	            customerTypeCodeCustomValue.setValue(resp.getBdyCustomerTypeCode());
	            customFieldValues.add(customerTypeCodeCustomValue);
            }
            String idTypeCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.idTypeCode");
            MemberCustomField idTypeCodeCustomField = getCustomField(cifHiddenPref + idTypeCode, customFields);
            if(idTypeCodeCustomField != null){
	            MemberCustomFieldValue idTypeCodeCustomValue = new MemberCustomFieldValue();
	            idTypeCodeCustomValue.setField(idTypeCodeCustomField);
	            idTypeCodeCustomValue.setValue(resp.getBydIDTypeCode());
	            customFieldValues.add(idTypeCodeCustomValue);
            }
            String idNumber = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.idNumber");
            MemberCustomField idNumberCustomField = getCustomField(cifHiddenPref + idNumber, customFields);
            if(idNumberCustomField != null){
	            MemberCustomFieldValue idNumberCustomValue = new MemberCustomFieldValue();
	            idNumberCustomValue.setField(idNumberCustomField);
	            idNumberCustomValue.setValue(resp.getBdyIDNumber());
	            customFieldValues.add(idNumberCustomValue);
            }
            String address1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.address1");
            MemberCustomField address1CustomField = getCustomField(cifHiddenPref + address1, customFields);
            if(address1CustomField != null){
	            MemberCustomFieldValue address1CustomValue = new MemberCustomFieldValue();
	            address1CustomValue.setField(address1CustomField);
	            address1CustomValue.setValue(resp.getBdyAddress1());
	            customFieldValues.add(address1CustomValue);
            }
            String address2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.address2");
            MemberCustomField address2CustomField = getCustomField(cifHiddenPref + address2, customFields);
            if(address2CustomField != null){
	            MemberCustomFieldValue address2CustomValue = new MemberCustomFieldValue();
	            address2CustomValue.setField(address2CustomField);
	            address2CustomValue.setValue(resp.getBdyAddress2());
	            customFieldValues.add(address2CustomValue);
            }
            String address3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.address3");
            MemberCustomField address3CustomField = getCustomField(cifHiddenPref + address3, customFields);
            if(address3CustomField != null){
	            MemberCustomFieldValue address3CustomValue = new MemberCustomFieldValue();
	            address3CustomValue.setField(address3CustomField);
	            address3CustomValue.setValue(resp.getBdyAddress3());
	            customFieldValues.add(address3CustomValue);
            }
            String address4 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.address4");
            MemberCustomField address4CustomField = getCustomField(cifHiddenPref + address4, customFields);
            if(address4CustomField != null){
	            MemberCustomFieldValue address4CustomValue = new MemberCustomFieldValue();
	            address4CustomValue.setField(address4CustomField);
	            address4CustomValue.setValue(resp.getBdyAddress4());
	            customFieldValues.add(address4CustomValue);
            }
            String postalCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.postalCode");
            MemberCustomField postalCodeCustomField = getCustomField(cifHiddenPref + postalCode, customFields);
            if(postalCodeCustomField != null){
	            MemberCustomFieldValue postalCodeCustomValue = new MemberCustomFieldValue();
	            postalCodeCustomValue.setField(postalCodeCustomField);
	            postalCodeCustomValue.setValue(resp.getBdyPostalCode());
	            customFieldValues.add(postalCodeCustomValue);
            }
            String motherMaidenName = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.motherMaidenName");
            MemberCustomField motherMaidenNameCustomField = getCustomField(cifHiddenPref + motherMaidenName, customFields);
            if(motherMaidenNameCustomField != null){
	            MemberCustomFieldValue motherMaidenNameCustomValue = new MemberCustomFieldValue();
	            motherMaidenNameCustomValue.setField(motherMaidenNameCustomField);
	            motherMaidenNameCustomValue.setValue(resp.getBdyMotherMaidenName());
	            customFieldValues.add(motherMaidenNameCustomValue);
            }
            String remarkLine1 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.remarkLine1");
            MemberCustomField remarkLine1CustomField = getCustomField(cifHiddenPref + remarkLine1, customFields);
            if(remarkLine1CustomField != null){
	            MemberCustomFieldValue remarkLine1CustomValue = new MemberCustomFieldValue();
	            remarkLine1CustomValue.setField(remarkLine1CustomField);
	            remarkLine1CustomValue.setValue(resp.getBdyRemarkLine1());
	            customFieldValues.add(remarkLine1CustomValue);
            }
            String remarkLine2 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.remarkLine2");
            MemberCustomField remarkLine2CustomField = getCustomField(cifHiddenPref + remarkLine2, customFields);
            if(remarkLine2CustomField != null){
	            MemberCustomFieldValue remarkLine2CustomValue = new MemberCustomFieldValue();
	            remarkLine2CustomValue.setField(remarkLine2CustomField);
	            remarkLine2CustomValue.setValue(resp.getBdyRemarkLine2());
	            customFieldValues.add(remarkLine2CustomValue);
            }
            String remarkLine3 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.remarkLine3");
            MemberCustomField remarkLine3CustomField = getCustomField(cifHiddenPref + remarkLine3, customFields);
            if(remarkLine3CustomField != null){
	            MemberCustomFieldValue remarkLine3CustomValue = new MemberCustomFieldValue();
	            remarkLine3CustomValue.setField(remarkLine3CustomField);
	            remarkLine3CustomValue.setValue(resp.getBdyRemarkLine3());
	            customFieldValues.add(remarkLine3CustomValue);
            }
            String remarkLine4 = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.customfield.remarkLine4");
            MemberCustomField remarkLine4CustomField = getCustomField(cifHiddenPref + remarkLine4, customFields);
            if(remarkLine4CustomField != null){
	            MemberCustomFieldValue remarkLine4CustomValue = new MemberCustomFieldValue();
	            remarkLine4CustomValue.setField(remarkLine4CustomField);
	            remarkLine4CustomValue.setValue(resp.getBdyRemarkLine4());
	            customFieldValues.add(remarkLine4CustomValue);
            }
        }
        request.setAttribute("member", member);
        request.setAttribute("customFieldValues", customFieldValues);
        
        return context.getInputForward();
    }
    
    
    private Member resolveToMember(final ActionContext context) {
        final SelectCIFForm form = context.getForm();
        final long toMemberId = form.getMemberId();
        Member toMember = null;

        // Load the member to send to, if any
        if (toMemberId > 0L) {
            final Element loggedElement = (Element) (context.isOperator() ? context.getAccountOwner() : context.getElement());
            // Cannot send to self
            if (toMemberId == loggedElement.getId()) {
                throw new ValidationException();
            }
            // Ensure a member
            final Element element = elementService.load(toMemberId, Element.Relationships.USER);
            if (!(element instanceof Member)) {
                throw new ValidationException();
            }
            toMember = (Member) element;
        }

        return toMember;
    }
    
     public CustInfoInquiryResponse getCIFInquiry(String cif) throws ExternalException {
    	CustInfoInquiryRequest request = new CustInfoInquiryRequest();
    	CustInfoInquiryResponse response = new CustInfoInquiryResponse();
        
    	SimpleDateFormat sdfTimestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'0Z'");
        try {
            final String extId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.extId");
            final String tellerId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.tellerId");
            final String journalSeq = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.journalSequence");
            final String tranCode = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.transactionCode");
            final String channelId = EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.channelId");
            final String bdyCIFNumber = cif;
            
            request.setBdyCIFNumber(bdyCIFNumber);
            
            final boolean reqSecHeader = Boolean.parseBoolean(EmoneyConfiguration.getEmoneyProperties().getProperty("core.cifinquiry.reqSecurityHeader"));
            Calendar actualDate = Calendar.getInstance();
            String sActualDate = sdfTimestamp.format(actualDate.getTime());
            
            RequestHeader header = new RequestHeader();
            header.setHdrExternalId(extId);
            header.setHdrTellerId(Long.parseLong(tellerId));
            header.setHdrJournalSequence(Long.parseLong(journalSeq));
            header.setHdrTransactionCode(tranCode);
            header.setHdrChannelId(channelId);
            header.setHdrTimestamp(sActualDate);
                        
            if (reqSecHeader) {
                StringBuffer oriDigestKey = new StringBuffer();
                oriDigestKey.append(header.getHdrExternalId());
                oriDigestKey.append(header.getHdrChannelId());
                SecurityHeader securityHeader = new SecurityHeader();
                securityHeader.setHdrCorporateId("1");
                securityHeader.setHdrChallenge1("1");
                securityHeader.setHdrChallengeType("1");
                securityHeader.setHdrAppliNo("1");
                request.setSecurityHeader(securityHeader);
            }

            request.setHeader(header);
            
            response = coreWSFactory.getCIFInquiryWebService().inquiry(request);
            String responseMsg = "";
            if (response != null) {
                if (response.getResponseHeader() != null) {
                    switch (response.getResponseHeader().getHdrResponseCode()){
                        case 1: 
                        	System.out.println("sukses");
                        	return response;
                        default:
                            responseMsg = response.getResponseHeader().getHdrResponseMessage();
                            logger.error(responseMsg);
                            throw new ExternalException("System Error. Please try again.");
                    }
                } else {
                    logger.error("Response Header is empty.");
                    throw new ExternalException("System Error (Response Header is empty).");
                }
            } else {
                logger.error("Response is empty.");
                throw new ExternalException("System Error (Response is empty).");
            }
            
        } 
        catch (final ExternalException e) {
            throw e;
        }
        catch (final SocketTimeoutException e) {
            logger.error(e);
            throw new ExternalException("System Error (Connection Timeout).");
        }
        catch (final Exception e) {
            logger.error(e);
            throw new ExternalException("System Error. Please contact system support.");
        } 
    }
 
    public MemberCustomField getCustomField(final String internalName, final Collection<MemberCustomField> customFields) {
    	
    	for (MemberCustomField field : customFields) {
    		if (field.getInternalName().equals(internalName)) {
    			return field;
    		}
    	}
    	
    	return null;
    }
    
}
