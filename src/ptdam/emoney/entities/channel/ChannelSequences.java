package ptdam.emoney.entities.channel;

import nl.strohalm.cyclos.entities.Entity;

public abstract class ChannelSequences extends Entity{
	/**
	 * 
	 */
	private static final long 			serialVersionUID = -5612732107567991207L;
	private String                      channelDate;
    private long						channelSequence;
    
	public String getChannelDate() {
		return channelDate;
	}
	public void setChannelDate(String channelDate) {
		this.channelDate = channelDate;
	}
	public long getChannelSequence() {
		return channelSequence;
	}
	public void setChannelSequence(long channelSequence) {
		this.channelSequence = channelSequence;
	}
}
