/*
    This file is part of Cyclos (www.cyclos.org).
    A project of the Social Trade Organisation (www.socialtrade.org).

    Cyclos is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Cyclos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cyclos; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

 */
package ptdam.emoney.dao.channel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import nl.strohalm.cyclos.dao.BaseDAOImpl;
import nl.strohalm.cyclos.dao.JDBCCallback;
import nl.strohalm.cyclos.utils.JDBCWrapper;
import ptdam.emoney.entities.channel.ChannelSequences;

/**
 * Implementation DAO for accounts
 * @author ptdam
 */
public class ChannelSequencesDAOImpl extends BaseDAOImpl<ChannelSequences> implements ChannelSequencesDAO {
	
	public ChannelSequencesDAOImpl() {
        super(ChannelSequences.class);
    }

	static Long result = 0L;
	public long getCurrentSequence() {
		runNative(new JDBCCallback() {
            @Override
            public void execute(final JDBCWrapper jdbc) throws SQLException {
                final StringBuilder sql = new StringBuilder();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                Calendar date = Calendar.getInstance();
                String currentDate = sdf.format(date.getTime());
                Long seq = 0L;
                sql.append(" select ifnull(channel_sequence, 0) ");
                sql.append(" from channel_sequences ");
                sql.append(" where channel_date = ? ");
                final ResultSet rs = jdbc.query(sql.toString(), currentDate);
                if(rs.next()){
                	seq = rs.getLong(1);
                }
                
                if(seq==0){
                	result = seq+1;
                	sql.setLength(0);
                	sql.append(" insert into channel_sequences(channel_date, channel_sequence) ");
                    sql.append(" values (?, ?) ");
                    jdbc.execute(sql.toString(), currentDate, result);
                }else{
                	result = seq+1;
                	sql.setLength(0);
                	sql.append(" update channel_sequences c ");
                    sql.append(" set c.channel_sequence = ? ");
                    sql.append(" where c.channel_date = ? ");
                    jdbc.execute(sql.toString(), result, currentDate);
                }
            }
        });

		return result;
	}
}
