function encryptWithRSA(plainText) {
	if (plainText.length > 0) {
		var rsa = new RSAKey();
		var modulus = "D6C00AC47FB8E87F5995B27107BF4C54E1873D0A520E632DA17EC3D1BF8F45F0B1C4B0C5A114299DAE655A301630294D58C8FE8B299EE6A45AE1966D2289E9BD";
		var pubExp = "010001";
		rsa.setPublic(modulus, pubExp);
		var res = rsa.encrypt(plainText);
		return res;
	} else {
		return null;
	}
}