<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-html" prefix="html" %>
<%@ taglib uri="http://sslext.sf.net/tags/sslext" prefix="ssl" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>

<cyclos:script src="/pages/members/cif/selectCIF.js" />
<script>
 	var memberId = "${member.id}";
</script>


<table class="defaultTableContent" cellspacing="0" cellpadding="0">
    <tr>
        <td class="tdHeaderTable"><bean:message key="profile.action.viewCIF" /></td>
        <cyclos:help page="documents#member_document"/>
    </tr>
    <tr>
        <td colspan="2" align="left" class="tdContentTableLists">
        	<table class="defaultTable" cellpadding="0" cellspacing="0">
				<tr>
					<td class="headerLabel"><bean:message key="member.username"/></td>
					<td class="headerField">${member.username}</td>
				</tr>
				<tr>
					<td class="headerLabel"><bean:message key="member.name"/></td>
					<td class="headerField">${member.name}</td>
				</tr>
				<c:if test="${not empty member.email and not member.hideEmail}">
					<tr>
						<td class="headerLabel"><bean:message key="member.email"/></td>
						<td class="headerField"><a class="default" href="mailto:${member.email}">${member.email}</a></td>
					</tr>
				</c:if>
				
				<c:forEach var="entry" items="${customFieldValues}">
			        <c:set var="field" value="${entry.field}"/>
			        <c:set var="value"><cyclos:customField field="${field}" value="${entry.value}" textOnly="true" /></c:set>
			        <c:if test="${not empty value}">
			        	<c:if test="${(not fn:containsIgnoreCase(field.internalName, 'hidden_secretword')) and (not fn:containsIgnoreCase(field.internalName, 'hidden_uid')) and (not fn:containsIgnoreCase(field.internalName, 'hidden_devicepairing')) and (not fn:containsIgnoreCase(field.internalName, 'hidden_initpwd'))}">
			        		<tr>
				                <td valign="top" class="headerLabel">${field.name}</td>
				   				<td width="70%" class="headerField">${value}</td>
							</tr>
			        	</c:if>
			        </c:if>
			    </c:forEach>
			</table>
		</td>
	</tr>
</table>

<table class="defaultTableContentHidden">
	<tr>
		<td align="left"><input id="backButton" type="button" class="button" value="<bean:message key='global.back'/>"></td>
	</tr>
</table>