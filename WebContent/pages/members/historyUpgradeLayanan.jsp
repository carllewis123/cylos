<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-html" prefix="html" %>
<%@ taglib uri="http://sslext.sf.net/tags/sslext" prefix="ssl" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>
<%@ taglib uri="http://www.servletsuite.com/servlets/toggletag" prefix="t" %> 


<cyclos:script src="/pages/members/historyUpgradeLayanan.js" />

<ssl:form method="POST" action="${formAction}">
<html:hidden property="memberId" />
<table class="defaultTableContent" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tdHeaderTable"><bean:message key="historyUpgradeLayanan.title.list"/></td>
		<cyclos:help page="payments#transfer_authorizations_by_${isAdmin ? 'admin' : 'member'}"/>
	</tr>
	<tr>
		<td colspan="2" align="left" class="tdContentTableForms">
			<cyclos:layout columns="4" className="defaultTable">
				<cyclos:cell className="label" width="20%"><bean:message key="historyUpgradeLayanan.action"/></cyclos:cell>
				<cyclos:cell nowrap="nowrap">
					<html:select property="query(action)">
						<html:option value=""><bean:message key="global.search.all"/></html:option>
						<c:forEach var="action" items="${actions}">
							<html:option value="${action}"><bean:message key="historyUpgradeLayanan.action.${action}"/></html:option>
						</c:forEach>
					</html:select>
				</cyclos:cell>
				<cyclos:rowBreak/>
				
				<cyclos:cell className="label" width="20%"><bean:message key="member.username"/></cyclos:cell>
				<cyclos:cell>
					<html:hidden styleId="memberId" property="query(member)"/>
					<input id="memberUsername" class="full" value="${query.member.username}">
					<div id="membersByUsername" class="autoComplete"></div>
				</cyclos:cell>
				<cyclos:cell className="label" width="20%"><bean:message key="member.memberName"/></cyclos:cell>
				<cyclos:cell>
					<input id="memberName" class="full" value="${query.member.name}">
					<div id="membersByName" class="autoComplete"></div>
				</cyclos:cell>
				
				<cyclos:cell className="label"><bean:message key="accountHistory.period.begin"/></cyclos:cell>
				<cyclos:cell nowrap="nowrap"><html:text property="query(period).begin" styleClass="date small"/></cyclos:cell>
				<cyclos:cell className="label"><bean:message key="accountHistory.period.end"/></cyclos:cell>
				<cyclos:cell nowrap="nowrap"><html:text property="query(period).end" styleClass="date small"/></cyclos:cell>
				
				
				<tr>
   					<td align="right" colspan="4"><input type="submit" class="button" value="<bean:message key="global.search"/>"></td>
   				</tr>
			</cyclos:layout>
		</td>
	</tr>
</table>
</ssl:form>

<table class="defaultTableContent" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tdHeaderTable"><bean:message key="global.searchResults"/></td>
		<cyclos:help page="payments#transfers_authorizations_result"/>
	</tr>
	<tr>
		<td colspan="2" align="left">
			<table class="defaultTable">
				<tr>
					<th class="tdHeaderContents" width="15%"><bean:message key="historyUpgradeLayanan.date"/></th>
					<th class="tdHeaderContents" width="20%"><bean:message key="historyUpgradeLayanan.username"/></th>
					<th class="tdHeaderContents" width="20%"><bean:message key="historyUpgradeLayanan.name"/></th>
					<th class="tdHeaderContents" width="20%"><bean:message key="historyUpgradeLayanan.action"/></th>
					<th class="tdHeaderContents" width="5%">&nbsp;</th>
				</tr>

				<c:forEach var="authorization" items="${authorizations}">
					<c:set var="subjectMember" value="${authorization.subject}"/>
					<tr class="<t:toggle>ClassColor1|ClassColor2</t:toggle>">
						<td align="center"><cyclos:format date="${authorization.date}"/></td>
						<td align="center">${subjectMember.username}</td>
						<td align="center">${subjectMember.name}</td>
						<c:if test="${authorization.oldGroup.name eq 'UNREGISTERED_PENDING_AGENT'}">
							<c:choose>
								<c:when test="${authorization.newGroup.name eq 'REGISTERED_AGENT'}">
									<td align="center">APPROVED</td>
								</c:when>
								<c:otherwise>
									<td align="center">REJECTED</td>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${authorization.oldGroup.name eq 'UNREGISTERED_PENDING_AGENT_REKHAPE'}">
							<c:choose>
								<c:when test="${authorization.newGroup.name eq 'REGISTERED_AGENT_REKHAPE'}">
									<td align="center">APPROVED</td>
								</c:when>
								<c:otherwise>
									<td align="center">REJECTED</td>
								</c:otherwise>
							</c:choose>
						</c:if>
						<td align="center">
							<img remarkId="${authorization.id}" class="view" src="<c:url value="/pages/images/view.gif"/>"> 
						</td>
					</tr>
				</c:forEach>
				
			</table>
		</td>
	</tr>
</table>

<table class="defaultTableContentHidden" cellpadding="0" cellspacing="0">
	<tr>
		<c:if test="${not empty by}">
			<td><input type="button" class="button" id="backButton" value="<bean:message key="global.back" />"/></td>
		</c:if>
		<td align="right"><cyclos:pagination items="${authorizations}"/></td>
	</tr>
</table>