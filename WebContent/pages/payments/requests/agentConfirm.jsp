<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-html" prefix="html" %>
<%@ taglib uri="http://sslext.sf.net/tags/sslext" prefix="ssl" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>


<ssl:form method="post" action="${formAction}">

<table class="defaultTableContent" cellspacing="0" cellpadding="0">
    <tr>
        <td class="tdHeaderTable"><bean:message key="otp.confirmation.title"  /></td>
    </tr>
    <tr>
        <td colspan="2" align="left" class="tdContentTableForms">
            <table class="defaultTable">
				<tr>
					<td class="label" width="25%"><bean:message key='member.username'/></td>
					<td>
						<input type="text" id="otpMemberUsername" class="large" name="ticket(otpfrom)">
					</td>
				</tr>
			<%-- 	<tr>
					<td class="label"><bean:message key='member.memberName'/></td>
					<td>
						<input id="otpMemberName" class="large">
					</td>
				</tr> --%>
				<tr>
					<td class="label"><bean:message key="sms.otp"/></td>
					<td><input type="text" id="otp" name="ticket(otp)"  class="large" autocomplete="off"></td>
					<td  align="right"><input type="submit" id="submitButton" class="button" value="<bean:message key='global.submit'/>"></td>

				</tr>
            </table>
        </td>
    </tr>
</table>
</ssl:form>
