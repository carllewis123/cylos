<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-html" prefix="html" %>
<%@ taglib uri="http://sslext.sf.net/tags/sslext" prefix="ssl" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>

<cyclos:script src="/pages/members/approvalUpgradeLayanan.js" />
<script>
	var ConfirmationMessageReject = "<cyclos:escapeJS><bean:message key="upgradelayanan.member.confirmReject"/></cyclos:escapeJS>";
</script>

<ssl:form action="${formAction}" method="post">
<html:hidden property="memberId" />
<table class="defaultTableContent" cellspacing="0" cellpadding="0">
	<tr>
        <td class="tdHeaderTable"><bean:message key="upgradelayanan.member.reject.title" arg0="${member.name}"/></td>
        <cyclos:help page="groups#change_group"/>
    </tr>
    <tr>
        <td colspan="2" align="left" class="tdContentTableForms">
            <table class="defaultTable">
				<tr>
                	<td class="label" width="20%"><bean:message key="changeGroup.current"/></td>
                	<td><input class="large InputBoxDisabled" readonly="readonly" value="${member.group.name}"></td>
                </tr>
				<tr>
				    <td valign="top" class="label"><bean:message key="remark.comments"/></td>
				  	<td>
				  		<%-- <html:textarea styleId="comments" styleClass="full" rows="5" property="comments"/> --%>
				  		<textarea rows="5" cols="5" class="full" id="comments" name="comments" maxlength="85"></textarea>
				  	</td>
				</tr>
				<tr>
				    <td colspan="2" align="right">
					  	<input type="submit" value="<bean:message key="global.submit"/>" class="button">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table class="defaultTableContentHidden">
	<tr>
		<td width="10%"><input type="button" id="backButton" id="backButton" class="button" value="<bean:message key="global.back"/>"></td>
		<td align="right">
		</td>
	</tr>
</table>
</ssl:form>