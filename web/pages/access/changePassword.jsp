<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://sslext.sf.net/tags/sslext" prefix="ssl" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>
<cyclos:script src="/pages/access/changePassword.js" />

<cyclos:script src="/pages/scripts/rsacryptolib/rng_c.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/ec.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/base64.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/sec.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/sha1.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/jsbn.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/jsbn2.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/prng4.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rng.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsa.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsa2.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsaencrypt.js" />


<script>
	
	function doSubmit() {
		if(document.getElementById('oldPasswordTemp') != null){
			var oldPassword = document.getElementById('oldPasswordTemp').value;
			var resOldPassword = encryptWithRSA(oldPassword);
		
			if (resOldPassword != null) {
				document.getElementById('oldPassword').value = hex2b64(resOldPassword);
			}
		}
		
		if(document.getElementById('newPasswordTemp') != null && document.getElementById('newPasswordConfirmationTemp') != null){
			var newPassword = document.getElementById('newPasswordTemp').value;
			var newPasswordConfirmation = document.getElementById('newPasswordConfirmationTemp').value;
			
			if(newPassword == newPasswordConfirmation){
				var resNewPassword = encryptWithRSA(newPassword);
				
				if (resNewPassword != null) {
					document.getElementById('newPassword').value = hex2b64(resNewPassword);
					document.getElementById('newPasswordConfirmation').value = hex2b64(resNewPassword);
				}	
			}else{
				var resNewPassword = encryptWithRSA(newPassword);
				var resNewPasswordConfirmation = encryptWithRSA(newPasswordConfirmation);
				document.getElementById('newPassword').value = hex2b64(resNewPassword);
				document.getElementById('newPasswordConfirmation').value = hex2b64(resNewPasswordConfirmation);
			}
		}
	}

</script>

<c:set var="numericPassword" value="${ofAdmin ? false : accessSettings.numericPassword}"/>

<ssl:form styleId="changePasswordForm" method="post" action="${formAction}">
<html:hidden property="userId"/>
<html:hidden property="embed"/>

<table class="defaultTableContent" cellspacing="0" cellpadding="0">
    <tr>
        <td class="tdHeaderTable">
        	<c:choose><c:when test="${myPassword}">
        		<bean:message key="changePassword.title.my"/>
        	</c:when><c:otherwise>
        		<bean:message key="changePassword.title.of" arg0="${user.element.name}"/>
        	</c:otherwise></c:choose>
        </td>
        <cyclos:help page="passwords#change"/>
    </tr>
    <tr>
        <td colspan="2" align="left" class="tdContentTableForms">
            <table class="defaultTable">
				<tr>
					<td colspan="2" align="center">
						<br/>
						<c:choose><c:when test="${passwordLength.min == passwordLength.max}">
							<bean:message key="changePassword.passwordLength" arg0="${passwordLength.min}"/>
						</c:when><c:otherwise>
							<bean:message key="changePassword.passwordLengthRange" arg0="${passwordLength.min}" arg1="${passwordLength.max}"/>
						</c:otherwise></c:choose>
						<br/><br/>
					</td>
				</tr>
				<c:if test="${shouldRequestOldPassword}">
	                <tr>
	                     <td class="label"><bean:message key="changePassword.oldPassword"/></td>
	                     <td><input type="password" id="oldPasswordTemp" name="oldPasswordTemp" class="medium ${numericPassword ? 'number' : ''}"></td>
	                     <td><input type="hidden" id="oldPassword" name="oldPassword" value=""></td>
	                </tr>
                </c:if>
                <tr>
                     <td class="label"><bean:message key="changePassword.newPassword"/></td>
                     <td><input type="password" id="newPasswordTemp" name="newPasswordTemp" class="medium ${numericPassword ? 'number' : ''}"></td>
                     <td><input type="hidden" id="newPassword" name="newPassword" value=""></td>
                </tr>
                <tr>
                     <td class="label"><bean:message key="changePassword.newPasswordConfirmation"/></td>
                     <td><input type="password" id="newPasswordConfirmationTemp" name="newPasswordConfirmationTemp" class="medium ${numericPassword ? 'number' : ''}"></td>
                     <td><input type="hidden" id="newPasswordConfirmation" name="newPasswordConfirmation" value=""></td>
                </tr>
                <c:if test="${!myPassword}">
	                <tr>
	                     <td class="label"><bean:message key="changePassword.forceChange"/></td>
	                     <td><input type="checkbox" class="checkbox" name="forceChange" value="true"></td>
	                </tr>
                </c:if>
                <tr>
                     <td colspan="2" align="right"><input type="submit" class="button" value="<bean:message key="global.submit"/>" onclick="doSubmit();"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</ssl:form>
<c:if test="${expired}">
	
	<div class="footerNote">
		<cyclos:escapeHTML>
			<bean:message key="changePassword.expired" />
		</cyclos:escapeHTML>
	</div>
</c:if>
