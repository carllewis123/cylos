<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="http://devel.cyclos.org/tlibs/cyclos-core" prefix="cyclos" %>

<form id="cyclosLogin">
<input type="hidden" name="operatorLogin" value="${empty param.operator ? false : param.operator}" />
<input type="hidden" name="principalType" value="${selectedPrincipalType}" />

<cyclos:script src="/pages/scripts/rsacryptolib/rng_c.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/ec.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/base64.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/sec.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/sha1.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/jsbn.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/jsbn2.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/prng4.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rng.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsa.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsa2.js" />
<cyclos:script src="/pages/scripts/rsacryptolib/rsaencrypt.js" />


<script>
	
	function doSubmit() {
		// get capctha response
		var resp = grecaptcha.getResponse(widgetId1);
		document.getElementById('cyclosCaptcha').value = resp;
		
		// existing ogin function
		var plainText = document.getElementById('cyclosPasswordTemp').value;
		var res = encryptWithRSA(plainText);
		
		if (res != null) {
			document.getElementById('cyclosPassword').value = hex2b64(res);
		}
	}

</script>

<table class="nested">
	<c:if test="${param.operator}">
		<tr>
			<td nowrap="nowrap" class="label" width="40%"><bean:message key="member.username"/></td>
			<td><input id="cyclosMember" name="member" class="medium"/></td>
		</tr>
	</c:if>
	<tr>
		<td nowrap="nowrap" class="label" width="40%">${selectedPrincipalLabel}</td>
		<td><c:choose><c:when test="${empty selectedPrincipalType.customField}">
				<input id="cyclosUsername" name="principal" class="small"/>
			</c:when><c:otherwise>
				<cyclos:customField field="${selectedPrincipalType.customField}" valueName="principal" styleId="cyclosUsername" search="true" size="medium"/>
			</c:otherwise></c:choose></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="label"><bean:message key="login.password"/></td>
		<td><input type="password" id="cyclosPasswordTemp" class="small"/></td>
		<input type="hidden" id="cyclosPassword" name="cyclosPassword" value="" />
		<input type="hidden" id="cyclosCaptcha" name="cyclosCaptcha" value="" />
	</tr>
	<c:choose><c:when test="${accessSettings.virtualKeyboard}">
		<tr>
			<td colspan="2" align="center">
				<script>renderVirtualKeyboard();</script>
			</td>
		</tr>
	</c:when><c:otherwise>
	
	<!--  Captcha div -->
		<tr>
			<td colspan="2" align="left">
				<script type="text/javascript">
				      var verifyCallback = function(response) {
				        alert(response);
				      };
				      var widgetId1;
				      var onloadCallback = function() {
				        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
				        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
				        widgetId1 = grecaptcha.render('example1', {
				          'sitekey' : '6Le34zYUAAAAAM5An1jrWUbHFsryu92uibFhr6DO',
				          //'sitekey' : '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
				        });
				      };
				      
			    </script>
				<!--  <div class="g-recaptcha" data-sitekey="6LdJpTAUAAAAAHpTTgdxYmMKruKCgDe3PgVkhAWm"></div> -->
				<form name="formcap" id="formcap"> <!--  action="javascript:save();"> -->
			      <div id="example1"></div>
			    </form>
			    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
			        async defer>
			    </script>
			</td>
		</tr>
		<!-- end of Capthcha div -->
	
		<tr>
			<td colspan="2" align="right">
				<br class="small">
				<input type="submit" class="button" value="<bean:message key="global.submit"/>" onclick="doSubmit();">
			</td>
		</tr>
	</c:otherwise></c:choose>
	<c:if test="${not empty accessLinks}">
		<tr>
			<td colspan="2" align="${accessSettings.virtualKeyboard ? 'center': 'right'}">
				<br class="small">
				<c:choose><c:when test="${not empty singleAccessLink}">
					<c:url value="/do/login" var="url">
						<c:param name="login" value="true" />
						<c:if test="${not empty singleAccessLink.paramName}">
							<c:param name="${singleAccessLink.paramName}" value="${singleAccessLink.paramValue}" />
						</c:if>
						<c:if test="${not empty param.groupId}">
							<c:param name="groupId" value="${param.groupId}" />
						</c:if>
						<c:if test="${not empty param.groupFilterId}">
							<c:param name="groupFilterId" value="${param.groupFilterId}" />
						</c:if>
					</c:url>
					<a class="default" href="${url}">${singleAccessLink.label}</a>
				</c:when><c:otherwise>
					<a class="default" id="accessOptionsLink"><bean:message key="login.accessOptions"/></a>
					<div id="accessOptions" style="display:none">
						<br class="small">
						<c:forEach var="link" items="${accessLinks}">
							<c:url value="/do/login" var="url">
								<c:param name="login" value="true" />
								<c:if test="${not empty link.paramName}">
									<c:param name="${link.paramName}" value="${link.paramValue}" />
								</c:if>
								<c:if test="${not empty param.groupId}">
									<c:param name="groupId" value="${param.groupId}" />
								</c:if>
								<c:if test="${not empty param.groupFilterId}">
									<c:param name="groupFilterId" value="${param.groupFilterId}" />
								</c:if>
							</c:url>
							<a class="default" href="${url}">${link.label}</a><br>
						</c:forEach>
					</div>
				</c:otherwise></c:choose>
			</td>
		</tr>
	</c:if>
</table>
</form>
